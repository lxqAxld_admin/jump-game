(function () {
    'use strict';
    window["oppoSdk"] = {};
    class GameConfig {
        constructor() {
        }
        static init() {
            var reg = Laya.ClassUtils.regClass;
        }
    }
    GameConfig.width = 640;
    GameConfig.height = 1136;
    GameConfig.scaleMode = "fixedwidth";
    GameConfig.screenMode = "none";
    GameConfig.alignV = "top";
    GameConfig.alignH = "left";
    GameConfig.startScene = "syyx_ad/ui_inner_interstitial.scene";
    GameConfig.sceneRoot = "";
    GameConfig.debug = false;
    GameConfig.stat = false;
    GameConfig.physicsDebug = false;
    GameConfig.exportSceneToJson = true;
    GameConfig.init();

    var View = Laya.View;
    var REG = Laya.ClassUtils.regClass;
    var ui;
    (function (ui) {
        var syyx_ad;
        (function (syyx_ad) {
            class ui_bannerUI extends View {
                constructor() {
                    super();
                }
                createChildren() {
                    super.createChildren();
                    this.createView(ui_bannerUI.uiView);
                }
            }
            ui_bannerUI.uiView = { "type": "View", "props": { "y": 0, "x": 0, "width": 1080, "scaleY": 1, "scaleX": 1, "mouseEnabled": true, "height": 200 }, "compId": 2, "child": [{ "type": "Image", "props": { "width": 1080, "var": "native_bg", "top": 0, "right": 0, "left": 0, "height": 200, "bottom": 0 }, "compId": 29, "child": [{ "type": "Image", "props": { "top": 0, "skin": "res/ads/bg_banner_01.png", "scaleX": 2, "right": 0, "left": 0, "bottom": 0 }, "compId": 61 }, { "type": "Box", "props": { "x": 0, "width": 847, "var": "box_big_banner", "top": 0, "height": 200, "bottom": 0 }, "compId": 56, "child": [{ "type": "Image", "props": { "x": 250, "width": 640, "var": "icon", "skin": "res/new_products/box_ht_dk6.png", "scaleY": 0.5, "scaleX": 0.5, "pivotY": 160, "pivotX": 320, "name": "icon", "height": 320, "centerY": 0 }, "compId": 52 }, { "type": "Box", "props": { "x": 450, "width": 389, "var": "text_box", "name": "text_box", "height": 200, "centerY": 0 }, "compId": 76, "child": [{ "type": "Text", "props": { "y": 111, "x": 0, "wordWrap": true, "width": 360, "var": "desc", "valign": "middle", "text": "描述11111111111111111111111111111", "overflow": "hidden", "name": "desc", "height": 40, "fontSize": 20, "color": "#000000", "align": "left", "runtime": "laya.display.Text" }, "compId": 55 }, { "type": "Text", "props": { "y": 62, "x": 0, "wordWrap": false, "width": 226, "var": "title", "valign": "middle", "text": "标题站提供站在在", "overflow": "hidden", "name": "title", "height": 40, "fontSize": 28, "color": "#000000", "align": "left", "runtime": "laya.display.Text" }, "compId": 54 }] }] }, { "type": "Image", "props": { "x": 839, "var": "btn_click_check", "skin": "res/ads/bt_banner_01.png", "scaleY": 0.55, "scaleX": 0.55, "centerY": 0 }, "compId": 36, "child": [{ "type": "Text", "props": { "y": 42, "x": 88, "text": "点击查看", "strokeColor": "#ffffff", "stroke": 2, "fontSize": 60, "color": "#ffffff", "runtime": "laya.display.Text" }, "compId": 72 }] }, { "type": "Image", "props": { "skin": "res/ads/bg_banner_02.png", "scaleY": -1, "right": 0, "bottom": 41 }, "compId": 70, "child": [{ "type": "Text", "props": { "y": 30, "x": 15.5, "text": "广告", "scaleY": -1, "fontSize": 28, "color": "#ffffff", "runtime": "laya.display.Text" }, "compId": 71 }] }] }, { "type": "Image", "props": { "width": 100, "var": "icon_close", "top": 0, "mouseEnabled": true, "left": 0, "height": 100 }, "compId": 35, "child": [{ "type": "Image", "props": { "skin": "res/ads/bt_banner_02.png", "scaleY": 1.5, "scaleX": 1.5, "mouseEnabled": true, "centerY": -18, "centerX": -13 }, "compId": 59 }] }], "loadList": ["res/ads/bg_banner_01.png", "res/new_products/box_ht_dk6.png", "res/ads/bt_banner_01.png", "res/ads/bg_banner_02.png", "res/ads/bt_banner_02.png"], "loadList3D": [] };
            syyx_ad.ui_bannerUI = ui_bannerUI;
            REG("ui.syyx_ad.ui_bannerUI", ui_bannerUI);
            class ui_banner_click_maskUI extends View {
                constructor() {
                    super();
                }
                createChildren() {
                    super.createChildren();
                    this.createView(ui_banner_click_maskUI.uiView);
                }
            }
            ui_banner_click_maskUI.uiView = { "type": "View", "props": { "y": 0, "x": 0, "width": 1080, "scaleY": 1, "scaleX": 1, "mouseEnabled": true, "height": 200 }, "compId": 2, "child": [{ "type": "Image", "props": { "var": "native_bg", "top": 0, "skin": "res/ads/bg_chaping_01.png", "sizeGrid": "29,19,17,16", "right": 0, "left": 0, "bottom": 0, "alpha": 0 }, "compId": 29 }, { "type": "Image", "props": { "width": 1080, "var": "banner_box", "scaleY": 1, "scaleX": 1, "mouseThrough": true, "mouseEnabled": true, "height": 200, "centerX": 0, "bottom": 0, "alpha": 0 }, "compId": 77, "child": [{ "type": "Image", "props": { "width": 100, "var": "btn_close", "top": 0, "skin": "res/ads/bt_banner_01.png", "mouseThrough": false, "mouseEnabled": true, "left": 0, "height": 100, "alpha": 0.5 }, "compId": 87 }] }], "loadList": ["res/ads/bg_chaping_01.png", "res/ads/bt_banner_01.png"], "loadList3D": [] };
            syyx_ad.ui_banner_click_maskUI = ui_banner_click_maskUI;
            REG("ui.syyx_ad.ui_banner_click_maskUI", ui_banner_click_maskUI);
            class ui_inner_interstitialUI extends View {
                constructor() {
                    super();
                }
                createChildren() {
                    super.createChildren();
                    this.createView(ui_inner_interstitialUI.uiView);
                }
            }
            ui_inner_interstitialUI.uiView = { "type": "View", "props": { "y": 0, "x": 0, "width": 720, "height": 350 }, "compId": 2, "child": [{ "type": "Image", "props": { "y": 16.5, "width": 886, "var": "icon_video", "scaleY": 0.75, "scaleX": 0.75, "height": 443, "centerX": 0 }, "compId": 218, "child": [{ "type": "Text", "props": { "y": 348, "x": 17, "wordWrap": false, "width": 851, "var": "text_desc", "text": "text_desc", "overflow": "hidden", "height": 45, "fontSize": 45, "color": "#ffffff", "bold": true, "align": "center", "runtime": "laya.display.Text" }, "compId": 227 }, { "type": "Sprite", "props": { "y": -9, "x": -9, "texture": "res/ads/img_title_bg2.png" }, "compId": 219 }, { "type": "Sprite", "props": { "y": 400, "x": 766, "texture": "res/ads/img_ad_text2.png" }, "compId": 220 }, { "type": "Text", "props": { "y": -6, "x": -4, "wordWrap": false, "width": 304, "var": "txt_title", "valign": "middle", "text": "视频APP", "overflow": "hidden", "height": 78, "fontSize": 50, "color": "#ffffff", "align": "center", "runtime": "laya.display.Text" }, "compId": 226 }] }, { "type": "Box", "props": { "y": -23, "x": 637, "width": 83, "var": "box_close", "height": 83 }, "compId": 221, "child": [{ "type": "Image", "props": { "skin": "res/ads/bt_chaping_02.png", "centerY": 0, "centerX": 0 }, "compId": 238 }] }], "loadList": ["res/ads/img_title_bg2.png", "res/ads/img_ad_text2.png", "res/ads/bt_chaping_02.png"], "loadList3D": [] };
            syyx_ad.ui_inner_interstitialUI = ui_inner_interstitialUI;
            REG("ui.syyx_ad.ui_inner_interstitialUI", ui_inner_interstitialUI);
            class ui_interstitialUI extends View {
                constructor() {
                    super();
                }
                createChildren() {
                    super.createChildren();
                    this.createView(ui_interstitialUI.uiView);
                }
            }
            ui_interstitialUI.uiView = { "type": "View", "props": { "width": 1080, "top": 0, "right": 0, "left": 0, "height": 1920, "bottom": 0 }, "compId": 2, "child": [{ "type": "Image", "props": { "var": "block_bg", "top": 0, "skin": "res/ads/zz.png", "right": 0, "mouseThrough": false, "mouseEnabled": true, "left": 0, "bottom": 0 }, "compId": 54 }, { "type": "Image", "props": { "width": 930, "height": 1100, "centerY": 150, "centerX": 0 }, "compId": 59, "child": [{ "type": "Image", "props": { "width": 930, "var": "click_box", "skin": "res/ads/bg_chaping_01.png", "sizeGrid": "12,12,15,10", "mouseThrough": false, "mouseEnabled": true, "height": 1100, "centerY": 0, "centerX": 0 }, "compId": 70 }, { "type": "Image", "props": { "width": 886, "var": "icon_video", "mouseThrough": false, "mouseEnabled": true, "height": 443, "centerY": -306, "centerX": 0 }, "compId": 38 }, { "type": "Sprite", "props": { "y": 1100, "x": 849, "texture": "res/ads/bg_banner_02.png", "scaleX": -1, "rotation": 180, "name": "" }, "compId": 45 }, { "type": "Text", "props": { "y": 1071, "x": 866, "text": "广告", "fontSize": 28, "color": "#ffffff", "runtime": "laya.display.Text" }, "compId": 60 }, { "type": "Text", "props": { "y": 581, "x": 165, "wordWrap": false, "width": 600, "var": "txt_title", "valign": "middle", "text": "标题", "overflow": "hidden", "name": "", "height": 78, "fontSize": 40, "color": "#000000", "align": "center", "runtime": "laya.display.Text" }, "compId": 35 }, { "type": "Text", "props": { "y": 686, "x": 65, "wordWrap": true, "width": 800, "var": "text_desc", "valign": "top", "text": "描述", "height": 150, "fontSize": 32, "color": "#000000", "align": "center", "runtime": "laya.display.Text" }, "compId": 37 }, { "type": "Image", "props": { "y": 943, "width": 326, "var": "btn_click_button", "skin": "res/ads/bt_chaping_01.png", "scaleY": 1, "scaleX": 1, "name": "", "height": 122, "centerX": 0, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 10, "child": [{ "type": "Text", "props": { "y": 37, "x": 67, "text": "点击查看", "fontSize": 48, "color": "#ffffff", "bold": true, "runtime": "laya.display.Text" }, "compId": 62 }] }] }, { "type": "Box", "props": { "width": 150, "var": "box_close", "height": 150, "centerY": -385, "centerX": 447 }, "compId": 52, "child": [{ "type": "Image", "props": { "skin": "res/ads/bt_chaping_02.png", "centerY": 0, "centerX": 0 }, "compId": 56 }] }], "loadList": ["res/ads/zz.png", "res/ads/bg_chaping_01.png", "res/ads/bg_banner_02.png", "res/ads/bt_chaping_01.png", "res/ads/bt_chaping_02.png"], "loadList3D": [] };
            syyx_ad.ui_interstitialUI = ui_interstitialUI;
            REG("ui.syyx_ad.ui_interstitialUI", ui_interstitialUI);
            class ui_interstitial_hUI extends View {
                constructor() {
                    super();
                }
                createChildren() {
                    super.createChildren();
                    this.createView(ui_interstitial_hUI.uiView);
                }
            }
            ui_interstitial_hUI.uiView = { "type": "View", "props": { "width": 1920, "top": 0, "right": 0, "left": 0, "height": 1080, "bottom": 0 }, "compId": 2, "child": [{ "type": "Image", "props": { "var": "block_bg", "top": 0, "skin": "res/ads/zz.png", "right": 0, "mouseThrough": false, "mouseEnabled": true, "left": 0, "bottom": 0 }, "compId": 54 }, { "type": "Image", "props": { "width": 932, "height": 828, "centerY": 0, "centerX": 0 }, "compId": 59, "child": [{ "type": "Image", "props": { "y": 0, "x": 0, "width": 932, "var": "click_box", "skin": "res/ads/bg_chaping_01.png", "sizeGrid": "11,13,15,9", "height": 828, "centerY": 0, "centerX": 0 }, "compId": 72 }, { "type": "Image", "props": { "width": 886, "var": "icon_video", "mouseThrough": false, "mouseEnabled": true, "height": 443, "centerY": -172, "centerX": -1 }, "compId": 38 }, { "type": "Sprite", "props": { "y": 828, "x": 851, "texture": "res/ads/bg_banner_02.png", "scaleX": -1, "rotation": 180, "name": "" }, "compId": 45 }, { "type": "Text", "props": { "y": 796, "x": 869, "text": "广告", "fontSize": 28, "color": "#ffffff", "runtime": "laya.display.Text" }, "compId": 60 }, { "type": "Text", "props": { "y": 489, "x": 165, "wordWrap": false, "width": 600, "var": "txt_title", "valign": "middle", "text": "标题", "overflow": "hidden", "name": "", "height": 78, "fontSize": 40, "color": "#000000", "align": "center", "runtime": "laya.display.Text" }, "compId": 35 }, { "type": "Text", "props": { "y": 588, "x": 65, "wordWrap": false, "width": 800, "var": "text_desc", "valign": "top", "text": "描述", "height": 50, "fontSize": 32, "color": "#000000", "align": "center", "runtime": "laya.display.Text" }, "compId": 37 }, { "type": "Image", "props": { "y": 727, "width": 326, "var": "btn_click_button", "skin": "res/ads/bt_chaping_01.png", "scaleY": 1, "scaleX": 1, "name": "", "mouseThrough": false, "mouseEnabled": true, "height": 122, "centerX": 0, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 10, "child": [{ "type": "Text", "props": { "y": 37, "x": 67, "text": "点击查看", "fontSize": 48, "color": "#ffffff", "bold": true, "runtime": "laya.display.Text" }, "compId": 62 }] }] }, { "type": "Box", "props": { "width": 150, "var": "box_close", "height": 150, "centerY": -482, "centerX": 450 }, "compId": 52, "child": [{ "type": "Image", "props": { "skin": "res/ads/bt_chaping_02.png", "centerY": 80, "centerX": 0 }, "compId": 56 }] }], "loadList": ["res/ads/zz.png", "res/ads/bg_chaping_01.png", "res/ads/bg_banner_02.png", "res/ads/bt_chaping_01.png", "res/ads/bt_chaping_02.png"], "loadList3D": [] };
            syyx_ad.ui_interstitial_hUI = ui_interstitial_hUI;
            REG("ui.syyx_ad.ui_interstitial_hUI", ui_interstitial_hUI);
            class ui_native_iconUI extends View {
                constructor() {
                    super();
                }
                createChildren() {
                    super.createChildren();
                    this.createView(ui_native_iconUI.uiView);
                }
            }
            ui_native_iconUI.uiView = { "type": "View", "props": { "width": 250, "height": 250 }, "compId": 2, "child": [{ "type": "Box", "props": { "y": 125, "x": 125, "width": 250, "var": "game_box", "height": 250, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 16, "child": [{ "type": "Image", "props": { "width": 250, "var": "game_icon", "height": 250, "centerY": 0, "centerX": 0 }, "compId": 14 }, { "type": "Image", "props": { "y": 208, "x": 129, "skin": "res/ads/img_ad_text2.png", "scaleY": 1, "scaleX": 1 }, "compId": 12 }, { "type": "Image", "props": { "y": 0, "x": 0, "width": 78, "var": "btn_close", "skin": "res/ads/img_close2.png", "scaleY": 0.8, "scaleX": 0.8, "height": 78, "centerY": -94, "centerX": -94 }, "compId": 17 }] }], "animations": [{ "nodes": [{ "target": 16, "keyframes": { "rotation": [{ "value": -5, "tweenMethod": "linearNone", "tween": true, "target": 16, "key": "rotation", "index": 0 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 16, "key": "rotation", "index": 2 }, { "value": 10, "tweenMethod": "linearNone", "tween": true, "target": 16, "key": "rotation", "index": 6 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 16, "key": "rotation", "index": 10 }, { "value": -5, "tweenMethod": "linearNone", "tween": true, "target": 16, "key": "rotation", "index": 14 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 16, "key": "rotation", "index": 16 }] } }], "name": "shake_ani", "id": 1, "frameRate": 24, "action": 0 }], "loadList": ["res/ads/img_ad_text2.png", "res/ads/img_close2.png"], "loadList3D": [] };
            syyx_ad.ui_native_iconUI = ui_native_iconUI;
            REG("ui.syyx_ad.ui_native_iconUI", ui_native_iconUI);
            class ui_toastUI extends View {
                constructor() {
                    super();
                }
                createChildren() {
                    super.createChildren();
                    this.createView(ui_toastUI.uiView);
                }
            }
            ui_toastUI.uiView = { "type": "View", "props": { "y": 0, "x": 0, "width": 550, "height": 100, "centerY": 0, "centerX": 0 }, "compId": 2, "child": [{ "type": "Image", "props": { "x": 0, "width": 550, "var": "tips_box", "height": 100, "centerY": 0, "centerX": 0 }, "compId": 56, "child": [{ "type": "Image", "props": { "x": 0, "width": 500, "skin": "res/ads/zz.png", "sizeGrid": "30,39,40,30", "height": 100, "centerY": 0, "centerX": 0, "alpha": 0.9 }, "compId": 57 }, { "type": "Label", "props": { "y": 33, "x": 14, "var": "lb_tips", "text": "tips", "fontSize": 35, "color": "#ffffff", "centerY": 0, "centerX": 0 }, "compId": 58 }] }], "loadList": ["res/ads/zz.png"], "loadList3D": [] };
            syyx_ad.ui_toastUI = ui_toastUI;
            REG("ui.syyx_ad.ui_toastUI", ui_toastUI);
        })(syyx_ad = ui.syyx_ad || (ui.syyx_ad = {}));
    })(ui || (ui = {}));
    (function (ui) {
        var syyx_ctr;
        (function (syyx_ctr) {
            class ui_ctrUI extends View {
                constructor() {
                    super();
                }
                createChildren() {
                    super.createChildren();
                    this.createView(ui_ctrUI.uiView);
                }
            }
            ui_ctrUI.uiView = { "type": "View", "props": { "width": 1080, "top": 0, "right": 0, "mouseThrough": false, "left": 0, "height": 1920, "bottom": 0 }, "compId": 2, "child": [{ "type": "Image", "props": { "var": "zz", "top": 0, "skin": "res/ads/zz.png", "right": 0, "mouseEnabled": false, "left": 0, "bottom": 0 }, "compId": 38 }, { "type": "Box", "props": { "y": 1012, "x": 540, "right": 0, "mouseThrough": true, "left": 0, "height": 1200, "centerY": 52, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 39, "child": [{ "type": "Image", "props": { "width": 1000, "var": "back_title", "height": 235, "centerY": -525, "centerX": 0 }, "compId": 40, "child": [{ "type": "Image", "props": { "y": 0, "skin": "res/new_products/om_ht_02.png", "scaleY": 2, "scaleX": 2, "centerX": 0 }, "compId": 109 }, { "type": "Image", "props": { "var": "tip_title1", "skin": "res/new_products/tet_ht_cx.png", "centerX": 0, "bottom": 30 }, "compId": 44 }, { "type": "Image", "props": { "y": 0, "x": 0, "var": "tip_title2", "skin": "res/new_products/tet_ht_cy.png", "centerX": 0, "bottom": 30 }, "compId": 50 }, { "type": "Image", "props": { "var": "btn_close", "top": 138, "skin": "res/new_products/bt_ht_gb.png", "right": 30 }, "compId": 51 }, { "type": "Image", "props": { "skin": "res/new_products/om_ht_03.png", "scaleX": -1, "centerX": 130, "bottom": 144 }, "compId": 101, "child": [{ "type": "Image", "props": { "y": 13, "x": 211, "skin": "res/new_products/tet_ht_jx.png", "scaleX": -1 }, "compId": 110 }] }] }, { "type": "Image", "props": { "width": 1000, "var": "back_body", "skin": "res/new_products/bg_ht_01.png", "sizeGrid": "32,48,40,23", "mouseThrough": false, "height": 1000, "centerY": 92, "centerX": 0, "alpha": 1 }, "compId": 41, "child": [{ "type": "Image", "props": { "var": "body_img1", "top": 79, "skin": "res/new_products/tet_ht_ng.png", "centerX": 0 }, "compId": 43 }, { "type": "List", "props": { "width": 889, "var": "game_list", "spaceY": 0, "spaceX": 20, "repeatY": 2, "repeatX": 3, "height": 750, "centerY": 72, "centerX": 0 }, "compId": 45, "child": [{ "type": "ui_ctr_item", "props": { "renderType": "render", "runtime": "ui.syyx_ctr.ui_ctr_itemUI" }, "compId": 108 }] }, { "type": "Image", "props": { "width": 94, "var": "diamondAniRoot", "scaleY": 1, "scaleX": 1, "pivotY": -28, "pivotX": 47, "height": 83, "centerY": 0, "centerX": 0 }, "compId": 52, "child": [{ "type": "Image", "props": { "y": -826, "x": 12, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 53 }, { "type": "Image", "props": { "y": -826, "x": -26, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 54 }, { "type": "Image", "props": { "y": -826, "x": -73, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 55 }, { "type": "Image", "props": { "y": -826, "x": 0, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 56 }, { "type": "Image", "props": { "y": -826, "x": 20, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 42, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 57 }, { "type": "Image", "props": { "y": -826, "x": 9, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 58 }, { "type": "Image", "props": { "y": -826, "x": -51, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 59 }, { "type": "Image", "props": { "y": -826, "x": 12, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 60 }, { "type": "Image", "props": { "y": -826, "x": -88, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 61 }, { "type": "Image", "props": { "y": -826, "x": -38, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 62 }, { "type": "Image", "props": { "y": -826, "x": -73, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 63 }, { "type": "Image", "props": { "y": -826, "x": -35, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 64 }, { "type": "Image", "props": { "y": -826, "x": -38, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 65 }] }, { "type": "Image", "props": { "y": 596, "x": 471, "width": 675, "var": "img_background", "skin": "res/new_products/om_common_gq2.png", "sizeGrid": "15,27,14,19", "height": 268, "centerX": 0, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 103, "child": [{ "type": "Image", "props": { "y": -446, "x": 204, "var": "body_img3", "top": 63, "skin": "res/new_products/tet_ht_ypqd.png", "centerX": 0, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 100 }, { "type": "Image", "props": { "var": "body_img2", "top": 145, "skin": "res/new_products/tet_ht_yp.png", "centerX": 0, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 49 }] }] }] }], "animations": [{ "nodes": [{ "target": 53, "keyframes": { "y": [{ "value": 74.5, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "y", "index": 0 }, { "value": 167.5, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "y", "index": 6 }, { "value": -784.5, "tweenMethod": "linearNone", "tween": true, "target": 53, "label": null, "key": "y", "index": 24 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 53, "label": null, "key": "y", "index": 25 }], "x": [{ "value": 62, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "x", "index": 0 }, { "value": 88, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "x", "index": 6 }, { "value": 12, "tweenMethod": "linearNone", "tween": true, "target": 53, "label": null, "key": "x", "index": 24 }, { "value": 12, "tweenMethod": "linearNone", "tween": true, "target": 53, "label": null, "key": "x", "index": 25 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 53, "label": null, "key": "scaleY", "index": 25 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 53, "label": null, "key": "scaleX", "index": 25 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "alpha", "index": 24 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "alpha", "index": 25 }] } }, { "target": 54, "keyframes": { "y": [{ "value": 59.5, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "y", "index": 0 }, { "value": 101, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "y", "index": 6 }, { "value": -784.5, "tweenMethod": "linearNone", "tween": true, "target": 54, "label": null, "key": "y", "index": 23 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 54, "label": null, "key": "y", "index": 24 }], "x": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "x", "index": 0 }, { "value": -47, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "x", "index": 6 }, { "value": -26, "tweenMethod": "linearNone", "tween": true, "target": 54, "label": null, "key": "x", "index": 23 }, { "value": -26, "tweenMethod": "linearNone", "tween": true, "target": 54, "label": null, "key": "x", "index": 24 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 54, "label": null, "key": "scaleY", "index": 24 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 54, "label": null, "key": "scaleX", "index": 24 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "alpha", "index": 23 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "alpha", "index": 24 }] } }, { "target": 55, "keyframes": { "y": [{ "value": 74.5, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "y", "index": 0 }, { "value": -83, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "y", "index": 6 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 55, "label": null, "key": "y", "index": 19 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 55, "label": null, "key": "y", "index": 20 }], "x": [{ "value": 35, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "x", "index": 0 }, { "value": 276, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "x", "index": 6 }, { "value": -73, "tweenMethod": "linearNone", "tween": true, "target": 55, "label": null, "key": "x", "index": 19 }, { "value": -73, "tweenMethod": "linearNone", "tween": true, "target": 55, "label": null, "key": "x", "index": 20 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 55, "label": null, "key": "scaleY", "index": 20 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 55, "label": null, "key": "scaleX", "index": 20 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "alpha", "index": 19 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "alpha", "index": 20 }] } }, { "target": 56, "keyframes": { "y": [{ "value": 74.5, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "y", "index": 0 }, { "value": -23.5, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "y", "index": 6 }, { "value": -808.4375, "tweenMethod": "linearNone", "tween": true, "target": 56, "label": null, "key": "y", "index": 21 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 56, "label": null, "key": "y", "index": 22 }], "x": [{ "value": 35, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "x", "index": 0 }, { "value": -47, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "x", "index": 6 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 56, "label": null, "key": "x", "index": 21 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 56, "label": null, "key": "x", "index": 22 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 56, "label": null, "key": "scaleY", "index": 22 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 56, "label": null, "key": "scaleX", "index": 22 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "alpha", "index": 21 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "alpha", "index": 22 }] } }, { "target": 57, "keyframes": { "y": [{ "value": 83, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "y", "index": 0 }, { "value": 29, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "y", "index": 6 }, { "value": -793.2777777777778, "tweenMethod": "linearNone", "tween": true, "target": 57, "label": null, "key": "y", "index": 22 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 57, "label": null, "key": "y", "index": 23 }], "x": [{ "value": -8, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "x", "index": 0 }, { "value": 109, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "x", "index": 6 }, { "value": 20, "tweenMethod": "linearNone", "tween": true, "target": 57, "label": null, "key": "x", "index": 22 }, { "value": 20, "tweenMethod": "linearNone", "tween": true, "target": 57, "label": null, "key": "x", "index": 23 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 57, "label": null, "key": "scaleY", "index": 23 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 57, "label": null, "key": "scaleX", "index": 23 }], "pivotY": [{ "value": 42, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "pivotY", "index": 0 }], "pivotX": [{ "value": 47, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "pivotX", "index": 0 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "alpha", "index": 22 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "alpha", "index": 23 }] } }, { "target": 58, "keyframes": { "y": [{ "value": 41.5, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "y", "index": 0 }, { "value": 198, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "y", "index": 6 }, { "value": -793, "tweenMethod": "linearNone", "tween": true, "target": 58, "label": null, "key": "y", "index": 29 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 58, "label": null, "key": "y", "index": 30 }], "x": [{ "value": 47, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "x", "index": 0 }, { "value": -32, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "x", "index": 6 }, { "value": 9, "tweenMethod": "linearNone", "tween": true, "target": 58, "label": null, "key": "x", "index": 29 }, { "value": 9, "tweenMethod": "linearNone", "tween": true, "target": 58, "label": null, "key": "x", "index": 30 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 58, "label": null, "key": "scaleY", "index": 30 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 58, "label": null, "key": "scaleX", "index": 30 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "alpha", "index": 29 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "alpha", "index": 30 }] } }, { "target": 59, "keyframes": { "y": [{ "value": 74.5, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "y", "index": 0 }, { "value": 101, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "y", "index": 6 }, { "value": -842, "tweenMethod": "linearNone", "tween": true, "target": 59, "label": null, "key": "y", "index": 26 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 59, "label": null, "key": "y", "index": 27 }], "x": [{ "value": 47, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "x", "index": 0 }, { "value": 221, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "x", "index": 6 }, { "value": -51, "tweenMethod": "linearNone", "tween": true, "target": 59, "label": null, "key": "x", "index": 26 }, { "value": -51, "tweenMethod": "linearNone", "tween": true, "target": 59, "label": null, "key": "x", "index": 27 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 59, "label": null, "key": "scaleY", "index": 27 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 59, "label": null, "key": "scaleX", "index": 27 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "alpha", "index": 26 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "alpha", "index": 27 }] } }, { "target": 60, "keyframes": { "y": [{ "value": 59.5, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "y", "index": 0 }, { "value": 306, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "y", "index": 6 }, { "value": -784.5, "tweenMethod": "linearNone", "tween": true, "target": 60, "label": null, "key": "y", "index": 35 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 60, "label": null, "key": "y", "index": 36 }], "x": [{ "value": -8, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "x", "index": 0 }, { "value": 35, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "x", "index": 6 }, { "value": 12, "tweenMethod": "linearNone", "tween": true, "target": 60, "label": null, "key": "x", "index": 35 }, { "value": 12, "tweenMethod": "linearNone", "tween": true, "target": 60, "label": null, "key": "x", "index": 36 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 60, "label": null, "key": "scaleY", "index": 36 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 60, "label": null, "key": "scaleX", "index": 36 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "alpha", "index": 35 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "alpha", "index": 36 }] } }, { "target": 61, "keyframes": { "y": [{ "value": 74.5, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "y", "index": 0 }, { "value": 59.5, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "y", "index": 6 }, { "value": -810.3461538461538, "tweenMethod": "linearNone", "tween": true, "target": 61, "label": null, "key": "y", "index": 18 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 61, "label": null, "key": "y", "index": 19 }], "x": [{ "value": 15, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "x", "index": 0 }, { "value": -169, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "x", "index": 6 }, { "value": -88, "tweenMethod": "linearNone", "tween": true, "target": 61, "label": null, "key": "x", "index": 18 }, { "value": -88, "tweenMethod": "linearNone", "tween": true, "target": 61, "label": null, "key": "x", "index": 19 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 61, "label": null, "key": "scaleY", "index": 19 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 61, "label": null, "key": "scaleX", "index": 19 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "alpha", "index": 18 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "alpha", "index": 19 }] } }, { "target": 62, "keyframes": { "y": [{ "value": 59.5, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "y", "index": 0 }, { "value": -65, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "y", "index": 6 }, { "value": -814, "tweenMethod": "linearNone", "tween": true, "target": 62, "label": null, "key": "y", "index": 17 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 62, "label": null, "key": "y", "index": 18 }], "x": [{ "value": 15, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "x", "index": 0 }, { "value": 62, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "x", "index": 6 }, { "value": -38, "tweenMethod": "linearNone", "tween": true, "target": 62, "label": null, "key": "x", "index": 17 }, { "value": -38, "tweenMethod": "linearNone", "tween": true, "target": 62, "label": null, "key": "x", "index": 18 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 62, "label": null, "key": "scaleY", "index": 18 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 62, "label": null, "key": "scaleX", "index": 18 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "alpha", "index": 17 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "alpha", "index": 18 }] } }, { "target": 63, "keyframes": { "y": [{ "value": 74.5, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "y", "index": 0 }, { "value": 209, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "y", "index": 6 }, { "value": -784.5, "tweenMethod": "linearNone", "tween": true, "target": 63, "label": null, "key": "y", "index": 30 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 63, "label": null, "key": "y", "index": 31 }], "x": [{ "value": 15, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "x", "index": 0 }, { "value": -177, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "x", "index": 6 }, { "value": -73, "tweenMethod": "linearNone", "tween": true, "target": 63, "label": null, "key": "x", "index": 30 }, { "value": -73, "tweenMethod": "linearNone", "tween": true, "target": 63, "label": null, "key": "x", "index": 31 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 63, "label": null, "key": "scaleY", "index": 31 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 63, "label": null, "key": "scaleX", "index": 31 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "alpha", "index": 30 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "alpha", "index": 31 }] } }, { "target": 64, "keyframes": { "y": [{ "value": 59.5, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "y", "index": 0 }, { "value": -106.5, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "y", "index": 6 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "y", "index": 15 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "y", "index": 16 }], "x": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "x", "index": 0 }, { "value": -94, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "x", "index": 6 }, { "value": -35, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "x", "index": 15 }, { "value": -35, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "x", "index": 16 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "scaleY", "index": 16 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "scaleX", "index": 16 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "alpha", "index": 15 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "alpha", "index": 16 }] } }, { "target": 65, "keyframes": { "y": [{ "value": 74.5, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "y", "index": 0 }, { "value": 264.5, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "y", "index": 6 }, { "value": -798, "tweenMethod": "linearNone", "tween": true, "target": 65, "label": null, "key": "y", "index": 33 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 65, "label": null, "key": "y", "index": 34 }], "x": [{ "value": 15, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "x", "index": 0 }, { "value": 259, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "x", "index": 6 }, { "value": -38, "tweenMethod": "linearNone", "tween": true, "target": 65, "label": null, "key": "x", "index": 33 }, { "value": -38, "tweenMethod": "linearNone", "tween": true, "target": 65, "label": null, "key": "x", "index": 34 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 65, "label": null, "key": "scaleY", "index": 34 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 65, "label": null, "key": "scaleX", "index": 34 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "alpha", "index": 33 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "alpha", "index": 34 }] } }], "name": "feizuanshi", "id": 5, "frameRate": 24, "action": 0 }, { "nodes": [{ "target": 39, "keyframes": { "scaleY": [{ "value": 0.5, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "scaleY", "index": 0 }, { "value": 1.1, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "scaleY", "index": 4 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "scaleY", "index": 9 }], "scaleX": [{ "value": 0.5, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "scaleX", "index": 0 }, { "value": 1.1, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "scaleX", "index": 4 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "scaleX", "index": 9 }], "alpha": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "alpha", "index": 4 }] } }], "name": "chuchang", "id": 6, "frameRate": 24, "action": 0 }, { "nodes": [{ "target": 49, "keyframes": { "scaleY": [{ "value": 0.5, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "scaleY", "index": 0 }, { "value": 1.4, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "scaleY", "index": 20 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "scaleY", "index": 25 }], "scaleX": [{ "value": 0.5, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "scaleX", "index": 0 }, { "value": 1.4, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "scaleX", "index": 20 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "scaleX", "index": 25 }], "alpha": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "alpha", "index": 20 }] } }, { "target": 100, "keyframes": { "alpha": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 100, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 100, "key": "alpha", "index": 22 }] } }, { "target": 103, "keyframes": { "x": [{ "value": 471, "tweenMethod": "linearNone", "tween": true, "target": 103, "key": "x", "index": 0 }, { "value": 471, "tweenMethod": "linearNone", "tween": true, "target": 103, "label": null, "key": "x", "index": 7 }, { "value": 471, "tweenMethod": "linearNone", "tween": true, "target": 103, "label": null, "key": "x", "index": 14 }], "visible": [{ "value": true, "tweenMethod": "linearNone", "tween": false, "target": 103, "key": "visible", "index": 0 }, { "value": true, "tweenMethod": "linearNone", "tween": false, "target": 103, "label": null, "key": "visible", "index": 7 }, { "value": true, "tweenMethod": "linearNone", "tween": false, "target": 103, "label": null, "key": "visible", "index": 14 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 103, "key": "scaleY", "index": 0 }, { "value": 1.1, "tweenMethod": "linearNone", "tween": true, "target": 103, "label": null, "key": "scaleY", "index": 7 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 103, "label": null, "key": "scaleY", "index": 14 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 103, "key": "scaleX", "index": 0 }, { "value": 1.1, "tweenMethod": "linearNone", "tween": true, "target": 103, "label": null, "key": "scaleX", "index": 7 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 103, "label": null, "key": "scaleX", "index": 14 }], "alpha": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 103, "key": "alpha", "index": 0 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 103, "label": null, "key": "alpha", "index": 7 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 103, "label": null, "key": "alpha", "index": 14 }] } }], "name": "zi", "id": 7, "frameRate": 24, "action": 0 }], "loadList": ["res/ads/zz.png", "res/new_products/om_ht_02.png", "res/new_products/tet_ht_cx.png", "res/new_products/tet_ht_cy.png", "res/new_products/bt_ht_gb.png", "res/new_products/om_ht_03.png", "res/new_products/tet_ht_jx.png", "res/new_products/bg_ht_01.png", "res/new_products/tet_ht_ng.png", "res/new_products/icon_item.png", "res/new_products/om_common_gq2.png", "res/new_products/tet_ht_ypqd.png", "res/new_products/tet_ht_yp.png"], "loadList3D": [] };
            syyx_ctr.ui_ctrUI = ui_ctrUI;
            REG("ui.syyx_ctr.ui_ctrUI", ui_ctrUI);
            class ui_ctr_hUI extends View {
                constructor() {
                    super();
                }
                createChildren() {
                    super.createChildren();
                    this.createView(ui_ctr_hUI.uiView);
                }
            }
            ui_ctr_hUI.uiView = { "type": "View", "props": { "width": 1920, "top": 0, "right": 0, "mouseThrough": false, "mouseEnabled": true, "left": 0, "height": 1080, "bottom": 0 }, "compId": 2, "child": [{ "type": "Image", "props": { "var": "zz", "top": 0, "skin": "res/ads/zz.png", "right": 0, "mouseEnabled": false, "left": 0, "bottom": 0 }, "compId": 38 }, { "type": "Box", "props": { "top": 0, "scaleY": 1, "scaleX": 1, "right": 0, "mouseThrough": true, "left": 0, "bottom": 0, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 39, "child": [{ "type": "Image", "props": { "width": 1117, "var": "back_body", "mouseThrough": false, "height": 787, "centerY": -75, "centerX": 0, "alpha": 1 }, "compId": 41, "child": [{ "type": "Image", "props": { "width": 1117, "var": "click_box", "skin": "res/new_products/bg_ht_01.png", "sizeGrid": "26,40,67,36", "mouseThrough": false, "mouseEnabled": true, "height": 787, "centerY": 0, "centerX": 0 }, "compId": 114 }, { "type": "Image", "props": { "x": 74, "var": "body_img1", "skin": "res/new_products/tet_ht_nlg.png", "centerY": 0 }, "compId": 43 }, { "type": "List", "props": { "width": 889, "var": "game_list", "spaceY": 0, "spaceX": 24, "repeatY": 2, "repeatX": 3, "height": 740, "centerY": 0, "centerX": 63 }, "compId": 45, "child": [{ "type": "ui_ctr_item", "props": { "renderType": "render", "runtime": "ui.syyx_ctr.ui_ctr_itemUI" }, "compId": 108 }] }, { "type": "Image", "props": { "width": 94, "var": "diamondAniRoot", "scaleY": 1, "scaleX": 1, "pivotY": -28, "pivotX": 47, "height": 83, "centerY": -45, "centerX": 0 }, "compId": 52, "child": [{ "type": "Image", "props": { "y": -826, "x": 12, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 53 }, { "type": "Image", "props": { "y": -826, "x": -26, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 54 }, { "type": "Image", "props": { "y": -826, "x": -73, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 55 }, { "type": "Image", "props": { "y": -826, "x": 0, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 56 }, { "type": "Image", "props": { "y": -826, "x": 20, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 42, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 57 }, { "type": "Image", "props": { "y": -826, "x": 9, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 58 }, { "type": "Image", "props": { "y": -826, "x": -51, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 59 }, { "type": "Image", "props": { "y": -826, "x": 12, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 60 }, { "type": "Image", "props": { "y": -826, "x": -88, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 61 }, { "type": "Image", "props": { "y": -826, "x": -38, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 62 }, { "type": "Image", "props": { "y": -826, "x": -73, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 63 }, { "type": "Image", "props": { "y": -826, "x": -35, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 64 }, { "type": "Image", "props": { "y": -826, "x": -38, "width": 94, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "pivotY": 41.5, "pivotX": 47, "name": "rewardAni", "height": 83, "alpha": 0 }, "compId": 65 }] }, { "type": "Image", "props": { "width": 675, "visible": true, "var": "img_background", "skin": "res/new_products/om_common_gq2.png", "sizeGrid": "15,27,14,19", "scaleY": 1, "scaleX": 1, "height": 268, "centerY": 0, "centerX": 60, "anchorY": 0.5, "anchorX": 0.5, "alpha": 1 }, "compId": 103, "child": [{ "type": "Image", "props": { "y": -446, "x": 204, "var": "body_img3", "top": 63, "skin": "res/new_products/tet_ht_ypqd.png", "centerX": 0, "anchorY": 0.5, "anchorX": 0.5, "alpha": 1 }, "compId": 100 }, { "type": "Image", "props": { "var": "body_img2", "top": 145, "skin": "res/new_products/tet_ht_yp.png", "scaleY": 1, "scaleX": 1, "centerX": 0, "anchorY": 0.5, "anchorX": 0.5, "alpha": 1 }, "compId": 49 }] }] }, { "type": "Image", "props": { "width": 200, "var": "back_title", "height": 900, "centerY": -108, "centerX": -644 }, "compId": 40, "child": [{ "type": "Image", "props": { "skin": "res/new_products/om_ht_0.png", "scaleY": 2, "scaleX": 2, "centerY": 1, "centerX": 0 }, "compId": 109 }, { "type": "Image", "props": { "var": "tip_title1", "skin": "res/new_products/tet_ht_xpcx.png", "name": "", "centerX": 24, "bottom": 230 }, "compId": 44 }, { "type": "Image", "props": { "var": "tip_title2", "skin": "res/new_products/tet_ht_xxcy.png", "name": "", "centerX": 24, "bottom": 231 }, "compId": 50 }, { "type": "Image", "props": { "var": "btn_close", "top": 792, "skin": "res/new_products/bt_ht_gb.png", "right": 52 }, "compId": 51 }, { "type": "Image", "props": { "skin": "res/new_products/om_ht_03.png", "centerX": 159, "bottom": 749 }, "compId": 101, "child": [{ "type": "Image", "props": { "y": 14, "x": 26, "skin": "res/new_products/tet_ht_jx.png" }, "compId": 110 }] }] }] }], "animations": [{ "nodes": [{ "target": 53, "keyframes": { "y": [{ "value": 74.5, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "y", "index": 0 }, { "value": 167.5, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "y", "index": 6 }, { "value": -784.5, "tweenMethod": "linearNone", "tween": true, "target": 53, "label": null, "key": "y", "index": 24 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 53, "label": null, "key": "y", "index": 25 }], "x": [{ "value": 62, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "x", "index": 0 }, { "value": 88, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "x", "index": 6 }, { "value": 12, "tweenMethod": "linearNone", "tween": true, "target": 53, "label": null, "key": "x", "index": 24 }, { "value": 12, "tweenMethod": "linearNone", "tween": true, "target": 53, "label": null, "key": "x", "index": 25 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 53, "label": null, "key": "scaleY", "index": 25 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 53, "label": null, "key": "scaleX", "index": 25 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "alpha", "index": 24 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 53, "key": "alpha", "index": 25 }] } }, { "target": 54, "keyframes": { "y": [{ "value": 59.5, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "y", "index": 0 }, { "value": 101, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "y", "index": 6 }, { "value": -784.5, "tweenMethod": "linearNone", "tween": true, "target": 54, "label": null, "key": "y", "index": 23 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 54, "label": null, "key": "y", "index": 24 }], "x": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "x", "index": 0 }, { "value": -47, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "x", "index": 6 }, { "value": -26, "tweenMethod": "linearNone", "tween": true, "target": 54, "label": null, "key": "x", "index": 23 }, { "value": -26, "tweenMethod": "linearNone", "tween": true, "target": 54, "label": null, "key": "x", "index": 24 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 54, "label": null, "key": "scaleY", "index": 24 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 54, "label": null, "key": "scaleX", "index": 24 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "alpha", "index": 23 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 54, "key": "alpha", "index": 24 }] } }, { "target": 55, "keyframes": { "y": [{ "value": 74.5, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "y", "index": 0 }, { "value": -83, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "y", "index": 6 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 55, "label": null, "key": "y", "index": 19 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 55, "label": null, "key": "y", "index": 20 }], "x": [{ "value": 35, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "x", "index": 0 }, { "value": 276, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "x", "index": 6 }, { "value": -73, "tweenMethod": "linearNone", "tween": true, "target": 55, "label": null, "key": "x", "index": 19 }, { "value": -73, "tweenMethod": "linearNone", "tween": true, "target": 55, "label": null, "key": "x", "index": 20 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 55, "label": null, "key": "scaleY", "index": 20 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 55, "label": null, "key": "scaleX", "index": 20 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "alpha", "index": 19 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 55, "key": "alpha", "index": 20 }] } }, { "target": 56, "keyframes": { "y": [{ "value": 74.5, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "y", "index": 0 }, { "value": -23.5, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "y", "index": 6 }, { "value": -808.4375, "tweenMethod": "linearNone", "tween": true, "target": 56, "label": null, "key": "y", "index": 21 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 56, "label": null, "key": "y", "index": 22 }], "x": [{ "value": 35, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "x", "index": 0 }, { "value": -47, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "x", "index": 6 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 56, "label": null, "key": "x", "index": 21 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 56, "label": null, "key": "x", "index": 22 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 56, "label": null, "key": "scaleY", "index": 22 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 56, "label": null, "key": "scaleX", "index": 22 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "alpha", "index": 21 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 56, "key": "alpha", "index": 22 }] } }, { "target": 57, "keyframes": { "y": [{ "value": 83, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "y", "index": 0 }, { "value": 29, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "y", "index": 6 }, { "value": -793.2777777777778, "tweenMethod": "linearNone", "tween": true, "target": 57, "label": null, "key": "y", "index": 22 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 57, "label": null, "key": "y", "index": 23 }], "x": [{ "value": -8, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "x", "index": 0 }, { "value": 109, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "x", "index": 6 }, { "value": 20, "tweenMethod": "linearNone", "tween": true, "target": 57, "label": null, "key": "x", "index": 22 }, { "value": 20, "tweenMethod": "linearNone", "tween": true, "target": 57, "label": null, "key": "x", "index": 23 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 57, "label": null, "key": "scaleY", "index": 23 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 57, "label": null, "key": "scaleX", "index": 23 }], "pivotY": [{ "value": 42, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "pivotY", "index": 0 }], "pivotX": [{ "value": 47, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "pivotX", "index": 0 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "alpha", "index": 22 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 57, "key": "alpha", "index": 23 }] } }, { "target": 58, "keyframes": { "y": [{ "value": 41.5, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "y", "index": 0 }, { "value": 198, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "y", "index": 6 }, { "value": -793, "tweenMethod": "linearNone", "tween": true, "target": 58, "label": null, "key": "y", "index": 29 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 58, "label": null, "key": "y", "index": 30 }], "x": [{ "value": 47, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "x", "index": 0 }, { "value": -32, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "x", "index": 6 }, { "value": 9, "tweenMethod": "linearNone", "tween": true, "target": 58, "label": null, "key": "x", "index": 29 }, { "value": 9, "tweenMethod": "linearNone", "tween": true, "target": 58, "label": null, "key": "x", "index": 30 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 58, "label": null, "key": "scaleY", "index": 30 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 58, "label": null, "key": "scaleX", "index": 30 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "alpha", "index": 29 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 58, "key": "alpha", "index": 30 }] } }, { "target": 59, "keyframes": { "y": [{ "value": 74.5, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "y", "index": 0 }, { "value": 101, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "y", "index": 6 }, { "value": -842, "tweenMethod": "linearNone", "tween": true, "target": 59, "label": null, "key": "y", "index": 26 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 59, "label": null, "key": "y", "index": 27 }], "x": [{ "value": 47, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "x", "index": 0 }, { "value": 221, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "x", "index": 6 }, { "value": -51, "tweenMethod": "linearNone", "tween": true, "target": 59, "label": null, "key": "x", "index": 26 }, { "value": -51, "tweenMethod": "linearNone", "tween": true, "target": 59, "label": null, "key": "x", "index": 27 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 59, "label": null, "key": "scaleY", "index": 27 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 59, "label": null, "key": "scaleX", "index": 27 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "alpha", "index": 26 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 59, "key": "alpha", "index": 27 }] } }, { "target": 60, "keyframes": { "y": [{ "value": 59.5, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "y", "index": 0 }, { "value": 306, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "y", "index": 6 }, { "value": -784.5, "tweenMethod": "linearNone", "tween": true, "target": 60, "label": null, "key": "y", "index": 35 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 60, "label": null, "key": "y", "index": 36 }], "x": [{ "value": -8, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "x", "index": 0 }, { "value": 35, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "x", "index": 6 }, { "value": 12, "tweenMethod": "linearNone", "tween": true, "target": 60, "label": null, "key": "x", "index": 35 }, { "value": 12, "tweenMethod": "linearNone", "tween": true, "target": 60, "label": null, "key": "x", "index": 36 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 60, "label": null, "key": "scaleY", "index": 36 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 60, "label": null, "key": "scaleX", "index": 36 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "alpha", "index": 35 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 60, "key": "alpha", "index": 36 }] } }, { "target": 61, "keyframes": { "y": [{ "value": 74.5, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "y", "index": 0 }, { "value": 59.5, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "y", "index": 6 }, { "value": -810.3461538461538, "tweenMethod": "linearNone", "tween": true, "target": 61, "label": null, "key": "y", "index": 18 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 61, "label": null, "key": "y", "index": 19 }], "x": [{ "value": 15, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "x", "index": 0 }, { "value": -169, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "x", "index": 6 }, { "value": -88, "tweenMethod": "linearNone", "tween": true, "target": 61, "label": null, "key": "x", "index": 18 }, { "value": -88, "tweenMethod": "linearNone", "tween": true, "target": 61, "label": null, "key": "x", "index": 19 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 61, "label": null, "key": "scaleY", "index": 19 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 61, "label": null, "key": "scaleX", "index": 19 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "alpha", "index": 18 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 61, "key": "alpha", "index": 19 }] } }, { "target": 62, "keyframes": { "y": [{ "value": 59.5, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "y", "index": 0 }, { "value": -65, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "y", "index": 6 }, { "value": -814, "tweenMethod": "linearNone", "tween": true, "target": 62, "label": null, "key": "y", "index": 17 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 62, "label": null, "key": "y", "index": 18 }], "x": [{ "value": 15, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "x", "index": 0 }, { "value": 62, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "x", "index": 6 }, { "value": -38, "tweenMethod": "linearNone", "tween": true, "target": 62, "label": null, "key": "x", "index": 17 }, { "value": -38, "tweenMethod": "linearNone", "tween": true, "target": 62, "label": null, "key": "x", "index": 18 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 62, "label": null, "key": "scaleY", "index": 18 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 62, "label": null, "key": "scaleX", "index": 18 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "alpha", "index": 17 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 62, "key": "alpha", "index": 18 }] } }, { "target": 63, "keyframes": { "y": [{ "value": 74.5, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "y", "index": 0 }, { "value": 209, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "y", "index": 6 }, { "value": -784.5, "tweenMethod": "linearNone", "tween": true, "target": 63, "label": null, "key": "y", "index": 30 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 63, "label": null, "key": "y", "index": 31 }], "x": [{ "value": 15, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "x", "index": 0 }, { "value": -177, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "x", "index": 6 }, { "value": -73, "tweenMethod": "linearNone", "tween": true, "target": 63, "label": null, "key": "x", "index": 30 }, { "value": -73, "tweenMethod": "linearNone", "tween": true, "target": 63, "label": null, "key": "x", "index": 31 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 63, "label": null, "key": "scaleY", "index": 31 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 63, "label": null, "key": "scaleX", "index": 31 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "alpha", "index": 30 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 63, "key": "alpha", "index": 31 }] } }, { "target": 64, "keyframes": { "y": [{ "value": 59.5, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "y", "index": 0 }, { "value": -106.5, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "y", "index": 6 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "y", "index": 15 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "y", "index": 16 }], "x": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "x", "index": 0 }, { "value": -94, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "x", "index": 6 }, { "value": -35, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "x", "index": 15 }, { "value": -35, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "x", "index": 16 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "scaleY", "index": 16 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "scaleX", "index": 16 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "alpha", "index": 15 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 64, "key": "alpha", "index": 16 }] } }, { "target": 65, "keyframes": { "y": [{ "value": 74.5, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "y", "index": 0 }, { "value": 264.5, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "y", "index": 6 }, { "value": -798, "tweenMethod": "linearNone", "tween": true, "target": 65, "label": null, "key": "y", "index": 33 }, { "value": -826, "tweenMethod": "linearNone", "tween": true, "target": 65, "label": null, "key": "y", "index": 34 }], "x": [{ "value": 15, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "x", "index": 0 }, { "value": 259, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "x", "index": 6 }, { "value": -38, "tweenMethod": "linearNone", "tween": true, "target": 65, "label": null, "key": "x", "index": 33 }, { "value": -38, "tweenMethod": "linearNone", "tween": true, "target": 65, "label": null, "key": "x", "index": 34 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "scaleY", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 65, "label": null, "key": "scaleY", "index": 34 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "scaleX", "index": 0 }, { "value": 0.7, "tweenMethod": "linearNone", "tween": true, "target": 65, "label": null, "key": "scaleX", "index": 34 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "alpha", "index": 33 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 65, "key": "alpha", "index": 34 }] } }], "name": "feizuanshi", "id": 5, "frameRate": 24, "action": 0 }, { "nodes": [{ "target": 39, "keyframes": { "scaleY": [{ "value": 0.5, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "scaleY", "index": 0 }, { "value": 1.1, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "scaleY", "index": 4 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "scaleY", "index": 9 }], "scaleX": [{ "value": 0.5, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "scaleX", "index": 0 }, { "value": 1.1, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "scaleX", "index": 4 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "scaleX", "index": 9 }], "alpha": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 39, "key": "alpha", "index": 4 }] } }], "name": "chuchang", "id": 6, "frameRate": 24, "action": 0 }, { "nodes": [{ "target": 49, "keyframes": { "scaleY": [{ "value": 0.5, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "scaleY", "index": 0 }, { "value": 1.4, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "scaleY", "index": 20 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "scaleY", "index": 25 }], "scaleX": [{ "value": 0.5, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "scaleX", "index": 0 }, { "value": 1.4, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "scaleX", "index": 20 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "scaleX", "index": 25 }], "alpha": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 49, "key": "alpha", "index": 20 }] } }, { "target": 100, "keyframes": { "alpha": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 100, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 100, "key": "alpha", "index": 22 }] } }, { "target": 103, "keyframes": { "visible": [{ "value": true, "tweenMethod": "linearNone", "tween": false, "target": 103, "key": "visible", "index": 0 }, { "value": true, "tweenMethod": "linearNone", "tween": false, "target": 103, "label": null, "key": "visible", "index": 7 }, { "value": true, "tweenMethod": "linearNone", "tween": false, "target": 103, "label": null, "key": "visible", "index": 14 }], "scaleY": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 103, "key": "scaleY", "index": 0 }, { "value": 1.1, "tweenMethod": "linearNone", "tween": true, "target": 103, "label": null, "key": "scaleY", "index": 7 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 103, "label": null, "key": "scaleY", "index": 14 }], "scaleX": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 103, "key": "scaleX", "index": 0 }, { "value": 1.1, "tweenMethod": "linearNone", "tween": true, "target": 103, "label": null, "key": "scaleX", "index": 7 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 103, "label": null, "key": "scaleX", "index": 14 }], "alpha": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 103, "key": "alpha", "index": 0 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 103, "label": null, "key": "alpha", "index": 7 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 103, "label": null, "key": "alpha", "index": 14 }] } }], "name": "zi", "id": 7, "frameRate": 24, "action": 0 }], "loadList": ["res/ads/zz.png", "res/new_products/bg_ht_01.png", "res/new_products/tet_ht_nlg.png", "res/new_products/icon_item.png", "res/new_products/om_common_gq2.png", "res/new_products/tet_ht_ypqd.png", "res/new_products/tet_ht_yp.png", "res/new_products/om_ht_0.png", "res/new_products/tet_ht_xpcx.png", "res/new_products/tet_ht_xxcy.png", "res/new_products/bt_ht_gb.png", "res/new_products/om_ht_03.png", "res/new_products/tet_ht_jx.png"], "loadList3D": [] };
            syyx_ctr.ui_ctr_hUI = ui_ctr_hUI;
            REG("ui.syyx_ctr.ui_ctr_hUI", ui_ctr_hUI);
            class ui_ctr_itemUI extends View {
                constructor() {
                    super();
                }
                createChildren() {
                    super.createChildren();
                    this.createView(ui_ctr_itemUI.uiView);
                }
            }
            ui_ctr_itemUI.uiView = { "type": "View", "props": { "width": 280, "height": 380 }, "compId": 2, "child": [{ "type": "Image", "props": { "y": 0, "width": 300, "var": "bg", "skin": "res/new_products/box_ht_zm.png", "sizeGrid": "48,63,77,69", "height": 362, "centerX": 0 }, "compId": 26, "child": [{ "type": "Image", "props": { "width": 278, "var": "game_icon", "height": 278, "centerY": -32, "centerX": 0 }, "compId": 27, "child": [{ "type": "Image", "props": { "y": 277, "x": 0, "width": 278, "var": "label_bg1", "skin": "res/new_products/box_ht_dk3.png", "height": 44 }, "compId": 31 }, { "type": "Image", "props": { "y": 277, "x": 0, "width": 278, "var": "label_bg2", "skin": "res/new_products/box_ht_dk4.png", "height": 44 }, "compId": 32 }, { "type": "Image", "props": { "y": 277, "x": 0, "width": 278, "var": "label_bg3", "skin": "res/new_products/box_ht_dk5.png", "height": 44 }, "compId": 33 }, { "type": "Image", "props": { "y": 277, "x": 0, "width": 278, "var": "label_bg4", "skin": "res/new_products/box_ht_dk6.png", "height": 44 }, "compId": 34 }, { "type": "Image", "props": { "y": 277, "x": 0, "width": 278, "var": "label_bg5", "skin": "res/new_products/box_ht_dk7.png", "height": 44 }, "compId": 36 }, { "type": "Label", "props": { "y": 299, "x": 126, "var": "game_name", "text": "label", "fontSize": 32, "color": "#ffffff", "centerX": 0, "bottom": -39, "anchorY": 0.5, "anchorX": 0.5, "align": "center" }, "compId": 61 }] }, { "type": "Image", "props": { "width": 100, "var": "reward_icon", "scaleY": 1.3, "scaleX": 1.3, "height": 100, "centerY": -9, "centerX": 0, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 51, "child": [{ "type": "Image", "props": { "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "centerY": -9, "centerX": 0, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 63 }, { "type": "Image", "props": { "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "centerY": 8, "centerX": -30, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 65 }, { "type": "Image", "props": { "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "centerY": 8, "centerX": 30, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 66 }, { "type": "Image", "props": { "y": 91, "x": 47, "skin": "res/new_products/icon_item.png", "scaleY": 0.7, "scaleX": 0.7, "centerY": 24, "centerX": 0, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 67 }, { "type": "FontClip", "props": { "var": "rewardNum", "value": "100", "skin": "res/new_products/nub_1.png", "sheet": "0123456789", "scaleY": 0.8, "scaleX": 0.8, "centerY": 59, "centerX": 0 }, "compId": 69 }] }] }], "animations": [{ "nodes": [{ "target": 51, "keyframes": { "scaleY": [{ "value": 1.3, "tweenMethod": "linearNone", "tween": true, "target": 51, "key": "scaleY", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 51, "label": null, "key": "scaleY", "index": 25 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 51, "label": null, "key": "scaleY", "index": 37 }], "scaleX": [{ "value": 1.3, "tweenMethod": "linearNone", "tween": true, "target": 51, "key": "scaleX", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 51, "label": null, "key": "scaleX", "index": 25 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 51, "label": null, "key": "scaleX", "index": 37 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 51, "key": "alpha", "index": 0 }, { "value": 0.4, "tweenMethod": "linearNone", "tween": true, "target": 51, "label": null, "key": "alpha", "index": 25 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 51, "label": null, "key": "alpha", "index": 37 }] } }], "name": "dianzuanshi", "id": 1, "frameRate": 24, "action": 0 }], "loadList": ["res/new_products/box_ht_zm.png", "res/new_products/box_ht_dk3.png", "res/new_products/box_ht_dk4.png", "res/new_products/box_ht_dk5.png", "res/new_products/box_ht_dk6.png", "res/new_products/box_ht_dk7.png", "res/new_products/icon_item.png", "res/new_products/nub_1.png"], "loadList3D": [] };
            syyx_ctr.ui_ctr_itemUI = ui_ctr_itemUI;
            REG("ui.syyx_ctr.ui_ctr_itemUI", ui_ctr_itemUI);
        })(syyx_ctr = ui.syyx_ctr || (ui.syyx_ctr = {}));
    })(ui || (ui = {}));
    (function (ui) {
        class ui_demoUI extends View {
            constructor() {
                super();
            }
            createChildren() {
                super.createChildren();
                this.createView(ui_demoUI.uiView);
            }
        }
        ui_demoUI.uiView = { "type": "View", "props": { "width": 640, "height": 1136 }, "compId": 2, "child": [{ "type": "Button", "props": { "y": 168, "x": 355, "width": 158, "var": "btn_banner_native", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "原生banner", "height": 62 }, "compId": 8 }, { "type": "Button", "props": { "y": 309, "x": 21, "width": 158, "var": "btn_native_icon", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "原生icon", "height": 62 }, "compId": 44 }, { "type": "Button", "props": { "y": 168, "x": 22, "width": 133, "var": "btn_interstitial_native", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "原生插屏", "height": 62 }, "compId": 9 }, { "type": "Button", "props": { "y": 239, "x": 205, "width": 133, "var": "btn_inner_interstitial_native", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "结算原生", "height": 62 }, "compId": 10 }, { "type": "Button", "props": { "y": 168, "x": 177, "width": 163, "var": "btn_close_natiev_interstitial", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "关闭原生插屏", "height": 62 }, "compId": 24 }, { "type": "Button", "props": { "y": 239, "x": 355, "width": 190, "var": "btn_close_native_banner", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "关闭原生banner", "height": 62 }, "compId": 25 }, { "type": "Button", "props": { "y": 660, "x": 34, "width": 126, "var": "btn_interstitial", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "插普通屏", "height": 62 }, "compId": 13 }, { "type": "Button", "props": { "y": 239, "x": 12, "width": 190, "var": "btn_native", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "预加载结算原生", "height": 62 }, "compId": 14 }, { "type": "Button", "props": { "y": 309, "x": 374, "width": 126, "var": "btn_video", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "name": "btn_video", "labelSize": 25, "label": "激励视频", "height": 62 }, "compId": 15 }, { "type": "Button", "props": { "y": 789, "x": 524, "width": 126, "var": "btn_share", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "分享卡片", "height": 47 }, "compId": 17 }, { "type": "Button", "props": { "y": 789, "x": 386, "width": 126, "var": "btn_appbox", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "广告盒子", "height": 47 }, "compId": 94 }, { "type": "Button", "props": { "y": 1011, "x": 55, "width": 126, "var": "btn_system_info", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "系统信息", "height": 62 }, "compId": 18 }, { "type": "Button", "props": { "y": 896, "x": 38, "width": 126, "var": "btn_start_record", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "开始录屏", "height": 47 }, "compId": 20 }, { "type": "Button", "props": { "y": 896, "x": 350, "width": 126, "var": "btn_stop_record", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "结束录屏", "height": 47 }, "compId": 21 }, { "type": "Button", "props": { "y": 896, "x": 195, "width": 126, "var": "btn_share_record", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "分享录屏", "height": 47 }, "compId": 23 }, { "type": "Button", "props": { "y": 436, "x": 188, "width": 180, "var": "btn_banner_box", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "scaleY": 0.8, "scaleX": 0.8, "labelSize": 25, "label": "oppo横版盒子", "height": 62 }, "compId": 35 }, { "type": "Button", "props": { "y": 436, "x": 342, "width": 180, "var": "btn_close_box", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "scaleY": 0.8, "scaleX": 0.8, "labelSize": 25, "label": "关闭oppo盒子", "height": 62 }, "compId": 37 }, { "type": "Button", "props": { "y": 435, "x": 494, "width": 186, "var": "btn_draw_box", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "scaleY": 0.8, "scaleX": 0.8, "labelSize": 25, "label": "oppo抽屉", "height": 62 }, "compId": 99 }, { "type": "Button", "props": { "y": 501, "x": 494, "width": 186, "var": "btn_close_drawer", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "scaleY": 0.8, "scaleX": 0.8, "labelSize": 25, "label": "关闭抽屉", "height": 62 }, "compId": 100 }, { "type": "Button", "props": { "y": 436, "x": 21, "width": 186, "var": "btn_portal_box", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "scaleY": 0.8, "scaleX": 0.8, "labelSize": 25, "label": "oppo九宫格盒子", "height": 62 }, "compId": 36 }, { "type": "Text", "props": { "y": 906, "x": 67, "wordWrap": true, "width": 514, "var": "msg", "text": "text", "name": "msg", "height": 182, "fontSize": 20, "color": "#ffffff", "runtime": "laya.display.Text" }, "compId": 16 }, { "type": "Label", "props": { "y": 1067, "x": 307, "var": "txt_num", "text": "1", "fontSize": 50, "color": "#ffffff" }, "compId": 22 }, { "type": "Button", "props": { "y": 309, "x": 191, "width": 158, "var": "btn_hide_native_icon", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "隐藏原生icon", "height": 62 }, "compId": 45 }, { "type": "Button", "props": { "y": 789, "x": 35, "width": 158, "var": "btn_show_block", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "显示手q积木", "height": 47 }, "compId": 48 }, { "type": "Button", "props": { "y": 789, "x": 215, "width": 158, "var": "btn_hide_block", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "隐藏手q积木", "height": 47 }, "compId": 49 }, { "type": "Box", "props": { "width": 500, "var": "inner_interstitial_parent", "scaleY": 0.5, "scaleX": 0.5, "height": 300, "centerY": 103, "centerX": 52 }, "compId": 26 }, { "type": "Box", "props": { "width": 100, "var": "gamebox_parent", "scaleY": 0.5, "scaleX": 0.5, "height": 100, "centerY": 453, "centerX": 22 }, "compId": 56 }, { "type": "Image", "props": { "skin": "res/ads/img_title_bg2.png", "scaleY": 0.5, "scaleX": 0.5, "left": 0 }, "compId": 61, "child": [{ "type": "Text", "props": { "y": 3, "x": 31, "valign": "middle", "text": "新品尝鲜", "fontSize": 70, "font": "SimHei", "color": "#ffffff", "align": "center", "runtime": "laya.display.Text" }, "compId": 62 }] }, { "type": "Image", "props": { "y": 117, "skin": "res/ads/img_title_bg2.png", "scaleY": 0.5, "scaleX": 0.5, "left": 8 }, "compId": 73, "child": [{ "type": "Text", "props": { "y": 3, "x": 31, "valign": "middle", "text": "原生广告", "fontSize": 70, "font": "SimHei", "color": "#ffffff", "align": "center", "runtime": "laya.display.Text" }, "compId": 74 }] }, { "type": "Image", "props": { "y": 378, "skin": "res/ads/img_title_bg2.png", "scaleY": 0.5, "scaleX": 0.5, "left": 8 }, "compId": 75, "child": [{ "type": "Text", "props": { "y": 3, "x": 31, "valign": "middle", "text": "OPPO", "fontSize": 70, "font": "SimHei", "color": "#ffffff", "align": "center", "runtime": "laya.display.Text" }, "compId": 76 }] }, { "type": "Image", "props": { "y": 605, "skin": "res/ads/img_title_bg2.png", "scaleY": 0.5, "scaleX": 0.5, "left": 26 }, "compId": 77, "child": [{ "type": "Text", "props": { "y": 3, "x": 31, "valign": "middle", "text": "VIVO", "fontSize": 70, "font": "SimHei", "color": "#ffffff", "align": "center", "runtime": "laya.display.Text" }, "compId": 78 }] }, { "type": "Image", "props": { "y": 729, "skin": "res/ads/img_title_bg2.png", "scaleY": 0.5, "scaleX": 0.5, "left": 26 }, "compId": 79, "child": [{ "type": "Text", "props": { "y": 3, "x": 31, "valign": "middle", "text": "手Q", "fontSize": 70, "font": "SimHei", "color": "#ffffff", "align": "center", "runtime": "laya.display.Text" }, "compId": 80 }] }, { "type": "Image", "props": { "y": 850, "skin": "res/ads/img_title_bg2.png", "scaleY": 0.5, "scaleX": 0.5, "left": 26 }, "compId": 81, "child": [{ "type": "Text", "props": { "y": 3, "x": 31, "valign": "middle", "text": "头条", "fontSize": 70, "font": "SimHei", "color": "#ffffff", "align": "center", "runtime": "laya.display.Text" }, "compId": 82 }] }, { "type": "Image", "props": { "y": 954, "skin": "res/ads/img_title_bg2.png", "scaleY": 0.5, "scaleX": 0.5, "left": 26 }, "compId": 83, "child": [{ "type": "Text", "props": { "y": 3, "x": 31, "valign": "middle", "text": "通用功能", "fontSize": 70, "font": "SimHei", "color": "#ffffff", "align": "center", "runtime": "laya.display.Text" }, "compId": 84 }] }, { "type": "Box", "props": { "width": 100, "var": "native_icon_parent", "height": 100, "centerY": 104, "centerX": 39 }, "compId": 46 }, { "type": "Image", "props": { "y": 976, "x": 367, "visible": false, "var": "btn_inner_check", "skin": "res/ads/bt_chaping_01.png", "scaleY": 0.5, "scaleX": 0.5 }, "compId": 90, "child": [{ "type": "Text", "props": { "y": 46.5, "x": 109, "text": "点击查看", "fontSize": 48, "color": "#ffffff", "runtime": "laya.display.Text" }, "compId": 95 }] }, { "type": "Button", "props": { "y": 49, "x": 18, "width": 158, "var": "btn_new_produce", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "新品尝鲜", "height": 62 }, "compId": 96 }, { "type": "Button", "props": { "y": 49, "x": 224, "width": 158, "var": "btn_video_interstitial", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "视频插屏", "height": 62 }, "compId": 97 }, { "type": "Button", "props": { "y": 501, "x": 21, "width": 180, "var": "btn_carousel_box", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "scaleY": 0.8, "scaleX": 0.8, "labelSize": 25, "label": "oppo轮播横幅", "height": 62 }, "compId": 101 }, { "type": "Button", "props": { "y": 660, "x": 164, "width": 126, "var": "btn_vivo_portal", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "vivo九宫格盒子", "height": 62 }, "compId": 102 }, { "type": "Button", "props": { "y": 660, "x": 296, "width": 126, "var": "btn_vivo_box", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "vivo横幅", "height": 62 }, "compId": 103 }, { "type": "Button", "props": { "y": 660, "x": 431, "width": 126, "var": "btn_vivo_close_box", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "关闭vivo盒子", "height": 62 }, "compId": 105 }], "loadList": ["comp/bt_commoni_01.png", "res/ads/img_title_bg2.png", "res/ads/bt_chaping_01.png"], "loadList3D": [] };
        ui.ui_demoUI = ui_demoUI;
        REG("ui.ui_demoUI", ui_demoUI);
        class ui_demo_apkUI extends View {
            constructor() {
                super();
            }
            createChildren() {
                super.createChildren();
                this.createView(ui_demo_apkUI.uiView);
            }
        }
        ui_demo_apkUI.uiView = { "type": "View", "props": { "width": 640, "height": 1136 }, "compId": 2, "child": [{ "type": "Button", "props": { "y": 251, "x": 22, "width": 158, "var": "btn_banner_native", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "原生banner", "height": 62 }, "compId": 8 }, { "type": "Button", "props": { "y": 168, "x": 183.5, "width": 133, "var": "btn_interstitial_normal", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "普通插屏", "height": 62 }, "compId": 112 }, { "type": "Button", "props": { "y": 168, "x": 22, "width": 133, "var": "btn_interstitial_native", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "原生插屏", "height": 62 }, "compId": 9 }, { "type": "Button", "props": { "y": 435, "x": 420.5, "width": 163, "var": "btn_close_natiev_interstitial", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "关闭原生", "height": 62 }, "compId": 24 }, { "type": "Button", "props": { "y": 251, "x": 217.90380859375, "width": 190, "var": "btn_close_native_banner", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "关闭原生banner", "height": 62 }, "compId": 25 }, { "type": "Button", "props": { "y": 344, "x": 217.90380859375, "width": 126, "var": "btn_video", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "name": "btn_video", "labelSize": 25, "label": "激励视频", "height": 62 }, "compId": 15 }, { "type": "Button", "props": { "y": 344, "x": 22, "width": 158, "var": "btn_video_interstitial", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "视频插屏", "height": 62 }, "compId": 98 }, { "type": "Button", "props": { "y": 344, "x": 377, "width": 250, "var": "btn_jump_leisure", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "跳转休闲主题大厅", "height": 62 }, "compId": 99 }, { "type": "Label", "props": { "y": 984, "x": 285.09619140625, "var": "txt_num", "text": "1", "fontSize": 50, "color": "#ffffff" }, "compId": 22 }, { "type": "Image", "props": { "y": 117, "skin": "res/ads/img_title_bg2.png", "scaleY": 0.5, "scaleX": 0.5, "left": 8 }, "compId": 73, "child": [{ "type": "Text", "props": { "y": 3, "x": 31, "valign": "middle", "text": "apk", "fontSize": 70, "font": "SimHei", "color": "#ffffff", "align": "center", "runtime": "laya.display.Text" }, "compId": 74 }] }, { "type": "Button", "props": { "y": 435, "x": 22, "width": 158, "var": "btn_load_inner", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "预加载结算原生", "height": 62 }, "compId": 100 }, { "type": "Button", "props": { "y": 435, "x": 217.90380859375, "width": 158, "var": "btn_show_inner", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "展示结算原生", "height": 62 }, "compId": 102 }, { "type": "Button", "props": { "y": 516, "x": 32, "width": 158, "var": "btn_get_channel_id", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "apk渠道id", "height": 62 }, "compId": 103 }, { "type": "Button", "props": { "y": 516, "x": 233.90380859375, "width": 158, "var": "btn_get_devicel_id", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "手机设备id", "height": 62 }, "compId": 104 }, { "type": "Button", "props": { "y": 516, "x": 435, "width": 158, "var": "btn_get_ad_channel_id", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "广告包id", "height": 62 }, "compId": 105 }, { "type": "Button", "props": { "y": 624, "x": 208.5, "width": 250, "var": "btn_check", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "结算原生—查看广告", "height": 62 }, "compId": 106 }, { "type": "Button", "props": { "y": 698, "x": 183.5, "width": 300, "var": "btn_check1", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "结算页—确定按钮", "height": 62 }, "compId": 107 }, { "type": "Button", "props": { "y": 168, "x": 333.5, "width": 250, "var": "btn_check2", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "结算页—返回按钮", "height": 62 }, "compId": 109 }, { "type": "Button", "props": { "y": 86, "x": 333.5, "width": 250, "var": "btn_check3", "stateNum": 1, "skin": "comp/bt_commoni_01.png", "labelSize": 25, "label": "结算页—退出按钮", "height": 62 }, "compId": 110 }, { "type": "Box", "props": { "width": 400, "var": "inner_box", "left": 0, "height": 200, "bottom": 127 }, "compId": 111 }], "loadList": ["comp/bt_commoni_01.png", "res/ads/img_title_bg2.png"], "loadList3D": [] };
        ui.ui_demo_apkUI = ui_demo_apkUI;
        REG("ui.ui_demo_apkUI", ui_demo_apkUI);
    })(ui || (ui = {}));

    var init_config = {
        web: {
            app_id: 600197,
            app_version: "1.0.0.1",
            pkg_name: "",
            channel_type: 1,
            channel_id: 0,
            engin_type: 2,
            engin_version: "2.0.10",
            log_level: 0,
            pay_key: "",
            login_key: "",
            plat_key: "123",
            stat_key: "",
            pay_url: "",
            pay_query_url: "",
            pay_last_query_url: "",
            login_url: "",
            ws_url: "",
            ws_cert_url: "",
            stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/gameEvent",
            batch_stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/batchGameEvent",
            configUrl: "https://igc-strategy.syyx.com:18980/strategy/get_config/",
            configAppSecKey: "",
            retry_connect: false,
            basic_config_version: "",
            is_login_soon: false,
            is_auto_relogin_full: false,
            has_red_envelop: false,
            has_multual_push: false
        },
        oppo_qg: {
            app_id: 600000,
            ad_app_id: 0,
            app_version: "1.0.0.0",
            pkg_name: "",
            channel_type: 2,
            channel_id: 1,
            engin_type: 2,
            engin_version: "2.0.10",
            log_level: 0,
            pay_key: "",
            login_key: "",
            plat_key: "123",
            stat_key: "",
            pay_url: "",
            pay_query_url: "",
            pay_last_query_url: "",
            login_url: "",
            ws_url: "",
            ws_cert_url: "",
            stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/gameEvent",
            batch_stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/batchGameEvent",
            configUrl: "https://tpf-common-config-api.syyx.com:37110/api/config",
            configAppSecKey: "",
            retry_connect: false,
            basic_config_version: "",
            is_login_soon: false,
            is_auto_relogin_full: false,
            has_red_envelop: false,
            has_multual_push: false
        },
        vivo_qg: {
            app_id: 600000,
            ad_app_id: 0,
            app_version: "1.0.0.0",
            pkg_name: "",
            channel_type: 4,
            channel_id: 2,
            engin_type: 1,
            engin_version: "2.0.10",
            log_level: 0,
            pay_key: "",
            login_key: "",
            plat_key: "123",
            stat_key: "",
            pay_url: "",
            pay_query_url: "",
            pay_last_query_url: "",
            login_url: "",
            ws_url: "",
            ws_cert_url: "",
            stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/gameEvent",
            batch_stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/batchGameEvent",
            configUrl: "https://tpf-common-config-api.syyx.com:37110/api/config",
            configAppSecKey: "",
            retry_connect: false,
            basic_config_version: "",
            is_login_soon: false,
            is_auto_relogin_full: false,
            has_red_envelop: false,
            has_multual_push: false,
            needAuth: false,
        },
        tt: {
            app_id: 600000,
            ad_app_id: 0,
            app_version: "1.0.0.0",
            pkg_name: "",
            channel_type: 8,
            channel_id: 10,
            engin_type: 2,
            engin_version: "2.0.10",
            log_level: 0,
            pay_key: "",
            login_key: "",
            plat_key: "123",
            stat_key: "",
            pay_url: "",
            pay_query_url: "",
            pay_last_query_url: "",
            login_url: "",
            ws_url: "",
            ws_cert_url: "",
            stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/gameEvent",
            batch_stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/batchGameEvent",
            configUrl: "https://tpf-common-config-api.syyx.com:37110/api/config",
            configAppSecKey: "",
            retry_connect: false,
            basic_config_version: "",
            is_login_soon: false,
            is_auto_relogin_full: false,
            has_red_envelop: false,
            has_multual_push: false,
            needAuth: false,
        },
        qq: {
            app_id: 600000,
            ad_app_id: 0,
            app_version: "1.0.0.0",
            pkg_name: "",
            channel_type: 5,
            channel_id: 8,
            engin_type: 2,
            engin_version: "2.0.10",
            log_level: 0,
            pay_key: "",
            login_key: "",
            plat_key: "123",
            stat_key: "",
            pay_url: "",
            pay_query_url: "",
            pay_last_query_url: "",
            login_url: "",
            ws_url: "",
            ws_cert_url: "",
            stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/gameEvent",
            batch_stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/batchGameEvent",
            configUrl: "https://tpf-common-config-api.syyx.com:37110/api/config",
            configAppSecKey: "",
            retry_connect: false,
            basic_config_version: "",
            is_login_soon: false,
            is_auto_relogin_full: false,
            has_red_envelop: false,
            has_multual_push: false,
            needAuth: false,
        },
        apk: {
            app_id: 600000,
            ad_app_id: 0,
            app_version: "1.0.0.0",
            pkg_name: "",
            channel_type: 7,
            channel_id: 11,
            engin_type: 2,
            engin_version: "2.0.10",
            log_level: 0,
            pay_key: "",
            login_key: "",
            plat_key: "123",
            stat_key: "",
            pay_url: "",
            pay_query_url: "",
            pay_last_query_url: "",
            login_url: "",
            ws_url: "",
            ws_cert_url: "",
            stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/gameEvent",
            batch_stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/batchGameEvent",
            configUrl: "https://tpf-common-config-api.syyx.com:37110/api/config",
            configAppSecKey: "",
            retry_connect: false,
            basic_config_version: "",
            is_login_soon: false,
            is_auto_relogin_full: false,
            has_red_envelop: false,
            has_multual_push: false
        },
        wx: {
            app_id: 600000,
            ad_app_id: 0,
            app_version: "1.0.0.0",
            pkg_name: "",
            channel_type: 3,
            channel_id: 4,
            engin_type: 2,
            engin_version: "2.0.10",
            log_level: 0,
            pay_key: "",
            login_key: "",
            plat_key: "123",
            stat_key: "",
            pay_url: "",
            pay_query_url: "",
            pay_last_query_url: "",
            login_url: "",
            ws_url: "",
            ws_cert_url: "",
            stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/gameEvent",
            batch_stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/batchGameEvent",
            configUrl: "https://tpf-common-config-api.syyx.com:37110/api/config",
            configAppSecKey: "",
            retry_connect: false,
            basic_config_version: "",
            is_login_soon: false,
            is_auto_relogin_full: false,
            has_red_envelop: false,
            has_multual_push: false,
            needAuth: false,
        },
        hw_qg: {
            app_id: 600000,
            ad_app_id: 103490213,
            channel_app_id: 103490213,
            app_version: "1.0.0.0",
            pkg_name: "",
            channel_type: 10,
            channel_id: 14,
            engin_type: 2,
            engin_version: "2.0.10",
            log_level: 0,
            pay_key: "",
            login_key: "",
            plat_key: "123",
            stat_key: "",
            pay_url: "",
            pay_query_url: "",
            pay_last_query_url: "",
            login_url: "",
            ws_url: "",
            ws_cert_url: "",
            stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/gameEvent",
            batch_stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/batchGameEvent",
            configUrl: "https://tpf-common-config-api.syyx.com:37110/api/config",
            configAppSecKey: "",
            retry_connect: false,
            basic_config_version: "",
            is_login_soon: false,
            is_auto_relogin_full: false,
            has_red_envelop: false,
            has_multual_push: false,
            needAuth: false,
        },
        ipa: {
            app_id: 600000,
            ad_app_id: 0,
            app_version: "1.0.0.17",
            pkg_name: "com.lewan.wyykd",
            channel_type: 7,
            channel_id: 11,
            engin_type: 2,
            engin_version: "2.0.10",
            log_level: 0,
            pay_key: "",
            login_key: "",
            plat_key: "123",
            stat_key: "",
            pay_url: "",
            pay_query_url: "",
            pay_last_query_url: "",
            login_url: "",
            ws_url: "",
            ws_cert_url: "",
            stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/gameEvent",
            batch_stat_event_url: "https://tpfdata.syyx.com:35002/bigdata/record/batchGameEvent",
            configUrl: "https://tpf-common-config-api.syyx.com:37110/api/config",
            configAppSecKey: "",
            retry_connect: false,
            basic_config_version: "",
            is_login_soon: false,
            is_auto_relogin_full: false,
            has_red_envelop: false,
            has_multual_push: false
        },
        _debug: true,
    };
    var e_syyx_sdk_publish_type;
    (function (e_syyx_sdk_publish_type) {
        e_syyx_sdk_publish_type[e_syyx_sdk_publish_type["in"] = 1] = "in";
        e_syyx_sdk_publish_type[e_syyx_sdk_publish_type["out"] = 2] = "out";
    })(e_syyx_sdk_publish_type || (e_syyx_sdk_publish_type = {}));
    var syyx_const = {
        syyx_sdk_version: "2.0.0.20",
        syyx_sdk_publish: e_syyx_sdk_publish_type.out,
        syyx_sdk_channel: igc.e_channel_type.oppo_qg,
        syyx_sdk_tag: "2021_08_11_17_10",
        syyx_sdk_version_key: "syyx_sdk_version_key",
        remote_business_config_key: "business_config",
        local_business_config_version: "syyx_business_config_version",
        local_business_config_data: "syyx_business_config_data",
        remote_ctr_config_key: "multual_push_v5",
        local_ctr_config_version: "syyx_ctr_config_version",
        local_ctr_config_data: "syyx_ctr_config_data",
    };

    class syyx_sdk_utils {
        static log(...data) {
            console.log("syyx_sdk: ", JSON.stringify(data));
        }
        static error(...data) {
            if (init_config._debug)
                console.error("syyx_sdk: ", JSON.stringify(data));
        }
        static get_random_number(list) {
            return list[0] + Math.floor(Math.random() * (list[1] - list[0]));
        }
        static set_default_scale(view) {
            let ratio = this.get_screen_ratio();
            ratio *= view.scaleX;
            view.scale(ratio, ratio);
            console.log("igc-----screen_ratio", ratio);
        }
        static get_screen_ratio() {
            if (window["Laya"]) {
                if (Laya.stage.width > Laya.stage.height) {
                    return window["Laya"].stage.height / 1080;
                }
                else {
                    return window["Laya"].stage.width / 1080;
                }
            }
            return 1;
        }
        static get_largest_zorder() {
            let max = 10000001;
            return max;
        }
        static get_size(view) {
            if (window["Laya"]) {
                return { width: view.width * view.scaleX, height: view.height * view.scaleY };
            }
            return {};
        }
        static get_stage() {
            if (window["Laya"]) {
                window["Laya"].stage;
            }
            else {
                return window["cc"].director.getScene().getChildByName("Canvas");
            }
        }
        static set_cp_load_function(load_function) {
            if (!this.cp_load_function) {
                this.cp_load_function = load_function;
            }
        }
        static parse_csv(data, key) {
            if (syyx_const.syyx_sdk_publish === e_syyx_sdk_publish_type.in) {
                return data;
            }
            else {
                return igc.utils_manager.parse_csv(data, key);
            }
        }
        static load_resource(file_path, load_back, self = undefined, error_back) {
            if (this.cp_load_function) {
                this.cp_load_function(file_path, load_back);
                return;
            }
            window["Laya"].loader.load(file_path, window["Laya"].Handler.create(self, data => {
                if (data) {
                    load_back && load_back(data);
                }
                else {
                    console.error("igc----- syyx_sdk_utils load_resource fail", file_path);
                    error_back && error_back();
                }
            }));
        }
        static set_texture_url(icon, icon_url, call_back) {
            if (window["cc"]) {
                let url = icon_url;
                if (this.is_texture_url(icon_url)) {
                    url = icon_url;
                }
                else {
                    url = icon_url + ".jpg";
                }
                try {
                    window["cc"].loader.load(icon_url, function (err, data) {
                        if (err) {
                            console.error("igc-----load texture fail", err);
                            call_back && call_back();
                        }
                        else if (window["cc"].isValid(icon) && data) {
                            let frames = new window["cc"].SpriteFrame(data);
                            icon.spriteFrame = frames;
                        }
                    });
                }
                catch (error) {
                    call_back && call_back();
                }
            }
        }
        static is_texture_url(icon_url) {
            if (icon_url.indexOf(".bmp") != -1 ||
                icon_url.indexOf(".jpg") != -1 ||
                icon_url.indexOf(".png") != -1 ||
                icon_url.indexOf(".tif") != -1 ||
                icon_url.indexOf(".gif") != -1 ||
                icon_url.indexOf(".pcx") != -1 ||
                icon_url.indexOf(".tga") != -1 ||
                icon_url.indexOf(".exif") != -1 ||
                icon_url.indexOf(".fpx") != -1 ||
                icon_url.indexOf(".svg") != -1 ||
                icon_url.indexOf(".psd") != -1 ||
                icon_url.indexOf(".cdr") != -1 ||
                icon_url.indexOf(".pcd") != -1 ||
                icon_url.indexOf(".dxf") != -1 ||
                icon_url.indexOf(".ufo") != -1 ||
                icon_url.indexOf(".eps") != -1 ||
                icon_url.indexOf(".ai") != -1 ||
                icon_url.indexOf(".raw") != -1 ||
                icon_url.indexOf(".WMF") != -1 ||
                icon_url.indexOf(".webp") != -1 ||
                icon_url.indexOf(".avif") != -1) {
                return true;
            }
            return false;
        }
        static check_is_same_day(left_time, right_tiem) {
            let left_date = new Date(left_time);
            let right_date = new Date(right_tiem);
            if (left_date.getDay() != right_date.getDay() || left_date.getMonth() != right_date.getMonth() || left_date.getFullYear() != right_date.getFullYear()) {
                return false;
            }
            return true;
        }
        static get_date_timestamp() {
            return (new Date()).getTime();
        }
        static set_item(key, value) {
            let str = JSON.stringify(value);
            localStorage.setItem(key, str);
        }
        static get_item(key) {
            let item = localStorage.getItem(key);
            if (item != "") {
                return JSON.parse(item);
            }
            return null;
        }
        static formatTime(time) {
            if (time <= 0) {
                return "00:00:00";
            }
            var h = Math.floor(time / 3600);
            var m = Math.floor(time / 60 % 60);
            var s = Math.floor(time % 60);
            var hStr = h + "";
            var mStr = m + "";
            var sStr = s + "";
            if (h < 10) {
                hStr = "0" + h;
            }
            if (m < 10) {
                mStr = "0" + m;
            }
            if (s < 10) {
                sStr = "0" + s;
            }
            return hStr + ":" + mStr + ":" + sStr;
        }
        static formatTime_mmss(time) {
            if (time <= 0) {
                return "00:00";
            }
            var m = Math.floor(time / 60 % 60);
            var s = Math.floor(time % 60);
            var mStr = m + "";
            var sStr = s + "";
            if (m < 10) {
                mStr = "0" + m;
            }
            if (s < 10) {
                sStr = "0" + s;
            }
            return mStr + ":" + sStr;
        }
        static changeToMb(Gold, fix_num = 1) {
            if (Gold < 10000) {
                return Gold + "";
            }
            let myGold = parseInt(Gold);
            let goldStr = "";
            if (myGold < 1000) {
                goldStr = Math.floor(myGold) + "";
            }
            else if (myGold < 1000000) {
                goldStr = (myGold / 1000).toFixed(fix_num) + "k";
            }
            else if (myGold < 1000000000) {
                goldStr = (myGold / 1000000).toFixed(fix_num) + "m";
            }
            else if (myGold < 1000000000000) {
                goldStr = (myGold / 1000000000).toFixed(fix_num) + "b";
            }
            else {
                goldStr = (myGold / 1000000000000).toFixed(fix_num) + "t";
            }
            return goldStr;
        }
        static replace_data(data1, data2) {
            if (!data1 && !data2) {
                console.log("the object is wrong");
                return;
            }
            for (let idx in data2) {
                data2[idx] && data2[idx].value && (data1[idx] = data2[idx]);
                if (!data2[idx].value) {
                    console.log("igc ----- remote config " + idx + " is wrong");
                }
            }
            console.log("-------");
            console.log(data2);
        }
        static format_remote_texture_url(url) {
            if (url) {
                let jpg_index = url.indexOf(".jpg");
                let png_index = url.indexOf(".png");
                if (jpg_index != -1) {
                    return url.substring(0, jpg_index + 4);
                }
                else if (png_index != -1) {
                    return url.substring(0, png_index + 4);
                }
                else {
                    return url;
                }
            }
            return url;
        }
        static get_random_int(min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }
        static shuffle(arr) {
            let _arr = arr.slice();
            for (let i = 0; i < _arr.length; i++) {
                let j = this.get_random_int(0, i);
                let t = _arr[i];
                _arr[i] = _arr[j];
                _arr[j] = t;
            }
            return _arr;
        }
        static preload_native_texture(url) {
            let self = this;
            if (window["Laya"]) {
                window["Laya"].loader.load(url, window["Laya"].Handler.create(self, data => {
                    if (data) {
                        console.log('igc----- preload native_texture success---->', url);
                    }
                }));
            }
            else {
                let texture_type = 'jpg';
                if (url.indexOf(".png") != -1) {
                    texture_type = 'png';
                }
                if (window["cc"]["assetManager"] && window["cc"]["assetManager"].loadRemote) {
                    window["cc"]["assetManager"].loadRemote(url, function (err, data) {
                        if (data) {
                            console.log('igc----- preload native_texture success---->', url);
                        }
                    });
                }
                else {
                    window["cc"].loader.load({ url: url, type: texture_type }, function (err, data) {
                        if (data) {
                            console.log('igc----- preload native_texture success---->', url);
                        }
                    });
                }
            }
        }
        static getScreenLeftNTop(node) {
            if (this.temp_point1 == null) {
                this.temp_point1 = new Laya.Point();
                this.temp_point2 = new Laya.Point();
            }
            this.temp_point1.x = 0;
            this.temp_point1.y = 0;
            let stage_point1 = node.localToGlobal(this.temp_point1, false);
            console.log("igc----- 按钮左上角的舞台坐标" + stage_point1.x + "," + stage_point1.y);
            let screen_x1 = stage_point1.x * Laya.Browser.clientWidth / Laya.stage.width;
            let screen_y1 = stage_point1.y * Laya.Browser.clientHeight / Laya.stage.height;
            console.log("igc----- 按钮左上角的屏幕坐标（像素）" + screen_x1 + "," + screen_y1);
            this.temp_point2.x = node.width * node.scaleX;
            this.temp_point2.y = node.height * node.scaleY;
            let stage_point2 = node.localToGlobal(this.temp_point2, false);
            console.log("igc----- 按钮右下角的舞台坐标" + stage_point2.x + "," + stage_point2.y);
            let screen_x2 = stage_point2.x * Laya.Browser.clientWidth / Laya.stage.width;
            let screen_y2 = stage_point2.y * Laya.Browser.clientHeight / Laya.stage.height;
            console.log("igc----- 按钮右下角的屏幕坐标（像素）" + screen_x2 + "," + screen_y2);
            let btn_width = screen_x2 - screen_x1;
            let btn_height = screen_y2 - screen_y1;
            console.log("igc----- 按钮在屏幕下的宽高（像素）" + btn_width + "," + btn_height);
            return { left: Math.ceil(screen_x1), top: Math.ceil(screen_y1), width: Math.ceil(btn_width), height: Math.ceil(btn_height) };
        }
    }
    syyx_sdk_utils.cp_load_function = undefined;
    syyx_sdk_utils.temp_point1 = null;
    syyx_sdk_utils.temp_point2 = null;
    window["oppoSdk"]["syyx_sdk_utils"] = syyx_sdk_utils;

    var e_syyx_ctr_event_type;
    (function (e_syyx_ctr_event_type) {
        e_syyx_ctr_event_type[e_syyx_ctr_event_type["show"] = 30002] = "show";
        e_syyx_ctr_event_type[e_syyx_ctr_event_type["click"] = 30001] = "click";
    })(e_syyx_ctr_event_type || (e_syyx_ctr_event_type = {}));
    var e_ad_native_type;
    (function (e_ad_native_type) {
        e_ad_native_type[e_ad_native_type["native_banner"] = 1] = "native_banner";
        e_ad_native_type[e_ad_native_type["native_inner_interstitial"] = 2] = "native_inner_interstitial";
        e_ad_native_type[e_ad_native_type["native_interstitial"] = 3] = "native_interstitial";
        e_ad_native_type[e_ad_native_type["native_icon"] = 4] = "native_icon";
    })(e_ad_native_type || (e_ad_native_type = {}));
    var e_ad_native_state;
    (function (e_ad_native_state) {
        e_ad_native_state[e_ad_native_state["none"] = 0] = "none";
        e_ad_native_state[e_ad_native_state["need_show"] = 1] = "need_show";
        e_ad_native_state[e_ad_native_state["show"] = 2] = "show";
        e_ad_native_state[e_ad_native_state["click"] = 3] = "click";
    })(e_ad_native_state || (e_ad_native_state = {}));
    var e_ad_native_click_pro_type;
    (function (e_ad_native_click_pro_type) {
        e_ad_native_click_pro_type[e_ad_native_click_pro_type["none"] = 1] = "none";
        e_ad_native_click_pro_type[e_ad_native_click_pro_type["cooling"] = 2] = "cooling";
        e_ad_native_click_pro_type[e_ad_native_click_pro_type["active"] = 3] = "active";
    })(e_ad_native_click_pro_type || (e_ad_native_click_pro_type = {}));
    var e_stat_event_type;
    (function (e_stat_event_type) {
        e_stat_event_type["hall"] = "10001";
        e_stat_event_type["chapter"] = "10002";
        e_stat_event_type["result"] = "10003";
    })(e_stat_event_type || (e_stat_event_type = {}));
    var e_stat_event_id;
    (function (e_stat_event_id) {
        e_stat_event_id["none"] = "0";
        e_stat_event_id["win_click_native_adv"] = "10001";
        e_stat_event_id["lose_click_native_adv"] = "10002";
    })(e_stat_event_id || (e_stat_event_id = {}));
    var e_chapter_result_type;
    (function (e_chapter_result_type) {
        e_chapter_result_type["enter_chapter"] = "1000000";
        e_chapter_result_type["win"] = "1000001";
        e_chapter_result_type["lose"] = "1000002";
    })(e_chapter_result_type || (e_chapter_result_type = {}));
    var e_hall_stat_type;
    (function (e_hall_stat_type) {
        e_hall_stat_type["enter_hall"] = "1";
    })(e_hall_stat_type || (e_hall_stat_type = {}));
    var e_settlement_stat_type;
    (function (e_settlement_stat_type) {
        e_settlement_stat_type["enter_settlement"] = "1";
    })(e_settlement_stat_type || (e_settlement_stat_type = {}));
    var e_methoh_enum;
    (function (e_methoh_enum) {
        e_methoh_enum["set_banner_height"] = "1";
        e_methoh_enum["has_native_data"] = "2";
        e_methoh_enum["set_apk_remote_config"] = "3";
    })(e_methoh_enum || (e_methoh_enum = {}));
    var syyx_prefab_path;
    (function (syyx_prefab_path) {
    })(syyx_prefab_path || (syyx_prefab_path = {}));
    var e_ad_priority_type;
    (function (e_ad_priority_type) {
        e_ad_priority_type[e_ad_priority_type["multi"] = 1] = "multi";
        e_ad_priority_type[e_ad_priority_type["big"] = 2] = "big";
        e_ad_priority_type[e_ad_priority_type["group"] = 3] = "group";
    })(e_ad_priority_type || (e_ad_priority_type = {}));
    var e_ad_id;
    (function (e_ad_id) {
        e_ad_id["interstitial_hall"] = "10100001";
        e_ad_id["interstitial_video"] = "10100002";
        e_ad_id["video_add_gold"] = "10200001";
        e_ad_id["video_add_diamond"] = "10200002";
        e_ad_id["video_add_bomb"] = "10200003";
        e_ad_id["video_forging"] = "10200004";
        e_ad_id["video_luck_draw"] = "10200005";
        e_ad_id["native_interstitial_hall"] = "10301001";
        e_ad_id["native_inner_interstitial_success"] = "10302001";
        e_ad_id["native_banner"] = "10304001";
        e_ad_id["native_icon"] = "10304002";
        e_ad_id["native_banner_1"] = "10304003";
        e_ad_id["native_banner_2"] = "10304004";
        e_ad_id["native_banner_3"] = "10304005";
        e_ad_id["banner_hall"] = "10400001";
        e_ad_id["appbox_hall"] = "10600001";
        e_ad_id["game_banner_box"] = "10600002";
        e_ad_id["game_portal_box"] = "10600003";
        e_ad_id["carousel_banner_box"] = "10600004";
        e_ad_id["drawer_box"] = "10600005";
        e_ad_id["bottom_block"] = "10900001";
        e_ad_id["left_block"] = "10900002";
        e_ad_id["right_block"] = "10900003";
        e_ad_id["native_interstitial_1"] = "10301002";
        e_ad_id["native_interstitial_2"] = "10301003";
        e_ad_id["native_interstitial_3"] = "10301004";
        e_ad_id["native_inner_interstitial_1"] = "10302002";
        e_ad_id["native_inner_interstitial_2"] = "10302003";
        e_ad_id["native_inner_interstitial_3"] = "10302004";
    })(e_ad_id || (e_ad_id = {}));
    class apk_ad_attr_enum {
        constructor() {
        }
    }
    class apk_ad_attr {
        constructor(containerUi, adImgUi, adDescUi, clickBtnUi, specialBtnGroup) {
            this.specialBtnGroup = {};
            this.styleModel = 0;
            this.container = containerUi || null;
            this.adImg = adImgUi || null;
            this.adDesc = adDescUi || null;
            this.clickBtn = clickBtnUi || null;
            this.specialBtnGroup = specialBtnGroup || {};
        }
        set_click_btn(value) {
            this.clickBtn = value || null;
        }
        set_special_btn_group(value) {
            this.specialBtnGroup = value || {};
        }
        set_container(value) {
            this.container = value || null;
        }
        set_style_model(value) {
            this.styleModel = value || null;
        }
    }

    var syyx_view;
    (function (syyx_view) {
        syyx_view[syyx_view["native_banner"] = 0] = "native_banner";
        syyx_view[syyx_view["inner_interstitial"] = 1] = "inner_interstitial";
        syyx_view[syyx_view["interstitial"] = 2] = "interstitial";
        syyx_view[syyx_view["native_icon"] = 3] = "native_icon";
        syyx_view[syyx_view["toast"] = 4] = "toast";
        syyx_view[syyx_view["ctr_check"] = 5] = "ctr_check";
    })(syyx_view || (syyx_view = {}));
    class native_ad_data {
        constructor() {
            this.order = 0;
        }
    }
    class launch_options {
    }

    class syyx_ctr_manager {
        static load_ctr_config() {
            let cur_super_version = syyx_manager.get_app_version();
            let key = syyx_manager.get_syyx_app_id() + this.__ctr_version;
            let last_ctr_version = localStorage.getItem(key);
            if (cur_super_version != last_ctr_version) {
                localStorage.removeItem(syyx_const.local_ctr_config_data);
                localStorage.setItem(syyx_const.local_ctr_config_version, null);
                localStorage.setItem(key, cur_super_version);
            }
            let ctr_data;
            if (cur_super_version == last_ctr_version && (ctr_data = localStorage.getItem(syyx_const.local_ctr_config_data))) {
                this.ctr_data = JSON.parse(ctr_data);
            }
            let super_version = localStorage.getItem(syyx_const.local_ctr_config_version);
            igc.igc_main.instance.tpf_sdk.getTpfConfig().httpGetconfig(syyx_const.remote_ctr_config_key, super_version, Date.now(), this.on_load_ctr.bind(this));
        }
        static on_load_ctr(ret, key, version, data) {
            if (ret === true) {
                console.log("igc----- syyx_ctr_manager remote config load compelete");
                this.ctr_data = JSON.parse(data);
                localStorage.setItem(syyx_const.local_ctr_config_version, version);
                localStorage.setItem(syyx_const.local_ctr_config_data, data);
                this.__remote_ctr_inited = true;
            }
            else {
                let save_data = localStorage.getItem(syyx_const.local_ctr_config_data);
                if (save_data) {
                    this.__remote_ctr_inited = true;
                }
            }
            console.log("igc----- syyx_ctr_manager multual_push_v5", this.ctr_data);
        }
        static set_ctr_test_compelete() {
            this.ctr_test_compelete = true;
        }
        static check_is_ctr_test_compelete() {
            return this.ctr_test_compelete;
        }
        static show_new_products(call_back) {
            if (this.ctr_test_compelete) {
                console.log("igc----- syyx_ctr_manager test compelete!");
                return;
            }
            if (this.check_is_open()) {
                syyx_manager.load_view(syyx_view.ctr_check, function (view) {
                    view && view.show && view.show(call_back);
                });
            }
        }
        static get_new_products_reward() {
            let _business_config_data = syyx_manager.get_business_config();
            if (_business_config_data && _business_config_data["ctr_test_reward_count"]) {
                return _business_config_data["ctr_test_reward_count"].value[0];
            }
            return 100;
        }
        static check_is_open() {
            if (this.ctr_data && this.ctr_data.open_switch == "1") {
                return true;
            }
            console.log("igc----- syyx_ctr_manager is close");
            return false;
        }
        static get_ctr_data() {
            if (this.check_is_open()) {
                return this.ctr_data;
            }
            return undefined;
        }
    }
    syyx_ctr_manager.__ctr_version = "__ctr_version";
    syyx_ctr_manager.__remote_ctr_inited = false;
    syyx_ctr_manager.ctr_data = null;
    syyx_ctr_manager.ctr_test_compelete = false;
    window["oppoSdk"]["syyx_ctr_manager"] = syyx_ctr_manager;

    class syyx_ui_banner_click_mask extends ui.syyx_ad.ui_banner_click_maskUI {
        constructor() {
            super();
            this.default_width = 1080;
            this.default_height = 200;
            this.param_x = 0;
            this.param_scale = 1;
            this.param_width = 0;
            this.param_height = 0;
            this.is_easy_click = false;
            this.btn_close.on(Laya.Event.CLICK, this, this.on_click_close);
        }
        on_click_adv() {
            syyx_sdk_api.load_view(syyx_view.native_banner, function (view) {
                if (view && view.parent) {
                    view.report_click && view.report_click();
                }
                else {
                    console.log('igc----- syyx_ui_banner_click_mask on_click_adv syyx_ui_banner not show1');
                }
            });
        }
        on_click_close() {
            syyx_sdk_api.load_view(syyx_view.native_banner, function (view) {
                if (view && view.parent) {
                    view.hide && view.hide();
                }
                else {
                    console.log('igc----- syyx_ui_banner_click_mask on_click_adv syyx_ui_banner not show2');
                }
            });
        }
        show(x, scale, width, height, easy_click) {
            if (!this.parent) {
                let order = syyx_sdk_utils.get_largest_zorder() + 1;
                Laya.stage.addChild(this);
                this.zOrder = order;
                this.param_x = x;
                this.param_scale = scale;
                this.param_width = width;
                this.param_height = height;
                this.is_easy_click = true;
                this.on_show();
                this.set_default_pos();
            }
        }
        adjust_mask_height(x, scale, width, height) {
            if (this.parent) {
                this.param_x = x;
                this.param_scale = scale;
                this.param_width = width;
                this.param_height = height;
                this.on_show();
                this.set_default_pos();
            }
        }
        get_change_scale() {
            let _business_config_data = syyx_manager.get_business_config();
            if (_business_config_data && _business_config_data["banner_click_mask_scale"]) {
                return _business_config_data["banner_click_mask_scale"].value[0];
            }
            return 1;
        }
        set_default_pos() {
            this.banner_box.height = this.param_height;
            this.banner_box.width = this.param_width;
            this.height = this.param_height * this.get_change_scale();
            this.width = this.param_width * this.get_change_scale();
            this.scaleX = this.param_scale;
            this.scaleY = this.param_scale;
            let x = this.param_x - (this.width * this.scaleX - this.param_width * this.param_scale) / 2;
            let y = Laya.stage.height - this.height * this.scaleY;
            this.pos(x, y);
        }
        on_hide() {
            this.native_bg.off(Laya.Event.MOUSE_OVER, this, this.on_click_adv);
            this.native_bg.off(Laya.Event.CLICK, this, this.on_click_adv);
        }
        on_show() {
            this.refresh();
            this.native_bg.off(Laya.Event.MOUSE_OVER, this, this.on_click_adv);
            this.native_bg.off(Laya.Event.CLICK, this, this.on_click_adv);
            if (this.is_easy_click) {
                this.native_bg.on(Laya.Event.MOUSE_OVER, this, this.on_click_adv);
            }
            else {
                this.native_bg.on(Laya.Event.CLICK, this, this.on_click_adv);
            }
        }
        refresh() {
            this.native_bg.alpha = 0;
            this.banner_box.alpha = 0;
            let _business_config_data = syyx_manager.get_business_config();
            if (_business_config_data && _business_config_data["banner_click_mask_preview"]) {
                if (_business_config_data["banner_click_mask_preview"].value[0] == 1) {
                    this.native_bg.alpha = 0.5;
                    this.banner_box.alpha = 0.5;
                }
            }
        }
        hide() {
            if (this.parent) {
                this.removeSelf();
                this.on_hide();
            }
        }
    }
    window["oppoSdk"]["syyx_ui_banner_click_mask"] = syyx_ui_banner_click_mask;

    class ad_wx_banner {
        static run_timer() {
            let self = this;
            if (!this.is_run_timer) {
                this.is_run_timer = true;
                this._business_config_data = syyx_manager.get_business_config();
                self.load_normal_banner();
                setInterval(() => {
                    if (self.banner_showing) {
                        self.banner_show_time++;
                    }
                    if (self.banner_show_time > 0 && self.banner_show_time % self.update_cd == 0) {
                        console.log("igc----- update_native_banner");
                        self.load_normal_banner();
                    }
                }, 1000);
            }
            else {
                this.show_normal_banner();
            }
        }
        static show_banner(ad_type, ad_pos_id, onLoad, onShow, onClose, onError) {
            if (!this.need_show) {
                this.need_show = true;
            }
            this._normal_banner_id = ad_pos_id;
            this._ad_param = {
                ad_type: ad_type,
                ad_pos_id: ad_pos_id,
                onLoad: onLoad,
                onShow: onShow,
                onClose: onClose,
                onError: onError,
            };
            let ad_id = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- ad_wx_banner normal_banner_id no configure in adv.csv");
                return;
            }
            this.run_timer();
        }
        static hide_banner() {
            this.need_show = false;
            this.banner_showing = false;
            this.hide_normal_banner();
        }
        static load_normal_banner() {
            let self = this;
            this.banner_show_time = 0;
            this.banner_timer_id && clearTimeout(this.banner_timer_id);
            this.update_cd = syyx_adv_manager.get_oppo_banner_show_update_time();
            if (this._business_config_data && this._business_config_data["show_normal_banner_switch"]) {
                if (this._business_config_data["show_normal_banner_switch"].value[0] == 0) {
                    console.log("igc ----- normal banner switch is close");
                    return;
                }
            }
            syyx_manager.create_ad(igc.e_ad_type.banner, self._normal_banner_id, function onLoad(param, res) {
                console.log("igc------ad_wx_banner load_normal_banner onLoad");
                self._ad_param.onLoad && self._ad_param.onLoad();
            }, function onShow() {
                console.log("igc------ad_wx_banner load_normal_banner onShow");
                self.banner_showing = self.need_show;
                if (self.need_show) {
                    self._ad_param.onShow && self._ad_param.onShow();
                }
                else {
                    self.hide_normal_banner();
                }
            }, function onClose(param, res) {
                self._ad_param.onClose && self._ad_param.onClose();
            }, function onError(param, err) {
                console.log("igc------ad_wx_banner load_normal_banner onError", err);
                self.banner_showing = false;
                self._ad_param.onError && self._ad_param.onError(param, err);
            });
        }
        static show_normal_banner() {
            let self = this;
            if (this.banner_showing) {
                console.log("igc----- ad_wx_banner show_normal_banner banner is showing");
                return;
            }
            if (this._business_config_data && this._business_config_data["show_normal_banner_switch"]) {
                if (this._business_config_data["show_normal_banner_switch"].value[0] == 0) {
                    console.log("igc ----- normal banner switch is close");
                    return;
                }
            }
            syyx_manager.show_ad(igc.e_ad_type.banner, self._normal_banner_id, function onLoad(param, res) {
                console.log("igc------ad_wx_banner show_normal_banner onLoad");
            }, function onShow() {
                console.log("igc------ad_wx_banner show_normal_banner onShow");
            }, function onClose(param, res) {
                console.log("igc------ad_wx_banner show_normal_banner onClose");
            }, function onError(param, err) {
                console.error("igc------ad_wx_banner show_normal_banner onError", err);
            });
        }
        static set_show_error_model() {
            let self = this;
            this.banner_timer_id && clearTimeout(this.banner_timer_id);
            this.banner_timer_id = setTimeout(function () {
                if (self.need_show && !self.banner_showing) {
                    self.load_normal_banner();
                }
            }, this.update_cd * 1000);
        }
        static hide_normal_banner() {
            let self = this;
            if (self._normal_banner_id) {
                self.banner_showing = false;
                syyx_manager.hide_ad(igc.e_ad_type.banner, self._normal_banner_id);
            }
        }
        static show_center_banner() {
        }
        static hide_center_banner() {
        }
    }
    ad_wx_banner._business_config_data = {};
    ad_wx_banner._normal_banner_id = undefined;
    ad_wx_banner.update_cd = 5;
    ad_wx_banner.is_run_timer = false;
    ad_wx_banner.need_show = true;
    ad_wx_banner.banner_showing = false;
    ad_wx_banner.banner_show_time = 0;
    window["oppoSdk"]["ad_wx_banner"] = ad_wx_banner;

    class ad_banner {
        static run_timer() {
            if (!this.is_run_timer) {
                this.is_run_timer = true;
                this.timer_func();
            }
        }
        static timer_func() {
            let self = this;
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["banner_cool_time"]) {
                this.auto_update_cd = this._business_config_data["banner_cool_time"].value;
            }
            this.load_normal_banner();
            let cd = syyx_sdk_utils.get_random_number(this.auto_update_cd);
            console.log("igc------ next time to refresh banner is", cd);
            this.timer_id && clearTimeout(this.timer_id);
            this.timer_id = setTimeout(() => {
                self.timer_func();
            }, cd * 1000);
        }
        static show_banner(ad_type, ad_pos_id, onLoad, onShow, onClose, onError) {
            this.need_show = true;
            this._normal_banner_id = ad_pos_id;
            this._native_banner_id = syyx_adv_manager._adv_config_data[ad_pos_id].backup_id;
            this._ad_param = {
                ad_type: ad_type,
                ad_pos_id: ad_pos_id,
                onLoad: onLoad,
                onShow: onShow,
                onClose: onClose,
                onError: onError,
            };
            if (!syyx_adv_manager.can_show_first_native || syyx_adv_manager.limit_show_banner) {
                console.log("igc----- banner is in cooling time ");
                return;
            }
            if (syyx_adv_manager.check_is_open_oppo_rule()) {
                ad_oppo_banner.show_banner(ad_type, ad_pos_id, onLoad, onShow, onClose, onError);
                return;
            }
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.wx) {
                ad_wx_banner.show_banner(ad_type, ad_pos_id, onLoad, onShow, onClose, onError);
                return;
            }
            let ad_id = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- ad_banner normal_banner_id no configure in adv.csv");
                return;
            }
            this.run_timer();
        }
        static auto_show_banner() {
            if (!this.need_show) {
                return;
            }
            if (this._ad_param && this._ad_param.ad_type && this._ad_param.ad_pos_id) {
                this.show_banner(this._ad_param.ad_type, this._ad_param.ad_pos_id, this._ad_param.onLoad, this._ad_param.onShow, this._ad_param.onClose, this._ad_param.onError);
            }
            else {
                this.show_banner(igc.e_ad_type.banner, e_ad_id.banner_hall, undefined, undefined, undefined, undefined);
            }
        }
        static hide_banner() {
            this.need_show = false;
            if (syyx_adv_manager.check_is_open_oppo_rule()) {
                ad_oppo_banner.hide_banner();
                return;
            }
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.wx) {
                ad_wx_banner.hide_banner();
                return;
            }
            if (!syyx_adv_manager.can_show_first_native || syyx_adv_manager.limit_show_banner) {
                console.log("igc----- banner is in cooling time ");
                return;
            }
            this.is_run_timer = false;
            this.hide_normal_banner();
            this.timer_id && clearTimeout(this.timer_id);
            this.timer_id = undefined;
        }
        static load_normal_banner() {
            let self = this;
            if (!this.need_show) {
                self.hide_banner();
                return;
            }
            if (!self._native_banner_id || !syyx_adv_manager.is_oppo_vivo()) {
                self.show_normal_banner();
                return;
            }
        }
        static report_ad_click(ad_pos_id, native_data) {
            if (syyx_adv_manager.check_is_open_oppo_rule()) {
                ad_oppo_banner.report_ad_click(ad_pos_id, native_data);
                return;
            }
        }
        static report_ad_show(ad_pos_id, native_data) {
            if (syyx_adv_manager.check_is_open_oppo_rule()) {
                ad_oppo_banner.report_ad_show(ad_pos_id, native_data);
                return;
            }
        }
        static get_easy_click_protect_count() {
            let protect_count = 0;
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["native_banner_click_protect"]) {
                protect_count = this._business_config_data["native_banner_click_protect"].value[0];
            }
            return protect_count;
        }
        static set_banenr_protect_model() {
            this.cur_protect_count = 0;
        }
        static get_is_easy_click_model() {
            let protect_count = this.get_easy_click_protect_count();
            if (protect_count > 0) {
                if (this.cur_protect_count >= 0) {
                    this.cur_protect_count++;
                    if (this.cur_protect_count <= protect_count) {
                        return false;
                    }
                }
            }
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["native_banner_click_switch"]) {
                if (this._business_config_data["native_banner_click_switch"].value[0] == 1) {
                    let pro = this._business_config_data["native_banner_click_pro"].value[0];
                    return Math.random() <= pro;
                }
            }
            return false;
        }
        static set_normal_banner_switch(value) {
            if (syyx_adv_manager.check_is_open_oppo_rule()) {
                ad_oppo_banner.set_normal_banner_switch(value);
            }
        }
        static show_normal_banner() {
            if (this._business_config_data && this._business_config_data["show_normal_banner_switch"]) {
                if (this._business_config_data["show_normal_banner_switch"].value[0] == 0) {
                    console.log("igc ----- normal banner switch is close");
                    return;
                }
            }
            if (!this.can_show_vivo_banner && syyx_const.syyx_sdk_channel === igc.e_channel_type.vivo_qg) {
                console.log("igc----- vivo ad_banner show_normal_banner create too often!!!");
                return;
            }
            let self = this;
            syyx_manager.create_ad(igc.e_ad_type.banner, self._normal_banner_id, function onLoad(param, res) {
                self._ad_param.onLoad && self._ad_param.onLoad();
            }, function onShow() {
                self.normal_banner_showing = true;
                if (self.need_show) {
                    self._ad_param.onShow && self._ad_param.onShow();
                }
                else {
                    self.hide_normal_banner();
                }
            }, function onClose(param, res) {
                self._ad_param.onClose && self._ad_param.onClose();
            }, function onError(param, err) {
                console.log("igc------syyx_adv_manager show_normal_banner onError", err);
                self._ad_param.onError && self._ad_param.onError(param, err);
            });
        }
        static hide_normal_banner() {
            let self = this;
            if (self._normal_banner_id) {
                if (self.normal_banner_showing && syyx_const.syyx_sdk_channel === igc.e_channel_type.vivo_qg) {
                    self.can_show_vivo_banner = false;
                    this.banner_timer_id && clearTimeout(this.banner_timer_id);
                    this.banner_timer_id = setTimeout(() => {
                        self.can_show_vivo_banner = true;
                    }, 11000);
                }
                self.normal_banner_showing = false;
                syyx_manager.destroy_ad(igc.e_ad_type.banner, self._normal_banner_id);
            }
        }
    }
    ad_banner._business_config_data = {};
    ad_banner._native_banner_id = undefined;
    ad_banner._normal_banner_id = undefined;
    ad_banner.auto_update_cd = [20, 20];
    ad_banner.is_run_timer = false;
    ad_banner.need_show = true;
    ad_banner.normal_banner_showing = false;
    ad_banner.can_show_vivo_banner = true;
    ad_banner.cur_protect_count = -1;
    window["oppoSdk"]["ad_banner"] = ad_banner;

    class syyx_ui_banner extends ui.syyx_ad.ui_bannerUI {
        constructor() {
            super();
            this.easy_click_model = false;
            this.is_set_style = false;
            this.show_count = 0;
            this.next_change_height_count = -1;
            this.next_change_scale_count = -1;
            this.banner_click_mask = new syyx_ui_banner_click_mask();
            this.timer_id = undefined;
            this.is_heighting = false;
            this.icon_close.on(Laya.Event.CLICK, this, this.on_click_close);
            this.set_background_on_show();
            this.set_default_pos();
        }
        on_click_adv2() {
            ad_banner.set_banenr_protect_model();
            this.report_click();
        }
        on_click_adv(evt) {
            this.report_click();
        }
        on_click_close() {
            this.hide();
            syyx_adv_manager.run_finger_close_banner_rule();
        }
        report_click() {
            if (this.native_data) {
                syyx_sdk_api.send_other_event(e_ad_id.native_banner, igc.igc_stat_ids.native_banner_click, this.native_data.native_type);
                syyx_adv_manager.report_ad_click(e_ad_id.native_banner, this.native_data);
            }
        }
        report_show() {
            if (this.native_data) {
                syyx_adv_manager.report_ad_show(e_ad_id.native_banner, this.native_data);
            }
        }
        show(native_data) {
            if (!this.parent) {
                let order = syyx_sdk_utils.get_largest_zorder() + 1;
                Laya.stage.addChild(this);
                this.zOrder = order;
                this.on_show(native_data);
            }
        }
        set_default_pos() {
            let size = syyx_sdk_utils.get_size(this);
            if (!this.is_set_style) {
                let x = (Laya.stage.width - size.width) / 2;
                this.x = x;
            }
            let y = Laya.stage.height - size.height;
            this.y = y;
            this.banner_click_mask.adjust_mask_height(this.x, this.scaleX, this.width, this.height);
            this.text_box.x = this.icon.x + this.icon.width * this.icon.scaleX / 2 + 40;
        }
        set_banner_height(is_auto = true) {
            if (this.parent) {
                let self = this;
                let _business_config_data = syyx_manager.get_business_config();
                if (_business_config_data && _business_config_data["native_banner_height_open_rule"]) {
                    let rule = _business_config_data["native_banner_height_open_rule"].value;
                    if (rule) {
                        if (this.next_change_height_count == -1) {
                            this.next_change_height_count = rule[0];
                        }
                        if (this.show_count == this.next_change_height_count || !is_auto) {
                            if (!is_auto) {
                                console.log("igc----- 强制banner变高");
                                this.next_change_height_count = this.show_count;
                            }
                            this.next_change_height_count += rule[1] + Math.floor(Math.random() * (rule[2] - rule[1] + 1));
                            console.log("igc----- 下一次banner自动变高的展示次数：", this.next_change_height_count);
                            let height_rule = _business_config_data["native_banner_height_rule"].value;
                            if (height_rule) {
                                let origin_h = this.height;
                                this.height = this.get_screen_adaptation_height(height_rule[1]);
                                let o_icon_scaleX = this.icon.scaleX;
                                let o_icon_scaleY = this.icon.scaleY;
                                this.scale_icon(origin_h);
                                this.set_default_pos();
                                let o_desc_width = this.desc.width;
                                if ((this.text_box.x + this.desc.width) > this.btn_click_check.x) {
                                    this.desc.width = this.btn_click_check.x - this.text_box.x - 2;
                                }
                                this.timer_id && clearTimeout(this.timer_id);
                                this.is_heighting = true;
                                this.timer_id = setTimeout(function () {
                                    self.is_heighting = false;
                                    self.height = self.get_screen_adaptation_height(height_rule[0]);
                                    self.icon.scaleY = o_icon_scaleY;
                                    self.icon.scaleX = o_icon_scaleX;
                                    self.set_default_pos();
                                    self.desc.width = o_desc_width;
                                    console.log("igc----- banner高度恢复");
                                }, height_rule[2] * 1000);
                            }
                        }
                    }
                }
            }
        }
        scale_icon(origin_h) {
            let rate = this.height / origin_h;
            let h_tag = this.icon.height * rate * this.icon.scaleY + 2 * Math.abs(this.icon.centerY) <= this.height * this.scaleY;
            let w_tag = this.icon.width * rate * this.icon.scaleX / 2 <= this.icon.x;
            if (h_tag && w_tag) {
                this.icon.scaleY *= rate;
                this.icon.scaleX *= rate;
            }
            else if (h_tag && !w_tag) {
                let r_rate = this.icon.x * 2 / this.icon.width / this.icon.scaleX;
                this.icon.scaleY *= r_rate;
                this.icon.scaleX *= r_rate;
            }
            else if (!h_tag && w_tag) {
                let r_rate = (this.height - 2 * Math.abs(this.icon.centerY)) / this.icon.height / this.icon.scaleY;
                this.icon.scaleY *= r_rate;
                this.icon.scaleX *= r_rate;
            }
            else {
                let h_rate = (this.height - 2 * Math.abs(this.icon.centerY)) / this.icon.height / this.icon.scaleY;
                let w_rate = this.icon.x * 2 / this.icon.width / this.icon.scaleX;
                let r_rate = (h_rate > w_rate) ? w_rate : h_rate;
                this.icon.scaleY *= r_rate;
                this.icon.scaleX *= r_rate;
            }
        }
        get_banner_default_height() {
            let _business_config_data = syyx_manager.get_business_config();
            if (_business_config_data && _business_config_data["native_banner_height_rule"]) {
                let height_rule = _business_config_data["native_banner_height_rule"].value;
                if (height_rule) {
                    return this.get_screen_adaptation_height(height_rule[0]);
                }
            }
            return 200;
        }
        resume_pos_and_scale() {
            this.is_set_style = false;
            this.scaleX = syyx_sdk_api.get_screen_ratio();
            this.scaleY = syyx_sdk_api.get_screen_ratio();
            this.set_default_pos();
        }
        set_pos_and_scale(x, y, scaleX = undefined, scaleY = undefined) {
            this.is_set_style = true;
            if (scaleX > -9999999) {
                this.scaleX = scaleX;
                this.scaleY = scaleY;
            }
            if (x > -9999999) {
                this.x = x;
            }
        }
        get_screen_adaptation_height(height) {
            if (Laya.stage.height > Laya.stage.width) {
                return (Laya.stage.height / this.scaleY) * height / 1920;
            }
            return height;
        }
        on_hide() {
            if (this.easy_click_model) {
                this.native_bg.off(Laya.Event.MOUSE_OVER, this, this.on_click_adv2);
            }
            else {
                this.native_bg.off(Laya.Event.CLICK, this, this.on_click_adv);
            }
            this.banner_click_mask.hide();
        }
        on_show(native_data) {
            this.show_count++;
            this.easy_click_model = ad_banner.get_is_easy_click_model();
            if (this.easy_click_model) {
                this.native_bg.on(Laya.Event.MOUSE_OVER, this, this.on_click_adv2);
            }
            else {
                this.native_bg.on(Laya.Event.CLICK, this, this.on_click_adv);
            }
            if (native_data) {
                this.native_data = native_data;
            }
            else {
                this.native_data = syyx_adv_manager.get_local_native_data(ad_banner._native_banner_id);
            }
            this.refresh();
        }
        show_click_mask() {
            let _business_config_data = syyx_manager.get_business_config();
            if (_business_config_data && _business_config_data["banner_click_mask_open_rule"]) {
                let rule = _business_config_data["banner_click_mask_open_rule"].value;
                if (rule) {
                    if (this.next_change_scale_count == -1) {
                        this.next_change_scale_count = rule[0];
                    }
                    if (this.show_count == this.next_change_scale_count) {
                        this.next_change_scale_count += rule[1] + Math.floor(Math.random() * (rule[2] - rule[1] + 1));
                        this.banner_click_mask.show(this.x, this.scaleX, this.width, this.height, this.easy_click_model);
                    }
                }
            }
        }
        refresh() {
            let node = this.box_big_banner;
            let icon = node.getChildByName("icon");
            let title = node.getChildByName("text_box").getChildByName("title");
            let desc = node.getChildByName("text_box").getChildByName("desc");
            icon.skin = this.native_data.imgUrlList;
            title.text = this.native_data.title;
            desc.text = this.native_data.desc;
            if (!this.is_heighting) {
                this.height = this.get_banner_default_height();
                this.set_default_pos();
            }
            this.report_show();
            this.set_banner_height();
            this.show_click_mask();
        }
        hide() {
            if (this.parent) {
                this.removeSelf();
                this.on_hide();
            }
        }
        set_background_on_show() {
            let self = this;
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.hw_qg) {
                syyx_sdk_api.on_show(function () {
                    if (self && !self.destroyed) {
                        console.log("igc----- native_banner set_background_on_show");
                        self.report_show();
                    }
                });
            }
        }
    }
    window["oppoSdk"]["syyx_ui_banner"] = syyx_ui_banner;

    class ad_native_interstitial {
        static check_is_click_wrap() {
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["native_interstitial_click_wrap"]) {
                let rule = syyx_sdk_api.get_business_data_by_key('native_interstitial_click_wrap');
                if (rule) {
                    if (this.next_click_wrap_count == -1) {
                        this.next_click_wrap_count = rule[0];
                    }
                    if (this.show_count == this.next_click_wrap_count) {
                        console.log("igc----- native_interstitial is easy click!");
                        this.next_click_wrap_count += rule[1] + Math.floor(Math.random() * (rule[2] - rule[1] + 1));
                        console.log("igc----- native_interstitial next easy click count：", this.next_click_wrap_count);
                        return true;
                    }
                }
                else {
                    return false;
                }
            }
            return false;
        }
        static report_ad_click(ad_pos_id, native_data) {
            if (!this._ad_pos_id) {
                console.log("igc----- ad_native_interstitial  report_ad_click this.ad_pos_id is null!!");
                this._ad_pos_id = e_ad_id.native_interstitial_hall;
            }
            if (this._ad_pos_id != ad_pos_id) {
                return;
            }
            this.hide_native_interstitial_ui();
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["native_interstitial_report_click_update_switch"]) {
                if (this._business_config_data["native_interstitial_report_click_update_switch"].value[0] == 1) {
                    this.load_native_interstitial(this._ad_param.ad_type, this._ad_param.ad_pos_id, this._ad_param.onLoad, this._ad_param.onShow, this._ad_param.onClose, this._ad_param.onError);
                }
            }
        }
        static report_ad_show(ad_pos_id, native_data) {
            if (!this._ad_pos_id) {
                console.log("igc----- ad_native_interstitial  report_ad_click this.ad_pos_id is null!!");
                this._ad_pos_id = e_ad_id.native_interstitial_hall;
            }
            if (this._ad_pos_id == ad_pos_id) {
                this._last_ad_id = native_data.id;
                ad_banner.hide_banner();
            }
        }
        static check_can_load_native_interstitial() {
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["load_native_interstitial_rule"]) {
                if ((this.show_count + 1) % this._business_config_data["load_native_interstitial_rule"].value[0] == 0) {
                    return true;
                }
                else {
                    return false;
                }
            }
            return true;
        }
        static load_native_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError) {
            let self = this;
            if (!syyx_adv_manager.can_show_first_native) {
                console.log("igc----- is in oppo first ad cd");
                return;
            }
            if (syyx_adv_manager.check_is_click_limit(e_ad_native_type.native_interstitial)) {
                this.hide_native_interstitial_ui();
                return;
            }
            this._ad_pos_id = ad_pos_id;
            this._ad_param = {
                ad_type: ad_type,
                ad_pos_id: ad_pos_id,
                onLoad: onLoad,
                onShow: onShow,
                onClose: onClose,
                onError: onError,
            };
            let ad_id = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- ad_native_interstitial native_interstitial_id no configure in adv.csv");
                return;
            }
            if (!this.check_can_load_native_interstitial()) {
                console.log("igc-----syyx_adv_manager------ limit load_native_interstitial!!!");
                console.error("igc-----syyx_adv_manager-------load_native_interstitial onError");
                onError && onError();
                self.load_native_interstitial_error();
                return;
            }
            let latest_data = syyx_adv_manager.get_latest_native_data(this._native_data_list);
            if (latest_data && latest_data.state == e_ad_native_state.need_show) {
                self.show_native_interstitial_ui(latest_data);
                return;
            }
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.web) {
                let data = new native_ad_data();
                data.id = igc.utils_manager.get_random_name();
                data.adPosId = ad_pos_id;
                data.adId = "1";
                data.adUnitId = syyx_sdk_utils.get_random_number([0, 100000]);
                data.imgUrlList = "https://h5-lg.syyx.com/coolbattle/share/share_img.jpg";
                data.title = "原生插屏标题" + syyx_sdk_utils.get_random_number([200, 300]);
                data.desc = "原生插屏描述" + syyx_sdk_utils.get_random_number([200, 300]);
                data.state = e_ad_native_state.need_show;
                data.native_type = e_ad_native_type.native_interstitial;
                this.add_native_data(data);
                this.show_native_interstitial_ui(data);
                return;
            }
            syyx_manager.create_ad(igc.e_ad_type.native, ad_pos_id, function on_load(param, ad_data_list) {
                console.log("igc-----syyx_adv_manager-------load_native_interstitial on_load", ad_data_list);
                if (ad_data_list == undefined || !ad_data_list[0]) {
                    onError && onError();
                }
                else {
                    let length = 0;
                    if (syyx_const.syyx_sdk_channel === igc.e_channel_type.vivo_qg) {
                        length = ad_data_list.length - 1;
                    }
                    let data = new native_ad_data();
                    data.id = igc.utils_manager.get_random_name();
                    data.adPosId = ad_pos_id;
                    data.adId = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
                    data.adUnitId = ad_data_list[length].adUnitId;
                    data.imgUrlList = syyx_sdk_utils.format_remote_texture_url(ad_data_list[length].imgUrlList[0]);
                    data.title = ad_data_list[length].title;
                    data.desc = ad_data_list[length].desc;
                    data.state = e_ad_native_state.need_show;
                    data.native_type = e_ad_native_type.native_interstitial;
                    self.add_native_data(data);
                    self.show_native_interstitial_ui(data);
                }
            }, function on_show() {
            }, function on_close(param, res) {
            }, function on_error(param, err) {
                onError && onError();
                console.error("igc-----syyx_adv_manager-------load_native_interstitial onError", err);
                self.load_native_interstitial_error();
            });
        }
        static load_native_interstitial_error() {
            let native_data = undefined;
            if (this.check_can_load_native_interstitial()) {
                native_data = syyx_adv_manager.get_native_data(this._last_ad_id);
            }
            else {
                native_data = this.get_native_data(this._last_ad_id, e_ad_native_type.native_interstitial);
            }
            if (native_data) {
                this.show_native_interstitial_ui(native_data);
            }
            else {
                this.show_normal_interstitial();
            }
        }
        static load_normal_interstitial_error() {
            let native_data = undefined;
            if (this.check_can_load_native_interstitial()) {
                native_data = syyx_adv_manager.get_native_data();
            }
            else {
                native_data = this.get_native_data(undefined, e_ad_native_type.native_interstitial);
            }
            if (native_data) {
                this.show_native_interstitial_ui(native_data);
            }
        }
        static show_normal_interstitial() {
            let self = this;
            this._normal_ad_pos_id = syyx_adv_manager._adv_config_data[e_ad_id.native_interstitial_hall].backup_id || undefined;
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.vivo_qg && this._normal_ad_pos_id) {
                syyx_manager.create_ad(igc.e_ad_type.interstitial, this._normal_ad_pos_id, function () {
                    self._ad_param && self._ad_param.onLoad && self._ad_param.onLoad();
                    ad_banner.hide_banner();
                }, function () {
                    self._ad_param && self._ad_param.onShow && self._ad_param.onShow();
                }, function () {
                    self._ad_param && self._ad_param.onClose && self._ad_param.onClose();
                }, function () {
                    self.load_normal_interstitial_error();
                });
            }
        }
        static show_native_interstitial_ui(native_data) {
            let self = this;
            syyx_sdk_api.create_interstitial(function (view) {
                view.show && view.show(native_data, self._ad_param);
            });
        }
        static hide_native_interstitial_ui() {
            syyx_sdk_api.load_view(syyx_view.interstitial, function (view) {
                view.hide && view.hide();
            });
        }
        static add_native_data(native_data) {
            if (syyx_adv_manager.check_is_open_oppo_rule()) {
                syyx_adv_manager.add_native_data(native_data);
            }
            else {
                this._native_data_list[0] = native_data;
            }
        }
        static get_native_data(ignore_id = undefined, ignore_type = undefined) {
            if (!syyx_adv_manager.can_show_first_native) {
                console.log("igc ----- oppo's first native ad is in cd");
                return undefined;
            }
            let banner_limit = syyx_adv_manager.check_is_click_limit(e_ad_native_type.native_banner);
            let inner_limit = syyx_adv_manager.check_is_click_limit(e_ad_native_type.native_inner_interstitial);
            let interstitial_limit = syyx_adv_manager.check_is_click_limit(e_ad_native_type.native_interstitial);
            let cur_data_cache = [];
            for (let i in syyx_adv_manager._native_data_cache) {
                if (syyx_adv_manager._native_data_cache[i].id != ignore_id && syyx_adv_manager._native_data_cache[i].native_type != ignore_type) {
                    if (syyx_adv_manager._native_data_cache[i].native_type == e_ad_native_type.native_banner && !banner_limit) {
                        cur_data_cache.push(syyx_adv_manager._native_data_cache[i]);
                    }
                    else if (syyx_adv_manager._native_data_cache[i].native_type == e_ad_native_type.native_inner_interstitial && !inner_limit) {
                        cur_data_cache.push(syyx_adv_manager._native_data_cache[i]);
                    }
                    else if (syyx_adv_manager._native_data_cache[i].native_type == e_ad_native_type.native_interstitial && !interstitial_limit) {
                        cur_data_cache.push(syyx_adv_manager._native_data_cache[i]);
                    }
                }
            }
            let banner_list = [];
            let inner_list = [];
            for (let i in cur_data_cache) {
                if (cur_data_cache[i].native_type == e_ad_native_type.native_banner) {
                    banner_list.push(cur_data_cache[i]);
                }
                else if (cur_data_cache[i].native_type == e_ad_native_type.native_inner_interstitial) {
                    inner_list.push(cur_data_cache[i]);
                }
            }
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["first_use_natibe_banner"]) {
                if (this._business_config_data["first_use_natibe_banner"].value[0] == 1) {
                    if (banner_list.length > 0) {
                        return this.get_cur_data(banner_list);
                    }
                    else {
                        if (inner_list.length > 0) {
                            return this.get_cur_data(inner_list);
                        }
                    }
                }
                else {
                    if (inner_list.length > 0) {
                        return this.get_cur_data(inner_list);
                    }
                    else {
                        if (banner_list.length > 0) {
                            return this.get_cur_data(banner_list);
                        }
                    }
                }
            }
            return undefined;
        }
        static get_cur_data(cur_data_cache) {
            if (syyx_adv_manager.check_native_data_list_is_reprot(cur_data_cache)) {
                console.log("igc----- syyx_adv_manager use old load native data");
                return syyx_adv_manager.get_min_order_native_data(cur_data_cache);
            }
            else {
                console.log("igc----- syyx_adv_manager use new load native data");
                return syyx_adv_manager.get_latest_native_data(cur_data_cache);
            }
        }
    }
    ad_native_interstitial._last_ad_id = undefined;
    ad_native_interstitial._native_data_list = [];
    ad_native_interstitial._business_config_data = {};
    ad_native_interstitial.show_count = 0;
    ad_native_interstitial.next_click_wrap_count = -1;
    window["oppoSdk"]["ad_native_interstitial"] = ad_native_interstitial;

    class syyx_ui_interstitial extends ui.syyx_ad.ui_interstitialUI {
        constructor() {
            super();
            this.ad_param = undefined;
            this.box_close.on(Laya.Event.CLICK, this, this.on_click_close);
            this.btn_click_button.on(Laya.Event.CLICK, this, this.on_click_adv);
            this.icon_video.on(Laya.Event.CLICK, this, this.on_click_adv);
            this.click_box.on(Laya.Event.CLICK, this, this.on_click_adv2);
            this.set_background_on_show();
        }
        on_click_adv2() {
            if (this.easy_click || ad_native_interstitial.check_is_click_wrap()) {
                this.report_click();
            }
        }
        on_click_adv(evt) {
            this.report_click();
        }
        on_click_close() {
            if (ad_native_interstitial.check_is_click_wrap()) {
                this.report_click();
                return;
            }
            let _business_config_data = syyx_manager.get_business_config();
            if (_business_config_data && _business_config_data["native_institial_click_close_pro"]) {
                let trap_pro = _business_config_data["native_institial_click_close_pro"].value[0];
                if (trap_pro >= 0 && Math.random() <= trap_pro) {
                    this.report_click();
                }
            }
            this.hide();
        }
        report_click() {
            if (this.native_data && this.native_data.adPosId) {
                syyx_sdk_api.send_other_event(e_ad_id.native_interstitial_hall, igc.igc_stat_ids.native_interstitial_click, this.native_data.native_type);
                syyx_adv_manager.report_ad_click(e_ad_id.native_interstitial_hall, this.native_data);
            }
        }
        report_show() {
            if (this.native_data && this.native_data.adPosId) {
                syyx_adv_manager.report_ad_show(e_ad_id.native_interstitial_hall, this.native_data);
            }
        }
        show(native_data, ad_param) {
            if (!this.parent) {
                this.native_data = native_data;
                this.ad_param = ad_param;
                let order = syyx_sdk_utils.get_largest_zorder();
                Laya.stage.addChild(this);
                this.zOrder = order;
                this.on_show();
            }
        }
        set_default_pos() {
        }
        set_style_pos(x, y) {
        }
        on_hide() {
            this.ad_param && this.ad_param.onClose && this.ad_param.onClose();
        }
        on_show() {
            this.refresh();
            ad_native_interstitial.show_count++;
            this.easy_click = false;
            let _business_config_data = syyx_manager.get_business_config();
            if (_business_config_data && _business_config_data["native_institial_white_easy_click"]) {
                let easy_click_pro = _business_config_data["native_institial_white_easy_click"].value[0];
                if (easy_click_pro > 0 && Math.random() <= easy_click_pro) {
                    this.easy_click = true;
                }
            }
            this.ad_param && this.ad_param.onLoad && this.ad_param.onLoad();
            this.ad_param && this.ad_param.onShow && this.ad_param.onShow();
        }
        refresh() {
            this.icon_video.skin = this.native_data.imgUrlList;
            this.txt_title.text = this.native_data.title;
            this.text_desc.text = this.native_data.desc;
            this.report_show();
        }
        hide() {
            if (this.parent) {
                this.removeSelf();
                this.on_hide();
            }
        }
        set_background_on_show() {
            let self = this;
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.hw_qg) {
                syyx_sdk_api.on_show(function () {
                    if (self && !self.destroyed) {
                        console.log("igc----- native_interstitial set_background_on_show");
                        self.report_show();
                    }
                });
            }
        }
    }
    window["oppoSdk"]["syyx_ui_interstitial"] = syyx_ui_interstitial;

    class ad_native_inner_interstitial {
        static auto_preload_native_inner_interstitial() {
            let self = this;
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["native_inner_interstitial_auto_preload_rule"]) {
                this._can_auto_preload = this._business_config_data["native_inner_interstitial_auto_preload_rule"].value[0];
                let cd = this._business_config_data["native_inner_interstitial_auto_preload_rule"].value[1];
                if (this._can_auto_preload) {
                    let call_back = () => {
                        console.log("igc----- ad_native_inner_interstitial auto preload");
                        self.preload_native_inner_interstitial(igc.e_ad_type.native, e_ad_id.native_inner_interstitial_success, undefined, undefined, undefined, undefined);
                    };
                    if (!this._auto_preload_timer_id) {
                        call_back && call_back();
                    }
                    this._auto_preload_timer_id = setTimeout(() => {
                        if (!self._is_showing) {
                            call_back && call_back();
                        }
                        self.auto_preload_native_inner_interstitial();
                    }, cd * 1000);
                }
                else {
                    this._auto_preload_timer_id && clearTimeout(this._auto_preload_timer_id);
                }
            }
        }
        static report_ad_click(ad_pos_id, native_data) {
            let self = this;
            if (!this._ad_pos_id) {
                console.log("igc----- ad_native_inner_interstitial  report_ad_click this.ad_pos_id is null!!");
                this._ad_pos_id = e_ad_id.native_inner_interstitial_success;
            }
            if (this._ad_pos_id != ad_pos_id) {
                return;
            }
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.hw_qg) {
                console.log("igc ------ hw hide inner interstitial");
                syyx_manager.hide_native_inner_interstitial();
                return;
            }
            console.log("igc ----- has in inner interstitial's report click ");
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["native_inner_report_click_update_switch"]) {
                if (this._business_config_data["native_inner_report_click_update_switch"].value[0] == 1) {
                    this.preload_native_inner_interstitial(igc.e_ad_type.native, ad_pos_id, function onLoad(args, native_data) {
                        console.log("igc----- ad_native_inner_interstitial click update success");
                        self.update_native_inner_interstitial_ui(native_data);
                    }, function () { }, function () { }, function onError() {
                        let native_data = self.get_native_data();
                        if (native_data) {
                            self.update_native_inner_interstitial_ui(native_data);
                        }
                        else {
                            syyx_manager.hide_native_inner_interstitial();
                        }
                    });
                }
                else {
                    syyx_manager.hide_native_inner_interstitial();
                }
            }
        }
        static report_ad_show(ad_pos_id, native_data) {
            if (!this._ad_pos_id) {
                console.log("igc----- ad_native_inner_interstitial  report_ad_click this.ad_pos_id is null!!");
                this._ad_pos_id = e_ad_id.native_inner_interstitial_success;
            }
            if (this._ad_pos_id == ad_pos_id) {
                this._last_ad_id = native_data.id;
            }
        }
        static preload_native_inner_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError) {
            let self = this;
            this._ad_pos_id = ad_pos_id;
            this._ad_param = {
                ad_type: ad_type,
                ad_pos_id: ad_pos_id,
                onLoad: onLoad,
                onShow: onShow,
                onClose: onClose,
                onError: onError,
            };
            let native_data = this.get_native_data();
            if (syyx_adv_manager.check_is_click_limit(e_ad_native_type.native_inner_interstitial)) {
                if (native_data) {
                    onLoad && onLoad({}, native_data);
                }
                else {
                    onError && onError();
                }
                return;
            }
            if (native_data && native_data.state == e_ad_native_state.need_show) {
                console.log("igc----- ad_native_inner interstitial has native data not report_show");
                onLoad && onLoad({}, native_data);
                return;
            }
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["native_inner_interstitial_switch"]) {
                if (this._business_config_data["native_inner_interstitial_switch"].value[0] == 0) {
                    console.log("igc----- ad_native_inner_interstitial native_inner_interstitial_switch is close!!!");
                    return;
                }
            }
            let ad_id = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- ad_native_inner_interstitial normal_banner_id no configure in adv.csv");
                return;
            }
            let latest_data = syyx_adv_manager.get_latest_native_data(this._native_data_list);
            if (latest_data && latest_data.state == e_ad_native_state.need_show) {
                onLoad && onLoad({}, latest_data);
                return;
            }
            if (syyx_adv_manager.check_is_show_count_limit()) {
                console.log("igc----- ad_native_inner_interstitial preload_native_inner_interstitial is show limit!!");
                if (native_data) {
                    onLoad && onLoad({}, native_data);
                }
                else {
                    syyx_manager.hide_native_inner_interstitial();
                }
                return;
            }
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.web) {
                let data = new native_ad_data();
                data.id = igc.utils_manager.get_random_name();
                data.adPosId = ad_pos_id;
                data.adId = syyx_sdk_utils.get_random_number([100, 200]) + "";
                data.adUnitId = syyx_sdk_utils.get_random_number([0, 100000]) + "";
                data.imgUrlList = "https://h5-lg.syyx.com/coolbattle/share/share_img.jpg";
                data.title = "结算原生标题" + syyx_sdk_utils.get_random_number([100, 200]);
                data.desc = "结算原生描述" + syyx_sdk_utils.get_random_number([100, 200]);
                data.state = e_ad_native_state.need_show;
                data.native_type = e_ad_native_type.native_inner_interstitial;
                this.add_native_data(data);
                onLoad && onLoad({}, data);
                syyx_adv_manager.add_native_show_count();
                this.update_native_inner_interstitial_ui(data);
                console.log("igc-----syyx_adv_manager-------load_native_inner_interstitial on_load", data);
                return;
            }
            syyx_manager.create_ad(igc.e_ad_type.native, ad_pos_id, function on_load(param, ad_data_list) {
                console.log("igc-----syyx_adv_manager-------load_native_inner_interstitial on_load", ad_data_list);
                if (ad_data_list == undefined || !ad_data_list[0]) {
                    onError && onError();
                }
                else {
                    let length = 0;
                    if (syyx_const.syyx_sdk_channel === igc.e_channel_type.vivo_qg) {
                        length = ad_data_list.length - 1;
                    }
                    let data = new native_ad_data();
                    data.id = igc.utils_manager.get_random_name();
                    data.adPosId = ad_pos_id;
                    data.adId = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
                    data.adUnitId = ad_data_list[length].adUnitId;
                    data.imgUrlList = syyx_sdk_utils.format_remote_texture_url(ad_data_list[length].imgUrlList[0]);
                    data.title = ad_data_list[length].title;
                    data.desc = ad_data_list[length].desc;
                    data.state = e_ad_native_state.need_show;
                    data.native_type = e_ad_native_type.native_inner_interstitial;
                    self.add_native_data(data);
                    onLoad && onLoad({}, data);
                    syyx_adv_manager.add_native_show_count();
                    syyx_sdk_utils.preload_native_texture(data.imgUrlList);
                    self.update_native_inner_interstitial_ui(data);
                }
            }, function on_show() {
            }, function on_close(param, res) {
            }, function on_error(param, err) {
                console.error("igc-----syyx_adv_manager-------load_native_inner_interstitial onError", err);
                onError && onError();
            });
        }
        static update_native_inner_interstitial_ui(native_data) {
            syyx_manager.load_view(syyx_view.inner_interstitial, function (view) {
                if (native_data) {
                    view.report_click_update_view(native_data);
                }
            });
        }
        static get_native_data() {
            return syyx_adv_manager.get_native_data();
        }
        static add_native_data(native_data) {
            if (syyx_adv_manager.check_is_open_oppo_rule()) {
                syyx_adv_manager.add_native_data(native_data);
            }
            else {
                this._native_data_list[0] = native_data;
            }
        }
        static set_on_click_inner_interstitial_btn(click_back = undefined) {
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["native_inner_institial_click_wrap"]) {
                let rule = this._business_config_data["native_inner_institial_click_wrap"].value;
                if (rule) {
                    if (this.next_click_wrap_count == -1) {
                        this.next_click_wrap_count = rule[0];
                    }
                    if (this.show_count >= this.next_click_wrap_count) {
                        console.log("igc----- native_inner_interstitial is easy click!");
                        this.next_click_wrap_count = this.show_count + rule[1] + Math.floor(Math.random() * (rule[2] - rule[1] + 1));
                        if (this.next_click_wrap_count < rule[0]) {
                            this.next_click_wrap_count = rule[0];
                        }
                        console.log("igc----- native_inner_interstitial next easy click count：", this.next_click_wrap_count);
                        syyx_manager.click_native_inner_interstitial(click_back);
                        return;
                    }
                }
            }
            console.log("igc----- native_inner_interstitial easy click is close!");
            click_back && click_back();
        }
    }
    ad_native_inner_interstitial._last_ad_id = undefined;
    ad_native_inner_interstitial._native_data_list = [];
    ad_native_inner_interstitial._business_config_data = {};
    ad_native_inner_interstitial._auto_preload_timer_id = undefined;
    ad_native_inner_interstitial._can_auto_preload = false;
    ad_native_inner_interstitial._is_showing = false;
    ad_native_inner_interstitial.next_click_wrap_count = -1;
    ad_native_inner_interstitial.show_count = 0;
    window["oppoSdk"]["ad_native_inner_interstitial"] = ad_native_inner_interstitial;

    class syyx_ui_inner_interstitial extends ui.syyx_ad.ui_inner_interstitialUI {
        constructor() {
            super();
            this.click_back = undefined;
            this.box_close.on(Laya.Event.CLICK, this, this.on_click_close);
            this.icon_video.on(Laya.Event.CLICK, this, this.on_click_adv);
            this.set_background_on_show();
        }
        on_click_adv(evt) {
            this.report_click();
        }
        on_click_close() {
            let _business_config_data = syyx_manager.get_business_config();
            if (_business_config_data && _business_config_data["native_inner_institial_click_close_pro"]) {
                let trap_pro = _business_config_data["native_inner_institial_click_close_pro"].value[0];
                if (trap_pro >= 0 && Math.random() <= trap_pro) {
                    this.report_click();
                }
            }
            this.hide();
        }
        report_click() {
            if (this.native_data) {
                syyx_sdk_api.send_other_event(e_ad_id.native_inner_interstitial_success, igc.igc_stat_ids.native_inner_interstitial_click, this.native_data.native_type);
                this.click_back && this.click_back();
                syyx_manager.report_ad_click(e_ad_id.native_inner_interstitial_success, this.native_data);
                console.log("igc----- has clicked native inner interstitial");
            }
            else {
                console.log("igc----- syyx_ui_inner_interstitial report_click native_data is null!");
            }
        }
        report_show() {
            if (this.native_data) {
                syyx_adv_manager.report_ad_show(e_ad_id.native_inner_interstitial_success, this.native_data);
            }
            else {
                console.log("igc----- syyx_ui_inner_interstitial report_show native_data is null!");
            }
        }
        show(parent, native_data, click_back, show_back, hide_back, is_new_type) {
            if (!this.parent) {
                this.native_data = native_data;
                this.show_back = show_back || undefined;
                this.hide_back = hide_back || undefined;
                this.click_back = click_back || undefined;
                parent.addChild(this);
                this.set_default_pos(parent);
                this.on_show();
            }
        }
        report_click_update_view(native_data) {
            if (this.parent) {
                this.native_data = native_data;
                this.refresh();
            }
        }
        set_default_pos(parent) {
            if (parent) {
                this.x = parent.width / 2 - (this.width * this.scaleX) / 2;
                this.y = parent.height - this.height * this.scaleY - 90;
            }
        }
        set_style_pos(x, y) {
        }
        on_hide() {
            ad_native_inner_interstitial._is_showing = false;
            this.hide_back && this.hide_back();
        }
        on_show() {
            this.refresh();
            ad_native_inner_interstitial._is_showing = true;
            ad_native_inner_interstitial.show_count++;
            this.show_back && this.show_back();
        }
        refresh() {
            this.icon_video.skin = this.native_data.imgUrlList;
            this.txt_title.text = this.native_data.title;
            this.text_desc.text = this.native_data.desc;
            this.report_show();
        }
        hide() {
            if (this.parent) {
                this.removeSelf();
                this.on_hide();
            }
        }
        set_background_on_show() {
            let self = this;
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.hw_qg) {
                syyx_sdk_api.on_show(function () {
                    if (self && !self.destroyed) {
                        console.log("igc----- native_inner_interstitial set_background_on_show");
                        self.report_show();
                    }
                });
            }
        }
    }
    window["oppoSdk"]["syyx_ui_inner_interstitial"] = syyx_ui_inner_interstitial;

    class syyx_ui_native_icon extends ui.syyx_ad.ui_native_iconUI {
        constructor() {
            super();
            this.game_icon.on(Laya.Event.CLICK, this, this.on_click_adv);
            this.btn_close.on(Laya.Event.CLICK, this, this.on_click_close);
            this.set_background_on_show();
        }
        on_click_adv(evt) {
            this.report_click();
        }
        on_click_close() {
            let _business_config_data = syyx_manager.get_business_config();
            if (_business_config_data && _business_config_data["native_icon_trap_pro"]) {
                let trap_pro = _business_config_data["native_icon_trap_pro"].value[0];
                if (trap_pro >= 0 && Math.random() <= trap_pro) {
                    this.report_click();
                }
            }
            this.hide();
        }
        report_click() {
            if (this.native_data) {
                syyx_adv_manager.report_ad_click(e_ad_id.native_icon, this.native_data);
            }
        }
        report_show() {
            if (this.native_data) {
                syyx_adv_manager.report_ad_show(e_ad_id.native_icon, this.native_data);
            }
        }
        show(parent, native_data) {
            if (!this.parent) {
                this.native_data = native_data;
                !this.parent && parent.addChild(this);
                this.set_default_pos(parent);
                this.on_show();
            }
        }
        report_click_update_view(native_data) {
            if (this.parent) {
                this.native_data = native_data;
                this.refresh();
            }
        }
        set_default_pos(parent) {
            if (parent) {
                this.x = parent.width / 2 - (this.width * this.scaleX) / 2;
                this.y = parent.height / 2 - (this.height * this.scaleY) / 2;
            }
        }
        set_style_pos(x, y) {
        }
        on_hide() {
        }
        on_show() {
            this.refresh();
        }
        refresh() {
            this.shake_ani.play(0, false);
            this.game_icon.skin = this.native_data.imgUrlList;
            this.report_show();
        }
        hide() {
            if (this.parent) {
                this.removeSelf();
                this.on_hide();
            }
        }
        set_background_on_show() {
            let self = this;
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.hw_qg) {
                syyx_sdk_api.on_show(function () {
                    if (self && !self.destroyed) {
                        console.log("igc----- native_icon set_background_on_show");
                        self.report_show();
                    }
                });
            }
        }
    }
    window["oppoSdk"]["syyx_ui_native_icon"] = syyx_ui_native_icon;

    class syyx_ui_toast extends ui.syyx_ad.ui_toastUI {
        constructor() {
            super();
        }
        show(desc) {
            if (!this.parent) {
                let order = syyx_sdk_utils.get_largest_zorder();
                Laya.stage.addChild(this);
                this.zOrder = order;
            }
            this.on_show(desc);
        }
        set_default_pos() {
            this.centerX = 0;
        }
        set_style_pos(x, y) {
        }
        on_hide() {
        }
        on_show(desc) {
            this.show_tips(desc);
        }
        show_tips(desc) {
            let self = this;
            this.centerY = 0;
            this.lb_tips.text = desc;
            Laya.Tween.clearAll(this);
            this.tips_box.centerY = 0;
            this.tips_tween = Laya.Tween.to(this, { centerY: -150 }, 1500, null, Laya.Handler.create(this, () => {
                self.hide();
            }));
        }
        hide() {
            if (this.parent) {
                this.removeSelf();
                this.on_hide();
            }
        }
    }
    window["oppoSdk"]["syyx_ui_toast"] = syyx_ui_toast;

    class syyx_ui_interstitial_h extends ui.syyx_ad.ui_interstitial_hUI {
        constructor() {
            super();
            this.ad_param = undefined;
            this.box_close.on(Laya.Event.CLICK, this, this.on_click_close);
            this.btn_click_button.on(Laya.Event.CLICK, this, this.on_click_adv);
            this.icon_video.on(Laya.Event.CLICK, this, this.on_click_adv);
            this.click_box.on(Laya.Event.CLICK, this, this.on_click_adv2);
            this.set_background_on_show();
        }
        on_click_adv2() {
            if (this.easy_click || ad_native_interstitial.check_is_click_wrap()) {
                this.report_click();
            }
        }
        on_click_adv(evt) {
            this.report_click();
        }
        on_click_close() {
            if (ad_native_interstitial.check_is_click_wrap()) {
                this.report_click();
                return;
            }
            let _business_config_data = syyx_manager.get_business_config();
            if (_business_config_data && _business_config_data["native_institial_click_close_pro"]) {
                let trap_pro = _business_config_data["native_institial_click_close_pro"].value[0];
                if (trap_pro >= 0 && Math.random() <= trap_pro) {
                    this.report_click();
                }
            }
            this.hide();
        }
        report_click() {
            if (this.native_data && this.native_data.adPosId) {
                syyx_sdk_api.send_other_event(e_ad_id.native_interstitial_hall, igc.igc_stat_ids.native_interstitial_click, this.native_data.native_type);
                syyx_adv_manager.report_ad_click(e_ad_id.native_interstitial_hall, this.native_data);
            }
        }
        report_show() {
            if (this.native_data && this.native_data.adPosId) {
                syyx_adv_manager.report_ad_show(e_ad_id.native_interstitial_hall, this.native_data);
            }
        }
        show(native_data, ad_param) {
            if (!this.parent) {
                this.native_data = native_data;
                this.ad_param = ad_param;
                let order = syyx_sdk_utils.get_largest_zorder();
                Laya.stage.addChild(this);
                this.zOrder = order;
                this.on_show();
            }
        }
        set_default_pos() {
        }
        set_style_pos(x, y) {
        }
        on_hide() {
            this.ad_param && this.ad_param.onClose && this.ad_param.onClose();
        }
        on_show() {
            this.refresh();
            ad_native_interstitial.show_count++;
            this.easy_click = false;
            let _business_config_data = syyx_manager.get_business_config();
            if (_business_config_data && _business_config_data["native_institial_white_easy_click"]) {
                let easy_click_pro = _business_config_data["native_institial_white_easy_click"].value[0];
                if (easy_click_pro > 0 && Math.random() <= easy_click_pro) {
                    this.easy_click = true;
                }
            }
            this.ad_param && this.ad_param.onLoad && this.ad_param.onLoad();
            this.ad_param && this.ad_param.onShow && this.ad_param.onShow();
        }
        refresh() {
            this.icon_video.skin = this.native_data.imgUrlList;
            this.txt_title.text = this.native_data.title;
            this.text_desc.text = this.native_data.desc;
            this.report_show();
        }
        hide() {
            if (this.parent) {
                this.removeSelf();
                this.on_hide();
            }
        }
        set_background_on_show() {
            let self = this;
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.hw_qg) {
                syyx_sdk_api.on_show(function () {
                    if (self && !self.destroyed) {
                        console.log("igc----- native_interstitial set_background_on_show");
                        self.report_show();
                    }
                });
            }
        }
    }
    window["oppoSdk"]["syyx_ui_interstitial_h"] = syyx_ui_interstitial_h;

    class syyx_ui_ctr_item extends ui.syyx_ctr.ui_ctr_itemUI {
        constructor() {
            super();
            this.on(Laya.Event.CLICK, this, this.on_click);
        }
        on_click() {
            if (syyx_ctr_manager.check_is_ctr_test_compelete()) {
                return;
            }
            if (!this.app) {
                return;
            }
            this.reward_icon.visible = true;
            this.rewardNum.visible = true;
            this.rewardNum.value = syyx_ctr_manager.get_new_products_reward() + "";
            this.dianzuanshi.play(0, false);
            var curAppId = syyx_sdk_api.get_syyx_app_id();
            igc.stat_manager.instance.send_user_event(5, e_syyx_ctr_event_type.click, this.index + 1, "", "", curAppId, this.app.syyx_app_id, "", this.app.id);
            igc.stat_manager.instance.send_user_event(5, 303, this.index + 1, "", "", curAppId, this.app.syyx_app_id, "", this.app.id);
            this.click_back && this.click_back();
        }
        refresh(index, click_back) {
            if (!this.app) {
                this.visible = false;
                return;
            }
            this.on(Laya.Event.CLICK, this, this.on_click);
            this.label_bg1.visible = false;
            this.label_bg2.visible = false;
            this.label_bg3.visible = false;
            this.label_bg4.visible = false;
            this.label_bg5.visible = false;
            this.visible = true;
            this.game_icon.visible = true;
            this.reward_icon.visible = false;
            this.rewardNum.visible = false;
            this.game_icon.skin = this.app.icon;
            this.game_name.text = this.app.name;
            this.index = index;
            this.click_back = click_back;
            this.set_label_bg_skin(index);
        }
        set_label_bg_skin(index) {
            if (index != null) {
                switch (index) {
                    case 0:
                    case 3:
                        this.label_bg5.visible = true;
                        break;
                    case 1:
                        this.label_bg4.visible = true;
                        break;
                    case 2:
                        this.label_bg3.visible = true;
                        break;
                    case 4:
                        this.label_bg2.visible = true;
                        break;
                    case 5:
                        this.label_bg1.visible = true;
                        break;
                    case 6:
                        this.label_bg2.visible = true;
                        break;
                    case 7:
                        this.label_bg5.visible = true;
                        break;
                }
            }
        }
        get app() {
            return this.dataSource;
        }
    }
    window["oppoSdk"]["syyx_ui_ctr_item"] = syyx_ui_ctr_item;

    class syyx_ui_ctr extends ui.syyx_ctr.ui_ctrUI {
        constructor() {
            super();
            this.call_back = undefined;
            this.game_list.itemRender = syyx_ui_ctr_item;
            this.game_list.renderHandler = new Laya.Handler(this, this.render_item);
            this.game_list.repeatX = 3;
        }
        set_default_pos() {
        }
        set_style_pos(x, y) {
        }
        show(call_back) {
            if (!this.parent) {
                let order = syyx_sdk_utils.get_largest_zorder() + 1;
                Laya.stage.addChild(this);
                this.zOrder = order;
                this.call_back = call_back;
                this.on_show();
            }
        }
        on_show() {
            this.btn_close.on(Laya.Event.CLICK, this, this.on_close);
            this.initView();
            this.initData();
        }
        hide() {
            if (this.parent) {
                this.removeSelf();
                this.on_hide();
            }
        }
        on_hide() {
            syyx_ctr_manager.set_ctr_test_compelete();
            this.btn_close.off(Laya.Event.CLICK, this, this.on_close);
        }
        render_item(box, index) {
            box.refresh(index, this.click_game_item.bind(this));
        }
        initView() {
            this.chuchang.play(0, false);
            this.tip_title1.visible = true;
            this.tip_title2.visible = false;
            this.body_img3.visible = false;
            this.body_img1.visible = true;
            this.body_img2.visible = false;
            this.btn_close.visible = false;
            this.img_background.visible = false;
            let time = syyx_sdk_api.get_business_data_by_key("ctr_test_close_button_delay")[0];
            setTimeout(() => {
                this.btn_close.visible = true;
            }, time * 1000);
        }
        initData() {
            let ctr_test_data = syyx_ctr_manager.get_ctr_data();
            if (ctr_test_data) {
                let cur_list = syyx_sdk_utils.shuffle(ctr_test_data.list);
                this.refresh(cur_list);
            }
        }
        refresh(apps) {
            this.game_list.array = apps;
            var curAppId = syyx_sdk_api.get_syyx_app_id();
            for (let i in apps) {
                let app = apps[i];
                let index = parseInt(i) + 1;
                igc.stat_manager.instance.send_user_event(5, e_syyx_ctr_event_type.show, index + "", "", "", curAppId, app.syyx_app_id, 0, app.id);
                igc.stat_manager.instance.send_user_event(5, 302, index + "", "", "", curAppId, app.syyx_app_id, 0, app.id);
            }
        }
        click_game_item() {
            if (syyx_ctr_manager.check_is_ctr_test_compelete()) {
                return;
            }
            this.zi.on(Laya.Event.COMPLETE, null, () => {
                setTimeout(() => {
                    this.close();
                }, 2000);
            });
            this.feizuanshi.on(Laya.Event.COMPLETE, null, () => {
                this.tip_title2.visible = true;
                this.body_img1.visible = false;
                this.img_background.visible = true;
                this.body_img2.visible = true;
                this.body_img3.visible = true;
                this.zi.play(0, false);
            });
            this.feizuanshi.play(0, false);
            this.call_back && this.call_back(syyx_ctr_manager.get_new_products_reward());
            syyx_ctr_manager.set_ctr_test_compelete();
        }
        on_close() {
            this.hide();
        }
    }
    window["oppoSdk"]["syyx_ui_ctr"] = syyx_ui_ctr;

    class syyx_ui_ctr_h extends ui.syyx_ctr.ui_ctr_hUI {
        constructor() {
            super();
            this.call_back = undefined;
            this.game_list.itemRender = syyx_ui_ctr_item;
            this.game_list.renderHandler = new Laya.Handler(this, this.render_item);
            this.game_list.repeatX = 3;
        }
        set_default_pos() {
        }
        set_style_pos(x, y) {
        }
        show(call_back) {
            if (!this.parent) {
                let order = syyx_sdk_utils.get_largest_zorder() + 1;
                Laya.stage.addChild(this);
                this.zOrder = order;
                this.call_back = call_back;
                this.on_show();
            }
        }
        on_show() {
            this.btn_close.on(Laya.Event.CLICK, this, this.on_close);
            this.initView();
            this.initData();
        }
        hide() {
            if (this.parent) {
                this.removeSelf();
                this.on_hide();
            }
        }
        on_hide() {
            syyx_ctr_manager.set_ctr_test_compelete();
            this.btn_close.off(Laya.Event.CLICK, this, this.on_close);
        }
        render_item(box, index) {
            box.refresh(index, this.click_game_item.bind(this));
        }
        initView() {
            this.tip_title1.visible = true;
            this.tip_title2.visible = false;
            this.body_img3.visible = false;
            this.body_img1.visible = true;
            this.body_img2.visible = false;
            this.btn_close.visible = false;
            this.img_background.visible = false;
            let time = syyx_sdk_api.get_business_data_by_key("ctr_test_close_button_delay")[0];
            setTimeout(() => {
                this.btn_close.visible = true;
            }, time * 1000);
        }
        initData() {
            let ctr_test_data = syyx_ctr_manager.get_ctr_data();
            if (ctr_test_data) {
                let cur_list = syyx_sdk_utils.shuffle(ctr_test_data.list);
                this.refresh(cur_list);
            }
        }
        refresh(apps) {
            this.game_list.array = apps;
            var curAppId = syyx_sdk_api.get_syyx_app_id();
            for (let i in apps) {
                let app = apps[i];
                let index = parseInt(i) + 1;
                igc.stat_manager.instance.send_user_event(5, e_syyx_ctr_event_type.show, index + "", "", "", curAppId, app.syyx_app_id, 0, app.id);
                igc.stat_manager.instance.send_user_event(5, 302, index + "", "", "", curAppId, app.syyx_app_id, 0, app.id);
            }
        }
        click_game_item() {
            if (syyx_ctr_manager.check_is_ctr_test_compelete()) {
                return;
            }
            this.zi.on(Laya.Event.COMPLETE, null, () => {
                setTimeout(() => {
                    this.close();
                }, 2000);
            });
            this.feizuanshi.on(Laya.Event.COMPLETE, null, () => {
                this.tip_title2.visible = true;
                this.body_img1.visible = false;
                this.img_background.visible = true;
                this.body_img2.visible = true;
                this.body_img3.visible = true;
                this.zi.play(0, false);
            });
            this.feizuanshi.play(0, false);
            this.call_back && this.call_back(syyx_ctr_manager.get_new_products_reward());
            syyx_ctr_manager.set_ctr_test_compelete();
        }
        on_close() {
            this.hide();
        }
    }
    window["oppoSdk"]["syyx_ui_ctr_h"] = syyx_ui_ctr_h;

    class syyx_laya_ui_manager {
        static run_call_back(view_type, view) {
            for (let i in this.call_back_list) {
                if (this.call_back_list[i] && this.call_back_list[i].length > 0) {
                    for (let j = 0; j < this.call_back_list[i].length; j++) {
                        let call_back = this.call_back_list[i][j];
                        let view1 = this.get_view_by_type(parseInt(i));
                        call_back && call_back(view1);
                    }
                    this.call_back_list[i] = [];
                }
            }
        }
        static load_atlas(view_type, call_back) {
            if (!this.call_back_list[view_type]) {
                this.call_back_list[view_type] = [];
            }
            this.call_back_list[view_type].push(call_back);
            if (!syyx_manager.__game_init_data || !syyx_manager.__game_init_data.auto_load_atlas) {
                call_back && call_back(this.get_view_by_type(view_type));
                return;
            }
            let self = this;
            let view = this.viewMap[view_type];
            if (view && !view.destroyed) {
                call_back && call_back(this.get_view_by_type(view_type));
                return;
            }
            this._atlas_root_path = syyx_manager.__game_init_data.atlas_path;
            let ui_class = this.get_view_class(view_type);
            let atlas_dict = {};
            if (ui_class && ui_class.uiView) {
                for (let i = 0; i < ui_class.uiView.loadList.length; ++i) {
                    let image_path = ui_class.uiView.loadList[i];
                    let atlas_path = image_path.substring(0, image_path.lastIndexOf("/"));
                    if (!atlas_dict[atlas_path]) {
                        atlas_dict[atlas_path] = image_path;
                    }
                }
            }
            let all_count = Object.keys(atlas_dict).length;
            let load_count = 0;
            for (let atlas in atlas_dict) {
                let atlas_name = atlas + ".atlas";
                let atlas_path = this._atlas_root_path + atlas_name;
                if (this._atlas_cache[atlas]) {
                    this._atlas_loading_state[atlas] = false;
                    load_count++;
                    if (load_count == all_count) {
                        this.run_call_back(view_type, this.get_view_by_type(view_type));
                    }
                }
                else {
                    if (this._atlas_loading_state[atlas]) {
                        console.log("igc----- syyx_laya_ui_manager atlas is loading , please wait", atlas);
                        return;
                    }
                    this._atlas_loading_state[atlas] = true;
                    syyx_sdk_utils.load_resource(atlas_path, function (data) {
                        self._atlas_loading_state[atlas] = false;
                        if (data) {
                            console.log("igc----- syyx_laya_ui_manager load_atlas success", atlas_name);
                            load_count++;
                            self.save_atlas(atlas);
                            if (load_count == all_count) {
                                self.run_call_back(view_type, self.get_view_by_type(view_type));
                            }
                        }
                        else {
                            console.error("igc-----syyx_laya_ui_manager load_atlas fail", atlas_name);
                        }
                    });
                }
            }
        }
        static save_atlas(atlas_path) {
            this._atlas_cache[atlas_path] = 1;
        }
        static get_view_by_type(view_type) {
            let view = this.viewMap[view_type];
            if (!view || view.destroyed) {
                switch (view_type) {
                    case syyx_view.native_banner:
                        view = new syyx_ui_banner();
                        break;
                    case syyx_view.inner_interstitial:
                        view = new syyx_ui_inner_interstitial();
                        break;
                    case syyx_view.interstitial:
                        if (Laya.stage.width > Laya.stage.height) {
                            view = new syyx_ui_interstitial_h();
                        }
                        else {
                            view = new syyx_ui_interstitial();
                        }
                        break;
                    case syyx_view.native_icon:
                        view = new syyx_ui_native_icon();
                        break;
                    case syyx_view.toast:
                        view = new syyx_ui_toast();
                        break;
                    case syyx_view.ctr_check:
                        if (Laya.stage.width > Laya.stage.height) {
                            view = new syyx_ui_ctr_h();
                        }
                        else {
                            view = new syyx_ui_ctr();
                        }
                        break;
                    default:
                        console.log("igc-----syyx_laya_ui_manager get_view_by_type is not exist type", view_type);
                        break;
                }
                view && (this.viewMap[view_type] = view);
                if (view) {
                    if (syyx_manager._ui_prefab_config && syyx_manager._ui_prefab_config[view_type].laya_auto_scale) {
                        syyx_sdk_utils.set_default_scale(view);
                        console.log("igc----- syyx_laya_ui_manager " + syyx_manager._ui_prefab_config[view_type].name + " auto scale");
                    }
                }
            }
            view.set_default_pos && view.set_default_pos();
            return view;
        }
        static get_view_class(view_type) {
            switch (view_type) {
                case syyx_view.native_banner:
                    return syyx_ui_banner;
                case syyx_view.interstitial:
                    return syyx_ui_interstitial;
                case syyx_view.inner_interstitial:
                    return syyx_ui_inner_interstitial;
                case syyx_view.native_icon:
                    return syyx_ui_native_icon;
                case syyx_view.toast:
                    return syyx_ui_toast;
                case syyx_view.ctr_check:
                    return syyx_ui_ctr;
                default:
                    console.log("igc-----syyx_laya_ui_manager get_view_class fail", view_type);
                    return undefined;
            }
        }
    }
    syyx_laya_ui_manager._atlas_root_path = "";
    syyx_laya_ui_manager._atlas_cache = {};
    syyx_laya_ui_manager.viewMap = {};
    syyx_laya_ui_manager._atlas_loading_state = {};
    syyx_laya_ui_manager.call_back_list = {};
    window["oppoSdk"]["syyx_laya_ui_manager"] = syyx_laya_ui_manager;

    class syyx_manager {
        static init(init_config_path = null, init_callback) {
            let self = this;
            let path = init_config_path;
            if (!init_config_path) {
                path = this.__game_init_file_path;
            }
            let channel_type = this.get_channel_type();
            console.log("igc-----channel type 1.web 2.oppo 4.vivo 5.qq 7.apk  8.tt  ------->channel ：", channel_type);
            syyx_sdk_utils.load_resource(path, (data) => {
                console.log("igc-----syyx_game_init.json", data);
                self.__game_init_data = data;
                if (syyx_const.syyx_sdk_publish === e_syyx_sdk_publish_type.out) {
                    if (syyx_const.syyx_sdk_channel === igc.e_channel_type.oppo_qg) {
                        init_config.oppo_qg.app_id = data.syyx_app_id;
                        init_config.oppo_qg.app_version =
                            data.channel[channel_type].app_version;
                        init_config.oppo_qg.pkg_name = data.channel[channel_type].pkg_name;
                        init_config.oppo_qg.stat_key = data.stat_key;
                        init_config.oppo_qg.configAppSecKey = data.config_key;
                    }
                    else if (syyx_const.syyx_sdk_channel === igc.e_channel_type.vivo_qg) {
                        init_config.vivo_qg.app_id = data.syyx_app_id;
                        init_config.vivo_qg.app_version =
                            data.channel[channel_type].app_version;
                        init_config.vivo_qg.pkg_name = data.channel[channel_type].pkg_name;
                        init_config.vivo_qg.stat_key = data.stat_key;
                        init_config.vivo_qg.configAppSecKey = data.config_key;
                    }
                    else if (syyx_const.syyx_sdk_channel === igc.e_channel_type.tt) {
                        init_config.tt.app_id = data.syyx_app_id;
                        init_config.tt.app_version = data.channel[channel_type].app_version;
                        init_config.tt.pkg_name = data.channel[channel_type].pkg_name;
                        init_config.tt.stat_key = data.stat_key;
                        init_config.tt.configAppSecKey = data.config_key;
                    }
                    else if (syyx_const.syyx_sdk_channel === igc.e_channel_type.qq) {
                        init_config.qq.app_id = data.syyx_app_id;
                        init_config.qq.app_version = data.channel[channel_type].app_version;
                        init_config.qq.pkg_name = data.channel[channel_type].pkg_name;
                        init_config.qq.stat_key = data.stat_key;
                        init_config.qq.configAppSecKey = data.config_key;
                    }
                    else if (syyx_const.syyx_sdk_channel === igc.e_channel_type.wx) {
                        init_config.wx.app_id = data.syyx_app_id;
                        init_config.wx.app_version = data.channel[channel_type].app_version;
                        init_config.wx.pkg_name = data.channel[channel_type].pkg_name;
                        init_config.wx.stat_key = data.stat_key;
                        init_config.wx.configAppSecKey = data.config_key;
                    }
                    else if (syyx_const.syyx_sdk_channel === igc.e_channel_type.hw_qg) {
                        console.log("进入hw");
                        init_config.hw_qg.app_id = data.syyx_app_id;
                        init_config.hw_qg.channel_app_id =
                            data.channel[channel_type].channel_app_id;
                        init_config.hw_qg.app_version =
                            data.channel[channel_type].app_version;
                        init_config.hw_qg.pkg_name = data.channel[channel_type].pkg_name;
                        init_config.hw_qg.stat_key = data.stat_key;
                        init_config.hw_qg.configAppSecKey = data.config_key;
                    }
                    else if (syyx_const.syyx_sdk_channel === igc.e_channel_type.apk) {
                        init_config.apk.app_id = data.syyx_app_id;
                        init_config.apk.pkg_name = data.channel[channel_type].pkg_name;
                        init_config.apk.stat_key = data.stat_key;
                        init_config.apk.configAppSecKey = data.config_key;
                        init_config.apk.channel_id = syyx_apk_adv_manager.get_apk_channel_id();
                        init_config.apk.app_version = this.get_app_version();
                        console.log("igc----- apk stat version  " + init_config.apk.app_version);
                    }
                    else if (syyx_const.syyx_sdk_channel === igc.e_channel_type.ipa) {
                        init_config.ipa.app_id = data.syyx_app_id;
                        init_config.ipa.app_version = data.channel[channel_type].app_version;
                        init_config.ipa.pkg_name = data.channel[channel_type].pkg_name;
                        init_config.ipa.stat_key = data.stat_key;
                        init_config.ipa.configAppSecKey = data.config_key;
                        init_config.ipa.channel_id = this.get_channel_id();
                    }
                    else {
                        init_config.web.app_id = data.syyx_app_id;
                        init_config.web.app_version = "123";
                        init_config.web.pkg_name = "123";
                        init_config.web.stat_key = data.stat_key;
                        init_config.web.configAppSecKey = data.config_key;
                    }
                    igc.igc_main.instance.init_wrap(syyx_const.syyx_sdk_channel, init_config);
                }
                self.__syyx_app_id = data.syyx_app_id;
                self.__business_config_file_path = data.business_config_file_path;
                self.__adv_config_file_path = data.adv_config_file_path;
                self.__ui_prefab_config_path = data.ui_prefab_config_path;
                self.__init_callback = init_callback;
                if (self.__init_callback) {
                    console.log("igc ----- game_init.json has loaded");
                    self.__init_callback(true, {
                        business_config: null,
                        load_init_complete: true,
                        load_local_complete: false,
                        load_remote_complete: false,
                    });
                }
                self.load_config();
            }, this);
        }
        static get_app_version() {
            let channel_type = syyx_const.syyx_sdk_channel + "";
            let version = "0.0.0.0";
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                let apk_stat_version = syyx_apk_adv_manager.get_stat_version();
                if (apk_stat_version && apk_stat_version != "0") {
                    return apk_stat_version;
                }
            }
            try {
                if (this.__game_init_data && this.__game_init_data.channel[channel_type]) {
                    version = this.__game_init_data.channel[channel_type].app_version;
                }
                else {
                    console.error("igc----- can not find app_version in syyx_game_init.json");
                    console.error("igc----- channel_type  ", channel_type);
                }
            }
            catch (error) {
                console.error("igc----- get_app_version error");
            }
            return version;
        }
        static get_syyx_app_id() {
            return this.__syyx_app_id || "";
        }
        static get_is_new_player() {
            return this.__is_new_player || 0;
        }
        static get_user_id() {
            return this.__user_id || "";
        }
        static init_remote_config_compelete() {
            if (this.__local_business_config_inited == true && !this.init_completed_tag) {
                syyx_adv_manager.load_adv_config();
                syyx_apk_adv_manager.init_instance();
                this.__inited = true;
                if (this.__init_callback) {
                    this.init_completed_tag = true;
                    console.log("igc ----- local data has been back");
                    this.__init_callback(true, {
                        business_config: this.__business_config_data,
                        load_init_complete: false,
                        load_local_complete: true,
                        load_remote_complete: false,
                    });
                }
            }
            else if (this.__remote_business_config_inited && !this.refresh_completed_tag) {
                syyx_adv_manager.load_adv_config(true);
                this.__inited = true;
                if (this.__init_callback) {
                    this.refresh_completed_tag = true;
                    console.log("igc ----- remote data has been back");
                    this.__init_callback(true, {
                        business_config: this.__business_config_data,
                        load_init_complete: false,
                        load_local_complete: false,
                        load_remote_complete: true,
                    });
                    this.set_remote_config();
                }
            }
        }
        static check_user_start_game_type() {
            let is_old_player = localStorage.getItem("is_old_player");
            this.__is_new_player = is_old_player != "1";
            if (is_old_player == "1") {
                console.log("igc----- old bird--------------");
            }
            else {
                console.log("igc----- new fish--------------");
                localStorage.setItem("is_old_player", "1");
            }
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.web) {
                return;
            }
            let options = this.get_launch_options_sync();
            if (options && options.query) {
                if (options.query.type == igc.e_share_type.card ||
                    options.query.type == igc.e_share_type.record) {
                    let event_id = is_old_player == "1"
                        ? igc.e_share_event_id.old_player
                        : igc.e_share_event_id.new_player;
                    this.send_user_event(event_id, igc.e_share_event_type.share, 0, 0, options.query.type + "");
                }
            }
        }
        static init_param(account, user_id) {
            if (account == "" || account == undefined || user_id == "" || user_id == undefined) {
                console.error("igc----- init_param user_id is undefined!");
                return;
            }
            if (this.has_init_param) {
                return;
            }
            else {
                this.has_init_param = true;
            }
            this.__user_id = user_id + "";
            igc.stat_manager.instance.set_uid(account, user_id, "1");
            let save = localStorage.getItem("syyx_igc_uid" + igc.igc_main.instance.app_config.game_param.app_id);
            if (save && save != "") {
                this.send_user_login();
            }
            else {
                this.send_user_register();
                setTimeout(this.send_user_login, 1000);
                localStorage.setItem("syyx_igc_uid" + igc.igc_main.instance.app_config.game_param.app_id, user_id);
            }
            this.check_user_start_game_type();
            this.set_stat_inited();
        }
        static is_inited() {
            return this.__inited;
        }
        static send_user_register() {
            igc.stat_manager.instance.send_user_register();
        }
        static send_user_event(event_id, event_type, place_id, place_type, extra, str1, str2, extra2, str3) {
            if (!this.__stat_inited) {
                this.__stat_data_cache.unshift({
                    event_id: event_id,
                    event_type: event_type,
                    place_id: place_id,
                    place_type: place_type,
                    extra: extra,
                    str1: str1,
                    str2: str2,
                    extra2: extra2,
                    str3: str3,
                });
                return;
            }
            console.log("igc----- 上报打点", "event_type", event_type, "event_id", event_id, "extra", extra, "str1", str1, "str2", str2, "extra2", extra2, "str3", str3);
            igc.stat_manager.instance.send_user_event(event_id, event_type, place_id, place_type, extra, str1, str2, extra2, str3);
        }
        static set_stat_inited() {
            if (!this.__is_stat_delay) {
                let self = this;
                this.__is_stat_delay = true;
                setTimeout(function () {
                    self.__stat_inited = true;
                    self.send_stat_event_cache();
                }, 1000);
            }
        }
        static send_stat_event_cache() {
            if (this.__stat_data_cache && this.__stat_data_cache.length > 0) {
                let self = this;
                let stat_data = this.__stat_data_cache.pop();
                igc.stat_manager.instance.send_user_event(stat_data.event_id, stat_data.event_type, stat_data.place_id, stat_data.place_type, stat_data.extra, stat_data.str1, stat_data.str2, stat_data.extra2, stat_data.str3);
                setTimeout(function () {
                    self.send_stat_event_cache();
                }, 100);
            }
        }
        static hide(viewType) {
            this.load_view(viewType, function (view) {
                view.hide && view.hide();
            });
        }
        static show(viewType, zOrder = -1, scene, chapter = 0) {
            this.load_view(viewType, function (view) {
                view.show(zOrder, scene, chapter);
            });
        }
        static send_user_login() {
            igc.stat_manager.instance.send_user_login();
        }
        static on_load_game_configs(ret, key, version, data) {
            if (ret == true) {
                this.remote_business_config_data = igc.utils_manager.parse_csv(data, "id");
                console.log("remote config data is " + this.remote_business_config_data);
                localStorage.setItem(syyx_const.local_business_config_version, version);
                syyx_sdk_utils.replace_data(this.__business_config_data, this.remote_business_config_data);
                let temp = JSON.stringify(this.__business_config_data);
                localStorage.setItem(syyx_const.local_business_config_data, temp);
                this.__remote_business_config_inited = true;
            }
            else {
                let save_data = localStorage.getItem(syyx_const.local_business_config_data);
                if (save_data) {
                    this.__remote_business_config_inited = true;
                }
            }
            this.init_remote_config_compelete();
        }
        static load_config() {
            let self = this;
            syyx_sdk_utils.load_resource(syyx_manager.__ui_prefab_config_path, function (data) {
                self._ui_prefab_config = igc.utils_manager.parse_csv(data, "id");
                console.log("igc-----ui prefabs have loaded", self._ui_prefab_config);
                self.load_business_config();
                syyx_ctr_manager.load_ctr_config();
            }, this, function () {
                console.log("igc-----ui prefabs loading failed");
                self.load_business_config();
                syyx_ctr_manager.load_ctr_config();
            });
        }
        static load_business_config() {
            let business_data;
            let cur_business_version = this.get_app_version();
            let key = this.get_syyx_app_id() + this.__business_version;
            let last_business_version = localStorage.getItem(key);
            let sdk_version = syyx_const.syyx_sdk_version;
            let last_sdk_version = localStorage.getItem(syyx_const.syyx_sdk_version_key);
            if (cur_business_version != last_business_version || sdk_version != last_sdk_version) {
                localStorage.removeItem(syyx_const.local_business_config_data);
                localStorage.setItem(syyx_const.local_business_config_version, null);
                localStorage.setItem(key, cur_business_version);
                localStorage.setItem(syyx_const.syyx_sdk_version_key, sdk_version);
                console.log("igc----- app_version change clear business_config localStorage");
            }
            if (cur_business_version == last_business_version && (business_data = localStorage.getItem(syyx_const.local_business_config_data))) {
                console.log("igc----- on_load_game_configs -----use the config  in localstorage");
                this.__business_config_data = JSON.parse(business_data);
                this.__local_business_config_inited = true;
                this.init_remote_config_compelete();
            }
            else {
                this.on_load_local_business_config();
            }
            let business_config_version = localStorage.getItem(syyx_const.local_business_config_version);
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.tt || syyx_const.syyx_sdk_channel === igc.e_channel_type.wx || syyx_const.syyx_sdk_channel === igc.e_channel_type.qq) {
                if (syyx_const.syyx_sdk_channel === igc.e_channel_type.qq) {
                    business_config_version = null;
                }
                igc.igc_main.instance.tpf_sdk.getTpfConfig().httpGetconfigWx(syyx_const.remote_business_config_key, business_config_version, Date.now(), this.on_load_game_configs.bind(this));
            }
            else {
                igc.igc_main.instance.tpf_sdk.getTpfConfig().httpGetconfig(syyx_const.remote_business_config_key, business_config_version, Date.now(), this.on_load_game_configs.bind(this));
            }
        }
        static create_native_banner(call_back) {
            if (!syyx_adv_manager.__adv_config_inited) {
                console.log("igc----- ad initialization is not achieve--->do not call interface too early  :create_native_banner");
                return null;
            }
            return this.create_view(syyx_view.native_banner, call_back);
        }
        static create_inner_interstitial(call_back) {
            if (!syyx_adv_manager.__adv_config_inited) {
                console.log("igc----- ad initialization is not achieve--->do not call interface too early  :create_inner_interstitial");
                return null;
            }
            return this.create_view(syyx_view.inner_interstitial, call_back);
        }
        static create_interstitial(call_back) {
            if (!syyx_adv_manager.__adv_config_inited) {
                console.log("igc----- ad initialization is not achieve--->do not call interface too early  :create_interstitial");
                return null;
            }
            return this.create_view(syyx_view.interstitial, call_back);
        }
        static create_native_icon(call_back) {
            if (!syyx_adv_manager.__adv_config_inited) {
                console.log("igc----- ad initialization is not achieve--->do not call interface too early  :create_native_icon");
                return null;
            }
            return this.create_view(syyx_view.native_icon, call_back);
        }
        static create_toast(desc) {
            this.create_view(syyx_view.toast, function (view) {
                view && view.show && view.show(desc);
            });
        }
        static create_view(viewType, call_back) {
            this.load_view(viewType, call_back);
        }
        static load_view(viewType, call_back) {
            if (window["Laya"]) {
                syyx_laya_ui_manager.load_atlas(viewType, function (view) {
                    call_back && call_back(view);
                });
            }
        }
        static get_business_config() {
            if (!this.__local_business_config_inited &&
                !this.__remote_business_config_inited) {
                return undefined;
            }
            return this.__business_config_data;
        }
        static login_channel(callback) {
            let self = this;
            if (this.has_login_channel) {
                return;
            }
            else {
                this.has_login_channel = true;
            }
            igc.igc_main.instance.only_login_channel(function back(res) {
                if (res && res.channel_user_info && res.channel_user_info.uid) {
                    let save = localStorage.getItem("syyx_igc_uid" + igc.igc_main.instance.app_config.game_param.app_id);
                    if (save && save != "") {
                        self.send_user_login();
                    }
                    else {
                        self.send_user_register();
                        setTimeout(self.send_user_login, 1000);
                        self.__user_id = res.channel_user_info.uid;
                        localStorage.setItem("syyx_igc_uid" + igc.igc_main.instance.app_config.game_param.app_id, res.channel_user_info.uid);
                    }
                }
                self.check_user_start_game_type();
                self.set_stat_inited();
                callback && callback(res);
            });
        }
        static get_business_data_by_key(key) {
            let business_config_data = syyx_manager.get_business_config();
            if (business_config_data && business_config_data[key]) {
                return business_config_data[key].value;
            }
            return undefined;
        }
        static create_ad(ad_type, ad_pos_id, onLoad, onShow, onClose, onError, sub_ad_type, extra = "", apk_sub_ad_type) {
            let top_offset = 0;
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.qq) {
                top_offset = syyx_adv_manager.get_qq_banner_top_offset();
            }
            let ad_id = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- syyx_manager create_ad ad_id no configure in adv.csv");
                return;
            }
            let param = {
                ad_type: ad_type,
                ad_id: ad_id,
                ad_pos_id: ad_pos_id,
                ad_event: ad_id,
                ad_scene: ad_id,
                top_offset: top_offset,
                sub_ad_type: sub_ad_type || 1,
                onLoad: onLoad,
                onShow: onShow,
                onClose: onClose,
                onError: onError,
                extra: extra,
                apk_sub_ad_type: apk_sub_ad_type,
            };
            return igc.igc_main.instance.create_ad(param);
        }
        static show_ad(ad_type, ad_pos_id, onLoad, onShow, onClose, onError, sub_ad_type, marginTop) {
            let ad_id = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- syyx_manager show_ad ad_id no configure in adv.csv");
                return;
            }
            console.log("mijia ---- show_ad " + ad_type);
            let param = {
                ad_type: ad_type,
                ad_id: ad_id,
                ad_pos_id: ad_pos_id,
                ad_event: ad_id,
                ad_scene: ad_id,
                sub_ad_type: sub_ad_type || igc.e_ad_native_type.native_banner_normal,
                onLoad: onLoad,
                onShow: onShow,
                onClose: onClose,
                onError: onError,
                marginTop: marginTop,
            };
            return igc.igc_main.instance.show_ad(param);
        }
        static destroy_ad(ad_type, ad_pos_id, sub_ad_type) {
            let ad_id = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- syyx_manager destroy_ad ad_id no configure in adv.csv");
                return;
            }
            let param = {
                ad_type: ad_type,
                ad_id: ad_id,
                ad_pos_id: ad_pos_id,
                sub_ad_type: sub_ad_type || igc.e_ad_native_type.native_banner_normal,
                ad_event: ad_id,
                ad_scene: ad_id,
            };
            return igc.igc_main.instance.destroy_ad(param);
        }
        static hide_ad(ad_type, ad_pos_id) {
            let ad_id = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- syyx_manager hide_ad ad_id no configure in adv.csv");
                return;
            }
            let param = {
                ad_type: ad_type,
                ad_id: ad_id,
                ad_pos_id: ad_pos_id,
                ad_event: ad_id,
                ad_scene: ad_id,
            };
            return igc.igc_main.instance.hide_ad(param);
        }
        static report_native_inner_interstitial_click(ad_pos_id) {
            if (!syyx_adv_manager.can_show_first_native) {
                console.log("igc----- is in oppo first ad cd ");
                return;
            }
            let native_data = this.get_local_native_data(ad_pos_id);
            this.create_inner_interstitial(function (view) {
                if (native_data) {
                    view.report_click();
                }
            });
        }
        static hide_native_inner_interstitial() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                syyx_apk_adv_manager.hide_native_inner_interstitial();
                return;
            }
            this.load_view(syyx_view.inner_interstitial, function (view) {
                if (view.parent) {
                    view.hide && view.hide();
                }
            });
        }
        static support_game_box() {
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.oppo_qg) {
                let systeminfo = syyx_manager.get_system_info_sync();
                if (systeminfo && systeminfo.platformVersion) {
                    return systeminfo.platformVersion >= 1076;
                }
            }
            else if (syyx_const.syyx_sdk_channel === igc.e_channel_type.vivo_qg) {
                let systeminfo = syyx_manager.get_system_info_sync();
                if (systeminfo && systeminfo.platformVersion) {
                    return systeminfo.platformVersion >= 1092;
                }
            }
            return false;
        }
        static check_can_add_desktop() {
            return igc.igc_main.instance.check_can_add_desktop({});
        }
        static check_is_add_desktop(can_add, has_add) {
            let param = {
                can_add: can_add,
                has_add: has_add,
            };
            return igc.igc_main.instance.check_is_add_desktop(param);
        }
        static add_desktop(on_success, on_failed, on_failed_back, has_create) {
            let param = {
                on_success: on_success,
                on_failed: on_failed,
                on_failed_back: on_failed_back,
                has_create: has_create,
            };
            return igc.igc_main.instance.add_desktop(param);
        }
        static on_show(callback) {
            return igc.igc_main.instance.on_show({
                on_show: callback,
            });
        }
        static on_hide(callback) {
            return igc.igc_main.instance.on_hide({
                on_hide: callback,
            });
        }
        static get_system_info_sync() {
            return igc.igc_main.instance.get_system_info_sync();
        }
        static get_launch_options_sync() {
            return igc.igc_main.instance.get_launch_options_sync();
        }
        static exit_mini_program() {
            return igc.igc_main.instance.exit_mini_program();
        }
        static navigate_to_mini_program(app_id, success) {
            let param = {
                app_id: app_id,
                success: success,
            };
            return igc.igc_main.instance.navigate_to_mini_program(param);
        }
        static share(title, imageUrl, query, desc, success, fail) {
            let param = {
                title: title,
                imageUrl: imageUrl,
                query: query,
                desc: desc,
                success: success,
                fail: fail,
            };
            return igc.igc_main.instance.share(param);
        }
        static on_share_app_message(title, imageUrl) {
            let param = {
                title: title,
                imageUrl: imageUrl,
            };
            return igc.igc_main.instance.on_share_app_message(param);
        }
        static start_record_screen(time, is_clip_end, clip_time) {
            let param = {
                time: time,
                is_clip_end: is_clip_end,
                clip_time: clip_time,
            };
            return igc.igc_main.instance.start_record_screen(param);
        }
        static stop_record_screen() {
            return igc.igc_main.instance.stop_record_screen();
        }
        static pause_record_screen() {
            return igc.igc_main.instance.pause_record_screen();
        }
        static resume_record_screen() {
            return igc.igc_main.instance.resume_record_screen();
        }
        static share_record_screen(videoTopics, title, desc, imageUrl, query, fail, success) {
            let param = {
                videoTopics: videoTopics,
                title: title,
                desc: desc,
                imageUrl: imageUrl,
                query: query,
                fail: fail,
                success: success,
            };
            return igc.igc_main.instance.share_record_screen(param);
        }
        static get_record_video() {
            return igc.igc_main.instance.get_record_video();
        }
        static on_load_local_business_config() {
            let self = this;
            syyx_sdk_utils.load_resource(this.__business_config_file_path, (data) => {
                let new_data = igc.utils_manager.parse_csv(data, "id");
                new_data["native_icon_switch"] = {
                    desc: "原生icon开关",
                    id: "native_icon_switch",
                    value: [1],
                };
                new_data["native_icon_trap_pro"] = {
                    desc: "原生icon易点击概率",
                    id: "native_icon_trap_pro",
                    value: [1],
                };
                new_data["banner_cool_time"] = {
                    desc: "banner自动刷新时间",
                    id: "banner_cool_time",
                    value: [20, 20],
                };
                new_data["native_icon_cool_time"] = {
                    desc: "原生icon自动刷新时间",
                    id: "native_icon_cool_time",
                    value: [20, 20],
                };
                new_data["adv_banner_cd"] = {
                    desc: "原生及banner广告冷却（秒）",
                    id: "adv_banner_cd",
                    value: [60],
                };
                new_data["banner_top_offset"] = {
                    desc: "手Q渠道普通banner上移距离",
                    id: "banner_top_offset",
                    value: [0, 0],
                };
                new_data["native_banner_open_switch"] = {
                    desc: "是否启用原生banner广告",
                    id: "native_banner_open_switch",
                    value: [1],
                };
                new_data["native_banner_click_switch"] = {
                    desc: "是否启用原生banner易点击处理",
                    id: "native_banner_click_switch",
                    value: [0],
                };
                new_data["native_banner_click_pro"] = {
                    desc: "原生banner易点击触发概率",
                    id: "native_banner_click_pro",
                    value: [0],
                };
                new_data["native_banner_click_protect"] = {
                    desc: "原生banner易点击保护",
                    id: "native_banner_click_protect",
                    value: [3],
                };
                new_data["native_institial_white_easy_click"] = {
                    desc: "原生插屏点击空白跳转",
                    id: "native_institial_white_easy_click",
                    value: [0],
                };
                new_data["native_banner_report_click_update_switch"] = {
                    desc: "原生Banner点击上报后立即刷新",
                    id: "native_banner_report_click_update_switch",
                    value: [1],
                };
                new_data["native_icon_report_click_update_switch"] = {
                    desc: "原生icon点击上报后立即刷新",
                    id: "native_icon_report_click_update_switch",
                    value: [1],
                };
                new_data["native_inner_report_click_update_switch"] = {
                    desc: "结算原生点击上报后立即刷新",
                    id: "native_inner_report_click_update_switch",
                    value: [1],
                };
                new_data["native_interstitial_report_click_update_switch"] = {
                    desc: "原生插屏点击上报后立即刷新",
                    id: "native_interstitial_report_click_update_switch",
                    value: [1],
                };
                new_data["show_normal_banner_switch"] = {
                    desc: "是否开启展示普通banner",
                    id: "show_normal_banner_switch",
                    value: [1],
                };
                new_data["native_icon_trap_pro"] = {
                    desc: "原生icon易点击概率",
                    id: "native_icon_trap_pro",
                    value: [0],
                };
                new_data["finger_close_banner_switch"] = {
                    desc: "关闭Banner后不再展示",
                    id: "finger_close_banner_switch",
                    value: [0, 60],
                };
                new_data["native_inner_institial_click_close_pro"] = {
                    desc: "关闭结算原生易跳转概率",
                    id: "native_inner_institial_click_close_pro",
                    value: [0],
                };
                new_data["native_institial_click_close_pro"] = {
                    desc: "关闭原生插屏易跳转概率",
                    id: "native_institial_click_close_pro",
                    value: [0],
                };
                new_data["open_oppo_new_rule"] = {
                    desc: "是否开启oppo新规",
                    id: "open_oppo_new_rule",
                    value: [1],
                };
                new_data["oppo_banner_cool_time"] = {
                    desc: "oppoBanner累计展示刷新时间",
                    id: "oppo_banner_cool_time",
                    value: [
                        [0, 120, 10],
                        [121, 180, 11],
                        [181, 240, 12],
                    ],
                };
                new_data["oppo_native_show_limit"] = {
                    desc: "oppo原生展示限制",
                    id: "oppo_native_show_limit",
                    value: [60, 60],
                };
                new_data["oppo_native_cache_length"] = {
                    desc: "oppo原生数据缓存数组长度",
                    id: "oppo_native_cache_length",
                    value: [5],
                };
                new_data["native_inner_interstitial_switch"] = {
                    desc: "结算原生开关",
                    id: "native_inner_interstitial_switch",
                    value: [1],
                };
                new_data["native_banner_click_pro_limit"] = {
                    desc: "原生banner点击率限制",
                    id: "native_banner_click_pro_limit",
                    value: [1000, 0.4, 60],
                };
                new_data["native_inner_click_pro_limit"] = {
                    desc: "结算原生点击率限制",
                    id: "native_inner_click_pro_limit",
                    value: [1000, 0.4, 60],
                };
                new_data["native_interstitial_click_pro_limit"] = {
                    desc: "原生插屏点击率限制",
                    id: "native_interstitial_click_pro_limit",
                    value: [1000, 0.4, 60],
                };
                new_data["native_interstitial_click_wrap"] = {
                    desc: "原生插屏展示策略",
                    id: "native_interstitial_click_wrap",
                    value: [1000, 2, 5],
                };
                new_data["native_inner_institial_click_wrap"] = {
                    desc: "结算原生展示策略",
                    id: "native_inner_institial_click_wrap",
                    value: [1000, 2, 5],
                };
                new_data["banner_strong_update_switch"] = {
                    desc: "banner强制刷新开关",
                    id: "banner_strong_update_switch",
                    value: [0],
                };
                new_data["native_banner_height_open_rule"] = {
                    desc: "原生banner高度启动规则",
                    id: "native_banner_height_open_rule",
                    value: [1000, 3, 5],
                };
                new_data["native_banner_height_rule"] = {
                    desc: "原生banner高度规则",
                    id: "native_banner_height_rule",
                    value: [200, 400, 1],
                };
                new_data["load_native_interstitial_rule"] = {
                    desc: "原生插屏加载规则（第X次去加载）",
                    id: "load_native_interstitial_rule",
                    value: [3],
                };
                new_data["first_use_natibe_banner"] = {
                    desc: "原生插屏优先使用原生Banner",
                    id: "first_use_natibe_banner",
                    value: [1],
                };
                new_data["only_interstitial"] = {
                    desc: "调用原生插屏改为普通插屏",
                    id: "only_interstitial",
                    value: [0],
                };
                new_data["ctr_test_close_button_delay"] = {
                    desc: "ctr测试关闭按钮延迟",
                    id: "ctr_test_close_button_delay",
                    value: [1],
                };
                new_data["ctr_test_reward_count"] = {
                    desc: "ctr测试奖励数量",
                    id: "ctr_test_reward_count",
                    value: [100],
                };
                new_data["banner_click_mask_open_rule"] = {
                    desc: "banner点击区域缩放规则",
                    id: "banner_click_mask_open_rule",
                    value: [2, 1, 2],
                };
                new_data["banner_click_mask_scale"] = {
                    desc: "banner点击区域缩放倍数",
                    id: "banner_click_mask_scale",
                    value: [1.5],
                };
                new_data["banner_click_mask_preview"] = {
                    desc: "banner点击区域缩放倍数虚影显示",
                    id: "banner_click_mask_preview",
                    value: [0],
                };
                new_data["native_inner_interstitial_auto_preload_rule"] = {
                    desc: "结算原生预加载规则",
                    id: "native_inner_interstitial_auto_preload_rule",
                    value: [0, 15],
                };
                new_data["apk_native_type_id_load_rule"] = {
                    desc: "原生广告3个类型广告id加载规则",
                    id: "apk_native_type_id_load_rule",
                    value: [1, 1, 1, 1],
                };
                new_data["apk_native_type_sub_id_load_rule"] = {
                    desc: "原生自渲染3个子类型广告id加载规则",
                    id: "apk_native_type_sub_id_load_rule",
                    value: [1, 1, 1],
                };
                if (self.__remote_business_config_inited) {
                    syyx_sdk_utils.replace_data(new_data, self.__business_config_data);
                    console.log("igc----- bussiness config is ", new_data);
                    self.__business_config_data = new_data;
                    let temp = JSON.stringify(self.__business_config_data);
                    localStorage.setItem(syyx_const.local_business_config_data, temp);
                }
                else {
                    self.__business_config_data = new_data;
                    console.log("igc-----local bussiness config is ", new_data);
                }
                self.__local_business_config_inited = true;
                self.init_remote_config_compelete();
            }, this);
        }
        static show_video(ad_pos_id, onLoad, onShow, onClose, onError, need_err_tips = false) {
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.web || (syyx_const.syyx_sdk_channel === igc.e_channel_type.apk && !syyx_sdk_api.get_apk_channel_id())) {
                onClose && onClose(null, { isEnded: true });
                return;
            }
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.apk) {
                syyx_apk_adv_manager.show_video(ad_pos_id, onLoad, onShow, onClose, onError);
                return;
            }
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.hw_qg) {
                syyx_manager.show_ad(igc.e_ad_type.video, ad_pos_id, onLoad, onShow, function (param, res) {
                    if (res.isEnded) {
                        syyx_sdk_api.send_other_event(ad_pos_id, igc.igc_stat_ids.video_compelete);
                    }
                    else {
                        syyx_sdk_api.send_other_event(ad_pos_id, igc.igc_stat_ids.video_uncompelete);
                    }
                    onClose && onClose(param, res);
                }, function () {
                    need_err_tips &&
                        syyx_manager.create_toast("目前暂时无广告，请稍后再试");
                    onError && onError();
                });
            }
            else {
                syyx_manager.create_ad(igc.e_ad_type.video, ad_pos_id, onLoad, onShow, function (param, res) {
                    if (res.isEnded) {
                        syyx_sdk_api.send_other_event(ad_pos_id, igc.igc_stat_ids.video_compelete);
                    }
                    else {
                        syyx_sdk_api.send_other_event(ad_pos_id, igc.igc_stat_ids.video_uncompelete);
                    }
                    onClose && onClose(param, res);
                }, function () {
                    need_err_tips &&
                        syyx_manager.create_toast("目前暂时无广告，请稍后再试");
                    onError && onError();
                });
            }
        }
        static show_interstitial(ad_pos_id, onLoad, onShow, onClose, onError) {
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.apk) {
                syyx_apk_adv_manager.show_interstitial(onLoad, onShow, onClose, onError);
                return;
            }
            syyx_manager.create_ad(igc.e_ad_type.interstitial, ad_pos_id, onLoad, onShow, onClose, onError);
        }
        static preload_video() {
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.hw_qg) {
                syyx_manager.create_ad(igc.e_ad_type.video, e_ad_id.video_add_gold, () => { }, () => { }, () => { }, () => { });
            }
        }
        static pre_load_game_portal_box(ad_pos_id, onLoad, onShow, onClose, onError) {
            let self = this;
            if (this.support_game_box()) {
                self.create_ad(igc.e_ad_type.app_box, ad_pos_id, null, null, function () {
                    onClose && onClose();
                    setTimeout(function () {
                        self.pre_load_game_portal_box(ad_pos_id, null, null, null, null);
                    }, 500);
                }, null, igc.e_ad_app_box_type.portal_box);
            }
        }
        static show_game_portal_box(ad_pos_id, onLoad, onShow, onClose, onError, need_err_tips = false, marginTop) {
            let self = this;
            this.show_ad(igc.e_ad_type.app_box, ad_pos_id, onLoad, onShow, function () {
                onClose && onClose();
            }, function () {
                need_err_tips && syyx_manager.create_toast("努力加载中，请稍后再试");
                onError && onError();
            }, igc.e_ad_app_box_type.portal_box, marginTop);
        }
        static show_native_inner_interstitial(ad_pos_id, parent, click_back, show_back, hide_back, is_new_type = true, apk_attr) {
            if (!syyx_adv_manager.can_show_first_native) {
                console.log("igc----- is in oppo first ad cd ");
                return;
            }
            if (this.get_channel_type() == igc.e_channel_type.apk) {
                syyx_apk_adv_manager.show_native_inner_interstitial({
                    ad_pos_id: ad_pos_id,
                    onClick: click_back,
                    onShow: show_back,
                    onClose: hide_back,
                    attrSet: apk_attr
                });
            }
            else {
                let native_data = this.get_local_native_data(ad_pos_id);
                this.create_inner_interstitial(function (view) {
                    if (native_data) {
                        view.show(parent, native_data, click_back, show_back, hide_back, is_new_type);
                    }
                });
            }
        }
        static click_native_inner_interstitial(call_back) {
            this.create_inner_interstitial(function (view) {
                if (view && view.parent) {
                    view.report_click();
                }
                else {
                    call_back && call_back();
                }
            });
        }
        static get_local_native_data(ad_pos_id) {
            return syyx_adv_manager.get_local_native_data(ad_pos_id);
        }
        static get_channel_ad_id(ad_pos_id) {
            return syyx_adv_manager.get_channel_ad_id(ad_pos_id);
        }
        static show_banner(ad_pos_id, onLoad, onShow, onClose, onError, attrSet) {
            syyx_adv_manager.show_banner(igc.e_ad_type.banner, ad_pos_id, onLoad, onShow, onClose, onError, attrSet);
        }
        static hide_banner() {
            syyx_adv_manager.hide_banner();
        }
        static report_ad_show(ad_pos_id, native_data) {
            return syyx_adv_manager.report_ad_show(ad_pos_id, native_data);
        }
        static report_ad_click(ad_pos_id, native_data) {
            return syyx_adv_manager.report_ad_click(ad_pos_id, native_data);
        }
        static show_new_products(call_back) {
            syyx_ctr_manager.show_new_products(call_back);
        }
        static get_channel_type() {
            let channel_type;
            if (window["qq"]) {
                channel_type = igc.e_channel_type.qq;
            }
            else if (window["hbs"]) {
                channel_type = igc.e_channel_type.hw_qg;
            }
            else if (window["tt"]) {
                channel_type = igc.e_channel_type.tt;
            }
            else if (window["qg"] && !window["hbs"]) {
                let qg = window["qg"];
                let provider = qg.getProvider();
                if (provider == "OPPO") {
                    channel_type = igc.e_channel_type.oppo_qg;
                }
                else if (provider == "vivo") {
                    channel_type = igc.e_channel_type.vivo_qg;
                }
                else if (window["hbs"]) {
                    channel_type = igc.e_channel_type.hw_qg;
                }
            }
            else if (window["wx"]) {
                channel_type = igc.e_channel_type.wx;
            }
            else if (window["loadingView"]) {
                channel_type = igc.e_channel_type.apk;
            }
            else {
                channel_type = igc.e_channel_type.web;
            }
            syyx_const.syyx_sdk_channel = channel_type;
            return syyx_const.syyx_sdk_channel;
        }
        static get_channel_id() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.ipa) {
                return igc.e_channel_id.ipa;
            }
            else if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                return syyx_apk_adv_manager.get_apk_channel_id();
            }
            else {
                try {
                    let channel_type = this.get_channel_type();
                    let channel_id = 0;
                    if (channel_type == igc.e_channel_type.qq) {
                        channel_id = igc.e_channel_id.qq;
                    }
                    else if (channel_type == igc.e_channel_type.hw_qg) {
                        channel_type = igc.e_channel_type.hw_qg;
                    }
                    else if (channel_type == igc.e_channel_type.tt) {
                        channel_type = igc.e_channel_type.tt;
                    }
                    else if (channel_type == igc.e_channel_type.oppo_qg) {
                        channel_type = igc.e_channel_type.oppo_qg;
                    }
                    else if (channel_type == igc.e_channel_type.vivo_qg) {
                        channel_type = igc.e_channel_type.vivo_qg;
                    }
                    else if (channel_type == igc.e_channel_type.wx) {
                        channel_type = igc.e_channel_type.wx;
                    }
                    else {
                        channel_type = igc.e_channel_type.web;
                    }
                }
                catch (error) { }
            }
            return "0";
        }
        static get_ad_channel_id() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                return syyx_apk_adv_manager.get_apk_ad_channel_id();
            }
            return "0";
        }
        static get_device_id() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                return syyx_apk_adv_manager.get_apk_device_id();
            }
            return "0";
        }
        static set_remote_config() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                syyx_apk_adv_manager.set_apk_remote_config();
            }
        }
    }
    syyx_manager.__syyx_app_id = 0;
    syyx_manager.viewMap = {};
    syyx_manager.func_open = {};
    syyx_manager.__game_init_file_path = "";
    syyx_manager.__business_config_file_path = "";
    syyx_manager.__adv_config_file_path = "";
    syyx_manager.__business_version = "__business_version";
    syyx_manager.__init_callback = undefined;
    syyx_manager.has_init_param = false;
    syyx_manager.has_login_channel = false;
    syyx_manager.__game_init_data = undefined;
    syyx_manager.__business_config_data = {};
    syyx_manager.__local_business_config_inited = false;
    syyx_manager.__remote_business_config_inited = false;
    syyx_manager.remote_business_config_data = null;
    syyx_manager.__is_new_player = false;
    syyx_manager.init_completed_tag = false;
    syyx_manager.refresh_completed_tag = false;
    syyx_manager.__stat_inited = false;
    syyx_manager.__is_stat_delay = false;
    syyx_manager.__stat_data_cache = [];
    syyx_manager.__user_id = "";
    window["oppoSdk"]["syyx_manager"] = syyx_manager;

    class ad_oppo_banner {
        static run_timer() {
            let self = this;
            if (!this.is_run_timer) {
                this.is_run_timer = true;
                this._business_config_data = syyx_manager.get_business_config();
                self.load_native_banner();
                setInterval(() => {
                    if (self.banner_showing) {
                        self.banner_show_time++;
                    }
                    if (self.banner_show_time > 0 && self.banner_show_time % self.update_cd == 0) {
                        console.log("igc----- update_native_banner");
                        self.load_native_banner();
                    }
                }, 1000);
            }
            else {
                if (this._cur_native_data && !this.need_load) {
                    this.show_native_banner_ui();
                }
                else {
                    this.load_native_banner();
                }
            }
        }
        static set_banner_height() {
            if (syyx_adv_manager.is_oppo_vivo()) {
                syyx_manager.load_view(syyx_view.native_banner, function (view) {
                    view && view.set_banner_height && view.set_banner_height(false);
                });
            }
        }
        static check_need_strong_load_native_banner() {
            let strong_switch = false;
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["banner_strong_update_switch"]) {
                strong_switch = this._business_config_data["banner_strong_update_switch"].value[0] == 1;
            }
            return strong_switch;
        }
        static show_banner(ad_type, ad_pos_id, onLoad, onShow, onClose, onError) {
            if (!this.need_show) {
                this.check_need_strong_load_native_banner() && (this.need_load = true);
                this.need_show = true;
            }
            this._normal_banner_id = ad_pos_id;
            this._native_banner_id = syyx_adv_manager._adv_config_data[ad_pos_id].backup_id;
            this._ad_param = {
                ad_type: ad_type,
                ad_pos_id: ad_pos_id,
                onLoad: onLoad,
                onShow: onShow,
                onClose: onClose,
                onError: onError,
            };
            if (!syyx_adv_manager.can_show_first_native || syyx_adv_manager.limit_show_banner) {
                console.log("igc----- banner is in cooling time ");
                return;
            }
            let ad_id = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- ad_oppo_banner normal_banner_id no configure in adv.csv");
                return;
            }
            this.run_timer();
        }
        static hide_banner() {
            this.need_show = false;
            this.banner_showing = false;
            if (!syyx_adv_manager.can_show_first_native || syyx_adv_manager.limit_show_banner) {
                console.log("igc----- banner is in cooling time ");
                return;
            }
            this.hide_native_banner_ui();
            this.hide_normal_banner();
        }
        static load_native_banner() {
            let self = this;
            this.need_load = false;
            this.banner_timer_id && clearTimeout(this.banner_timer_id);
            this.update_cd = syyx_adv_manager.get_oppo_banner_show_update_time();
            this.hide_native_banner_ui();
            if (!this.need_show) {
                self.hide_banner();
                return;
            }
            this.update_cur_native_data(this._last_ad_id);
            if (syyx_adv_manager.check_is_click_limit(e_ad_native_type.native_banner)) {
                if (this._cur_native_data) {
                    self.show_native_banner_ui();
                }
                else {
                    self.set_show_error_model();
                }
                return;
            }
            if (syyx_adv_manager.check_is_show_count_limit()) {
                console.log("igc----- ad_oppo_banner show native is show limit !!!");
                if (this._cur_native_data) {
                    self.show_native_banner_ui();
                }
                else {
                    self.set_show_error_model();
                }
                return;
            }
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.web) {
                let data = new native_ad_data();
                data.id = igc.utils_manager.get_random_name();
                data.adPosId = self._native_banner_id;
                data.adId = "1";
                data.adUnitId = syyx_sdk_utils.get_random_number([0, 100000]);
                data.imgUrlList = "https://h5-lg.syyx.com/coolbattle/share/share_img.jpg";
                data.title = "banner测试标题" + syyx_sdk_utils.get_random_number([0, 100]);
                data.desc = "banner测试描述" + syyx_sdk_utils.get_random_number([0, 100]);
                data.state = e_ad_native_state.need_show;
                data.native_type = e_ad_native_type.native_banner;
                this.add_native_data(data);
                console.log("igc------syyx_adv_manager-------native_banner on_load web", data);
                self.show_native_banner_ui();
                self._ad_param.onLoad && self._ad_param.onLoad();
                syyx_adv_manager.add_native_show_count();
                return;
            }
            if (!self._native_banner_id || !syyx_adv_manager.is_oppo_vivo()) {
                self.show_normal_banner();
                return;
            }
            if (this._business_config_data && this._business_config_data["native_banner_open_switch"]) {
                if (this._business_config_data["native_banner_open_switch"].value[0] == 0) {
                    self.show_normal_banner();
                    return;
                }
            }
            if (this._cur_native_data && this._cur_native_data.state == e_ad_native_state.need_show) {
                self.show_native_banner_ui();
                return;
            }
            syyx_manager.create_ad(igc.e_ad_type.native, self._native_banner_id, function on_load(param, ad_data_list) {
                console.log("igc------syyx_adv_manager-------native_banner on_load", ad_data_list);
                if (ad_data_list == undefined || !ad_data_list[0]) {
                    self.load_native_banner_error();
                }
                else {
                    let length = 0;
                    if (syyx_const.syyx_sdk_channel === igc.e_channel_type.vivo_qg) {
                        length = ad_data_list.length - 1;
                    }
                    let data = new native_ad_data();
                    data.id = igc.utils_manager.get_random_name();
                    data.adPosId = self._native_banner_id;
                    data.adId = syyx_adv_manager.get_channel_ad_id(self._native_banner_id);
                    data.adUnitId = ad_data_list[length].adUnitId;
                    data.imgUrlList = syyx_sdk_utils.format_remote_texture_url(ad_data_list[length].imgUrlList[0]);
                    data.title = ad_data_list[length].title;
                    data.desc = ad_data_list[length].desc;
                    data.state = e_ad_native_state.need_show;
                    data.native_type = e_ad_native_type.native_banner;
                    self.add_native_data(data);
                    self.show_native_banner_ui();
                    self._ad_param.onLoad && self._ad_param.onLoad();
                    syyx_adv_manager.add_native_show_count();
                }
            }, function on_show() {
            }, function on_close(param, res) {
            }, function on_error(param, err) {
                console.error("igc-----syyx_adv_manager-------native_banner onError", err);
                self.load_native_banner_error();
            });
        }
        static report_ad_click(ad_pos_id, native_data) {
            if (this._native_banner_id != ad_pos_id) {
                return;
            }
            if (this._business_config_data && this._business_config_data["native_banner_report_click_update_switch"]) {
                if (this._business_config_data["native_banner_report_click_update_switch"].value[0] == 1) {
                    this.need_show = true;
                    this.load_native_banner();
                }
            }
        }
        static report_ad_show(ad_pos_id, native_data) {
            if (!this._native_banner_id) {
                this._native_banner_id = e_ad_id.native_banner;
            }
            if (this._native_banner_id == ad_pos_id) {
                this._last_ad_id = native_data.id;
            }
        }
        static show_native_banner_ui() {
            let self = this;
            this.hide_normal_banner();
            if (this.need_show) {
                syyx_manager.create_native_banner(function (view) {
                    if (self._cur_native_data) {
                        self.banner_showing = true;
                        self._ad_param.onShow && self._ad_param.onShow();
                        view.show && view.show(self._cur_native_data);
                    }
                });
            }
        }
        static hide_native_banner_ui() {
            if (syyx_adv_manager.is_oppo_vivo()) {
                syyx_manager.load_view(syyx_view.native_banner, function (view) {
                    view && view.hide && view.hide();
                });
            }
        }
        static load_native_banner_error() {
            this.update_cur_native_data(this._last_ad_id);
            if (this._cur_native_data) {
                this.show_native_banner_ui();
            }
            else {
                this.show_normal_banner();
            }
        }
        static load_normal_banner_error() {
            this.banner_showing = false;
            if (this.need_show) {
                this.update_cur_native_data();
                if (this._cur_native_data) {
                    this.show_native_banner_ui();
                }
                else {
                    this.set_show_error_model();
                }
            }
        }
        static set_normal_banner_switch(value) {
            this.normal_banner_switch = value;
            if (!value) {
                this.hide_normal_banner();
            }
        }
        static show_normal_banner() {
            if (this._business_config_data && this._business_config_data["show_normal_banner_switch"]) {
                if (this._business_config_data["show_normal_banner_switch"].value[0] == 0) {
                    console.log("igc ----- normal banner switch is close");
                    return;
                }
            }
            if (!this.normal_banner_switch) {
                console.log("igc----- vivo ad_banner show_normal_banner normal_banner_switch is close!!!");
                this.load_normal_banner_error();
                return;
            }
            let self = this;
            if (!this.can_show_vivo_banner && syyx_const.syyx_sdk_channel === igc.e_channel_type.vivo_qg) {
                console.log("igc----- vivo ad_banner show_normal_banner create too often!!!");
                console.log("igc----- vivo ad_banner show_normal_banner so that use old native banner data!!!");
                this.load_normal_banner_error();
                return;
            }
            syyx_manager.create_ad(igc.e_ad_type.banner, self._normal_banner_id, function onLoad(param, res) {
                self._ad_param.onLoad && self._ad_param.onLoad();
            }, function onShow() {
                self.normal_banner_showing = true;
                console.log("igc----- show_normal_banner success");
                self.hide_native_banner_ui();
                self.banner_showing = self.need_show;
                if (self.need_show) {
                    self._ad_param.onShow && self._ad_param.onShow();
                    syyx_adv_manager.add_native_show_count();
                }
                else {
                    self.hide_normal_banner();
                }
            }, function onClose(param, res) {
                self._ad_param.onClose && self._ad_param.onClose();
            }, function onError(param, err) {
                console.error("igc------syyx_adv_manager show_normal_banner onError", err);
                self._ad_param.onError && self._ad_param.onError(param, err);
                self.load_normal_banner_error();
            });
        }
        static set_show_error_model() {
            let self = this;
            if (syyx_adv_manager.check_is_open_oppo_rule()) {
                this.banner_timer_id && clearTimeout(this.banner_timer_id);
                this.banner_timer_id = setTimeout(function () {
                    self.update_cur_native_data();
                    if (self.need_show && !self._cur_native_data) {
                        self.load_native_banner();
                    }
                }, this.update_cd * 1000);
            }
        }
        static hide_normal_banner() {
            let self = this;
            if (self._normal_banner_id) {
                if (self.normal_banner_showing && syyx_const.syyx_sdk_channel === igc.e_channel_type.vivo_qg) {
                    self.can_show_vivo_banner = false;
                    this.normal_banner_timer_id && clearTimeout(this.normal_banner_timer_id);
                    this.normal_banner_timer_id = setTimeout(() => {
                        self.can_show_vivo_banner = true;
                    }, 11000);
                }
                self.normal_banner_showing = false;
                syyx_manager.destroy_ad(igc.e_ad_type.banner, self._normal_banner_id);
            }
        }
        static update_cur_native_data(ignore_id = undefined) {
            this.banner_show_time = 0;
            this._cur_native_data = syyx_adv_manager.get_native_data(ignore_id);
        }
        static add_native_data(native_data) {
            syyx_adv_manager.add_native_data(native_data);
            this.update_cur_native_data(this._last_ad_id);
        }
    }
    ad_oppo_banner._last_ad_id = undefined;
    ad_oppo_banner._business_config_data = {};
    ad_oppo_banner._native_banner_id = undefined;
    ad_oppo_banner._normal_banner_id = undefined;
    ad_oppo_banner.update_cd = 5;
    ad_oppo_banner.is_run_timer = false;
    ad_oppo_banner.need_show = true;
    ad_oppo_banner.banner_showing = false;
    ad_oppo_banner.banner_show_time = 0;
    ad_oppo_banner._cur_native_data = undefined;
    ad_oppo_banner.normal_banner_showing = false;
    ad_oppo_banner.can_show_vivo_banner = true;
    ad_oppo_banner.normal_banner_switch = true;
    ad_oppo_banner.need_load = false;
    window["oppoSdk"]["ad_oppo_banner"] = ad_oppo_banner;

    class ad_native_icon {
        static run_timer() {
            if (!this.is_run_timer && syyx_adv_manager.is_oppo_vivo()) {
                this.is_run_timer = true;
                this.timer_func();
            }
        }
        static timer_func() {
            let self = this;
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["native_icon_cool_time"]) {
                this.auto_update_cd = this._business_config_data["native_icon_cool_time"].value;
            }
            this.load_native_icon();
            let cd = syyx_sdk_utils.get_random_number(this.auto_update_cd);
            console.log("igc-----syyx_ui_native_icon next time to refresh native icon is ", cd);
            this.timer_id && clearTimeout(this.timer_id);
            this.timer_id = setTimeout(() => {
                self.timer_func();
            }, cd * 1000);
        }
        static report_ad_click(ad_pos_id, native_data) {
            if (this._ad_pos_id != ad_pos_id) {
                return;
            }
            console.log("igc ----- has in native icon 's report click ");
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["native_icon_report_click_update_switch"]) {
                if (this._business_config_data["native_icon_report_click_update_switch"].value[0] == 1) {
                    this.destroy_timer();
                    this.icon_parent && this.show_native_icon(this.icon_parent, this._ad_param.ad_type, this._ad_param.ad_pos_id, this._ad_param.onLoad, this._ad_param.onShow, this._ad_param.onClose, this._ad_param.onError);
                }
            }
        }
        static report_ad_show(ad_pos_id, native_data) {
            if (this._ad_pos_id == ad_pos_id) {
            }
        }
        static show_native_icon(parent, ad_type, ad_pos_id, onLoad, onShow, onClose, onError) {
            this._ad_pos_id = ad_pos_id;
            this._ad_param = {
                ad_type: ad_type,
                ad_pos_id: ad_pos_id,
                onLoad: onLoad,
                onShow: onShow,
                onClose: onClose,
                onError: onError,
            };
            this.need_show = true;
            this.icon_parent = parent;
            let ad_id = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- ad_block ad_native_icon no configure in adv.csv");
                return;
            }
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["native_icon_switch"]) {
                if (this._business_config_data["native_icon_switch"].value[0] == 1) {
                    this.run_timer();
                }
                else {
                    console.log("igc----- the remote switch of native icon is close");
                }
            }
        }
        static hide_native_icon() {
            this.need_show = false;
            this.icon_parent = undefined;
            this.hide_native_icon_ui();
            this.destroy_timer();
        }
        static destroy_timer() {
            this.is_run_timer = false;
            this.timer_id && clearTimeout(this.timer_id);
            this.timer_id = undefined;
        }
        static load_native_icon(call_back) {
            let self = this;
            if (!syyx_adv_manager.can_show_first_native) {
                console.log("igc----- is in oppo first ad cd");
                return;
            }
            if (!this.need_show) {
                this.hide_native_icon_ui();
                return;
            }
            let native_data = this.get_native_data();
            if (native_data && native_data.state == e_ad_native_state.need_show) {
                this.show_native_icon_ui();
                return;
            }
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.web) {
                let data = new native_ad_data();
                data.id = igc.utils_manager.get_random_name();
                data.adPosId = this._ad_pos_id;
                data.adId = syyx_sdk_utils.get_random_number([100, 200]);
                data.adUnitId = syyx_sdk_utils.get_random_number([0, 100000]);
                data.imgUrlList = "https://h5-lg.syyx.com/coolbattle/share/share_img.jpg";
                data.title = "原生icon标题" + syyx_sdk_utils.get_random_number([100, 200]);
                data.desc = "原生icon描述" + syyx_sdk_utils.get_random_number([100, 200]);
                data.state = e_ad_native_state.need_show;
                data.native_type = e_ad_native_type.native_icon;
                this.add_native_data(data);
                this._ad_param.onLoad && this._ad_param.onLoad({}, data);
                this.show_native_icon_ui();
                call_back && call_back();
                return;
            }
            syyx_manager.create_ad(igc.e_ad_type.native, this._ad_pos_id, function on_load(param, ad_data_list) {
                console.log("igc-----syyx_adv_manager-------load_native_icon on_load", ad_data_list);
                if (ad_data_list == undefined || !ad_data_list[0]) {
                }
                else {
                    let length = 0;
                    if (syyx_const.syyx_sdk_channel === igc.e_channel_type.vivo_qg) {
                        length = ad_data_list.length - 1;
                    }
                    let imgUrlList;
                    if (syyx_const.syyx_sdk_channel === igc.e_channel_type.oppo_qg) {
                        imgUrlList = syyx_sdk_utils.format_remote_texture_url(ad_data_list[length].iconUrlList[0]);
                    }
                    else {
                        imgUrlList = syyx_sdk_utils.format_remote_texture_url(ad_data_list[length].imgUrlList[0]);
                    }
                    let data = new native_ad_data();
                    data.id = igc.utils_manager.get_random_name();
                    data.adPosId = self._ad_pos_id;
                    data.adId = syyx_adv_manager.get_channel_ad_id(self._ad_pos_id);
                    data.adUnitId = ad_data_list[length].adUnitId;
                    data.imgUrlList = imgUrlList;
                    data.title = ad_data_list[length].title;
                    data.desc = ad_data_list[length].desc;
                    data.state = e_ad_native_state.need_show;
                    data.native_type = e_ad_native_type.native_icon;
                    self.add_native_data(data);
                    self._ad_param.onLoad && self._ad_param.onLoad({}, data);
                    self.show_native_icon_ui();
                }
            }, function on_show() {
            }, function on_close(param, res) {
            }, function on_error(param, err) {
                console.error("igc-----syyx_adv_manager-------load_native_icon onError", err);
                let native_data = self.get_native_data();
                if (native_data) {
                    self.show_native_icon_ui();
                }
                self._ad_param.onError && self._ad_param.onError();
            });
        }
        static show_native_icon_ui() {
            let self = this;
            self.hide_native_icon_ui();
            if (!this.need_show) {
                console.log("igc----- the current interface doesn't need to show native icon so that do not refresh native data");
                return;
            }
            if (!this.icon_parent) {
                console.log("igc----- the native icon's parent node is not exist");
                return;
            }
            if (syyx_adv_manager.is_oppo_vivo()) {
                let native_data = this.get_native_data();
                if (this.need_show && native_data) {
                    syyx_manager.create_native_icon(function (view) {
                        view.show && view.show(self.icon_parent, native_data);
                    });
                }
            }
        }
        static hide_native_icon_ui() {
            if (syyx_adv_manager.is_oppo_vivo()) {
                syyx_manager.create_native_icon(function (view) {
                    view.hide && view.hide();
                });
            }
        }
        static get_native_data() {
            if (syyx_adv_manager.check_is_open_oppo_rule()) {
                if (syyx_adv_manager.check_native_data_list_is_reprot(this._native_data_list)) {
                    console.log("igc----- ad_native_icon use old load native data");
                    return syyx_adv_manager.get_min_order_native_data(this._native_data_list);
                }
                else {
                    return syyx_adv_manager.get_latest_native_data(this._native_data_list);
                }
            }
            else {
                return this._native_data_list[0];
            }
        }
        static add_native_data(native_data) {
            if (syyx_adv_manager.check_is_open_oppo_rule()) {
                for (let i in this._native_data_list) {
                    if (this._native_data_list[i].adUnitId == native_data.adUnitId) {
                        return;
                    }
                }
                let length = syyx_adv_manager.get_oppo_native_cache_max_length();
                if (this._native_data_list.length >= length) {
                    this._native_data_list.splice(0, 1);
                }
                this._native_data_list.push(native_data);
            }
            else {
                this._native_data_list[0] = native_data;
            }
        }
    }
    ad_native_icon._native_data_list = [];
    ad_native_icon._business_config_data = {};
    ad_native_icon.auto_update_cd = [20, 20];
    ad_native_icon.is_run_timer = false;
    ad_native_icon.need_show = true;
    window["oppoSdk"]["ad_native_icon"] = ad_native_icon;

    class ad_block {
        constructor() {
            this._business_config_data = {};
            this.auto_update_cd = [20, 20];
            this.is_run_timer = false;
            this.need_show = true;
        }
        run_timer() {
            let is_qq = syyx_const.syyx_sdk_channel === igc.e_channel_type.qq
                || syyx_const.syyx_sdk_channel === igc.e_channel_type.web;
            if (!this.is_run_timer && is_qq) {
                this.is_run_timer = true;
                this.timer_func();
            }
        }
        timer_func() {
            let self = this;
            this._business_config_data = syyx_manager.get_business_config();
            if (this._business_config_data && this._business_config_data["banner_cool_time"]) {
                this.auto_update_cd = this._business_config_data["banner_cool_time"].value;
            }
            this.hide_block();
            this.need_show = true;
            this.load_block();
            if (this._ad_param && this._ad_param.style && this._ad_param.style.auto_update) {
                let cd = syyx_sdk_utils.get_random_number(this.auto_update_cd);
                console.log("igc------syyx_ui_block next time to refresh right side block's  cd", cd);
                this.timer_id && clearTimeout(this.timer_id);
                this.timer_id = setTimeout(() => {
                    self.timer_func();
                }, cd * 1000);
            }
        }
        show_block(style, ad_type, ad_pos_id, onLoad, onShow, onClose, onError) {
            this._ad_pos_id = ad_pos_id;
            this._ad_param = {
                style: style,
                ad_type: ad_type,
                ad_pos_id: ad_pos_id,
                onLoad: onLoad,
                onShow: onShow,
                onClose: onClose,
                onError: onError,
            };
            let ad_id = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- ad_block native_interstitial_id no configure in adv.csv");
                return;
            }
            this.run_timer();
        }
        hide_block() {
            this.need_show = false;
            this.destroy_timer();
            syyx_manager.hide_ad(igc.e_ad_type.block, this._ad_pos_id);
        }
        destroy_timer() {
            this.is_run_timer = false;
            this.timer_id && clearTimeout(this.timer_id);
            this.timer_id = undefined;
        }
        load_block(call_back) {
            let self = this;
            if (!this.need_show) {
                this.hide_block();
                return;
            }
            let block_bottom_offset = 5;
            if (this._business_config_data && this._business_config_data["block_bottom_offset"]) {
                if (this._business_config_data["block_bottom_offset"].value[0] > 0) {
                    block_bottom_offset = this._business_config_data["block_bottom_offset"];
                }
            }
            let vertical_center_y = undefined;
            if (this._ad_param.style.vertical_center_y >= -888888) {
                vertical_center_y = this._ad_param.style.vertical_center_y;
            }
            let vertical_right = undefined;
            if (this._ad_param.style.vertical_right >= 0) {
                vertical_right = this._ad_param.style.vertical_right;
            }
            let ad_param = {
                ad_type: igc.e_ad_type.block,
                ad_id: syyx_adv_manager.get_channel_ad_id(this._ad_pos_id),
                ad_pos_id: this._ad_pos_id,
                ad_event: this._ad_pos_id,
                ad_scene: this._ad_pos_id,
                style: {
                    left: this._ad_param.style.left >= 20 ? this._ad_param.style.left : 20,
                    top: this._ad_param.style.top >= 50 ? this._ad_param.style.top : 50
                },
                vertical_center_y: vertical_center_y,
                vertical_right: vertical_right,
                bottom_offset: block_bottom_offset,
                size: this._ad_param.style.size || 5,
                orientation: this._ad_param.style.orientation || "landscape",
                onShow: function () {
                    console.error("igc-----syyx_adv_manager-------load_block onShow");
                    if (!self.need_show) {
                        self.hide_block();
                        return;
                    }
                },
                onError: function (param, err) {
                    console.error("igc-----syyx_adv_manager-------load_block onError", err);
                    self._ad_param.onError && self._ad_param.onError(param, err);
                }
            };
            igc.igc_main.instance.create_ad(ad_param);
        }
    }
    window["oppoSdk"]["ad_block"] = ad_block;

    class syyx_adv_manager {
        static load_adv_config(is_remote = false) {
            let self = this;
            if (!this.__inited) {
                this.__inited = true;
                this.login_timestamp = (new Date()).getTime();
                syyx_sdk_utils.load_resource(syyx_manager.__adv_config_file_path, function (data) {
                    if (syyx_const.syyx_sdk_publish === e_syyx_sdk_publish_type.in) {
                        self._adv_config_data = data;
                    }
                    else {
                        self._adv_config_data = syyx_sdk_utils.parse_csv(data, "id");
                    }
                    self.__adv_config_inited = true;
                    console.log("igc----- adv config has loaded", self._adv_config_data);
                    syyx_manager.pre_load_game_portal_box(e_ad_id.game_portal_box, null, null, null, null);
                    syyx_manager.preload_video();
                }, this);
                this.init_native_report_record();
            }
            this.init_first_banner_cd();
            if (is_remote && syyx_adv_manager.is_oppo_vivo()) {
                ad_native_inner_interstitial.auto_preload_native_inner_interstitial();
            }
        }
        static set_banner_height() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                syyx_apk_adv_manager.set_banner_height();
                return;
            }
            ad_oppo_banner.set_banner_height();
        }
        static init_native_report_record() {
            this.native_report_record[e_ad_native_type.native_inner_interstitial] = {
                "start_count": 0,
                "show_count": 0,
                "click_count": 0,
            };
            this.native_report_record[e_ad_native_type.native_banner] = {
                "start_count": 0,
                "show_count": 0,
                "click_count": 0,
            };
            this.native_report_record[e_ad_native_type.native_interstitial] = {
                "start_count": 0,
                "show_count": 0,
                "click_count": 0,
            };
            this.native_report_record[e_ad_native_type.native_icon] = {
                "start_count": 0,
                "show_count": 0,
                "click_count": 0,
            };
        }
        static init_first_banner_cd() {
            let self = this;
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.oppo_qg) {
                let first_banner_cd = 0;
                let business_config_data = syyx_manager.get_business_config();
                if (business_config_data && business_config_data["adv_banner_cd"]) {
                    first_banner_cd = business_config_data["adv_banner_cd"].value[0];
                    this.can_show_first_native = first_banner_cd <= 0;
                    console.log("igc----- the banner's cd in oppo is", first_banner_cd);
                }
                this.first_banner_timer_id && clearTimeout(this.first_banner_timer_id);
                this.first_banner_timer_id = setTimeout(() => {
                    self.can_show_first_native = true;
                    ad_banner.auto_show_banner();
                }, first_banner_cd * 1000);
            }
        }
        static add_native_data(native_data) {
            for (let i in this._native_data_cache) {
                if (this._native_data_cache[i].id == native_data.id) {
                    return;
                }
            }
            let length = this.get_oppo_native_cache_max_length();
            if (this._native_data_cache.length >= length) {
                this._native_data_cache.splice(0, 1);
            }
            this._native_data_cache.push(native_data);
        }
        static show_banner(ad_type, ad_pos_id, onLoad, onShow, onClose, onError, attrSet) {
            if (!this.__adv_config_inited) {
                return;
            }
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                console.log("进入apk的banner展示");
                syyx_apk_adv_manager.show_banner(ad_type, ad_pos_id, onLoad, onShow, onClose, onError, attrSet);
                return;
            }
            ad_banner.show_banner(ad_type, ad_pos_id, onLoad, onShow, onClose, onError);
        }
        static hide_banner() {
            if (!this.__adv_config_inited) {
                return;
            }
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                syyx_apk_adv_manager.hide_banner();
                return;
            }
            ad_banner.hide_banner();
        }
        static show_native_icon(parent, ad_type, ad_pos_id, onLoad, onShow, onClose, onError) {
            if (!this.__adv_config_inited) {
                return;
            }
            ad_native_icon.show_native_icon(parent, ad_type, ad_pos_id, onLoad, onShow, onClose, onError);
        }
        static hide_native_icon() {
            if (!this.__adv_config_inited) {
                return;
            }
            ad_native_icon.hide_native_icon();
        }
        static show_native_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError, attrSet) {
            if (!this.__adv_config_inited) {
                return;
            }
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                syyx_apk_adv_manager.show_native_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError, attrSet);
                return;
            }
            ad_native_interstitial.load_native_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError);
        }
        static show_video_interstitial(onLoad, onShow, onClose, onError) {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                syyx_apk_adv_manager.show_video_interstitial(onLoad, onShow, onClose, onError);
            }
        }
        static preload_native_inner_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError) {
            if (!this.__adv_config_inited) {
                return;
            }
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                syyx_apk_adv_manager.preload_native_inner_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError);
                return;
            }
            if (ad_native_inner_interstitial._can_auto_preload) {
                console.log("igc----- auto preload native_inner_interstitial is open!!");
                return;
            }
            ad_native_inner_interstitial.preload_native_inner_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError);
        }
        static set_on_click_inner_interstitial_btn(click_back) {
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.ipa) {
                click_back && click_back();
                return;
            }
            if (!this.__adv_config_inited) {
                return;
            }
            ad_native_inner_interstitial.set_on_click_inner_interstitial_btn(click_back);
        }
        static get_native_data(ignore_id = undefined) {
            if (!this.can_show_first_native) {
                console.log("igc ----- oppo's first native ad is in cd");
                return undefined;
            }
            let banner_limit = this.check_is_click_limit(e_ad_native_type.native_banner);
            let inner_limit = this.check_is_click_limit(e_ad_native_type.native_inner_interstitial);
            let interstitial_limit = this.check_is_click_limit(e_ad_native_type.native_interstitial);
            let cur_data_cache = [];
            for (let i in this._native_data_cache) {
                if (this._native_data_cache[i].id != ignore_id) {
                    if (this._native_data_cache[i].native_type == e_ad_native_type.native_banner && !banner_limit) {
                        cur_data_cache.push(this._native_data_cache[i]);
                    }
                    else if (this._native_data_cache[i].native_type == e_ad_native_type.native_inner_interstitial && !inner_limit) {
                        cur_data_cache.push(this._native_data_cache[i]);
                    }
                    else if (this._native_data_cache[i].native_type == e_ad_native_type.native_interstitial && !interstitial_limit) {
                        cur_data_cache.push(this._native_data_cache[i]);
                    }
                }
            }
            if (this.check_native_data_list_is_reprot(cur_data_cache)) {
                return this.get_min_order_native_data(cur_data_cache);
            }
            else {
                return this.get_latest_native_data(cur_data_cache);
            }
        }
        static remove_native_data(native_data) {
            for (let i in this._native_data_cache) {
                if (this._native_data_cache[i].id == native_data.id) {
                    console.log("igc----- syyx_adv_manager remove native_data:", native_data);
                    this._native_data_cache.splice(parseInt(i), 1);
                    return;
                }
            }
        }
        static get_local_native_data(ad_pos_id) {
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.apk) {
                return syyx_apk_adv_manager.get_local_native_data();
            }
            return this.get_native_data();
        }
        static report_ad_click(ad_pos_id, native_data) {
            if (!this.__adv_config_inited) {
                return;
            }
            let ad_id = this.get_channel_ad_id(native_data.adPosId);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- syyx_manager report_ad_click ad_id no configure in adv.csv");
                return;
            }
            if (!native_data) {
                console.log("igc----- syyx_adv_mamager report_ad_click native_data is null");
                return;
            }
            console.log("igc ----- has been in report ad click");
            if (native_data.state != e_ad_native_state.show) {
                console.log("igc----- syyx_adv_mamager report_ad_click native_data state is not e_ad_native_state.show");
                return;
            }
            let report_data = this.native_report_record[native_data.native_type];
            report_data.click_count = Math.min(++report_data.click_count, report_data.show_count);
            let ad_unit_id = native_data.adUnitId;
            let param = {
                ad_id: ad_id,
                ad_unit_id: ad_unit_id,
                ad_type: igc.e_ad_type.native,
                ad_pos_id: native_data.adPosId,
                ad_event: ad_id,
                ad_scene: ad_id,
                sub_ad_type: igc.e_ad_native_type.native_banner_dialog
            };
            igc.igc_main.instance.report_ad_click(param);
            this.remove_native_data(native_data);
            ad_banner.report_ad_click(ad_pos_id, native_data);
            ad_native_icon.report_ad_click(ad_pos_id, native_data);
            ad_native_interstitial.report_ad_click(ad_pos_id, native_data);
            ad_native_inner_interstitial.report_ad_click(ad_pos_id, native_data);
        }
        static report_ad_show(ad_pos_id, native_data) {
            if (!this.__adv_config_inited) {
                return;
            }
            let ad_id = syyx_adv_manager.get_channel_ad_id(native_data.adPosId);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- syyx_manager report_ad_show ad_id no configure in adv.csv");
                return;
            }
            if (!native_data) {
                console.log("igc----- syyx_adv_mamager report_ad_show native_data is null");
                return;
            }
            let max_order = syyx_adv_manager.get_native_data_list_max_order();
            native_data.order = max_order + 1;
            if (native_data.state == e_ad_native_state.need_show || syyx_const.syyx_sdk_channel === igc.e_channel_type.hw_qg) {
                if (native_data.state == e_ad_native_state.need_show) {
                    let report_data = this.native_report_record[native_data.native_type];
                    report_data.show_count++;
                    report_data.start_count++;
                }
                let param = {
                    ad_id: ad_id,
                    ad_unit_id: native_data.adUnitId,
                    ad_type: igc.e_ad_type.native,
                    ad_pos_id: native_data.adPosId,
                    ad_event: ad_id,
                    ad_scene: ad_id,
                    sub_ad_type: igc.e_ad_native_type.native_banner_dialog
                };
                igc.igc_main.instance.report_ad_show(param);
                native_data.state = e_ad_native_state.show;
            }
            ad_banner.report_ad_show(ad_pos_id, native_data);
            ad_native_icon.report_ad_show(ad_pos_id, native_data);
            ad_native_interstitial.report_ad_show(ad_pos_id, native_data);
            ad_native_inner_interstitial.report_ad_show(ad_pos_id, native_data);
        }
        static set_normal_banner_switch(value) {
            ad_banner.set_normal_banner_switch(value);
        }
        static get_qq_banner_top_offset() {
            let business_config = syyx_manager.get_business_config();
            if (business_config && business_config["banner_top_offset"]) {
                if (business_config["banner_top_offset"].value) {
                    let offset = business_config["banner_top_offset"].value;
                    return offset[0] + Math.floor(Math.random() * (offset[1] - offset[0]));
                }
            }
            return 0;
        }
        static show_block(style, ad_type, ad_pos_id, onLoad, onShow, onClose, onError) {
            if (!this._block_instance[ad_pos_id]) {
                this._block_instance[ad_pos_id] = new ad_block();
            }
            if (!this.__adv_config_inited) {
                return;
            }
            this._block_instance[ad_pos_id].show_block(style, ad_type, ad_pos_id, onLoad, onShow, onClose, onError);
        }
        static hide_block(ad_pos_id) {
            if (!this.__adv_config_inited) {
                return;
            }
            if (this._block_instance[ad_pos_id]) {
                this._block_instance[ad_pos_id].hide_block();
            }
        }
        static hide_all_block() {
            if (!this.__adv_config_inited) {
                return;
            }
            for (let i in this._adv_config_data) {
                if (this._adv_config_data[i].adv_type == igc.e_ad_type.block) {
                    let ad_pos_id = this._adv_config_data[i].id;
                    if (this._block_instance[ad_pos_id]) {
                        this._block_instance[ad_pos_id].hide_block();
                    }
                }
            }
        }
        static check_is_open_oppo_rule() {
            let business_config = syyx_manager.get_business_config();
            if (syyx_const.syyx_sdk_channel === igc.e_channel_type.oppo_qg || syyx_const.syyx_sdk_channel === igc.e_channel_type.vivo_qg || syyx_const.syyx_sdk_channel === igc.e_channel_type.web) {
                return true;
            }
            return false;
        }
        static add_native_show_count() {
            if (this.check_is_open_oppo_rule()) {
                this.cur_native_show_count++;
            }
        }
        static get_play_game_time() {
            let cur_timestamp = (new Date()).getTime();
            return (cur_timestamp - this.login_timestamp) / 1000;
        }
        static check_is_show_count_limit() {
            if (!this.check_is_open_oppo_rule()) {
                return false;
            }
            let oppo_native_show_limit = [60, 2];
            let business_config = syyx_manager.get_business_config();
            if (business_config && business_config["oppo_native_show_limit"] && business_config["oppo_native_show_limit"].value) {
                oppo_native_show_limit = business_config["oppo_native_show_limit"].value;
            }
            let play_game_time = this.get_play_game_time();
            let cur_show_limit = 2;
            if (play_game_time >= 0) {
                cur_show_limit = (Math.floor(play_game_time / oppo_native_show_limit[0]) + 1) * oppo_native_show_limit[1];
            }
            return syyx_adv_manager.cur_native_show_count >= cur_show_limit;
        }
        static get_oppo_native_cache_max_length() {
            let length = 20;
            let business_config = syyx_manager.get_business_config();
            if (business_config && business_config["oppo_native_cache_length"] && business_config["oppo_native_cache_length"].value) {
                length = business_config["oppo_native_cache_length"].value[0];
            }
            return length || 20;
        }
        static check_native_data_list_is_reprot(native_data_list) {
            if (native_data_list.length > 0) {
                for (let i in native_data_list) {
                    if (native_data_list[i].state != e_ad_native_state.show) {
                        return false;
                    }
                }
            }
            return true;
        }
        static get_latest_native_data(native_data_list) {
            for (let i in native_data_list) {
                if (native_data_list[i].state == e_ad_native_state.need_show) {
                    return native_data_list[i];
                }
            }
            if (native_data_list.length > 0) {
                return native_data_list[native_data_list.length - 1];
            }
            return undefined;
        }
        static get_native_data_list_max_order(native_data_list = undefined) {
            let list = [];
            let order = 0;
            if (native_data_list && native_data_list.length > 0) {
                list = native_data_list;
            }
            else {
                list = this._native_data_cache;
            }
            if (list.length > 0) {
                for (let i in list) {
                    if (list[i].order > order) {
                        order = list[i].order;
                    }
                }
            }
            return order || 0;
        }
        static get_min_order_native_data(native_data_list) {
            let data = undefined;
            let length = native_data_list.length;
            if (length > 0) {
                for (let i in native_data_list) {
                    if (!data || native_data_list[i].order <= data.order) {
                        data = native_data_list[i];
                    }
                }
            }
            return data;
        }
        static check_is_click_limit(native_ad_type) {
            if (!this.__adv_config_inited) {
                return false;
            }
            let self = this;
            let is_limit = false;
            let report_data = this.native_report_record[native_ad_type];
            if (this.native_click_state[native_ad_type] == e_ad_native_click_pro_type.cooling) {
                return true;
            }
            let cur_click_pro = report_data.click_count / report_data.show_count;
            let start_count = 10;
            let limit_pro = 1;
            let cool_time = 60;
            let business_config = syyx_manager.get_business_config();
            if (business_config && business_config["native_interstitial_click_pro_limit"]) {
                if (native_ad_type == e_ad_native_type.native_banner) {
                    start_count = business_config["native_banner_click_pro_limit"].value[0];
                    limit_pro = business_config["native_banner_click_pro_limit"].value[1];
                    cool_time = business_config["native_banner_click_pro_limit"].value[2];
                }
                else if (native_ad_type == e_ad_native_type.native_inner_interstitial) {
                    start_count = business_config["native_inner_click_pro_limit"].value[0];
                    limit_pro = business_config["native_inner_click_pro_limit"].value[1];
                    cool_time = business_config["native_inner_click_pro_limit"].value[2];
                }
                else if (native_ad_type == e_ad_native_type.native_interstitial) {
                    start_count = business_config["native_interstitial_click_pro_limit"].value[0];
                    limit_pro = business_config["native_interstitial_click_pro_limit"].value[1];
                    cool_time = business_config["native_interstitial_click_pro_limit"].value[2];
                }
                if (report_data.start_count > 0 && report_data.start_count % start_count == 0) {
                    is_limit = cur_click_pro >= limit_pro;
                }
            }
            else {
                return false;
            }
            if (!this.native_click_state[native_ad_type]) {
                this.native_click_state[native_ad_type] = e_ad_native_click_pro_type.active;
            }
            if (is_limit && this.native_click_state[native_ad_type] == e_ad_native_click_pro_type.active) {
                this.native_click_state[native_ad_type] = e_ad_native_click_pro_type.cooling;
                console.log("igc----- syyx_adv_manager run native click limit cool timer!!!", cool_time);
                setTimeout(function () {
                    self.native_click_state[native_ad_type] = e_ad_native_click_pro_type.active;
                    report_data.start_count = 0;
                }, cool_time * 1000);
            }
            return is_limit;
        }
        static run_finger_close_banner_rule() {
            let self = this;
            let business_config = syyx_manager.get_business_config();
            if (business_config["finger_close_banner_switch"].value[0] == 1) {
                let cd = business_config["finger_close_banner_switch"].value[1] || 60;
                this.hide_banner();
                this.limit_show_banner = true;
                setTimeout(function () {
                    self.limit_show_banner = false;
                    ad_banner.auto_show_banner();
                }, cd * 1000);
            }
        }
        static get_oppo_banner_show_update_time() {
            let play_game_time = syyx_adv_manager.get_play_game_time();
            let update_time_arr = undefined;
            let _business_config_data = syyx_manager.get_business_config();
            if (_business_config_data && _business_config_data["oppo_banner_cool_time"]) {
                update_time_arr = _business_config_data["oppo_banner_cool_time"].value;
            }
            if (!update_time_arr || update_time_arr.length <= 0) {
                return 10;
            }
            let cd = 0;
            let last_arr = [];
            for (let i in update_time_arr) {
                last_arr = update_time_arr[i];
                if (update_time_arr[i][0] <= play_game_time && update_time_arr[i][1] >= play_game_time) {
                    cd = update_time_arr[i][2];
                    break;
                }
            }
            return cd || last_arr[2];
        }
        static get_channel_ad_id(ad_pos_id) {
            if (!this.__adv_config_inited) {
                return "";
            }
            try {
                if (syyx_const.syyx_sdk_channel === igc.e_channel_type.oppo_qg) {
                    return this._adv_config_data[ad_pos_id].oppo_adv_id;
                }
                else if (syyx_const.syyx_sdk_channel === igc.e_channel_type.vivo_qg) {
                    return this._adv_config_data[ad_pos_id].vivo_adv_id;
                }
                else if (syyx_const.syyx_sdk_channel === igc.e_channel_type.tt) {
                    return this._adv_config_data[ad_pos_id].tt_adv_id;
                }
                else if (syyx_const.syyx_sdk_channel === igc.e_channel_type.qq) {
                    return this._adv_config_data[ad_pos_id].qq_adv_id;
                }
                else if (syyx_const.syyx_sdk_channel === igc.e_channel_type.wx) {
                    return this._adv_config_data[ad_pos_id].wx_adv_id;
                }
                else if (syyx_const.syyx_sdk_channel === igc.e_channel_type.apk) {
                    if (syyx_apk_adv_manager.is_oppo_apk()) {
                        return this._adv_config_data[ad_pos_id].oppo_apk_adv_id;
                    }
                    else if (syyx_apk_adv_manager.is_vivo_apk()) {
                        return this._adv_config_data[ad_pos_id].vivo_apk_adv_id;
                    }
                    else if (syyx_apk_adv_manager.is_xm_apk()) {
                        return this._adv_config_data[ad_pos_id].xm_apk_adv_id;
                    }
                    return this._adv_config_data[ad_pos_id].oppo_apk_adv_id;
                }
                else if (syyx_const.syyx_sdk_channel === igc.e_channel_type.web) {
                    return "web_ad_id";
                }
            }
            catch (error) {
                console.error("adv.csv do not have the ad_id of the ad_pos_id: " + ad_pos_id);
            }
        }
        static is_inited() {
            return this.__inited;
        }
        static is_oppo_vivo() {
            return syyx_const.syyx_sdk_channel === igc.e_channel_type.oppo_qg || syyx_const.syyx_sdk_channel === igc.e_channel_type.vivo_qg ||
                syyx_const.syyx_sdk_channel === igc.e_channel_type.hw_qg || syyx_const.syyx_sdk_channel === igc.e_channel_type.web;
        }
    }
    syyx_adv_manager._native_data_cache = [];
    syyx_adv_manager.__adv_config_inited = false;
    syyx_adv_manager._block_instance = {};
    syyx_adv_manager.cur_native_show_count = 0;
    syyx_adv_manager.login_timestamp = 0;
    syyx_adv_manager.__inited = false;
    syyx_adv_manager.can_show_first_native = true;
    syyx_adv_manager.limit_show_banner = false;
    syyx_adv_manager.first_banner_timer_id = undefined;
    syyx_adv_manager.native_click_state = {};
    syyx_adv_manager.native_report_record = {};
    window["oppoSdk"]["syyx_adv_manager"] = syyx_adv_manager;

    class ad_banner_oppo_apk {
        constructor() {
            this._business_config_data = {};
            this.update_cd = 5;
            this.is_run_timer = false;
            this.need_show = true;
            this.banner_showing = false;
            this.banner_show_time = 0;
        }
        run_timer() {
            let self = this;
            console.log("mijia ---- is run timer is " + this.is_run_timer);
            if (!this.is_run_timer) {
                this.is_run_timer = true;
                this._business_config_data = syyx_manager.get_business_config();
                this.update_cd = syyx_adv_manager.get_oppo_banner_show_update_time();
                this.load_native_banner();
                setInterval(() => {
                    self.banner_showing && self.banner_show_time++;
                    console.log("mijia ---- banner_show_time is " + self.banner_show_time);
                    if (self.banner_show_time > 0 &&
                        self.banner_show_time % self.update_cd == 0) {
                        console.log("igc----- update_native_banner");
                        self.update_cd = syyx_adv_manager.get_oppo_banner_show_update_time();
                        self.banner_show_time = 0;
                        self.load_native_banner();
                    }
                }, 1000);
            }
            else {
                console.log("mijia ---- 第二次展示banner");
                self.banner_show_time = 0;
                self.load_native_banner();
            }
        }
        show_banner(ad_type, ad_pos_id, onLoad, onShow, onClose, onError, attrSet) {
            this.need_show = true;
            this._ad_param = {
                ad_type: ad_type,
                ad_pos_id: ad_pos_id,
                onLoad: onLoad,
                onShow: onShow,
                onClose: onClose,
                onError: onError,
                attrSet: attrSet
            };
            console.log("mijia ---- in show banner ");
            this.run_timer();
        }
        load_native_banner(ad_pos_id = e_ad_id.native_banner_1) {
            let self = this;
            this.banner_showing = false;
            this.hide_native_banner();
            this.hide_normal_banner();
            let ad_id = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- ad_banner_oppo_apk native_banner_id no configure in adv.csv" +
                    ad_pos_id);
                this.load_native_banner_error(ad_pos_id);
                return;
            }
            let param = {
                ad_type: igc.e_ad_type.native,
                ad_id: ad_id,
                ad_pos_id: ad_pos_id,
                sub_ad_type: 0,
                ad_event: ad_id,
                ad_scene: ad_id,
                auto_show: true,
                attrSet: this._ad_param.attrSet,
                onLoad: function () {
                    console.log("igc-----ad_banner_oppo_apk load_native_banner onLoad2", ad_pos_id);
                    self._ad_param && self._ad_param.onLoad && self._ad_param.onLoad();
                },
                onShow: function () {
                    if (self.need_show) {
                        console.log("igc-----ad_banner_oppo_apk load_native_banner onShow", ad_pos_id);
                        self._ad_param && self._ad_param.onShow && self._ad_param.onShow();
                        self.banner_showing = true;
                    }
                    else {
                        self.hide_banner();
                    }
                },
                onClose: function (param, res) {
                    console.log("igc-----ad_banner_oppo_apk load_native_banner onClose", ad_pos_id);
                    self._ad_param && self._ad_param.onClose && self._ad_param.onClose();
                },
                onError: function (param, err) {
                    console.log("igc-----ad_banner_oppo_apk load_native_banner onError:" +
                        ad_pos_id +
                        JSON.stringify(err));
                    self.load_native_banner_error(ad_pos_id);
                    self._ad_param && self._ad_param.onError && self._ad_param.onError();
                },
                onClick: function () {
                    console.log("igc-----ad_banner_oppo_apk load_native_banner onClick:" + ad_pos_id);
                    self.click_auto_update();
                },
                apk_sub_ad_type: igc.e_native_advance_sub_type.banner,
                extra: "2222",
            };
            igc.igc_main.instance.create_ad(param);
        }
        load_native_banner_error(ad_pos_id) {
            switch (ad_pos_id) {
                case e_ad_id.native_banner_1:
                    this.load_native_banner(e_ad_id.native_banner_2);
                    break;
                case e_ad_id.native_banner_2:
                    this.load_native_banner(e_ad_id.native_banner_3);
                    break;
                case e_ad_id.native_banner_3:
                    this.show_old_native_data_banner(true);
                    break;
            }
        }
        show_old_native_data_banner(ignore_last = true) {
            let self = this;
            let ad_id = syyx_adv_manager.get_channel_ad_id(e_ad_id.native_banner_1);
            let parameter = {
                use_cache: true,
                ignore_last: ignore_last,
            };
            let param = {
                ad_type: igc.e_ad_type.native,
                ad_id: ad_id,
                ad_pos_id: e_ad_id.native_banner_1,
                sub_ad_type: 0,
                ad_event: ad_id,
                ad_scene: ad_id,
                auto_show: true,
                parameter: parameter,
                attrSet: this._ad_param.attrSet,
                onLoad: function () {
                    console.log("igc-----ad_banner_oppo_apk show_old_native_data_banner onLoad");
                    self._ad_param && self._ad_param.onLoad && self._ad_param.onLoad();
                },
                onShow: function () {
                    if (self.need_show) {
                        console.log("igc-----ad_banner_oppo_apk show_old_native_data_banner onShow");
                        self._ad_param && self._ad_param.onShow && self._ad_param.onShow();
                        self.banner_showing = true;
                    }
                    else {
                        self.hide_banner();
                    }
                },
                onClose: function (param, res) {
                    console.log("igc-----ad_banner_oppo_apk show_old_native_data_banner onClose");
                    self._ad_param && self._ad_param.onClose && self._ad_param.onClose();
                },
                onError: function (param, err) {
                    console.log("igc-----ad_banner_oppo_apk show_old_native_data_banner onError:" +
                        JSON.stringify(err));
                    self._ad_param && self._ad_param.onError && self._ad_param.onError();
                    ignore_last && self.load_normal_banner();
                },
                onClick: function () {
                    console.log("igc-----ad_banner_oppo_apk show_old_native_data_banner onClick:");
                    self.click_auto_update();
                },
                apk_sub_ad_type: igc.e_native_advance_sub_type.banner,
                extra: "",
            };
            igc.igc_main.instance.show_ad(param);
        }
        click_auto_update() {
            console.log("igc-----ad_inner_interstitial_oppo_apk click_auto_update");
            this.banner_show_time = 0;
            this.load_native_banner();
        }
        load_normal_banner() {
            let self = this;
            console.log("igc show_normal_banner-----------------------------------------------------------------");
            let ad_id = syyx_adv_manager.get_channel_ad_id(e_ad_id.banner_hall);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- ad_banner_oppo_apk normal_banner_id no configure in adv.csv");
                this.load_normal_banner_error();
                return;
            }
            let param = {
                ad_type: igc.e_ad_type.banner,
                ad_id: ad_id,
                ad_pos_id: e_ad_id.banner_hall,
                sub_ad_type: 0,
                ad_event: ad_id,
                ad_scene: ad_id,
                auto_show: true,
                onLoad: function () {
                    console.log("igc-----load_normal_banner show_normal_banner onLoad");
                    self._ad_param && self._ad_param.onLoad && self._ad_param.onLoad();
                },
                onShow: function (param) {
                    if (self.need_show) {
                        self.banner_showing = true;
                        console.log("igc-----load_normal_banner show_normal_banner onShow");
                        self._ad_param && self._ad_param.onShow && self._ad_param.onShow();
                    }
                    else {
                        self.hide_banner();
                    }
                },
                onClose: function (param, res) {
                    console.log("igc-----load_normal_banner show_normal_banner onClose");
                    self._ad_param && self._ad_param.onClose && self._ad_param.onClose();
                },
                onError: function (param, err) {
                    console.log("igc-----load_normal_banner show_normal_banner onError:" +
                        JSON.stringify(err));
                    self._ad_param && self._ad_param.onError && self._ad_param.onError();
                    self.load_normal_banner_error();
                },
                onClick: function () {
                    console.log("igc-----load_normal_banner show_normal_banner onClick:");
                },
                extra: "",
            };
            igc.igc_main.instance.create_ad(param);
        }
        load_normal_banner_error() {
            this.show_old_native_data_banner(false);
        }
        hide_banner() {
            this.need_show = false;
            this.banner_showing = false;
            this.hide_native_banner();
            this.hide_normal_banner();
        }
        hide_native_banner() {
            let list = [
                e_ad_id.native_banner_1,
                e_ad_id.native_banner_2,
                e_ad_id.native_banner_3,
            ];
            for (let i in list) {
                let ad_id = syyx_adv_manager.get_channel_ad_id(list[i]);
                let param = {
                    ad_type: igc.e_ad_type.native,
                    ad_id: ad_id,
                    ad_pos_id: list[i],
                    sub_ad_type: 0,
                    ad_event: ad_id,
                    ad_scene: ad_id,
                    apk_sub_ad_type: igc.e_native_advance_sub_type.banner,
                };
                igc.igc_main.instance.hide_ad(param);
            }
        }
        hide_normal_banner() {
            syyx_sdk_api.destroy_ad(igc.e_ad_type.banner, e_ad_id.banner_hall);
        }
    }
    window["oppoSdk"]["ad_banner_oppo_apk"] = ad_banner_oppo_apk;

    class ad_inner_interstitial_oppo_apk {
        constructor() {
            this._business_config_data = {};
        }
        preload_native_inner_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError) {
            this._ad_param = {
                ad_type: ad_type,
                ad_pos_id: ad_pos_id,
                onLoad: onLoad,
                onShow: onShow,
                onClose: onClose,
                onError: onError,
            };
            this.load_native_inner_interstitial(e_ad_id.native_inner_interstitial_1);
        }
        load_native_inner_interstitial(ad_pos_id = e_ad_id.native_inner_interstitial_1, auto = false, is_click_update = false) {
            let self = this;
            let ad_id = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- ad_inner_interstitial_oppo_apk native_inner_interstitial_id no configure in adv.csv" + ad_pos_id);
                this.load_native_inner_interstitial_error(ad_pos_id);
                return;
            }
            console.log("igc ---- load inner interstitial ad pos id is" + ad_pos_id);
            let parameter = {
                use_cache: true,
                ignore_last: false
            };
            let param = {
                ad_type: igc.e_ad_type.native,
                ad_id: ad_id,
                ad_pos_id: ad_pos_id,
                sub_ad_type: 0,
                ad_event: ad_id,
                ad_scene: ad_id,
                auto_show: auto,
                parameter: parameter,
                attrSet: this._ad_param.attrSet,
                onLoad: function () {
                    console.log("igc-----ad_inner_interstitial_oppo_apk load_native_inner_interstitial onLoad" + ad_pos_id);
                    self._ad_param && self._ad_param.onLoad && self._ad_param.onLoad();
                },
                onShow: function () {
                    console.log("igc-----ad_inner_interstitial_oppo_apk load_native_inner_interstitial onShow" + ad_pos_id);
                    self._ad_param && self._ad_param.onShow && self._ad_param.onShow();
                },
                onClose: function (param, res) {
                    console.log("igc-----ad_inner_interstitial_oppo_apk load_native_inner_interstitial onClose" + ad_pos_id);
                    self._ad_param && self._ad_param.onClose && self._ad_param.onClose();
                },
                onError: function (param, err) {
                    console.log("igc-----ad_inner_interstitial_oppo_apk load_native_inner_interstitial onError:" + ad_pos_id + JSON.stringify(err));
                    self.load_native_inner_interstitial_error(ad_pos_id, auto, is_click_update);
                    self._ad_param && self._ad_param.onError && self._ad_param.onError();
                    self._ad_param && self._ad_param.onClose && self._ad_param.onClose();
                },
                onClick: function (param) {
                    console.log("igc-----ad_inner_interstitial_oppo_apk load_native_inner_interstitial onClick:" + ad_pos_id);
                    self._ad_param && self._ad_param.onClick && self._ad_param.onClick();
                    self.click_auto_update();
                },
                apk_sub_ad_type: igc.e_native_advance_sub_type.inner_interstitial,
                extra: "",
            };
            igc.igc_main.instance.create_ad(param);
        }
        load_native_inner_interstitial_error(ad_pos_id, auto = false, is_click_update = false) {
            switch (ad_pos_id) {
                case e_ad_id.native_inner_interstitial_1:
                    console.log("igc----- native inner now has loaded e_ad_id.native_inner_interstitial_1 " + e_ad_id.native_inner_interstitial_1);
                    this.load_native_inner_interstitial(e_ad_id.native_inner_interstitial_2, auto, is_click_update);
                    break;
                case e_ad_id.native_inner_interstitial_2:
                    console.log("igc----- native inner now has loaded e_ad_id.native_inner_interstitial_2 " + e_ad_id.native_inner_interstitial_2);
                    this.load_native_inner_interstitial(e_ad_id.native_inner_interstitial_3, auto, is_click_update);
                    break;
                case e_ad_id.native_inner_interstitial_3:
                    console.log("igc----- native inner now has load e_ad_id.native_inner_interstitial_3 " + e_ad_id.native_inner_interstitial_3);
                    this.show_old_native_data_inner_interstitial(false);
                    break;
            }
        }
        show_native_inner_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError, onClick, attrSet) {
            this._ad_param = {
                ad_type: ad_type,
                ad_pos_id: ad_pos_id,
                onLoad: onLoad,
                onShow: onShow,
                onClose: onClose,
                onError: onError,
                onClick: onClick,
                attrSet: attrSet,
            };
            this.show_old_native_data_inner_interstitial(false);
        }
        show_old_native_data_inner_interstitial(ignore_last = false) {
            if (ignore_last) {
                console.log("mijia --- show old data in inner interstitial with ignore_last");
            }
            else {
                console.log("mijia --- show old data in inner interstitial without ignore_last");
            }
            let self = this;
            let ad_id = syyx_adv_manager.get_channel_ad_id(e_ad_id.native_inner_interstitial_1);
            let parameter = {
                use_cache: true,
                ignore_last: ignore_last
            };
            let param = {
                ad_type: igc.e_ad_type.native,
                ad_id: ad_id,
                ad_pos_id: e_ad_id.native_inner_interstitial_1,
                sub_ad_type: 0,
                ad_event: ad_id,
                ad_scene: ad_id,
                auto_show: true,
                parameter: parameter,
                attrSet: this._ad_param.attrSet,
                onLoad: function () {
                    console.log("igc-----ad_inner_interstitial_oppo_apk show_old_native_data_inner_interstitial onLoad");
                    self._ad_param && self._ad_param.onLoad && self._ad_param.onLoad();
                },
                onShow: function () {
                    console.log("igc-----ad_inner_interstitial_oppo_apk show_old_native_data_inner_interstitial onShow");
                    self._ad_param && self._ad_param.onShow && self._ad_param.onShow();
                },
                onClose: function (param, res) {
                    console.log("igc-----ad_inner_interstitial_oppo_apk show_old_native_data_inner_interstitial onClose");
                    self._ad_param && self._ad_param.onClose && self._ad_param.onClose();
                },
                onError: function (param, err) {
                    console.log("igc-----ad_inner_interstitial_oppo_apk show_old_native_data_inner_interstitial onError:" + JSON.stringify(err));
                    self._ad_param && self._ad_param.onError && self._ad_param.onError();
                    self._ad_param && self._ad_param.onClose && self._ad_param.onClose();
                },
                onClick: function (param) {
                    console.log("igc-----ad_inner_interstitial_oppo_apk show_old_native_data_inner_interstitial onClick:");
                    self.click_auto_update();
                    self._ad_param && self._ad_param.onClick && self._ad_param.onClick();
                },
                apk_sub_ad_type: igc.e_native_advance_sub_type.inner_interstitial,
                extra: "",
            };
            igc.igc_main.instance.show_ad(param);
        }
        click_auto_update() {
            this.hide_native_inner_interstitial();
            console.log('igc-----ad_inner_interstitial_oppo_apk click_auto_update');
            this.load_native_inner_interstitial(e_ad_id.native_inner_interstitial_1, true, true);
        }
        hide_native_inner_interstitial() {
            let list = [e_ad_id.native_inner_interstitial_1, e_ad_id.native_inner_interstitial_2, e_ad_id.native_inner_interstitial_3];
            for (let i in list) {
                let ad_id = syyx_adv_manager.get_channel_ad_id(list[i]);
                let param = {
                    ad_type: igc.e_ad_type.native,
                    ad_id: ad_id,
                    ad_pos_id: list[i],
                    sub_ad_type: 0,
                    ad_event: ad_id,
                    ad_scene: ad_id,
                    apk_sub_ad_type: igc.e_native_advance_sub_type.inner_interstitial,
                };
                igc.igc_main.instance.hide_ad(param);
            }
        }
    }
    window["oppoSdk"]["ad_inner_interstitial_oppo_apk"] = ad_inner_interstitial_oppo_apk;

    class ad_interstitial_oppo_apk {
        constructor() {
            this._business_config_data = {};
        }
        show_native_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError, attrSet) {
            this._ad_param = {
                ad_type: ad_type,
                ad_pos_id: ad_pos_id,
                onLoad: onLoad,
                onShow: onShow,
                onClose: onClose,
                onError: onError,
            };
            console.log("mijia ------ now is show native interstitital");
            this.load_native_interstitial();
        }
        load_native_interstitial(ad_pos_id = e_ad_id.native_interstitial_1) {
            let self = this;
            let ad_id = syyx_adv_manager.get_channel_ad_id(ad_pos_id);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- ad_interstitial_oppo_apk native_interstitial_id no configure in adv.csv" +
                    ad_pos_id);
                this.load_native_interstitial_error(ad_pos_id);
                return;
            }
            let param = {
                ad_type: igc.e_ad_type.native,
                ad_id: ad_id,
                ad_pos_id: ad_pos_id,
                sub_ad_type: 0,
                ad_event: ad_id,
                ad_scene: ad_id,
                auto_show: true,
                onLoad: function () {
                    console.log("igc-----ad_interstitial_oppo_apk load_native_interstitial onLoad", ad_pos_id);
                    self._ad_param && self._ad_param.onLoad && self._ad_param.onLoad();
                },
                onShow: function () {
                    console.log("igc-----ad_interstitial_oppo_apk load_native_interstitial onShow", ad_pos_id);
                    self._ad_param && self._ad_param.onShow && self._ad_param.onShow();
                },
                onClose: function (param, res) {
                    console.log("igc-----ad_interstitial_oppo_apk load_native_interstitial onClose", ad_pos_id);
                    self._ad_param && self._ad_param.onClose && self._ad_param.onClose();
                },
                onError: function (param, err) {
                    console.log("igc-----ad_interstitial_oppo_apk load_native_interstitial onError:" +
                        ad_pos_id +
                        JSON.stringify(err));
                    self.load_native_interstitial_error(ad_pos_id);
                    self._ad_param && self._ad_param.onError && self._ad_param.onError();
                },
                onClick: function () {
                    console.log("igc-----ad_interstitial_oppo_apk load_native_interstitial onClick:" +
                        ad_pos_id);
                    let sw = syyx_manager.get_business_data_by_key('native_interstitial_report_click_update_switch');
                    console.log('kuluo1111 native_interstitial_report_click_update_switch----->' + sw);
                    if (sw && sw[0] == 1) {
                        self.click_auto_update();
                    }
                },
                apk_sub_ad_type: igc.e_native_advance_sub_type.interstitial,
                extra: "",
            };
            igc.igc_main.instance.create_ad(param);
        }
        load_native_interstitial_error(ad_pos_id) {
            switch (ad_pos_id) {
                case e_ad_id.native_interstitial_1:
                    this.load_native_interstitial(e_ad_id.native_interstitial_2);
                    break;
                case e_ad_id.native_interstitial_2:
                    this.load_native_interstitial(e_ad_id.native_interstitial_3);
                    break;
                case e_ad_id.native_interstitial_3:
                    this.show_old_native_data_interstitial(true);
                    break;
            }
        }
        show_old_native_data_interstitial(ignore_last = true) {
            let self = this;
            let ad_id = syyx_adv_manager.get_channel_ad_id(e_ad_id.native_interstitial_1);
            let parameter = {
                use_cache: true,
                ignore_last: ignore_last,
            };
            let param = {
                ad_type: igc.e_ad_type.native,
                ad_id: ad_id,
                ad_pos_id: e_ad_id.native_interstitial_1,
                sub_ad_type: 0,
                ad_event: ad_id,
                ad_scene: ad_id,
                auto_show: true,
                parameter: parameter,
                onLoad: function () {
                    console.log("igc-----ad_interstitial_oppo_apk show_old_native_data_interstitial onLoad");
                    self._ad_param && self._ad_param.onLoad && self._ad_param.onLoad();
                },
                onShow: function (param) {
                    console.log("igc-----ad_interstitial_oppo_apk show_old_native_data_interstitial onShow");
                    self._ad_param && self._ad_param.onShow && self._ad_param.onShow();
                },
                onClose: function (param, res) {
                    console.log("igc-----ad_interstitial_oppo_apk show_old_native_data_interstitial onClose");
                    self._ad_param && self._ad_param.onClose && self._ad_param.onClose();
                },
                onError: function (param, err) {
                    console.log("igc-----ad_interstitial_oppo_apk show_old_native_data_interstitial onError:" +
                        JSON.stringify(err));
                    self._ad_param && self._ad_param.onError && self._ad_param.onError();
                    ignore_last && self.load_video_interstitial();
                },
                onClick: function () {
                    console.log("igc-----ad_interstitial_oppo_apk show_old_native_data_interstitial onClick:");
                    let sw = syyx_manager.get_business_data_by_key('native_interstitial_report_click_update_switch');
                    console.log('kuluo222 native_interstitial_report_click_update_switch----->' + sw);
                    if (sw && sw[0] == 1) {
                        self.click_auto_update();
                    }
                },
                apk_sub_ad_type: igc.e_native_advance_sub_type.interstitial,
                extra: "",
            };
            igc.igc_main.instance.show_ad(param);
        }
        click_auto_update() {
            this.hide_native_interstitial();
            console.log("igc-----ad_interstitial_oppo_apk click_auto_update");
            this.load_native_interstitial();
        }
        load_video_interstitial(onLoad, onShow, onClose, onError) {
            let self = this;
            let ad_id = syyx_adv_manager.get_channel_ad_id(e_ad_id.interstitial_video);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- ad_interstitial_oppo_apk native_interstitial_id no configure in adv.csv" +
                    e_ad_id.interstitial_video);
                this.load_normal_interstitial();
                return;
            }
            let param = {
                ad_type: igc.e_ad_type.interstitial_video,
                ad_id: ad_id,
                ad_pos_id: e_ad_id.interstitial_video,
                sub_ad_type: 0,
                ad_event: ad_id,
                ad_scene: ad_id,
                auto_show: true,
                onLoad: function () {
                    console.log("igc-----ad_interstitial_oppo_apk load_video_interstitial onLoad");
                    onLoad && onLoad();
                },
                onShow: function () {
                    console.log("igc-----ad_interstitial_oppo_apk load_video_interstitial onShow");
                    onShow && onShow();
                },
                onClose: function (param, res) {
                    console.log("igc-----ad_interstitial_oppo_apk load_video_interstitial onClose");
                    onClose && onClose();
                },
                onError: function (param, err) {
                    console.log("igc-----ad_interstitial_oppo_apk load_video_interstitial onError:" + JSON.stringify(err));
                    onError && onError();
                    self.load_normal_interstitial();
                },
                extra: "",
            };
            igc.igc_main.instance.create_ad(param);
        }
        load_normal_interstitial(onLoad, onShow, onClose, onError) {
            let self = this;
            console.log("igc show_normal_interstitial-----------------------------------------------------------------");
            let ad_id = syyx_adv_manager.get_channel_ad_id(e_ad_id.interstitial_hall);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- ad_interstitial_oppo_apk normal_interstitial_id no configure in adv.csv");
                return;
            }
            let param = {
                ad_type: igc.e_ad_type.interstitial,
                ad_id: ad_id,
                ad_pos_id: e_ad_id.interstitial_hall,
                sub_ad_type: 0,
                ad_event: ad_id,
                ad_scene: ad_id,
                auto_show: true,
                onLoad: function () {
                    console.log("igc-----load_normal_interstitial show_normal_interstitial onLoad");
                    onLoad && onLoad();
                },
                onShow: function () {
                    console.log("igc-----load_normal_interstitial show_normal_interstitial onShow");
                    onShow && onShow();
                },
                onClose: function (param, res) {
                    console.log("igc-----load_normal_interstitial show_normal_interstitial onClose");
                    onClose && onClose();
                },
                onError: function (param, err) {
                    console.log("igc-----load_normal_interstitial show_normal_interstitial onError:" + JSON.stringify(err));
                    onError && onError();
                    self.load_normal_interstitial_error();
                },
                onClick: function () {
                    console.log("igc-----load_normal_interstitial show_normal_interstitial onClick:" + param.ad_pos_id);
                },
                extra: "",
            };
            igc.igc_main.instance.create_ad(param);
        }
        load_normal_interstitial_error() {
            this.show_old_native_data_interstitial(false);
        }
        hide_interstitial() {
            this.hide_native_interstitial();
            this.hide_normal_interstitial();
        }
        hide_native_interstitial() {
            let list = [
                e_ad_id.native_interstitial_1,
                e_ad_id.native_interstitial_2,
                e_ad_id.native_interstitial_3,
            ];
            for (let i in list) {
                let ad_id = syyx_adv_manager.get_channel_ad_id(list[i]);
                let param = {
                    ad_type: igc.e_ad_type.native,
                    ad_id: ad_id,
                    ad_pos_id: list[i],
                    sub_ad_type: 0,
                    ad_event: ad_id,
                    ad_scene: ad_id,
                    apk_sub_ad_type: igc.e_native_advance_sub_type.interstitial,
                };
                igc.igc_main.instance.hide_ad(param);
            }
        }
        hide_normal_interstitial() {
            syyx_sdk_api.destroy_ad(igc.e_ad_type.interstitial, e_ad_id.interstitial_hall);
        }
    }
    window["oppoSdk"]["ad_interstitial_oppo_apk"] = ad_interstitial_oppo_apk;

    class syyx_apk_adv_manager {
        static init_instance() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                if (this.is_oppo_apk() || this.is_xm_apk()) {
                    this._banner_instance = new ad_banner_oppo_apk();
                    this._inner_instance = new ad_inner_interstitial_oppo_apk();
                    this._interstitial_instance = new ad_interstitial_oppo_apk();
                }
                else if (this.is_test_apk()) {
                    this._banner_instance = new ad_banner_oppo_apk();
                    this._inner_instance = new ad_inner_interstitial_oppo_apk();
                    this._interstitial_instance = new ad_interstitial_oppo_apk();
                }
            }
        }
        static preload_video() {
            console.log("igc-----preload_video begin");
            let ad_id = syyx_adv_manager.get_channel_ad_id(e_ad_id.video_add_gold);
            if (!ad_id || ad_id == "1" || ad_id == "0") {
                console.log("igc----- syyx_manager create_ad ad_id no configure in adv.csv");
                return;
            }
            let param = {
                ad_type: igc.e_ad_type.video,
                ad_id: ad_id,
                ad_pos_id: e_ad_id.video_add_gold,
                ad_event: ad_id,
                ad_scene: ad_id,
                sub_ad_type: igc.e_ad_native_type.native_banner_normal,
                auto_show: false,
                onLoad: function () {
                    console.log("igc-----show preload_video on_load");
                },
                onShow: undefined,
                onClose: undefined,
                onError: undefined,
                extra: "",
            };
            return igc.igc_main.instance.create_ad(param);
        }
        static show_video(ad_pos_id, onLoad, onShow, onClose, onError) {
            if (this._show_video_count == 0) {
                ad_pos_id = e_ad_id.video_add_gold;
            }
            this._show_video_count++;
            syyx_manager.create_ad(igc.e_ad_type.video, ad_pos_id, onLoad, onShow, onClose, onError, -1, 1 + "");
        }
        static show_banner(ad_type, ad_pos_id, onLoad, onShow, onClose, onError, attrSet) {
            if (!this._banner_instance) {
                console.log("banner instance 为空");
            }
            this._banner_instance &&
                this._banner_instance.show_banner(ad_type, ad_pos_id, onLoad, onShow, onClose, onError, attrSet);
        }
        static hide_banner() {
            this._banner_instance && this._banner_instance.hide_banner();
        }
        static show_native_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError, attrSet) {
            let sw = syyx_manager.get_business_data_by_key("only_interstitial");
            console.log('mijia+++ only_interstitial is' + sw);
            if (sw && sw[0] == 1) {
                this._interstitial_instance &&
                    this._interstitial_instance.load_normal_interstitial();
            }
            else {
                this._interstitial_instance &&
                    this._interstitial_instance.show_native_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError, attrSet);
            }
        }
        static show_video_interstitial(onLoad, onShow, onClose, onError) {
            this._interstitial_instance && this._interstitial_instance.load_video_interstitial(onLoad, onShow, onClose, onError);
        }
        static show_interstitial(onLoad, onShow, onClose, onError) {
            this._interstitial_instance && this._interstitial_instance.load_normal_interstitial(onLoad, onShow, onClose, onError);
        }
        static preload_native_inner_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError) {
            this._inner_instance && this._inner_instance.preload_native_inner_interstitial(ad_type, ad_pos_id, onLoad, onShow, onClose, onError);
        }
        static show_native_inner_interstitial(param) {
            this._inner_instance &&
                this._inner_instance.show_native_inner_interstitial(param.ad_type, param.ad_pos_id, param.onLoad, param.onShow, param.onClose, param.onError, param.onClick, param.attrSet);
        }
        static hide_native_inner_interstitial() {
            this._inner_instance &&
                this._inner_instance.hide_native_inner_interstitial();
        }
        static is_oppo_apk() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                let channel_id = this.get_apk_channel_id();
                if (channel_id == 607010 || channel_id == "607010") {
                    return true;
                }
            }
            return false;
        }
        static is_xm_apk() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                let channel_id = this.get_apk_channel_id();
                if (channel_id == 607030 || channel_id == "607030") {
                    return true;
                }
            }
            return false;
        }
        static is_test_apk() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                let channel_id = this.get_apk_channel_id();
                if (!channel_id) {
                    return true;
                }
            }
            return false;
        }
        static is_vivo_apk() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                let channel_id = this.get_apk_channel_id();
                if (channel_id == 607020 || channel_id == "607020") {
                    return true;
                }
            }
            return false;
        }
        static get_apk_channel_id() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                return igc.utils_manager.callJava("org/cocos2dx/javascript/CoCosJSBridge", "getChannelExId", "()Ljava/lang/String;");
            }
        }
        static get_apk_device_id() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                return igc.utils_manager.callJava("org/cocos2dx/javascript/CoCosJSBridge", "getDeviceId", "()Ljava/lang/String;");
            }
        }
        static get_apk_ad_channel_id() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                return igc.utils_manager.callJava("org/cocos2dx/javascript/CoCosJSBridge", "getAdChannelId", "()Ljava/lang/String;");
            }
        }
        static get_stat_version() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                return igc.utils_manager.callJava("org/cocos2dx/javascript/CoCosJSBridge", "getStatVersion", "()Ljava/lang/String;");
            }
            return "0";
        }
        static set_banner_height() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                console.log("igc set_banner_height");
                let data = {};
                data["methodEnum"] = e_methoh_enum.set_banner_height;
                igc.utils_manager.callJava("org/cocos2dx/javascript/CoCosJSBridge", "runCustomMethod", "(Ljava/lang/String;)Ljava/lang/String;", JSON.stringify(data));
            }
        }
        static get_local_native_data() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                console.log("igc get_local_native_data");
                let data = {};
                data["methodEnum"] = e_methoh_enum.has_native_data;
                let result = igc.utils_manager.callJava("org/cocos2dx/javascript/CoCosJSBridge", "runCustomMethod", "(Ljava/lang/String;)Ljava/lang/String;", JSON.stringify(data));
                return result == "1";
            }
            return null;
        }
        static set_apk_remote_config() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                console.log("igc set_apk_remote_config");
                let data = {};
                data["methodEnum"] = e_methoh_enum.set_apk_remote_config;
                data["param"] = JSON.stringify(syyx_manager.get_business_config());
                return igc.utils_manager.callJava("org/cocos2dx/javascript/CoCosJSBridge", "runCustomMethod", "(Ljava/lang/String;)Ljava/lang/String;", JSON.stringify(data));
            }
        }
        static jump_leisure_subject() {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                igc.igc_main.instance.jump_leisure_subject();
            }
        }
        static set_on_exit_finish(call_back) {
            igc.igc_main.instance.set_on_exit_finish(call_back);
        }
        static show_privacy_view() {
            if (this.is_vivo_apk()) {
                window["loadingView"] && window["loadingView"].showPrivacyView();
            }
        }
        static hide_loading_view() {
            window["loadingView"] && window["loadingView"].hideLoadingView();
        }
        static set_loading_view_value(value) {
            if (syyx_const.syyx_sdk_channel == igc.e_channel_type.apk) {
                window["loadingView"] && (window["loadingView"].loading = value);
            }
        }
    }
    syyx_apk_adv_manager._banner_instance = null;
    syyx_apk_adv_manager._inner_instance = null;
    syyx_apk_adv_manager._interstitial_instance = null;
    syyx_apk_adv_manager._show_video_count = 0;
    window["oppoSdk"]["syyx_apk_adv_manager"] = syyx_apk_adv_manager;

    class syyx_sdk_api {
        static init(game_init_file_path = null, init_callback) {
            console.log("igc--------------------syyx_apk version" + syyx_const.syyx_sdk_version);
            syyx_manager.init(game_init_file_path, init_callback);
        }
        static login_channel(callback) {
            syyx_manager.login_channel(callback);
        }
        static init_param(account, user_id) {
            syyx_manager.init_param(account, user_id);
        }
        static get_business_data_by_key(key) {
            return syyx_manager.get_business_data_by_key(key);
        }
        static send_user_register() {
            syyx_manager.send_user_register();
        }
        static send_user_login() {
            syyx_manager.send_user_login();
        }
        static send_hall_event(event_id, extra_int = 0, str1 = "") {
            syyx_manager.send_user_event(event_id, e_stat_event_type.hall, "", "", "", str1, "", extra_int, "");
        }
        static send_chapter_event(event_id, is_win = undefined, str1 = "", extra_int = 0) {
            let extra = is_win ? e_chapter_result_type.win : e_chapter_result_type.lose;
            if (is_win == true) {
                extra = e_chapter_result_type.win;
            }
            else if (is_win == false) {
                extra = e_chapter_result_type.lose;
            }
            else {
                extra = e_chapter_result_type.enter_chapter;
            }
            syyx_manager.send_user_event(event_id, e_stat_event_type.chapter, "", "", extra, str1, "", extra_int, "");
        }
        static send_settlement_event(event_id, extra = "", str1 = "", extra_int = 0) {
            syyx_manager.send_user_event(event_id, e_stat_event_type.result, "", "", extra, str1, "", extra_int, "");
        }
        static send_other_event(event_id, event_type, extra = "", extra_int = 0, str1 = "", str2 = "", str3 = "") {
            syyx_manager.send_user_event(event_id, event_type, "", "", extra, str1, str2, extra_int, str3);
        }
        static is_inited() {
            return syyx_manager.is_inited();
        }
        static create_native_banner(call_back) {
            return syyx_manager.create_native_banner(call_back);
        }
        static create_interstitial(call_back) {
            return syyx_manager.create_interstitial(call_back);
        }
        static create_inner_interstitial(call_back) {
            return syyx_manager.create_inner_interstitial(call_back);
        }
        static create_native_icon(call_back) {
            return syyx_manager.create_native_icon(call_back);
        }
        static create_toast(desc) {
            return syyx_manager.create_toast(desc);
        }
        static hide(viewType) {
            syyx_manager.hide(viewType);
        }
        static show(viewType, zOrder = -1, scene, chapter = 0) {
            syyx_manager.show(viewType, zOrder, scene, chapter);
        }
        static load_view(viewType, call_bcak) {
            return syyx_manager.load_view(viewType, call_bcak);
        }
        static create_ad(ad_type, ad_pos_id, onLoad, onShow, onClose, onError) {
            return false;
        }
        static destroy_ad(ad_type, ad_pos_id) {
            return syyx_manager.destroy_ad(ad_type, ad_pos_id);
        }
        static hide_ad(ad_type, ad_pos_id) {
            return syyx_manager.hide_ad(ad_type, ad_pos_id);
        }
        static report_ad_show(ad_pos_id, native_data) {
            return syyx_manager.report_ad_show(ad_pos_id, native_data);
        }
        static report_ad_click(ad_pos_id, native_data) {
            return syyx_manager.report_ad_click(ad_pos_id, native_data);
        }
        static get_local_native_data(ad_pos_id) {
            return syyx_manager.get_local_native_data(ad_pos_id);
        }
        static get_channel_ad_id(ad_pos_id) {
            return syyx_manager.get_channel_ad_id(ad_pos_id);
        }
        static show_banner(ad_pos_id, onLoad, onShow, onClose, onError, attrSet) {
            syyx_manager.show_banner(ad_pos_id, onLoad, onShow, onClose, onError, attrSet);
        }
        static hide_banner() {
            syyx_manager.hide_banner();
        }
        static set_banner_height() {
            syyx_adv_manager.set_banner_height();
        }
        static set_normal_banner_switch(value = true) {
            syyx_adv_manager.set_normal_banner_switch(value);
        }
        static show_video(ad_pos_id, onLoad, onShow, onClose, onError, need_err_tips) {
            syyx_manager.show_video(ad_pos_id, onLoad, onShow, onClose, onError, need_err_tips);
        }
        static show_interstitial(ad_pos_id, onLoad, onShow, onClose, onError) {
            syyx_manager.show_interstitial(ad_pos_id, onLoad, onShow, onClose, onError);
        }
        static show_video_interstitial(onLoad, onShow, onClose, onError) {
            syyx_adv_manager.show_video_interstitial(onLoad, onShow, onClose, onError);
        }
        static show_native_interstitial(ad_pos_id, onLoad, onShow, onClose, onError, apk_ad_attr) {
            syyx_adv_manager.show_native_interstitial(igc.e_ad_type.native, ad_pos_id, onLoad, onShow, onClose, onError, apk_ad_attr);
        }
        static hide_native_interstitial() {
            ad_native_interstitial.hide_native_interstitial_ui();
        }
        static preload_native_inner_interstitial(ad_pos_id, onLoad, onShow, onClose, onError) {
            syyx_adv_manager.preload_native_inner_interstitial(igc.e_ad_type.native, ad_pos_id, onLoad, onShow, onClose, onError);
        }
        static show_native_inner_interstitial(ad_pos_id, parent, click_back, show_back, hide_back, is_new_type = true, apk_attr) {
            syyx_manager.show_native_inner_interstitial(ad_pos_id, parent, click_back, show_back, hide_back, is_new_type, apk_attr);
        }
        static click_native_inner_interstitial() {
            syyx_manager.click_native_inner_interstitial();
        }
        static set_on_click_inner_interstitial_btn(click_back) {
            syyx_adv_manager.set_on_click_inner_interstitial_btn(click_back);
        }
        static hide_native_inner_interstitial() {
            syyx_manager.hide_native_inner_interstitial();
        }
        static gen_inner_btn_group(arr) {
            let res = {};
            for (let i in arr) {
                let temp = arr[i];
                res['btn' + i] = syyx_sdk_utils.getScreenLeftNTop(temp);
            }
            return res;
        }
        static show_native_icon(parent, ad_pos_id, onLoad, onShow, onClose, onError) {
            syyx_adv_manager.show_native_icon(parent, igc.e_ad_type.native, ad_pos_id, onLoad, onShow, onClose, onError);
        }
        static hide_native_icon() {
            syyx_adv_manager.hide_native_icon();
        }
        static support_game_box() {
            return syyx_manager.support_game_box();
        }
        static show_game_banner_box(ad_pos_id, onLoad, onShow, onClose, onError) {
            syyx_manager.create_ad(igc.e_ad_type.app_box, ad_pos_id, onLoad, onShow, onClose, onError, igc.e_ad_app_box_type.banner_box);
        }
        static hide_game_banner_box(ad_pos_id) {
            syyx_manager.destroy_ad(igc.e_ad_type.app_box, ad_pos_id, igc.e_ad_app_box_type.banner_box);
        }
        static show_game_portal_box(ad_pos_id, onLoad, onShow, onClose, onError, need_err_tips, marginTop) {
            syyx_manager.show_game_portal_box(ad_pos_id, onLoad, onShow, onClose, onError, need_err_tips, marginTop);
        }
        static hide_game_portal_box(ad_pos_id) {
            syyx_manager.destroy_ad(igc.e_ad_type.app_box, ad_pos_id, igc.e_ad_app_box_type.portal_box);
        }
        static show_drawer_box(ad_pos_id, onLoad, onShow, onClose, onError) {
            syyx_manager.create_ad(igc.e_ad_type.app_box, ad_pos_id, onLoad, onShow, onClose, onError, igc.e_ad_app_box_type.drawer);
        }
        static hide_drawer_box(ad_pos_id) {
            syyx_manager.destroy_ad(igc.e_ad_type.app_box, ad_pos_id, igc.e_ad_app_box_type.drawer);
        }
        static show_app_box(ad_pos_id, onLoad, onShow, onClose, onError) {
            syyx_manager.create_ad(igc.e_ad_type.app_box, ad_pos_id, onLoad, onShow, onClose, onError);
        }
        static show_block(style, ad_pos_id, onLoad, onShow, onClose, onError) {
            syyx_adv_manager.show_block(style, igc.e_ad_type.block, ad_pos_id, onLoad, onShow, onClose, onError);
        }
        static hide_block(ad_pos_id) {
            syyx_adv_manager.hide_block(ad_pos_id);
        }
        static hide_all_block() {
            syyx_adv_manager.hide_all_block();
        }
        static show_new_products(call_back) {
            syyx_manager.show_new_products(call_back);
        }
        static check_can_add_desktop() {
            return syyx_manager.check_can_add_desktop();
        }
        static check_is_add_desktop(can_add, has_add) {
            return syyx_manager.check_is_add_desktop(can_add, has_add);
        }
        static add_desktop(on_success, on_failed, on_failed_back, has_create) {
            return syyx_manager.add_desktop(on_success, on_failed, on_failed_back, has_create);
        }
        static on_show(callback) {
            return syyx_manager.on_show(callback);
        }
        static on_hide(callback) {
            return syyx_manager.on_hide(callback);
        }
        static get_system_info_sync() {
            return syyx_manager.get_system_info_sync();
        }
        static get_launch_options_sync() {
            let options = syyx_manager.get_launch_options_sync();
            let data = new launch_options();
            data.scene = options.scene || "";
            data.query = options.query || {};
            data.referrerInfo = options.referrerInfo || {};
            data.entryDataHash = options.entryDataHash || {};
            data.extra = options.extra || {};
            return data;
        }
        static exit_mini_program() {
            return syyx_manager.exit_mini_program();
        }
        static navigate_to_mini_program(app_id, succss) {
            return syyx_manager.navigate_to_mini_program(app_id, succss);
        }
        static share(title, imageUrl, query, desc, success, fail) {
            return syyx_manager.share(title, imageUrl, query, desc, success, fail);
        }
        static on_share_app_message(title, imageUrl) {
            return syyx_manager.on_share_app_message(title, imageUrl);
        }
        static start_record_screen(time, is_clip_end, clip_time) {
            return syyx_manager.start_record_screen(time, is_clip_end, clip_time);
        }
        static stop_record_screen() {
            return syyx_manager.stop_record_screen();
        }
        static pause_record_screen() {
            return syyx_manager.pause_record_screen();
        }
        static resume_record_screen() {
            return syyx_manager.resume_record_screen();
        }
        static share_record_screen(videoTopics, title, desc, imageUrl, query, fail, success) {
            return syyx_manager.share_record_screen(videoTopics, title, desc, imageUrl, query, fail, success);
        }
        static get_record_video() {
            return syyx_manager.get_record_video();
        }
        static get_is_new_player() {
            return syyx_manager.get_is_new_player();
        }
        static get_syyx_app_id() {
            return syyx_manager.get_syyx_app_id();
        }
        static get_app_version() {
            return syyx_manager.get_app_version();
        }
        static get_screen_ratio() {
            if (window["Laya"]) {
                if (Laya.stage.width > Laya.stage.height) {
                    return window["Laya"].stage.height / 1080;
                }
                else {
                    return window["Laya"].stage.width / 1080;
                }
            }
            return 1;
        }
        static get_channel_type() {
            return syyx_manager.get_channel_type();
        }
        static is_oppo_apk() {
            return syyx_apk_adv_manager.is_oppo_apk();
        }
        static is_xm_apk() {
            return syyx_apk_adv_manager.is_oppo_apk();
        }
        static is_vivo_apk() {
            return syyx_apk_adv_manager.is_vivo_apk();
        }
        static jump_leisure_subject() {
            return syyx_apk_adv_manager.jump_leisure_subject();
        }
        static set_on_exit_finish(call_back) {
            syyx_apk_adv_manager.set_on_exit_finish(call_back);
        }
        static hide_loading_view() {
            syyx_apk_adv_manager.hide_loading_view();
        }
        static set_loading_view_value(value) {
            syyx_apk_adv_manager.set_loading_view_value(value);
        }
        static get_apk_channel_id() {
            return syyx_apk_adv_manager.get_apk_channel_id();
        }
        static get_channel_id() {
            return syyx_manager.get_channel_id();
        }
        static get_apk_device_id() {
            return syyx_apk_adv_manager.get_apk_device_id();
        }
        static get_device_id() {
            return syyx_manager.get_device_id();
        }
        static get_apk_ad_channel_id() {
            return syyx_apk_adv_manager.get_apk_ad_channel_id();
        }
        static get_ad_channel_id() {
            return syyx_manager.get_ad_channel_id();
        }
        static show_privacy_view() {
            syyx_apk_adv_manager.show_privacy_view();
        }
    }
    window["oppoSdk"]["syyx_sdk_api"] = syyx_sdk_api;

}());
console.log("oppoSdk");