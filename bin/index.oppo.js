/**
 * 设置LayaNative屏幕方向，可设置以下值
 * landscape           横屏
 * portrait            竖屏
 * sensor_landscape    横屏(双方向)
 * sensor_portrait     竖屏(双方向)
 */
window.screenOrientation = "portrait";
loadLib("3rd/syyxh5frameworksdk.js");
loadLib("3rd/tpfsdk/3rd/md5.js");
loadLib("3rd/tpfsdk/communication/httputils.js");
loadLib("3rd/tpfsdk/sdk/tpfclientsdk.js");
loadLib("3rd/tpfsdk/sdk/tpfconfig.js");
loadLib("3rd/tpfsdk/sdk/tpfstat.js");
console.log("加载lw 3rd完成");
//-----libs-begin-----
loadLib("libs/laya.core.js");
loadLib("libs/laya.html.js");
loadLib("libs/laya.d3.js");//-----libs-end-------
loadLib("libs/laya.ui.js");
loadLib("libs/laya.physics3D.js");
loadLib("libs/ltgame/generator.js");
loadLib("libs/ltgame/promise.js");
loadLib("libs/fairygui/fairygui.js");
loadLib("libs/laya.ani.js");
loadLib("libs/oppoSdk.js");
loadLib("js/bundle.js");