/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./Number-Jump/scripts/main.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./Number-Jump/gen/ui/NumberJump/NumberJumpBinder.ts":
/*!***********************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/NumberJumpBinder.ts ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return NumberJumpBinder; });
/* harmony import */ var _UI_heroIcon__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UI_heroIcon */ "./Number-Jump/gen/ui/NumberJump/UI_heroIcon.ts");
/* harmony import */ var _UI_mask__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UI_mask */ "./Number-Jump/gen/ui/NumberJump/UI_mask.ts");
/* harmony import */ var _UI_heroItem__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./UI_heroItem */ "./Number-Jump/gen/ui/NumberJump/UI_heroItem.ts");
/* harmony import */ var _UI_jianciItem__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./UI_jianciItem */ "./Number-Jump/gen/ui/NumberJump/UI_jianciItem.ts");
/* harmony import */ var _UI_huadaoItem__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./UI_huadaoItem */ "./Number-Jump/gen/ui/NumberJump/UI_huadaoItem.ts");
/* harmony import */ var _UI_bigJianciItem__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./UI_bigJianciItem */ "./Number-Jump/gen/ui/NumberJump/UI_bigJianciItem.ts");
/* harmony import */ var _UI_boxItem__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./UI_boxItem */ "./Number-Jump/gen/ui/NumberJump/UI_boxItem.ts");
/* harmony import */ var _UI_huadaoAni__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./UI_huadaoAni */ "./Number-Jump/gen/ui/NumberJump/UI_huadaoAni.ts");
/* harmony import */ var _UI_blackItem__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./UI_blackItem */ "./Number-Jump/gen/ui/NumberJump/UI_blackItem.ts");
/* harmony import */ var _UI_bigTree__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./UI_bigTree */ "./Number-Jump/gen/ui/NumberJump/UI_bigTree.ts");
/* harmony import */ var _UI_nearMap__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./UI_nearMap */ "./Number-Jump/gen/ui/NumberJump/UI_nearMap.ts");
/* harmony import */ var _UI_yun__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./UI_yun */ "./Number-Jump/gen/ui/NumberJump/UI_yun.ts");
/* harmony import */ var _UI_propItem__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./UI_propItem */ "./Number-Jump/gen/ui/NumberJump/UI_propItem.ts");
/* harmony import */ var _UI_bgm__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./UI_bgm */ "./Number-Jump/gen/ui/NumberJump/UI_bgm.ts");
/* harmony import */ var _UI_break__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./UI_break */ "./Number-Jump/gen/ui/NumberJump/UI_break.ts");
/* harmony import */ var _UI_fall__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./UI_fall */ "./Number-Jump/gen/ui/NumberJump/UI_fall.ts");
/* harmony import */ var _UI_jump__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./UI_jump */ "./Number-Jump/gen/ui/NumberJump/UI_jump.ts");
/* harmony import */ var _UI_levelup__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./UI_levelup */ "./Number-Jump/gen/ui/NumberJump/UI_levelup.ts");
/* harmony import */ var _UI_wrong__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./UI_wrong */ "./Number-Jump/gen/ui/NumberJump/UI_wrong.ts");
/* harmony import */ var _UI_hechengBtn__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./UI_hechengBtn */ "./Number-Jump/gen/ui/NumberJump/UI_hechengBtn.ts");
/* harmony import */ var _UI_lvTitle__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./UI_lvTitle */ "./Number-Jump/gen/ui/NumberJump/UI_lvTitle.ts");
/* harmony import */ var _UI_ProgressBar1__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./UI_ProgressBar1 */ "./Number-Jump/gen/ui/NumberJump/UI_ProgressBar1.ts");
/* harmony import */ var _UI_lv1__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./UI_lv1 */ "./Number-Jump/gen/ui/NumberJump/UI_lv1.ts");
/* harmony import */ var _UI_heroNum__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./UI_heroNum */ "./Number-Jump/gen/ui/NumberJump/UI_heroNum.ts");
/* harmony import */ var _UI_miaosuCom__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./UI_miaosuCom */ "./Number-Jump/gen/ui/NumberJump/UI_miaosuCom.ts");
/* harmony import */ var _UI_reward__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./UI_reward */ "./Number-Jump/gen/ui/NumberJump/UI_reward.ts");
/* harmony import */ var _UI_sendBtn__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./UI_sendBtn */ "./Number-Jump/gen/ui/NumberJump/UI_sendBtn.ts");
/* harmony import */ var _UI_restart__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./UI_restart */ "./Number-Jump/gen/ui/NumberJump/UI_restart.ts");
/* harmony import */ var _UI_box__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./UI_box */ "./Number-Jump/gen/ui/NumberJump/UI_box.ts");
/* harmony import */ var _UI_mapLayer__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./UI_mapLayer */ "./Number-Jump/gen/ui/NumberJump/UI_mapLayer.ts");
/* harmony import */ var _UI_heroicontemp__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./UI_heroicontemp */ "./Number-Jump/gen/ui/NumberJump/UI_heroicontemp.ts");
/* harmony import */ var _UI_farMap__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./UI_farMap */ "./Number-Jump/gen/ui/NumberJump/UI_farMap.ts");
/* harmony import */ var _UI_dimian__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./UI_dimian */ "./Number-Jump/gen/ui/NumberJump/UI_dimian.ts");
/* harmony import */ var _UI_jianciani__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./UI_jianciani */ "./Number-Jump/gen/ui/NumberJump/UI_jianciani.ts");
/* harmony import */ var _UI_Main__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./UI_Main */ "./Number-Jump/gen/ui/NumberJump/UI_Main.ts");
/* harmony import */ var _UI_dimianjin__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./UI_dimianjin */ "./Number-Jump/gen/ui/NumberJump/UI_dimianjin.ts");
/* harmony import */ var _UI_jianciItemTemp__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./UI_jianciItemTemp */ "./Number-Jump/gen/ui/NumberJump/UI_jianciItemTemp.ts");
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/





































class NumberJumpBinder {
    static bindAll() {
        fgui.UIObjectFactory.setExtension(_UI_heroIcon__WEBPACK_IMPORTED_MODULE_0__["default"].URL, _UI_heroIcon__WEBPACK_IMPORTED_MODULE_0__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_mask__WEBPACK_IMPORTED_MODULE_1__["default"].URL, _UI_mask__WEBPACK_IMPORTED_MODULE_1__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_heroItem__WEBPACK_IMPORTED_MODULE_2__["default"].URL, _UI_heroItem__WEBPACK_IMPORTED_MODULE_2__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_jianciItem__WEBPACK_IMPORTED_MODULE_3__["default"].URL, _UI_jianciItem__WEBPACK_IMPORTED_MODULE_3__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_huadaoItem__WEBPACK_IMPORTED_MODULE_4__["default"].URL, _UI_huadaoItem__WEBPACK_IMPORTED_MODULE_4__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_bigJianciItem__WEBPACK_IMPORTED_MODULE_5__["default"].URL, _UI_bigJianciItem__WEBPACK_IMPORTED_MODULE_5__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_boxItem__WEBPACK_IMPORTED_MODULE_6__["default"].URL, _UI_boxItem__WEBPACK_IMPORTED_MODULE_6__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_huadaoAni__WEBPACK_IMPORTED_MODULE_7__["default"].URL, _UI_huadaoAni__WEBPACK_IMPORTED_MODULE_7__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_blackItem__WEBPACK_IMPORTED_MODULE_8__["default"].URL, _UI_blackItem__WEBPACK_IMPORTED_MODULE_8__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_bigTree__WEBPACK_IMPORTED_MODULE_9__["default"].URL, _UI_bigTree__WEBPACK_IMPORTED_MODULE_9__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_nearMap__WEBPACK_IMPORTED_MODULE_10__["default"].URL, _UI_nearMap__WEBPACK_IMPORTED_MODULE_10__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_yun__WEBPACK_IMPORTED_MODULE_11__["default"].URL, _UI_yun__WEBPACK_IMPORTED_MODULE_11__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_propItem__WEBPACK_IMPORTED_MODULE_12__["default"].URL, _UI_propItem__WEBPACK_IMPORTED_MODULE_12__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_bgm__WEBPACK_IMPORTED_MODULE_13__["default"].URL, _UI_bgm__WEBPACK_IMPORTED_MODULE_13__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_break__WEBPACK_IMPORTED_MODULE_14__["default"].URL, _UI_break__WEBPACK_IMPORTED_MODULE_14__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_fall__WEBPACK_IMPORTED_MODULE_15__["default"].URL, _UI_fall__WEBPACK_IMPORTED_MODULE_15__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_jump__WEBPACK_IMPORTED_MODULE_16__["default"].URL, _UI_jump__WEBPACK_IMPORTED_MODULE_16__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_levelup__WEBPACK_IMPORTED_MODULE_17__["default"].URL, _UI_levelup__WEBPACK_IMPORTED_MODULE_17__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_wrong__WEBPACK_IMPORTED_MODULE_18__["default"].URL, _UI_wrong__WEBPACK_IMPORTED_MODULE_18__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_hechengBtn__WEBPACK_IMPORTED_MODULE_19__["default"].URL, _UI_hechengBtn__WEBPACK_IMPORTED_MODULE_19__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_lvTitle__WEBPACK_IMPORTED_MODULE_20__["default"].URL, _UI_lvTitle__WEBPACK_IMPORTED_MODULE_20__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_ProgressBar1__WEBPACK_IMPORTED_MODULE_21__["default"].URL, _UI_ProgressBar1__WEBPACK_IMPORTED_MODULE_21__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_lv1__WEBPACK_IMPORTED_MODULE_22__["default"].URL, _UI_lv1__WEBPACK_IMPORTED_MODULE_22__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_heroNum__WEBPACK_IMPORTED_MODULE_23__["default"].URL, _UI_heroNum__WEBPACK_IMPORTED_MODULE_23__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_miaosuCom__WEBPACK_IMPORTED_MODULE_24__["default"].URL, _UI_miaosuCom__WEBPACK_IMPORTED_MODULE_24__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_reward__WEBPACK_IMPORTED_MODULE_25__["default"].URL, _UI_reward__WEBPACK_IMPORTED_MODULE_25__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_sendBtn__WEBPACK_IMPORTED_MODULE_26__["default"].URL, _UI_sendBtn__WEBPACK_IMPORTED_MODULE_26__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_restart__WEBPACK_IMPORTED_MODULE_27__["default"].URL, _UI_restart__WEBPACK_IMPORTED_MODULE_27__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_box__WEBPACK_IMPORTED_MODULE_28__["default"].URL, _UI_box__WEBPACK_IMPORTED_MODULE_28__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_mapLayer__WEBPACK_IMPORTED_MODULE_29__["default"].URL, _UI_mapLayer__WEBPACK_IMPORTED_MODULE_29__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_heroicontemp__WEBPACK_IMPORTED_MODULE_30__["default"].URL, _UI_heroicontemp__WEBPACK_IMPORTED_MODULE_30__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_farMap__WEBPACK_IMPORTED_MODULE_31__["default"].URL, _UI_farMap__WEBPACK_IMPORTED_MODULE_31__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_dimian__WEBPACK_IMPORTED_MODULE_32__["default"].URL, _UI_dimian__WEBPACK_IMPORTED_MODULE_32__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_jianciani__WEBPACK_IMPORTED_MODULE_33__["default"].URL, _UI_jianciani__WEBPACK_IMPORTED_MODULE_33__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_Main__WEBPACK_IMPORTED_MODULE_34__["default"].URL, _UI_Main__WEBPACK_IMPORTED_MODULE_34__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_dimianjin__WEBPACK_IMPORTED_MODULE_35__["default"].URL, _UI_dimianjin__WEBPACK_IMPORTED_MODULE_35__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_jianciItemTemp__WEBPACK_IMPORTED_MODULE_36__["default"].URL, _UI_jianciItemTemp__WEBPACK_IMPORTED_MODULE_36__["default"]);
    }
}


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_Main.ts":
/*!**************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_Main.ts ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_Main; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_Main extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "Main"));
    }
    onConstruct() {
        this.m_gameState = this.getControllerAt(0);
        this.m_state = this.getControllerAt(1);
        this.m_liuhai = this.getControllerAt(2);
        this.m_treeLayer = (this.getChildAt(2));
        this.m_mapLayer = (this.getChildAt(3));
        this.m_touch = (this.getChildAt(4));
        this.m_restartBtn = (this.getChildAt(5));
        this.m_hudunBtn = (this.getChildAt(6));
        this.m_dffDamageBtn = (this.getChildAt(7));
        this.m_yinying = (this.getChildAt(8));
        this.m_heroNum = (this.getChildAt(9));
        this.m_moveMvBtn = (this.getChildAt(10));
        this.m_gamePro = (this.getChildAt(11));
        this.m_barNum = (this.getChildAt(12));
        this.m_damageNum = (this.getChildAt(13));
        this.m_targetCom = (this.getChildAt(16));
        this.m_maxScore = (this.getChildAt(18));
        this.m_top = (this.getChildAt(19));
        this.m_spine = (this.getChildAt(20));
    }
}
UI_Main.URL = "ui://mmmoyow0v6gca1";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_ProgressBar1.ts":
/*!**********************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_ProgressBar1.ts ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_ProgressBar1; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_ProgressBar1 extends fgui.GProgressBar {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "ProgressBar1"));
    }
    onConstruct() {
        this.m_maxbar = (this.getChildAt(2));
    }
}
UI_ProgressBar1.URL = "ui://mmmoyow0hq5u14e";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_bgm.ts":
/*!*************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_bgm.ts ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_bgm; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_bgm extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "bgm"));
    }
    onConstruct() {
        this.m_bgm = this.getTransitionAt(0);
    }
}
UI_bgm.URL = "ui://mmmoyow0h4w415d";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_bigJianciItem.ts":
/*!***********************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_bigJianciItem.ts ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_bigJianciItem; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_bigJianciItem extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "bigJianciItem"));
    }
    onConstruct() {
        this.m_dici = (this.getChildAt(0));
        this.m_normaldiciItem = (this.getChildAt(1));
    }
}
UI_bigJianciItem.URL = "ui://mmmoyow0gddi1a3";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_bigTree.ts":
/*!*****************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_bigTree.ts ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_bigTree; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_bigTree extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "bigTree"));
    }
    onConstruct() {
        this.m_tree = (this.getChildAt(0));
    }
}
UI_bigTree.URL = "ui://mmmoyow0gddi1a8";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_blackItem.ts":
/*!*******************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_blackItem.ts ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_blackItem; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_blackItem extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "blackItem"));
    }
    onConstruct() {
        this.m_black = (this.getChildAt(0));
    }
}
UI_blackItem.URL = "ui://mmmoyow0gddi1a6";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_box.ts":
/*!*************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_box.ts ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_box; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_box extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "box"));
    }
    onConstruct() {
        this.m_propItem2 = (this.getChildAt(3));
        this.m_propItem1 = (this.getChildAt(4));
        this.m_noBtn = (this.getChildAt(6));
        this.m_mvBtn = (this.getChildAt(7));
    }
}
UI_box.URL = "ui://mmmoyow0qdxy1e0";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_boxItem.ts":
/*!*****************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_boxItem.ts ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_boxItem; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_boxItem extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "boxItem"));
    }
    onConstruct() {
        this.m_type = this.getControllerAt(0);
        this.m_box = (this.getChildAt(0));
    }
}
UI_boxItem.URL = "ui://mmmoyow0gddi1a4";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_break.ts":
/*!***************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_break.ts ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_break; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_break extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "break"));
    }
    onConstruct() {
        this.m_break = this.getTransitionAt(0);
    }
}
UI_break.URL = "ui://mmmoyow0h4w415e";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_dimian.ts":
/*!****************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_dimian.ts ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_dimian; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_dimian extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "dimian"));
    }
    onConstruct() {
        this.m_near = (this.getChildAt(0));
    }
}
UI_dimian.URL = "ui://mmmoyow0qdxy1e6";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_dimianjin.ts":
/*!*******************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_dimianjin.ts ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_dimianjin; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_dimianjin extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "dimianjin"));
    }
    onConstruct() {
        this.m_near = (this.getChildAt(0));
    }
}
UI_dimianjin.URL = "ui://mmmoyow0vma71e7";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_fall.ts":
/*!**************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_fall.ts ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_fall; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_fall extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "fall"));
    }
    onConstruct() {
        this.m_fall = this.getTransitionAt(0);
    }
}
UI_fall.URL = "ui://mmmoyow0h4w415g";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_farMap.ts":
/*!****************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_farMap.ts ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_farMap; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_farMap extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "farMap"));
    }
    onConstruct() {
        this.m_near = (this.getChildAt(0));
    }
}
UI_farMap.URL = "ui://mmmoyow0qdxy1e5";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_hechengBtn.ts":
/*!********************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_hechengBtn.ts ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_hechengBtn; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_hechengBtn extends fgui.GButton {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "hechengBtn"));
    }
    onConstruct() {
        this.m_num = (this.getChildAt(3));
    }
}
UI_hechengBtn.URL = "ui://mmmoyow0hq5u147";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_heroIcon.ts":
/*!******************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_heroIcon.ts ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_heroIcon; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_heroIcon extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "heroIcon"));
    }
    onConstruct() {
        this.m_icon = (this.getChildAt(1));
        this.m_name = (this.getChildAt(2));
    }
}
UI_heroIcon.URL = "ui://mmmoyow0dk571cu";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_heroItem.ts":
/*!******************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_heroItem.ts ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_heroItem; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_heroItem extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "heroItem"));
    }
    onConstruct() {
        this.m_heroNum = (this.getChildAt(0));
        this.m_hundunBreak = (this.getChildAt(1));
        this.m_hundun = (this.getChildAt(2));
        this.m_upani = (this.getChildAt(3));
        this.m_luodiAni = (this.getChildAt(4));
        this.m_idle = this.getTransitionAt(0);
    }
}
UI_heroItem.URL = "ui://mmmoyow0gddi19y";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_heroNum.ts":
/*!*****************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_heroNum.ts ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_heroNum; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_heroNum extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "heroNum"));
    }
    onConstruct() {
        this.m_type = this.getControllerAt(0);
        this.m_num = (this.getChildAt(0));
        this.m_num23 = (this.getChildAt(1));
        this.m_num22 = (this.getChildAt(2));
        this.m_num21 = (this.getChildAt(3));
        this.m_num12 = (this.getChildAt(4));
        this.m_num11 = (this.getChildAt(5));
        this.m_idle = this.getTransitionAt(0);
    }
}
UI_heroNum.URL = "ui://mmmoyow0m44s1en";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_heroicontemp.ts":
/*!**********************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_heroicontemp.ts ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_heroicontemp; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_heroicontemp extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "heroicontemp"));
    }
    onConstruct() {
        this.m_icon = (this.getChildAt(0));
    }
}
UI_heroicontemp.URL = "ui://mmmoyow0qdxy1e3";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_huadaoAni.ts":
/*!*******************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_huadaoAni.ts ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_huadaoAni; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_huadaoAni extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "huadaoAni"));
    }
    onConstruct() {
        this.m_gp = (this.getChildAt(24));
        this.m_move = this.getTransitionAt(0);
    }
}
UI_huadaoAni.URL = "ui://mmmoyow0gddi1a5";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_huadaoItem.ts":
/*!********************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_huadaoItem.ts ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_huadaoItem; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_huadaoItem extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "huadaoItem"));
    }
    onConstruct() {
        this.m_huadao = (this.getChildAt(1));
    }
}
UI_huadaoItem.URL = "ui://mmmoyow0gddi1a1";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_jianciItem.ts":
/*!********************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_jianciItem.ts ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_jianciItem; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_jianciItem extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "jianciItem"));
    }
    onConstruct() {
        this.m_item = (this.getChildAt(0));
        this.m_jianci1 = (this.getChildAt(1));
    }
}
UI_jianciItem.URL = "ui://mmmoyow0gddi1a0";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_jianciItemTemp.ts":
/*!************************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_jianciItemTemp.ts ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_jianciItemTemp; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_jianciItemTemp extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "jianciItemTemp"));
    }
    onConstruct() {
        this.m_jianci1 = (this.getChildAt(0));
    }
}
UI_jianciItemTemp.URL = "ui://mmmoyow0vma71ed";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_jianciani.ts":
/*!*******************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_jianciani.ts ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_jianciani; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_jianciani extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "jianciani"));
    }
    onConstruct() {
        this.m_jianciani = (this.getChildAt(0));
    }
}
UI_jianciani.URL = "ui://mmmoyow0r3yv1ds";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_jump.ts":
/*!**************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_jump.ts ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_jump; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_jump extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "jump"));
    }
    onConstruct() {
        this.m_jump = this.getTransitionAt(0);
    }
}
UI_jump.URL = "ui://mmmoyow0h4w415h";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_levelup.ts":
/*!*****************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_levelup.ts ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_levelup; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_levelup extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "levelup"));
    }
    onConstruct() {
        this.m_levelup = this.getTransitionAt(0);
    }
}
UI_levelup.URL = "ui://mmmoyow0h4w415i";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_lv1.ts":
/*!*************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_lv1.ts ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_lv1; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_lv1 extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "lv1"));
    }
    onConstruct() {
        this.m_blackitem1 = (this.getChildAt(0));
    }
}
UI_lv1.URL = "ui://mmmoyow0kx8o12x";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_lvTitle.ts":
/*!*****************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_lvTitle.ts ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_lvTitle; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_lvTitle extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "lvTitle"));
    }
    onConstruct() {
        this.m_text = (this.getChildAt(0));
        this.m_nowAni = this.getTransitionAt(0);
        this.m_small = this.getTransitionAt(1);
        this.m_big = this.getTransitionAt(2);
    }
}
UI_lvTitle.URL = "ui://mmmoyow0hq5u14a";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_mapLayer.ts":
/*!******************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_mapLayer.ts ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_mapLayer; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_mapLayer extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "mapLayer"));
    }
    onConstruct() {
        this.m_gameDesc = (this.getChildAt(3));
        this.m_startGp = (this.getChildAt(4));
        this.m_propLayer = (this.getChildAt(5));
    }
}
UI_mapLayer.URL = "ui://mmmoyow0qdxy1e2";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_mask.ts":
/*!**************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_mask.ts ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_mask; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_mask extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "mask"));
    }
    onConstruct() {
        this.m_bg = (this.getChildAt(0));
    }
}
UI_mask.URL = "ui://mmmoyow0ds7515x";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_miaosuCom.ts":
/*!*******************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_miaosuCom.ts ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_miaosuCom; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_miaosuCom extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "miaosuCom"));
    }
    onConstruct() {
        this.m_targetNum = (this.getChildAt(2));
    }
}
UI_miaosuCom.URL = "ui://mmmoyow0qdxy1dv";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_nearMap.ts":
/*!*****************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_nearMap.ts ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_nearMap; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_nearMap extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "nearMap"));
    }
    onConstruct() {
        this.m_near = (this.getChildAt(0));
    }
}
UI_nearMap.URL = "ui://mmmoyow0gddi1a9";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_propItem.ts":
/*!******************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_propItem.ts ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_propItem; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_propItem extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "propItem"));
    }
    onConstruct() {
        this.m_c1 = this.getControllerAt(0);
        this.m_propIcon = (this.getChildAt(1));
        this.m_numItem = (this.getChildAt(2));
    }
}
UI_propItem.URL = "ui://mmmoyow0gddi1ac";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_restart.ts":
/*!*****************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_restart.ts ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_restart; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_restart extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "restart"));
    }
    onConstruct() {
        this.m_noBtn = (this.getChildAt(3));
        this.m_enterBtn = (this.getChildAt(4));
    }
}
UI_restart.URL = "ui://mmmoyow0qdxy1dz";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_reward.ts":
/*!****************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_reward.ts ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_reward; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_reward extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "reward"));
    }
    onConstruct() {
        this.m_jiangjuanNum = (this.getChildAt(4));
        this.m_getBtn = (this.getChildAt(7));
    }
}
UI_reward.URL = "ui://mmmoyow0qdxy1dx";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_sendBtn.ts":
/*!*****************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_sendBtn.ts ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_sendBtn; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_sendBtn extends fgui.GButton {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "sendBtn"));
    }
    onConstruct() {
        this.m_type = this.getControllerAt(0);
        this.m_num = (this.getChildAt(3));
    }
}
UI_sendBtn.URL = "ui://mmmoyow0qdxy1dy";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_wrong.ts":
/*!***************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_wrong.ts ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_wrong; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_wrong extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "wrong"));
    }
    onConstruct() {
        this.m_wrong = this.getTransitionAt(0);
    }
}
UI_wrong.URL = "ui://mmmoyow0h4w415j";


/***/ }),

/***/ "./Number-Jump/gen/ui/NumberJump/UI_yun.ts":
/*!*************************************************!*\
  !*** ./Number-Jump/gen/ui/NumberJump/UI_yun.ts ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_yun; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_yun extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("NumberJump", "yun"));
    }
    onConstruct() {
        this.m_yun = (this.getChildAt(0));
    }
}
UI_yun.URL = "ui://mmmoyow0gddi1aa";


/***/ }),

/***/ "./Number-Jump/scripts/EventEmitter.ts":
/*!*********************************************!*\
  !*** ./Number-Jump/scripts/EventEmitter.ts ***!
  \*********************************************/
/*! exports provided: EventEmitter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventEmitter", function() { return EventEmitter; });
/* harmony import */ var _HashObject__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HashObject */ "./Number-Jump/scripts/HashObject.ts");

/**
 * 事件派发器，事件派发机制的实现
 *
 * @export
 * @class EventEmitter
 * @extends {HashObject}
 */
class EventEmitter extends _HashObject__WEBPACK_IMPORTED_MODULE_0__["HashObject"] {
    constructor() {
        super();
        this.$all = new Map();
    }
    /**
     * 添加事件监听器
     *
     * @param {EventType} type 事件类型
     * @param {EventHandler} handler 事件回调
     * @param {boolean} [options] 派发一次事件后移除该事件
     * @returns 返回绑定的回调函数，可用于取消绑定
     * @memberof EventEmitter
     */
    on(type, handler, options) {
        const handlers = this.$all.get(type);
        const h = { handler, options };
        const added = handlers && handlers.push(h);
        if (!added) {
            this.$all.set(type, [h]);
        }
        return h.handler;
    }
    /**
     * 添加事件监听器, 事件派发后自动解绑
     *
     * @param {EventType} type 事件类型
     * @param {EventHandler} handler 事件回调
     * @param {boolean} [options] 派发一次事件后移除该事件
     * @returns 返回绑定的回调函数，可用于取消绑定
     * @returns EventEmitter
     */
    once(type, handler, options) {
        const opt = options || {};
        opt.once = true;
        return this.on(type, handler, opt);
    }
    /**
     * 转发事件
     * @param target 目标代理对象
     * @param type 时间类型
     * @param options 配置参数
     * @returns 返回代理回调，可用于取消转发
     */
    pipe(target, type, options) {
        // @ts-ignore
        if (target == this)
            return undefined;
        const callback = function () {
            // eslint-disable-next-line prefer-rest-params
            target.emit.apply(target, [type, ...arguments]);
        };
        this.on(type, callback, options);
        return callback;
    }
    /**
     * 移除事件监听器
     *
     * @param {EventType} type 事件类型
     * @param {EventHandler} handler 要移除的回调
     * @memberof EventEmitter
     */
    off(type, handler) {
        const handlers = this.$all.get(type);
        if (handlers) {
            const h = handlers.find((h) => h.handler === handler);
            if (h) {
                handlers.splice(handlers.indexOf(h), 1);
            }
        }
    }
    /**
     * 移除特定类型是所有事件监听器
     *
     * @param {EventType} type 事件类型，传入`*`则清理所有类型的事件回调
     * @memberof EventEmitter
     */
    offAll(type) {
        if (type === "*") {
            this.$all.clear();
        }
        else {
            this.$all.delete(type);
        }
    }
    /**
     * 派发事件
     *
     * @param {EventType} type 事件类型
     * @param {...any[]} args 派发参数表
     * @memberof EventEmitter
     */
    emit(type, ...args) {
        const handlers = [];
        let blocking = false;
        for (const h of (this.$all.get(type) || []).slice()) {
            const currentBlocking = h.options && h.options.block;
            const currentOnce = h.options && h.options.once;
            if (!blocking)
                h.handler(...args);
            if (!currentOnce || blocking)
                handlers.push(h);
            blocking = blocking || currentBlocking;
        }
        this.$all.set(type, handlers);
    }
    /** 等待事件派发 */
    wait(event) {
        return new Promise((resolve, reject) => {
            this.on(event, (...args) => {
                resolve(args);
            }, { once: true });
        });
    }
    /** 销毁时解绑所有事件 */
    dispose() {
        this.$all.clear();
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/HashObject.ts":
/*!*******************************************!*\
  !*** ./Number-Jump/scripts/HashObject.ts ***!
  \*******************************************/
/*! exports provided: GenerateObjectHashCode, HashObject */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenerateObjectHashCode", function() { return GenerateObjectHashCode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HashObject", function() { return HashObject; });
/**
 * 哈希计数
 */
let $hashCount = 0;
/** 生成新的 HashCode */
function GenerateObjectHashCode() {
    return ++$hashCount;
}
class HashObject {
    constructor() {
        this.$hashCode = GenerateObjectHashCode();
    }
    get hashCode() {
        return this.$hashCode;
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/Async/Awaiters.ts":
/*!******************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/Async/Awaiters.ts ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Awaiters; });
/* harmony import */ var _script_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../script/common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");

class Awaiters {
    static NextFrame() {
        return this.Frames(1);
    }
    static NextTFrame() {
        return this.Frames(2);
    }
    static Frames(num) {
        return new Promise(function (resolve) {
            _script_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_0__["default"].gameScript.timer.frameOnce(num, null, () => {
                resolve();
            });
        });
    }
    static Seconds(num) {
        return new Promise(function (resolve) {
            _script_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_0__["default"].gameScript.timer.once(num * 1000, null, () => {
                resolve();
            });
        });
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/Commom/EScreenOrientation.ts":
/*!*****************************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/Commom/EScreenOrientation.ts ***!
  \*****************************************************************/
/*! exports provided: EScreenOrientation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EScreenOrientation", function() { return EScreenOrientation; });
var EScreenOrientation;
(function (EScreenOrientation) {
    /**
     * 竖屏
     */
    EScreenOrientation[EScreenOrientation["Portrait"] = 0] = "Portrait";
    /**
     * 横屏
     */
    EScreenOrientation[EScreenOrientation["Landscape"] = 1] = "Landscape";
})(EScreenOrientation || (EScreenOrientation = {}));


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/Config/ConfigManager.ts":
/*!************************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/Config/ConfigManager.ts ***!
  \************************************************************/
/*! exports provided: ConfigManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigManager", function() { return ConfigManager; });
/* harmony import */ var _script_common_Const__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../script/common/Const */ "./Number-Jump/scripts/script/common/Const.ts");

class ConfigManager {
    static get needLoadCount() {
        return this._configList.length;
    }
    static AddConfig(configName) {
        ConfigManager._configList.push(configName);
    }
    static StartLoad(onFinished, onProgress = null) {
        if (ConfigManager._configList.length == 0) {
            if (onFinished) {
                onFinished.run();
                onFinished = null;
            }
            return;
        }
        var loadUrls = [];
        for (let configName of ConfigManager._configList) {
            loadUrls.push("Assets/Number-Jump/" + configName.path);
        }
        Laya.loader.load(loadUrls, Laya.Handler.create(this, () => {
            for (let configName of ConfigManager._configList) {
                configName.data = Laya.loader.getRes("Assets/Number-Jump/" + configName.path);
                configName.dataList = [];
                for (let configKey in configName.data) {
                    let value = configName.data[configKey];
                    if (value != null) {
                        configName.dataList.push(value);
                    }
                }
                if (configName.dataList.length > 0) {
                    configName.lastData = configName.dataList[configName.dataList.length - 1];
                }
            }
            if (onFinished) {
                onFinished.run();
                onFinished = null;
            }
        }), onProgress, null, 0, true, _script_common_Const__WEBPACK_IMPORTED_MODULE_0__["default"].gameNameJian, true);
    }
}
ConfigManager._configList = [];


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/Fsm/BaseState.ts":
/*!*****************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/Fsm/BaseState.ts ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return BaseState; });
class BaseState {
    constructor(id) {
        this.id = id;
    }
    GetNextState() {
        if (this.isFinished) {
            return this.nextState;
        }
        return 0;
    }
    OnEnter(exitState, param) {
        this.isFinished = false;
        this.nextState = 0;
        this.passTime = 0;
        this.deltaTime = 0;
        this._DoEnter(exitState, param);
    }
    OnRunning(param, dt) {
        if (this.isFinished)
            return;
        this.deltaTime = dt;
        this.passTime += dt;
        this._DoRunning(param);
    }
    OnExit(enterState, param) {
        this._DoExit(enterState, param);
    }
    OnLateUpdate(dt) {
        if (this.isFinished)
            return;
        this._DoLateUpdate(dt);
    }
    _DoLateUpdate(dt) { }
    _DoEnter(exitStte, param) { }
    _DoRunning(param) { }
    _DoExit(enterState, param) { }
}


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/Fsm/StateMachine.ts":
/*!********************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/Fsm/StateMachine.ts ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return StateMachine; });
/* harmony import */ var _LTUtils_LTDictionary__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../LTUtils/LTDictionary */ "./Number-Jump/scripts/LTGame/LTUtils/LTDictionary.ts");

class StateMachine {
    constructor() {
        this._states = new _LTUtils_LTDictionary__WEBPACK_IMPORTED_MODULE_0__["default"]();
    }
    get count() {
        return this._states.length;
    }
    Add(addState) {
        this._states.set(addState.id, addState);
    }
    Remove(id) {
        return this._states.remove(id);
    }
    RemoveAll() {
        this._states.clear();
        this.currState = null;
    }
    ExitCurrState(param = null) {
        if (null != this.currState) {
            this.currState.OnExit(null, param);
            this.currState = null;
        }
    }
    ChangeState(id, param = null) {
        var state = this.Find(id);
        if (state != null) {
            if (null != this.currState) {
                this.currState.OnExit(state, param);
            }
            this.lastState = this.currState;
            this.currState = state;
            state.OnEnter(this.lastState, param);
            return true;
        }
        console.error("不存在的状态ID:" + id);
        return false;
    }
    LogicUpdate(dt) {
        let nextState = this.currState.GetNextState();
        if (nextState != 0) {
            this.ChangeState(nextState);
        }
        this.OnRunning(null, dt);
    }
    OnRunning(param, dt) {
        if (null == this.currState) {
            return;
        }
        this.currState.OnRunning(param, dt);
    }
    Find(id) {
        return this._states.get(id);
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/LTUtils/ArrayEx.ts":
/*!*******************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/LTUtils/ArrayEx.ts ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ArrayEx; });
class ArrayEx {
    static Replace(arr, oldObj, newObj) {
        let index = arr.indexOf(oldObj);
        if (index < 0)
            return false;
        arr.splice(index, 1, newObj);
        return true;
    }
    static Remove(arr, obj) {
        let index = arr.indexOf(obj);
        if (index < 0)
            return false;
        arr.splice(index, 1);
        return true;
    }
    static RemoveAt(arr, index) {
        if (arr.length <= index)
            return false;
        arr.splice(index, 1);
        return true;
    }
    static Contains(arr, obj) {
        let index = arr.indexOf(obj);
        return index >= 0;
    }
    static Copy(arr) {
        let result = [];
        for (let i = 0; i < arr.length; ++i) {
            result.push(arr[i]);
        }
        return result;
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/LTUtils/LTDictionary.ts":
/*!************************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/LTUtils/LTDictionary.ts ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return LTDictionary; });
class LTDictionary {
    constructor() {
        this.items = {};
    }
    set(key, value) {
        this.items[key] = value;
        return true;
    }
    has(key) {
        return this.items.hasOwnProperty(key);
    }
    remove(key) {
        if (!this.has(key))
            return false;
        delete this.items[key];
        return true;
    }
    get(key) {
        return this.has(key) ? this.items[key] : undefined;
    }
    keys() {
        return Object.keys(this.items);
    }
    // values() {
    // }
    get length() {
        return this.keys().length;
    }
    clear() {
        this.items = {};
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/LTUtils/LTUtils.ts":
/*!*******************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/LTUtils/LTUtils.ts ***!
  \*******************************************************/
/*! exports provided: LTUtils */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LTUtils", function() { return LTUtils; });
class LTUtils {
    static get deltaTime() {
        if (this._recordFrame != Laya.timer.currFrame) {
            this._deltaTime = Math.min(Laya.timer.delta, 30);
        }
        return this._deltaTime;
    }
    static deltaTimeBytimer(timer) {
        return Math.min(timer.delta, 30);
    }
    static removeNull(arr) {
        let newArr = [];
        for (let i = 0; i < arr.length; i++) {
            if (arr[i]) {
                newArr.push(arr[i]);
            }
        }
        return newArr;
    }
    static arrRemove(arr, remove) {
        if (arr && remove != null) {
            let index = arr.indexOf(remove);
            if (index >= 0) {
                arr.splice(index, 1);
            }
        }
    }
    // 只复制一层
    static copyFguiCom(com) {
        if (!com || !com.resourceURL)
            return null;
        let newCom = fgui.UIPackage.createObjectFromURL(com.resourceURL);
        newCom.width = com.width;
        newCom.height = com.height;
        newCom.setScale(com.scaleX, com.scaleY);
        newCom.setPivot(com.pivotX, com.pivotY, com.pivotAsAnchor);
        com._children.forEach((child, index) => {
            if (child instanceof fgui.GLoader) {
                newCom.getChildAt(index).url = child.url;
            }
            else if (child instanceof fgui.GTextField) {
                newCom.getChildAt(index).text = child.text;
            }
        });
        com.controllers.forEach((controller, index) => {
            if (controller) {
                newCom.controllers[index].selectedIndex = controller.selectedIndex;
            }
        });
        return newCom;
    }
    static addSomeOne(arr, data) {
        if (arr && data != null) {
            let index = arr.indexOf(data);
            if (index < 0) {
                arr.push(data);
                return true;
            }
            return false;
        }
        return false;
    }
    /**
     * 设置层级
     * @param obj
     * @param layerIndex
     */
    static SetLayer(obj, layerIndex) {
        obj.layer = layerIndex;
        for (let i = 0; i < obj.numChildren; ++i) {
            let getChild = obj.getChildAt(i);
            this.SetLayer(getChild, layerIndex);
        }
    }
}
LTUtils._recordFrame = 0;
LTUtils._deltaTime = 30;


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/LTUtils/MathEx.ts":
/*!******************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/LTUtils/MathEx.ts ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MathEx; });
/* harmony import */ var _ArrayEx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ArrayEx */ "./Number-Jump/scripts/LTGame/LTUtils/ArrayEx.ts");
/* harmony import */ var _LTUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./LTUtils */ "./Number-Jump/scripts/LTGame/LTUtils/LTUtils.ts");


class MathEx {
    static ToHex(num) {
        return num.toString(16);
    }
    static angleByPoint(point1, point2) {
        return parseFloat(((Math.atan2(point1.x - point2.x, -(point1.y - point2.y)) * 180) / Math.PI).toFixed(2));
    }
    static direRotate(point, angle) {
        let x2 = point.x * Math.cos(angle) - point.y * Math.sin(angle);
        let y2 = point.y * Math.cos(angle) + point.x * Math.sin(angle);
        return { x: x2, y: y2 };
    }
    static RandomFromArrayExcept(numArr, except) {
        let fakeRandomList = [];
        for (let i = 0; i < numArr.length; ++i) {
            if (except == numArr[i])
                continue;
            fakeRandomList.push(numArr[i]);
        }
        return this.RandomFromArray(fakeRandomList);
    }
    static RandomFromArrayExcepts(numArr, excepts) {
        let fakeRandomList = [];
        for (let i = 0; i < numArr.length; ++i) {
            if (excepts.indexOf(numArr[i]) >= 0)
                continue;
            fakeRandomList.push(numArr[i]);
        }
        return this.RandomFromArray(fakeRandomList);
    }
    static fromArrayExcepts(numArr, excepts) {
        for (let i = 0; i < excepts.length; i++) {
            _LTUtils__WEBPACK_IMPORTED_MODULE_1__["LTUtils"].arrRemove(numArr, excepts[i]);
        }
        return numArr;
    }
    static RandomFromArray(numArr) {
        let randomIndex = MathEx.RandomInt(0, numArr.length);
        return numArr[randomIndex];
    }
    static checkVersionPass(versionNow, versionTarget) {
        let temp1 = versionNow.split(".");
        let temp2 = versionTarget.split(".");
        if (parseInt(temp1[0]) != parseInt(temp2[0])) {
            return parseInt(temp1[0]) > parseInt(temp2[0]);
        }
        if (parseInt(temp1[1]) != parseInt(temp2[1])) {
            return parseInt(temp1[1]) > parseInt(temp2[1]);
        }
        if (parseInt(temp1[2]) != parseInt(temp2[2])) {
            return parseInt(temp1[2]) > parseInt(temp2[2]);
        }
        return 0;
    }
    static RandomArrayFromArray(arr, count) {
        let result = [];
        let indexList = [];
        for (let i = 0; i < arr.length; ++i) {
            indexList.push(i);
        }
        for (let i = 0; i < count; ++i) {
            let randomIndex = MathEx.RandomInt(0, indexList.length);
            let getIndex = indexList[randomIndex];
            _ArrayEx__WEBPACK_IMPORTED_MODULE_0__["default"].RemoveAt(indexList, randomIndex);
            result.push(arr[getIndex]);
        }
        return result;
    }
    static RandomFromWithWeight(numArr, weightArr) {
        if (numArr == null || numArr.length == 0) {
            return null;
        }
        var totalWeight = 0;
        for (var weight of weightArr) {
            totalWeight += weight;
        }
        var randomWeight = MathEx.Random(0, totalWeight);
        var currentWeight = 0;
        for (var i = 0; i < numArr.length; ++i) {
            currentWeight += weightArr[i];
            if (randomWeight < currentWeight) {
                return numArr[i];
            }
        }
        return numArr[numArr.length - 1];
    }
    // max  = max + 1
    static RandomInt(min, maxAddOne) {
        return Math.floor(this.Random(min, maxAddOne));
    }
    static Random(min, max) {
        return (max - min) * Math.random() + min;
    }
    /**
     * 判定概率命中
     * @param ratio 概率，百分数
     */
    static RandomRatio(ratio) {
        let v = MathEx.RandomInt(0, 10000) * 0.01;
        if (ratio > v) {
            return true;
        }
        return false;
    }
    static Clamp(value, min, max) {
        if (value < min)
            return min;
        if (value > max)
            return max;
        return value;
    }
    static Clamp01(value) {
        return this.Clamp(value, 0, 1);
    }
    static Sign(value) {
        if (value == 0)
            return 1;
        return value > 0 ? 1 : -1;
    }
    static GetNumCount(num) {
        var numberCount = 0;
        var newNumber = num;
        while (newNumber / 10 > 0) {
            newNumber = Math.floor(newNumber / 10);
            numberCount++;
        }
        return numberCount;
    }
    static Lerp(from, to, progress) {
        return from + (to - from) * MathEx.Clamp01(progress);
    }
    static LerpPos(sx, sy, ex, ey, t, temp) {
        if (t > 1) {
            t = 1;
        }
        else if (t < 0) {
            t = 0;
        }
        temp.x = sx + (ex - sx) * t;
        temp.y = sy + (ey - sy) * t;
    }
    static dis(pos, pos2) {
        return Math.sqrt(Math.pow(pos.x - pos2.x, 2) + Math.pow(pos.y - pos2.y, 2));
    }
    static MoveTowardsAngle(current, target, maxDelta) {
        var num = MathEx.DeltaAngle(current, target);
        if (0 - maxDelta < num && num < maxDelta) {
            return target;
        }
        target = current + num;
        return MathEx.MoveTowards(current, target, maxDelta);
    }
    static MoveTowards(current, target, maxDelta) {
        if (Math.abs(target - current) <= maxDelta) {
            return target;
        }
        return current + Math.sign(target - current) * maxDelta;
    }
    static DeltaAngle(current, target) {
        var num = MathEx.Repeat(target - current, 360);
        if (num > 180) {
            num -= 360;
        }
        return num;
    }
    static Dire(pos, target, scale = 1) {
        let dire = new Laya.Point(target.x - pos.x, target.y - pos.y);
        dire.normalize();
        dire.x *= scale;
        dire.y *= scale;
        return dire;
    }
    static add(pos, dire) {
        return new Laya.Point(dire.x + pos.x, dire.y + pos.y);
    }
    static Repeat(t, length) {
        return MathEx.Clamp(t - Math.floor(t / length) * length, 0, length);
    }
    static IsSimilar(n1, n2) {
        return n1 == n2;
    }
    // n1 下限 n2 上限
    static isInLimit(value, n1, n2) {
        return value >= n1 && value <= n2;
    }
    /**
     * 根据数字概率返回选择项
     * @param probabilities 概率
     * @returns index
     */
    static getIndexByProbability(probabilities) {
        let sum = 0;
        let result;
        probabilities.forEach((probability) => (sum += probability));
        let random = Math.random() * sum;
        sum = 0;
        for (let i = 0, length = probabilities.length; i < length; i++) {
            sum += probabilities[i];
            if (sum > random) {
                result = i;
                break;
            }
        }
        return result;
    }
    /**
     * 格式化数字后面带上单位(M,B,T等)
     * @param number
     * @param unit 单位
     * @param ratio 倍率
     */
    static formatNumber(number, unit = ["K", "M", "B", "T"], ratio = 1000) {
        let length = unit.length;
        let index = 0;
        while (number / ratio >= 1 && index < length) {
            number /= ratio;
            index++;
        }
        if (index == 0) {
            return number + "";
        }
        if (number >= 100) {
            return Math.floor(number) + unit[index - 1];
        }
        if (number >= 10) {
            return Math.floor(number * 10) / 10 + unit[index - 1];
        }
        return Math.floor(number * 100) / 100 + unit[index - 1];
    }
    static timeFromArrIndex(time, arr) {
        if (time == 0) {
            return 0;
        }
        let total = 0;
        arr.forEach((a) => (total += a));
        if (total == 0) {
            return 0;
        }
        time = time % total || total;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] >= time) {
                return i;
            }
            time -= arr[i];
        }
        return 0;
    }
}
MathEx.Deg2Rad = 0.0175;
MathEx.Rad2Deg = 57.2958;


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/LTUtils/MonoHelper.ts":
/*!**********************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/LTUtils/MonoHelper.ts ***!
  \**********************************************************/
/*! exports provided: default, EActionType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonoHelper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EActionType", function() { return EActionType; });
class MonoHelper extends Laya.Script {
    constructor() {
        super(...arguments);
        this._updateActions = [];
        this._lateUpdateActions = [];
    }
    static get instance() {
        if (this._instance == null) {
            let monoHelper = new Laya.Sprite();
            this._instance = monoHelper.addComponent(MonoHelper);
            Laya.stage.addChild(monoHelper);
        }
        return this._instance;
    }
    _GetActionList(actionType) {
        switch (actionType) {
            case EActionType.Update:
                return this._updateActions;
            case EActionType.LateUpdate:
                return this._lateUpdateActions;
            default:
                console.error("未处理的actionType类型", actionType);
                return null;
        }
    }
    AddAction(actionType, caller, func, args) {
        let list = this._GetActionList(actionType);
        if (list == null)
            return;
        for (let i = 0; i < list.length; ++i) {
            let handleInList = list[i];
            if (handleInList.caller == caller && handleInList.method == func) {
                console.warn("已存在重复注册事件,取消注册");
                return;
            }
        }
        let handle = Laya.Handler.create(caller, func, args, false);
        list.push(handle);
    }
    RemoveAction(actionType, caller, func) {
        let list = this._GetActionList(actionType);
        if (list == null)
            return;
        for (let i = 0; i < list.length; ++i) {
            let handleInList = list[i];
            if (handleInList.caller == caller && handleInList.method == func) {
                list = list.splice(i, 1);
                return;
            }
        }
    }
    onUpdate() {
        for (let i = 0; i < this._updateActions.length; ++i) {
            let action = this._updateActions[i];
            action && action.run();
            if (action && action.once) {
                this._updateActions[i] = null;
            }
        }
    }
    onLateUpdate() {
        for (let i = 0; i < this._lateUpdateActions.length; ++i) {
            let action = this._lateUpdateActions[i];
            action && action.run();
            if (action && action.once) {
                this._lateUpdateActions[i] = null;
            }
        }
    }
    onDestroy() {
        MonoHelper._instance = null;
    }
}
var EActionType;
(function (EActionType) {
    EActionType[EActionType["Update"] = 0] = "Update";
    EActionType[EActionType["LateUpdate"] = 1] = "LateUpdate";
})(EActionType || (EActionType = {}));


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/Manager/BoneNewMgr.ts":
/*!**********************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/Manager/BoneNewMgr.ts ***!
  \**********************************************************/
/*! exports provided: default, NewBoneManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return newBone; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewBoneManager", function() { return NewBoneManager; });
class newBone {
    constructor() {
        this._loadSuc = false;
        this._visible = true;
        this._loop = false;
        this._speed = 1;
        this.skinName = "";
    }
    get playType() {
        return this._playType;
    }
    get skeletonTemplet() {
        return this._skeletonTemplet;
    }
    init(url, _firstCall) {
        if (!url)
            return;
        this._url = url;
        this._firstCall = _firstCall;
        this.onDraw();
    }
    setFirstCall(call) {
        this._firstCall = call;
    }
    refresh() {
        this._skeleton && this._skeleton.templet && this._skeleton.templet.destroy();
        this.onDraw();
    }
    // 加载龙骨动画
    onDraw() {
        //背景防止点击
        if (!this._skeletonTemplet) {
            this._skeletonTemplet = new Laya.SpineTemplet(Laya.SpineVersion.v3_8);
            this._skeletonTemplet.loadAni(this._url);
            this._skeletonTemplet.on(Laya.Event.COMPLETE, this, this.onSkeleton, [this._url]);
            this._skeletonTemplet.on(Laya.Event.ERROR, this, this.onError);
        }
    }
    playByState(name, skinName, loop = false, call, _duration = 0, speed) {
        this._playType = name;
        this.skinName = skinName;
        this._loop = loop;
        if (_duration) {
            this._duration = _duration;
        }
        if (this._loadSuc && this._skeleton) {
            if (this._duration) {
                this._skeleton["state"].data.skeletonData.findAnimation(this._playType).duration = this._duration;
            }
            let aSpeed = this._speed;
            if (speed)
                aSpeed = speed;
            try {
                this._skeleton.playbackRate(aSpeed);
                if (skinName) {
                    this._skeleton.showSkinByName(skinName);
                }
                this._skeleton.play(name, loop, true);
            }
            catch (error) {
                console.error("bone error", error);
            }
        }
        this.call && this.call.recover();
        this.call = call;
    }
    setScale(x, y) {
        if (this._bonesLayer) {
            this._bonesLayer.scale(x, y);
        }
    }
    setSpeed(speed) {
        this._speed = speed;
    }
    get scaleX() {
        if (this._bonesLayer) {
            return this._bonesLayer.scaleX;
        }
        return 1;
    }
    get scaleY() {
        if (this._bonesLayer) {
            return this._bonesLayer.scaleY;
        }
        return 1;
    }
    onError() {
    }
    showSkinByName(name) {
        if (this._skeleton) {
            this._skeleton.showSkinByName(name);
        }
    }
    onSkeleton(url) {
        if (this._url != url)
            return;
        if (this._skeleton) {
            this._skeleton.destroy();
        }
        this._skeleton = this._skeletonTemplet.buildArmature();
        this.bonesLayer.addChild(this._skeleton);
        if (this._skeleton && this._playType != null) {
            if (this._duration) {
                this._skeleton["state"].data.skeletonData.findAnimation(this._playType).duration = this._duration;
            }
            this._skeleton.playbackRate(this._speed);
            this.skinName && this._skeleton.showSkinByName(this.skinName);
            this._skeleton.play(this._playType, this._loop);
        }
        this._skeleton && this._skeleton.on(Laya.Event.STOPPED, this, this.onOver);
        this._loadSuc = true;
        this._firstCall && this._firstCall.run();
        this._firstCall = null;
    }
    get loadSuc() {
        return this._loadSuc;
    }
    onOver() {
        // this.skeleton.off(Laya.Event.STOPPED, this, this.onOver);
        // this.skeleton.stop();
        console.log("compelete");
        if (this.call) {
            this.call.run();
            if (this.call && this.call.once) {
                this.call = null;
            }
        }
    }
    get boneSk() {
        return this._skeleton;
    }
    set bonesLayer(value) {
        this._bonesLayer = value;
    }
    get bonesLayer() {
        if (!this._bonesLayer) {
            this._bonesLayer = new Laya.Sprite();
        }
        return this._bonesLayer;
    }
    dispose() {
        if (this._skeletonTemplet) {
            this._skeletonTemplet.off(Laya.Event.COMPLETE, this, this.onSkeleton);
            this._skeletonTemplet.off(Laya.Event.ERROR, this, this.onError);
        }
        this._skeleton && this._skeleton.off(Laya.Event.STOPPED, this, this.onOver);
        this.call && this.call.recover();
        this.call = null;
        // LIUTODO 模板内存泄漏
        // this._skeletonTemplet && this._skeletonTemplet.destroy();
        this._skeletonTemplet = null;
        this._skeleton && this._skeleton.destroy(true);
        this._skeleton = null;
        if (this._bonesLayer) {
            if (this._bonesLayer.parent) {
                this._bonesLayer.parent.removeChild(this._bonesLayer);
            }
            this._bonesLayer.destroy();
            this._bonesLayer = null;
        }
        this._firstCall && this._firstCall.recover();
        this._firstCall = null;
    }
    set visible(value) {
        this._visible = value;
        this.bonesLayer.visible = value;
    }
    get visible() {
        return this._visible;
    }
    setXY(x, y) {
        this.bonesLayer.x = x;
        this.bonesLayer.y = y;
    }
    // 切换父对象
    switchParent(parent, index) {
        if (parent) {
            parent.addChildAt(this.bonesLayer, index);
        }
    }
}
class NewBoneManager {
    static createBone(url, parent, firstCall, followerSkPack = null, index = -1) {
        let b = new newBone();
        b.init(url, firstCall);
        if (index != -1)
            parent && parent.addChildAt(b.bonesLayer, index);
        else
            parent && parent.addChild(b.bonesLayer);
        return b;
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/Manager/MesManager.ts":
/*!**********************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/Manager/MesManager.ts ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MesManager; });
/* harmony import */ var _script_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../script/common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");

// 简易信息类
class MesManager extends Laya.EventDispatcher {
    static get instance() {
        if (this._instance == null) {
            this._instance = new MesManager();
        }
        return this._instance;
    }
    eventUI(event, data) {
        _script_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_0__["default"].log("UI " + event);
        MesManager.instance.event("UI " + event, data);
    }
    event3d(event, data) {
        _script_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_0__["default"].log("Scene3d " + event);
        MesManager.instance.event("Scene3d" + event, data);
    }
    event2d(event, data) {
        _script_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_0__["default"].log("Scene2d " + event);
        MesManager.instance.event("Scene2d" + event, data);
    }
    onUI(type, caller, listener, args) {
        MesManager.instance.on("UI " + type, caller, listener, args);
    }
    offUI(type, caller, listener) {
        MesManager.instance.off("UI " + type, caller, listener);
    }
    off3d(type, caller, listener) {
        MesManager.instance.off("Scene3d" + type, caller, listener);
    }
    off2d(type, caller, listener) {
        MesManager.instance.off("Scene2d" + type, caller, listener);
    }
    on2d(type, caller, listener, args) {
        MesManager.instance.on("Scene2d" + type, caller, listener, args);
    }
    on3d(type, caller, listener, args) {
        MesManager.instance.on("Scene3d" + type, caller, listener, args);
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/Manager/ShopManager.ts":
/*!***********************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/Manager/ShopManager.ts ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ShopManager; });
/* harmony import */ var _script_common_GameData__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../script/common/GameData */ "./Number-Jump/scripts/script/common/GameData.ts");

class ShopManager {
    constructor() {
        // 商店的道具
        this._shopItems = [];
        this._Init();
    }
    static get instance() {
        if (this._instance == null) {
            this._instance = new ShopManager();
        }
        return this._instance;
    }
    _Init() {
        _script_common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].SaveToDisk();
    }
    usedHero() {
        return this._tryHeroId;
    }
    clearTry() {
        this._tryHeroId = null;
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/Start/ESceneType.ts":
/*!********************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/Start/ESceneType.ts ***!
  \********************************************************/
/*! exports provided: ESceneType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ESceneType", function() { return ESceneType; });
var ESceneType;
(function (ESceneType) {
    ESceneType[ESceneType["None"] = 0] = "None";
    ESceneType[ESceneType["Splash"] = 1] = "Splash";
    ESceneType[ESceneType["Main"] = 2] = "Main";
})(ESceneType || (ESceneType = {}));


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/Start/LTMain.ts":
/*!****************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/Start/LTMain.ts ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return LTMain; });
/* harmony import */ var _Commom_EScreenOrientation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Commom/EScreenOrientation */ "./Number-Jump/scripts/LTGame/Commom/EScreenOrientation.ts");
/* harmony import */ var _script_common_Const__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../script/common/Const */ "./Number-Jump/scripts/script/common/Const.ts");


class LTMain {
    constructor(mainLogic) {
        this._mainLogic = mainLogic;
        // 关闭多点触控  LIUTODO
        Laya.MouseManager.multiTouchEnabled = false;
        let set = () => {
            switch (this._mainLogic.screenOrientation) {
                case _Commom_EScreenOrientation__WEBPACK_IMPORTED_MODULE_0__["EScreenOrientation"].Landscape:
                    Laya.stage.screenMode = Laya.Stage.SCREEN_HORIZONTAL;
                    break;
                case _Commom_EScreenOrientation__WEBPACK_IMPORTED_MODULE_0__["EScreenOrientation"].Portrait:
                    Laya.stage.screenMode = Laya.Stage.SCREEN_VERTICAL;
                    break;
                default:
                    console.error("未处理的屏幕初始化方向", this._mainLogic.screenOrientation);
                    break;
            }
            Laya.stage.scaleMode = Laya.Stage.SCALE_FIXED_AUTO;
            Laya.stage.alignV = Laya.Stage.ALIGN_MIDDLE;
            Laya.stage.alignH = Laya.Stage.ALIGN_CENTER;
            //兼容微信不支持加载scene后缀场景
            Laya.URL.exportSceneToJson = true;
            //打开调试面板（通过IDE设置调试模式，或者url地址增加debug=true参数，均可打开调试面板）
            // if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true") Laya.enableDebugPanel();
            // if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"]) Laya["PhysicsDebugDraw"].enable();
            if (_script_common_Const__WEBPACK_IMPORTED_MODULE_1__["default"].enableStat) {
                Laya.Stat.show();
            }
            Laya.alertGlobalError(false);
            this._mainLogic.InitGame();
        };
        //根据IDE设置初始化引擎
        if (_script_common_Const__WEBPACK_IMPORTED_MODULE_1__["default"].support3D) {
            let config3D = new Config3D();
            config3D.octreeCulling = false;
            config3D.enableMultiLight = true;
            config3D.isAntialias = true;
            Laya3D.init(this._mainLogic.designWidth, this._mainLogic.designHeight, config3D, Laya.Handler.create(null, () => {
                set();
            }));
        }
        else {
            Config.useRetinalCanvas = true;
            Laya.init(this._mainLogic.designWidth, this._mainLogic.designHeight, Laya["WebGL"]);
            set();
        }
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/Start/LTSplashScene.ts":
/*!***********************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/Start/LTSplashScene.ts ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return LTSplashScene; });
/* harmony import */ var _Fsm_BaseState__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Fsm/BaseState */ "./Number-Jump/scripts/LTGame/Fsm/BaseState.ts");
/* harmony import */ var _Config_ConfigManager__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Config/ConfigManager */ "./Number-Jump/scripts/LTGame/Config/ConfigManager.ts");
/* harmony import */ var _ESceneType__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ESceneType */ "./Number-Jump/scripts/LTGame/Start/ESceneType.ts");
/* harmony import */ var _script_common_Const__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../script/common/Const */ "./Number-Jump/scripts/script/common/Const.ts");
/* harmony import */ var _script_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../script/scene/Main3d/NumberJump */ "./Number-Jump/scripts/script/scene/Main3d/NumberJump.ts");
/* harmony import */ var _script_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../script/common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");






class LTSplashScene extends _Fsm_BaseState__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor() {
        super(_ESceneType__WEBPACK_IMPORTED_MODULE_2__["ESceneType"].Splash);
        /**
         * 资源加载权重:
         * 子包
         * 配置
         * 资源
         * 其他ui
         */
        this._loadProgressWeight = [3, 5, 15, 7, 3];
        this._configProgress = 0;
        this._otherUIProgress = 0;
        this._otherPackProgress = 0;
        this._resProgress = 0;
        this._subPackProgress = 0;
        this._isUIShowed = false;
        this._maxPro = 100;
        this._OnConfigLoaded = () => {
            if (_script_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_5__["default"]._needLoadOtherUIPack.length == 0) {
                this._OnOtherUILoaded();
                return;
            }
            let uiLoadData = [];
            for (let i = 0; i < _script_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_5__["default"]._needLoadOtherUIPack.length; ++i) {
                let otherUIPack = _script_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_5__["default"]._needLoadOtherUIPack[i];
                otherUIPack.PushUrl(uiLoadData);
            }
            this._otherPackProgress = 1;
            if (!_script_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_4__["default"].release)
                Laya.loader.load(uiLoadData, Laya.Handler.create(this, this._OnOtherUILoaded), Laya.Handler.create(this, this._OnOtherUIProgress, null, false), null, null, true, _script_common_Const__WEBPACK_IMPORTED_MODULE_3__["default"].gameNameJian);
            else
                this._OnOtherUILoaded();
        };
        this._lastValue = 0;
        this._loadProgressWeight = [0, 1, 10, 3, 1];
    }
    get _loadProgress() {
        let totalWeight = this._loadProgressWeight[0] +
            this._loadProgressWeight[1] +
            this._loadProgressWeight[2] +
            this._loadProgressWeight[3] +
            this._loadProgressWeight[4];
        let loadWeight = this._loadProgressWeight[0] * this._subPackProgress +
            this._loadProgressWeight[1] * this._configProgress +
            this._loadProgressWeight[2] * this._resProgress +
            this._loadProgressWeight[3] * this._otherUIProgress +
            this._loadProgressWeight[4] * this._otherPackProgress;
        return (loadWeight / totalWeight) * this._maxPro;
    }
    _DoEnter() {
        this._OnJsonLoaded();
    }
    _OnJsonLoaded() {
        this._InitUI();
    }
    _InitUI() {
        this._OnBindUI();
        this._subPackProgress = 1;
        this._StartLoad();
    }
    _OnBindUI() { }
    _StartLoad() {
        this._OnSetLoadConfig();
        if (_Config_ConfigManager__WEBPACK_IMPORTED_MODULE_1__["ConfigManager"].needLoadCount <= 0) {
            this._configProgress = 1;
            this._OnConfigLoaded();
            return;
        }
        _Config_ConfigManager__WEBPACK_IMPORTED_MODULE_1__["ConfigManager"].StartLoad(Laya.Handler.create(this, this._OnConfigLoaded), Laya.Handler.create(this, this._OnConfigProgress, null, false));
    }
    _OnSetLoadConfig() { }
    _OnConfigProgress(value) {
        this._configProgress = value;
    }
    _OnOtherUIProgress(value) {
        this._otherUIProgress = value;
    }
    _OnOtherUILoaded() {
        if (!_script_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_4__["default"].release) {
            for (let i = 0; i < _script_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_5__["default"]._needLoadOtherUIPack.length; ++i) {
                let otherUIPack = _script_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_5__["default"]._needLoadOtherUIPack[i];
                otherUIPack.AddPackage();
            }
        }
        // this.setPackageItemExtension();
        let loadUrls = [];
        this._OnGameResPrepared(loadUrls);
        if (loadUrls.length == 0) {
            this._resProgress = 1;
            this._OnResLoaded();
            return;
        }
        Laya.loader.create(loadUrls, Laya.Handler.create(this, this._OnResLoaded), Laya.Handler.create(this, this._OnResProgress, null, false));
    }
    setPackageItemExtension() {
        let mainComToClass = _script_common_Const__WEBPACK_IMPORTED_MODULE_3__["default"].mainPackToClass;
        for (const key in mainComToClass) {
            fgui.UIObjectFactory.setExtension(fgui.UIPackage.getItemURL(_script_common_Const__WEBPACK_IMPORTED_MODULE_3__["default"].gameNameJian, key), mainComToClass[key]);
        }
    }
    /**
     * 准备需要在splash界面加载的资源,会体现在进度条上
     * @param urls
     */
    _OnGameResPrepared(urls) { }
    _OnResProgress(value) {
        this._resProgress = value;
    }
    _OnResLoaded() {
        Laya.timer.clearAll(this);
        this._OnGameResLoaded();
    }
    _OnGameResLoaded() { }
    _DoRunning() {
        let pro = this._loadProgress;
        if (pro <= this._lastValue) {
            this._lastValue += 0.1;
            pro = this._lastValue;
        }
        let value = Math.min(100, pro);
        this._progressFunc && this._progressFunc(value);
    }
    setProgressFunc(progressFunc) {
        this._progressFunc = progressFunc;
    }
    setLoadOverCall(loadOverCall) {
        this._loadOverCall = loadOverCall;
    }
    loadOverCall() {
        if (this._loadOverCall) {
            this._loadOverCall.run();
        }
        this._loadOverCall = null;
    }
    _DoExit() { }
}


/***/ }),

/***/ "./Number-Jump/scripts/LTGame/Start/LTStart.ts":
/*!*****************************************************!*\
  !*** ./Number-Jump/scripts/LTGame/Start/LTStart.ts ***!
  \*****************************************************/
/*! exports provided: LTStart */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LTStart", function() { return LTStart; });
/* harmony import */ var _Fsm_StateMachine__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Fsm/StateMachine */ "./Number-Jump/scripts/LTGame/Fsm/StateMachine.ts");
/* harmony import */ var _UIExt_FGui_FGuiEx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../UIExt/FGui/FGuiEx */ "./Number-Jump/scripts/UIExt/FGui/FGuiEx.ts");
/* harmony import */ var _LTUtils_MonoHelper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../LTUtils/MonoHelper */ "./Number-Jump/scripts/LTGame/LTUtils/MonoHelper.ts");
/* harmony import */ var _Commom_EScreenOrientation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Commom/EScreenOrientation */ "./Number-Jump/scripts/LTGame/Commom/EScreenOrientation.ts");




class LTStart {
    constructor() {
        this.screenOrientation = _Commom_EScreenOrientation__WEBPACK_IMPORTED_MODULE_3__["EScreenOrientation"].Portrait;
    }
    get _currentState() {
        return this._fsm.currState;
    }
    get designWidth() {
        switch (this.screenOrientation) {
            case _Commom_EScreenOrientation__WEBPACK_IMPORTED_MODULE_3__["EScreenOrientation"].Portrait:
                return 828;
            case _Commom_EScreenOrientation__WEBPACK_IMPORTED_MODULE_3__["EScreenOrientation"].Landscape:
                return 1656;
            default:
                console.error("未处理的屏幕初始化方向", this.screenOrientation);
                return 0;
        }
    }
    get designHeight() {
        switch (this.screenOrientation) {
            case _Commom_EScreenOrientation__WEBPACK_IMPORTED_MODULE_3__["EScreenOrientation"].Portrait:
                return 1656;
            case _Commom_EScreenOrientation__WEBPACK_IMPORTED_MODULE_3__["EScreenOrientation"].Landscape:
                return 828;
            default:
                console.error("未处理的屏幕初始化方向", this.screenOrientation);
                return 0;
        }
    }
    InitGame() {
        this._Init();
    }
    _Init() {
        _UIExt_FGui_FGuiEx__WEBPACK_IMPORTED_MODULE_1__["default"].Init();
        this._NextFramUpdate();
    }
    _NextFramUpdate() {
        this._fsm = new _Fsm_StateMachine__WEBPACK_IMPORTED_MODULE_0__["default"]();
        this._InitFsm();
        this._RegistUpdate();
    }
    _RegistUpdate() {
        _LTUtils_MonoHelper__WEBPACK_IMPORTED_MODULE_2__["default"].instance.AddAction(_LTUtils_MonoHelper__WEBPACK_IMPORTED_MODULE_2__["EActionType"].Update, this, this._LogicUpdate);
        _LTUtils_MonoHelper__WEBPACK_IMPORTED_MODULE_2__["default"].instance.AddAction(_LTUtils_MonoHelper__WEBPACK_IMPORTED_MODULE_2__["EActionType"].LateUpdate, this, this._LateUpdate);
    }
    _UnRegistUpdate() {
        _LTUtils_MonoHelper__WEBPACK_IMPORTED_MODULE_2__["default"].instance.RemoveAction(_LTUtils_MonoHelper__WEBPACK_IMPORTED_MODULE_2__["EActionType"].Update, this, this._LogicUpdate);
        _LTUtils_MonoHelper__WEBPACK_IMPORTED_MODULE_2__["default"].instance.RemoveAction(_LTUtils_MonoHelper__WEBPACK_IMPORTED_MODULE_2__["EActionType"].LateUpdate, this, this._LateUpdate);
    }
    _LogicUpdate() {
        let dt = Laya.timer.delta / 1000;
        if (!this._currentState)
            return;
        let nextState = this._currentState.GetNextState();
        if (nextState != 0) {
            var changeResult = this._fsm.ChangeState(nextState, null);
            if (!changeResult) {
                console.error("场景切换发生错误,无法进行切换,终止游戏流程");
                this._UnRegistUpdate();
                return;
            }
        }
        this._fsm.OnRunning(null, dt);
    }
    _LateUpdate() {
        let dt = Laya.timer.delta / 1000;
        this._currentState && this._currentState.OnLateUpdate(dt);
    }
    _InitFsm() {
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/UIExt/FGui/BaseUIMediator.ts":
/*!**********************************************************!*\
  !*** ./Number-Jump/scripts/UIExt/FGui/BaseUIMediator.ts ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return BaseUIMediator; });
/* harmony import */ var _FGuiEx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./FGuiEx */ "./Number-Jump/scripts/UIExt/FGui/FGuiEx.ts");
/* harmony import */ var _FGuiData__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FGuiData */ "./Number-Jump/scripts/UIExt/FGui/FGuiData.ts");
/* harmony import */ var _LTGame_Manager_MesManager__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../LTGame/Manager/MesManager */ "./Number-Jump/scripts/LTGame/Manager/MesManager.ts");
/* harmony import */ var _script_enum_EEventUI__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../script/enum/EEventUI */ "./Number-Jump/scripts/script/enum/EEventUI.ts");




class BaseUIMediator {
    constructor() {
        this._scaleSmall = 0.8;
        this._tweenTime = 180;
        this.openAniTime = 180;
        this._noClose = false;
        this._needLoadPanel = true;
        this._isLoginOpen = false;
        this._needSubRes = [];
        this._checkboxPortalAd = true;
        this._layer = _FGuiEx__WEBPACK_IMPORTED_MODULE_0__["EUILayer"].Panel;
        this._isHaveBg = true;
        this._isFullView = false;
        /**
         * 是否适配顶部刘海屏
         */
        this._needFilScreen = false;
        this._isShow = false;
        this._parent = null;
        this._sortOrder = 10;
    }
    static get instance() { return null; }
    ;
    get ui() {
        return this._ui;
    }
    get isShow() {
        return this._isShow;
    }
    _InitBottom() {
        let bottomGroup = this.ui["m_bottom_group"];
        if (bottomGroup == null) {
            this._defaultBottomHeight = 0;
        }
        else {
            this._defaultBottomHeight = bottomGroup.y;
        }
    }
    _SetBottomDown(downDistance = 0) {
        let bottomGroup = this.ui["m_bottom_group"];
        if (bottomGroup == null)
            return;
        bottomGroup.y = this._defaultBottomHeight + downDistance;
    }
    _closeLoadPanel() {
    }
    /**
     * 异步打开
     * @param obj
     * @param backPanel
     * @returns
     */
    Show(obj = null, backPanel) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this._ui) {
                this._ui.visible = true;
            }
            if (this._isShow) {
                if (this._isShow && this._parent) {
                    this._parent.addChild(this._ui);
                }
                return;
            }
            this.eventListers = [];
            this.eventObjects = [];
            this.eventTypes = [];
            this._isShow = true;
            yield this.loadRes();
            if (!this._isShow)
                return;
            this._closeLoadPanel();
            this._backPanel = backPanel;
            this._openParam = obj;
            let uiData = new _FGuiData__WEBPACK_IMPORTED_MODULE_1__["default"]();
            uiData.needFitScreen = this._needFilScreen;
            if (this._ui) {
                this._parent.addChild(this._ui);
            }
            else {
                this._ui = _FGuiEx__WEBPACK_IMPORTED_MODULE_0__["default"].AddUI(this._classDefine, uiData, this._layer);
                this._parent = this._ui.parent;
            }
            if (!this._ui) {
                return;
            }
            this._ui["mediator"] = this;
            if (this._layer == _FGuiEx__WEBPACK_IMPORTED_MODULE_0__["EUILayer"].Panel || this._layer == _FGuiEx__WEBPACK_IMPORTED_MODULE_0__["EUILayer"].MainPanel) {
                _FGuiEx__WEBPACK_IMPORTED_MODULE_0__["default"].panels.push(this);
            }
            else if (this._layer == _FGuiEx__WEBPACK_IMPORTED_MODULE_0__["EUILayer"].Main) {
                _FGuiEx__WEBPACK_IMPORTED_MODULE_0__["default"].mainPanels.push(this);
            }
            this._ui.sortingOrder = this._sortOrder;
            this._OnShow();
            this._finalOpen();
        });
    }
    _finalOpen() {
        if (!this.ui) {
            return;
        }
        _LTGame_Manager_MesManager__WEBPACK_IMPORTED_MODULE_2__["default"].instance.eventUI(_script_enum_EEventUI__WEBPACK_IMPORTED_MODULE_3__["EEventUI"].PanelOpen, [this.ui.resourceURL, this]);
    }
    _OnEnterAnimEnd() {
    }
    onClick(object, listener, args) {
        this.addEvent(Laya.Event.CLICK, object, listener, args);
    }
    addEvent(type, object, listener, args) {
        object.on(type, this, listener, args);
        this.eventTypes.push(type);
        this.eventObjects.push(object);
        this.eventListers.push(listener);
    }
    removeEvents() {
        for (let i = 0; i < this.eventTypes.length; i++) {
            this.eventObjects[i].off(this.eventTypes[i], this, this.eventListers[i]);
        }
        this.eventListers.length = 0;
        this.eventObjects.length = 0;
        this.eventListers.length = 0;
    }
    _OnShow() { }
    Hide(dispose = true, backPanel = true) {
        if (!this._isShow)
            return;
        this._closeLoadPanel();
        if (this._ui == null)
            return;
        if (this._ui.isDisposed)
            return;
        this._isShow = false;
        this._OnHide();
        this.removeEvents();
        Laya.Tween.clearAll(this._ui);
        _LTGame_Manager_MesManager__WEBPACK_IMPORTED_MODULE_2__["default"].instance.eventUI(_script_enum_EEventUI__WEBPACK_IMPORTED_MODULE_3__["EEventUI"].PanelHide, this.ui.resourceURL);
        this._DoHide(dispose, backPanel);
    }
    /**
    * 其他面板关闭 此面板置于顶层调用(暂时main 和 panel层级执行)
    */
    viewIntop() {
    }
    /**
     * 其他界面关闭回调
     */
    viewBackCall(view) {
    }
    _DoHide(dispose, backPanel = true) {
        return __awaiter(this, void 0, void 0, function* () {
            if (dispose) {
                this.ui.dispose();
                this._ui = null;
            }
            else {
                this.ui.removeFromParent();
            }
            if (this._backPanel && backPanel) {
                this._isLoginOpen && (this._backPanel.instance._isLoginOpen = true);
                yield this._backPanel.instance.Show();
                this._backPanel.instance.viewBackCall(this);
            }
            this._backPanel = null;
            this._isLoginOpen = false;
        });
    }
    /**资源加载 */
    loadRes() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    _OnHide() { }
}


/***/ }),

/***/ "./Number-Jump/scripts/UIExt/FGui/FGuiData.ts":
/*!****************************************************!*\
  !*** ./Number-Jump/scripts/UIExt/FGui/FGuiData.ts ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FGuiData; });
class FGuiData {
    constructor() {
        /**
         * 是否需要适配刘海屏
         */
        this.needFitScreen = false;
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/UIExt/FGui/FGuiEx.ts":
/*!**************************************************!*\
  !*** ./Number-Jump/scripts/UIExt/FGui/FGuiEx.ts ***!
  \**************************************************/
/*! exports provided: EUILayer, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EUILayer", function() { return EUILayer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FGuiEx; });
/* harmony import */ var _LTGame_LTUtils_LTDictionary__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../LTGame/LTUtils/LTDictionary */ "./Number-Jump/scripts/LTGame/LTUtils/LTDictionary.ts");
/* harmony import */ var _FGuiData__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FGuiData */ "./Number-Jump/scripts/UIExt/FGui/FGuiData.ts");
/* harmony import */ var _script_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../script/scene/Main3d/NumberJump */ "./Number-Jump/scripts/script/scene/Main3d/NumberJump.ts");



var EUILayer;
(function (EUILayer) {
    EUILayer["Battle"] = "Battle";
    EUILayer["BattleTop"] = "BattleTop";
    EUILayer["Main"] = "Main";
    EUILayer["Panel"] = "Panel";
    EUILayer["MainPanel"] = "MainPanel";
})(EUILayer || (EUILayer = {}));
class FGuiEx {
    static getLayer(layerType) {
        switch (layerType) {
            case EUILayer.Main:
                return this.mainLayer;
            case EUILayer.Panel:
                return this.mainLayer;
            case EUILayer.MainPanel:
                // return this.mainLayer;
                return _script_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_2__["default"].instance.view;
        }
        return null;
    }
    static Init() {
        fgui.UIConfig.packageFileExtension = "bin";
        if (!_script_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_2__["default"].release) {
            Laya.stage.addChild(fgui.GRoot.inst.displayObject);
            this.mainLayer = fgui.GRoot.inst.addChild(new fgui.GComponent());
            this.UpdateAllUI();
            Laya.stage.on(Laya.Event.RESIZE, this, this.UpdateAllUI);
        }
    }
    static removeAllLayer() {
        this.remove(FGuiEx.popupPanels);
        this.remove(FGuiEx.panels);
        this.remove(FGuiEx.mainPanels);
    }
    static remove(layer) {
        layer && layer.forEach((panel) => {
            panel && panel.Hide();
        });
        layer = [];
    }
    static AddUI(uiType, param = null, layer) {
        let ui = uiType.createInstance();
        if (!_script_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_2__["default"].release || layer == EUILayer.MainPanel) {
            this.getLayer(layer).addChild(ui);
        }
        if (param == null) {
            param = new _FGuiData__WEBPACK_IMPORTED_MODULE_1__["default"]();
        }
        if (param == null || !param.needFitScreen) {
            ui.setSize(fgui.GRoot.inst.width, fgui.GRoot.inst.height);
        }
        else {
            ui.setSize(fgui.GRoot.inst.width, fgui.GRoot.inst.height - this.top - this.bottom);
            ui.y = this.top;
        }
        this._cacheMap.set(ui.constructor.name, param);
        window[ui.constructor.name] = ui;
        return ui;
    }
    static UpdateAllUI() {
        let setWidth = Laya.stage.width;
        let setHeight = Laya.stage.height;
        fgui.GRoot.inst.setSize(setWidth, setHeight);
        let childCount = fgui.GRoot.inst.numChildren;
        for (let i = 0; i < childCount; ++i) {
            let ui = fgui.GRoot.inst.getChildAt(i);
            let getData = this._cacheMap.get(ui.constructor.name);
            if (getData == null || !getData.needFitScreen) {
                ui.setSize(fgui.GRoot.inst.width, fgui.GRoot.inst.height);
            }
            else {
                ui.setSize(fgui.GRoot.inst.width, fgui.GRoot.inst.height - this.top - this.bottom);
                ui.y = this.top;
            }
        }
    }
    static CheckIn(ui, x, y) {
        if (x > ui.x && x < ui.x + ui.width && y > ui.y && y < ui.y + ui.height) {
            return true;
        }
        return false;
    }
}
FGuiEx.top = 0;
FGuiEx.bottom = 0;
FGuiEx._cacheMap = new _LTGame_LTUtils_LTDictionary__WEBPACK_IMPORTED_MODULE_0__["default"]();
FGuiEx.panels = [];
FGuiEx.mainPanels = [];
FGuiEx.popupPanels = [];


/***/ }),

/***/ "./Number-Jump/scripts/UIExt/FGui/LoadUIPack.ts":
/*!******************************************************!*\
  !*** ./Number-Jump/scripts/UIExt/FGui/LoadUIPack.ts ***!
  \******************************************************/
/*! exports provided: LoadUIPack */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadUIPack", function() { return LoadUIPack; });
/* harmony import */ var _script_common_Const__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../script/common/Const */ "./Number-Jump/scripts/script/common/Const.ts");

class LoadUIPack {
    constructor(packPath, atliasCount = 1) {
        this.packPath = packPath;
        this.atliasCount = atliasCount;
    }
    PushUrl(urls) {
        urls.push({ url: this.packPath + ".bin", type: Laya.Loader.BUFFER });
        if (this.atliasCount == 0)
            return;
        urls.push({ url: this.packPath + "_atlas0.png", type: Laya.Loader.IMAGE });
        for (let i = 1; i < this.atliasCount; ++i) {
            urls.push({ url: this.packPath + "_atlas0_" + i + ".png", type: Laya.Loader.IMAGE });
        }
    }
    load(callBack) {
        let urls = [];
        this.PushUrl(urls);
        Laya.loader.load(urls, Laya.Handler.create(this, () => {
            this.AddPackage();
            callBack && callBack.run();
        }), null, null, null, null, _script_common_Const__WEBPACK_IMPORTED_MODULE_0__["default"].gameNameJian);
    }
    AddPackage() {
        let pack = fgui.UIPackage.getById(this.packPath);
        if (!pack) {
            fgui.UIPackage.addPackage(this.packPath);
        }
    }
    removePackage() {
        let pack = fgui.UIPackage.getById(this.packPath);
        if (pack) {
            fgui.UIPackage.removePackage(this.packPath);
        }
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/main.ts":
/*!*************************************!*\
  !*** ./Number-Jump/scripts/main.ts ***!
  \*************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _LTGame_Start_LTMain__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./LTGame/Start/LTMain */ "./Number-Jump/scripts/LTGame/Start/LTMain.ts");
/* harmony import */ var _script_MainStart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./script/MainStart */ "./Number-Jump/scripts/script/MainStart.ts");
/* harmony import */ var _script_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./script/scene/Main3d/NumberJump */ "./Number-Jump/scripts/script/scene/Main3d/NumberJump.ts");



if (!_script_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_2__["default"].release) {
    new _LTGame_Start_LTMain__WEBPACK_IMPORTED_MODULE_0__["default"](_script_MainStart__WEBPACK_IMPORTED_MODULE_1__["default"].instance);
}


/***/ }),

/***/ "./Number-Jump/scripts/script/MainStart.ts":
/*!*************************************************!*\
  !*** ./Number-Jump/scripts/script/MainStart.ts ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MainStart; });
/* harmony import */ var _LTGame_Start_LTStart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../LTGame/Start/LTStart */ "./Number-Jump/scripts/LTGame/Start/LTStart.ts");
/* harmony import */ var _scene_SplashScene__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scene/SplashScene */ "./Number-Jump/scripts/script/scene/SplashScene.ts");
/* harmony import */ var _scene_MainScene__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./scene/MainScene */ "./Number-Jump/scripts/script/scene/MainScene.ts");
/* harmony import */ var _LTGame_Start_ESceneType__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../LTGame/Start/ESceneType */ "./Number-Jump/scripts/LTGame/Start/ESceneType.ts");
/* harmony import */ var _demo_Demo__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../demo/Demo */ "./demo/Demo.ts");
/* harmony import */ var _demo_gen_ui_Demo_UI_Demo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../demo/gen/ui/Demo/UI_Demo */ "./demo/gen/ui/Demo/UI_Demo.ts");
/* harmony import */ var _UIExt_FGui_FGuiData__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../UIExt/FGui/FGuiData */ "./Number-Jump/scripts/UIExt/FGui/FGuiData.ts");
/* harmony import */ var _UIExt_FGui_FGuiEx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../UIExt/FGui/FGuiEx */ "./Number-Jump/scripts/UIExt/FGui/FGuiEx.ts");
/* harmony import */ var _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./scene/Main3d/NumberJump */ "./Number-Jump/scripts/script/scene/Main3d/NumberJump.ts");
/* harmony import */ var _UIExt_FGui_LoadUIPack__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../UIExt/FGui/LoadUIPack */ "./Number-Jump/scripts/UIExt/FGui/LoadUIPack.ts");
/* harmony import */ var _demo_gen_ui_Demo_DemoBinder__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../demo/gen/ui/Demo/DemoBinder */ "./demo/gen/ui/Demo/DemoBinder.ts");











class MainStart extends _LTGame_Start_LTStart__WEBPACK_IMPORTED_MODULE_0__["LTStart"] {
    constructor() {
        super();
        /**
         * 用于初始化的ui包
         */
        this._initUiPack = new _UIExt_FGui_LoadUIPack__WEBPACK_IMPORTED_MODULE_9__["LoadUIPack"]("demo/ui/Demo");
    }
    static get instance() {
        if (this._instance == null) {
            this._instance = new MainStart();
        }
        return this._instance;
    }
    loadSplashScene() {
        this._fsm.ChangeState(1);
    }
    enterMainScene() {
        this.splashScene.nextState = _LTGame_Start_ESceneType__WEBPACK_IMPORTED_MODULE_3__["ESceneType"].Main;
    }
    _InitFsm() {
        this.splashScene = new _scene_SplashScene__WEBPACK_IMPORTED_MODULE_1__["default"]();
        this.mainScene = new _scene_MainScene__WEBPACK_IMPORTED_MODULE_2__["default"]();
        this._fsm.Add(this.splashScene);
        this._fsm.Add(this.mainScene);
        if (!_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_8__["default"].release) {
            let loadUrl = [];
            _demo_gen_ui_Demo_DemoBinder__WEBPACK_IMPORTED_MODULE_10__["default"].bindAll();
            this._initUiPack.PushUrl(loadUrl);
            Laya.loader.load(loadUrl, Laya.Handler.create(this, this.demo));
        }
    }
    demo() {
        this._initUiPack.AddPackage();
        // 打开界面
        let needFit = new _UIExt_FGui_FGuiData__WEBPACK_IMPORTED_MODULE_6__["default"]();
        needFit.needFitScreen = false;
        this._ui = _UIExt_FGui_FGuiEx__WEBPACK_IMPORTED_MODULE_7__["default"].AddUI(_demo_gen_ui_Demo_UI_Demo__WEBPACK_IMPORTED_MODULE_5__["default"], needFit, _UIExt_FGui_FGuiEx__WEBPACK_IMPORTED_MODULE_7__["EUILayer"].Panel);
        this._ui.sortingOrder = Number.MAX_SAFE_INTEGER;
        this._demo = new _demo_Demo__WEBPACK_IMPORTED_MODULE_4__["Demo"](this._ui, _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_8__["default"].instance);
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/common/Const.ts":
/*!****************************************************!*\
  !*** ./Number-Jump/scripts/script/common/Const.ts ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Const; });
/* harmony import */ var _ui_com_heroItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../ui/com/heroItem */ "./Number-Jump/scripts/script/ui/com/heroItem.ts");
/* harmony import */ var _ui_com_mapLayer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../ui/com/mapLayer */ "./Number-Jump/scripts/script/ui/com/mapLayer.ts");
/* harmony import */ var _ui_prop_bigJianciItem__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../ui/prop/bigJianciItem */ "./Number-Jump/scripts/script/ui/prop/bigJianciItem.ts");
/* harmony import */ var _ui_prop_blackItem__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ui/prop/blackItem */ "./Number-Jump/scripts/script/ui/prop/blackItem.ts");
/* harmony import */ var _ui_prop_boxItem__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../ui/prop/boxItem */ "./Number-Jump/scripts/script/ui/prop/boxItem.ts");
/* harmony import */ var _ui_prop_huadaoItem__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../ui/prop/huadaoItem */ "./Number-Jump/scripts/script/ui/prop/huadaoItem.ts");
/* harmony import */ var _ui_prop_jianciItem__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../ui/prop/jianciItem */ "./Number-Jump/scripts/script/ui/prop/jianciItem.ts");







// 不需配表的字段  (项目配置字段)
class Const {
}
/**资源版本 */
Const.resVersion = "1015";
/**资源版本 */
Const.ttResVersion = "1015";
/**项目名 */
Const.gameName = "数字跳一跳";
/**项目名 */
Const.gameNameJian = "NumberJump";
/*选择的关卡 */
Const.choseLv = 3;
/**
 * 显示性能面板
 */
Const.enableStat = true;
/**
 * 主包类名绑定
 */
Const.workPackToClass = {};
Const.mainPackToClass = {
    'bigJianciItem': _ui_prop_bigJianciItem__WEBPACK_IMPORTED_MODULE_2__["default"],
    'blackItem': _ui_prop_blackItem__WEBPACK_IMPORTED_MODULE_3__["default"],
    'boxItem': _ui_prop_boxItem__WEBPACK_IMPORTED_MODULE_4__["default"],
    'huadaoItem': _ui_prop_huadaoItem__WEBPACK_IMPORTED_MODULE_5__["default"],
    'jianciItem': _ui_prop_jianciItem__WEBPACK_IMPORTED_MODULE_6__["default"],
    'mapLayer': _ui_com_mapLayer__WEBPACK_IMPORTED_MODULE_1__["default"],
    'heroItem': _ui_com_heroItem__WEBPACK_IMPORTED_MODULE_0__["default"],
};
/**
 * 重力
 */
Const.gravity = -15;
/**
 * 支持2d物理
 */
Const.support2D = false;
/**
 * 支持3d物理
 */
Const.support3D = false;


/***/ }),

/***/ "./Number-Jump/scripts/script/common/GameData.ts":
/*!*******************************************************!*\
  !*** ./Number-Jump/scripts/script/common/GameData.ts ***!
  \*******************************************************/
/*! exports provided: SaveData, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SaveData", function() { return SaveData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return GameData; });
/* harmony import */ var _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../config/NumberJumpGameConst */ "./Number-Jump/scripts/script/config/NumberJumpGameConst.ts");
/* harmony import */ var _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../scene/Main3d/NumberJump */ "./Number-Jump/scripts/script/scene/Main3d/NumberJump.ts");


class SaveData {
    constructor() {
        /**
         * 最后一次打开时间戳
         */
        this.lastOpenTick = 0;
        /**
         * 钻石
         */
        this.diamondsCount = 0;
        // 默认关卡
        this.levelId = 1;
        // 护盾道具数量
        this.hundunNum = 0;
        // 伤害减少道具数量
        this.dffNum = 0;
        // 关卡数据
        // levelData: any;
        // 先保存关卡进度
        this.levelNowScore = 0;
        this.levelNowMaxScore = 0;
        this.chengfaNum = 0;
        this.backHomeNum = 0;
        this.bacKNum = 0;
    }
}
/**
 * 数据保存类
 */
class GameData {
    constructor() {
        this.isInitdata = false;
        this._savePath = "NumberJump-game01.sav";
    }
    static get instance() {
        if (this._instance == null) {
            this._instance = new GameData();
            this._instance._ReadFromFile();
        }
        if (!this._instance.isInitdata && _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_0__["NumberJumpGameConst"].data) {
            this._instance.isInitdata = true;
            this._instance._backGame();
        }
        return this._instance._saveData;
    }
    static addDiamonds(num) {
        GameData.instance.diamondsCount += num;
        if (GameData.instance.diamondsCount < 0)
            GameData.instance.diamondsCount = 0;
        GameData.SaveToDisk();
    }
    static reduceDiamonds(num) {
        GameData.instance.diamondsCount -= num;
        if (GameData.instance.diamondsCount < 0)
            GameData.instance.diamondsCount = 0;
        GameData.SaveToDisk();
    }
    // 保存到本地 （改变本地数据时调用）
    static SaveToDisk() {
        if (!this._instance)
            return;
        GameData.instance.lastOpenTick = Date.now();
        if (!_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_1__["default"].release) {
            // LIUTODO 删除
            let json = JSON.stringify(this._instance._saveData);
            Laya.LocalStorage.setJSON(this._instance._savePath, json);
        }
    }
    IsNullOrEmpty(str) {
        if (str == null)
            return true;
        if (str == "")
            return true;
        return false;
    }
    _ReadFromFile() {
        this._saveData = new SaveData();
        if (_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_1__["default"].release)
            return;
        let readStr = Laya.LocalStorage.getJSON(this._savePath);
        if (!this.IsNullOrEmpty(readStr)) {
            let jsonData = JSON.parse(readStr);
            for (let key in jsonData) {
                this._saveData[key] = jsonData[key];
            }
        }
    }
    readFromFile(json) {
        // this._saveData = new SaveData();
        if (!this.IsNullOrEmpty(json)) {
            let jsonData = JSON.parse(json);
            for (let key in jsonData) {
                this._saveData[key] = jsonData[key];
            }
        }
        _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_1__["default"].instance.stage = this._saveData.levelId;
    }
    _backGame() { }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts":
/*!*******************************************************************!*\
  !*** ./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return NumberJumpGlobalUnit; });
/* harmony import */ var _LTGame_Manager_MesManager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../LTGame/Manager/MesManager */ "./Number-Jump/scripts/LTGame/Manager/MesManager.ts");
/* harmony import */ var _UIExt_FGui_LoadUIPack__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../UIExt/FGui/LoadUIPack */ "./Number-Jump/scripts/UIExt/FGui/LoadUIPack.ts");
/* harmony import */ var _enum_EEventUI__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../enum/EEventUI */ "./Number-Jump/scripts/script/enum/EEventUI.ts");
/* harmony import */ var _scene_script_GameScript__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../scene/script/GameScript */ "./Number-Jump/scripts/script/scene/script/GameScript.ts");
/* harmony import */ var _Const__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Const */ "./Number-Jump/scripts/script/common/Const.ts");





class NumberJumpGlobalUnit {
    static InitAll() {
        return __awaiter(this, void 0, void 0, function* () {
            _LTGame_Manager_MesManager__WEBPACK_IMPORTED_MODULE_0__["default"].instance.onUI(_enum_EEventUI__WEBPACK_IMPORTED_MODULE_2__["EEventUI"].GamePasue, this, this._changePlay, [false]);
            _LTGame_Manager_MesManager__WEBPACK_IMPORTED_MODULE_0__["default"].instance.onUI(_enum_EEventUI__WEBPACK_IMPORTED_MODULE_2__["EEventUI"].GameResume, this, this._changePlay, [true]);
            this.gameScript = new _scene_script_GameScript__WEBPACK_IMPORTED_MODULE_3__["default"]();
        });
    }
    static get gameOverOrPause() {
        return this.pause || this.gameOver;
    }
    static _changePlay(play) {
        this.pause = !play;
    }
    static log(msg) {
        if (false) {}
    }
    static dispose() {
        Laya.loader.clearResByGroup(_Const__WEBPACK_IMPORTED_MODULE_4__["default"].gameNameJian);
        _LTGame_Manager_MesManager__WEBPACK_IMPORTED_MODULE_0__["default"].instance.offUI(_enum_EEventUI__WEBPACK_IMPORTED_MODULE_2__["EEventUI"].GamePasue, this, this._changePlay);
        _LTGame_Manager_MesManager__WEBPACK_IMPORTED_MODULE_0__["default"].instance.offUI(_enum_EEventUI__WEBPACK_IMPORTED_MODULE_2__["EEventUI"].GameResume, this, this._changePlay);
        this.pause = null;
        this.gameOver = null;
        this.gameScript = null;
        // 移除所有绑定
        let mainComToClass = _Const__WEBPACK_IMPORTED_MODULE_4__["default"].mainPackToClass;
        for (const key in mainComToClass) {
            fgui.UIObjectFactory.setExtension(fgui.UIPackage.getItemURL(_Const__WEBPACK_IMPORTED_MODULE_4__["default"].gameNameJian, key), null);
        }
        // 删除本包体
        this._needLoadOtherUIPack.forEach((pack) => {
            pack && pack.removePackage();
        });
    }
}
/**
 * 需要加载的其他UI包
 */
NumberJumpGlobalUnit._needLoadOtherUIPack = [
    new _UIExt_FGui_LoadUIPack__WEBPACK_IMPORTED_MODULE_1__["LoadUIPack"]("Assets/Number-Jump/ui/NumberJump", 2),
];


/***/ }),

/***/ "./Number-Jump/scripts/script/config/NumberJumpGameConst.ts":
/*!******************************************************************!*\
  !*** ./Number-Jump/scripts/script/config/NumberJumpGameConst.ts ***!
  \******************************************************************/
/*! exports provided: NumberJumpGameConst */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumberJumpGameConst", function() { return NumberJumpGameConst; });
var NumberJumpGameConst;
(function (NumberJumpGameConst) {
    class config {
    }
    NumberJumpGameConst.config = config;
    NumberJumpGameConst.path = "res/config/NumberJumpGameConst.json";
})(NumberJumpGameConst || (NumberJumpGameConst = {}));


/***/ }),

/***/ "./Number-Jump/scripts/script/config/NumberJumpGameName.ts":
/*!*****************************************************************!*\
  !*** ./Number-Jump/scripts/script/config/NumberJumpGameName.ts ***!
  \*****************************************************************/
/*! exports provided: NumberJumpGameName */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumberJumpGameName", function() { return NumberJumpGameName; });
var NumberJumpGameName;
(function (NumberJumpGameName) {
    class config {
    }
    NumberJumpGameName.config = config;
    NumberJumpGameName.path = "res/config/NumberJumpGameName.json";
})(NumberJumpGameName || (NumberJumpGameName = {}));


/***/ }),

/***/ "./Number-Jump/scripts/script/config/NumberJumpSkinConfig.ts":
/*!*******************************************************************!*\
  !*** ./Number-Jump/scripts/script/config/NumberJumpSkinConfig.ts ***!
  \*******************************************************************/
/*! exports provided: NumberJumpSkinConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumberJumpSkinConfig", function() { return NumberJumpSkinConfig; });
var NumberJumpSkinConfig;
(function (NumberJumpSkinConfig) {
    class config {
    }
    NumberJumpSkinConfig.config = config;
    NumberJumpSkinConfig.path = "res/config/NumberJumpSkinConfig.json";
})(NumberJumpSkinConfig || (NumberJumpSkinConfig = {}));


/***/ }),

/***/ "./Number-Jump/scripts/script/data/LevelProxy.ts":
/*!*******************************************************!*\
  !*** ./Number-Jump/scripts/script/data/LevelProxy.ts ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return LevelProxy; });
/* harmony import */ var _common_GameData__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/GameData */ "./Number-Jump/scripts/script/common/GameData.ts");
/* harmony import */ var _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../config/NumberJumpGameConst */ "./Number-Jump/scripts/script/config/NumberJumpGameConst.ts");


/*
 * 关卡数据处理类
 */
class LevelProxy {
    constructor() {
        this.level_data = [];
        this.skinConfigs = [];
        this.skinW = [];
        this.initData();
    }
    static get instance() {
        if (this._instance == null) {
            this._instance = new LevelProxy();
        }
        return this._instance;
    }
    initData() {
        this.level_data = [];
    }
    nowLevelArr() {
        if (!this.level_data[_common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelId]) {
            this.level_data[_common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelId] = [];
        }
        return this.level_data[_common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelId];
    }
    enterNextLv() {
        if (_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_1__["NumberJumpGameConst"].data.lvNums[this.nowLevelId()]) {
            _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelId++;
            _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].SaveToDisk();
            return true;
        }
        else {
            _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelId = 1;
            _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].SaveToDisk();
            return false;
        }
    }
    nowLevelId() {
        return _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_1__["NumberJumpGameConst"].data.forceLv || _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelId;
    }
    viewNumOffset() {
        return _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_1__["NumberJumpGameConst"].data.viewLvOffset[this.nowLevelId() - 1];
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/enum/EEventUI.ts":
/*!*****************************************************!*\
  !*** ./Number-Jump/scripts/script/enum/EEventUI.ts ***!
  \*****************************************************/
/*! exports provided: EEventUI */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EEventUI", function() { return EEventUI; });
var EEventUI;
(function (EEventUI) {
    EEventUI["HPCHANGE"] = "HPCHANGE";
    EEventUI["COINCHANGE"] = "COINCHANGE";
    EEventUI["PanelOpen"] = "PanelOpen";
    EEventUI["PanelHide"] = "PanelHide";
    EEventUI["GameResume"] = "GameResume";
    EEventUI["GamePasue"] = "GamePasue";
})(EEventUI || (EEventUI = {}));


/***/ }),

/***/ "./Number-Jump/scripts/script/enum/EPropType.ts":
/*!******************************************************!*\
  !*** ./Number-Jump/scripts/script/enum/EPropType.ts ***!
  \******************************************************/
/*! exports provided: EpropType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EpropType", function() { return EpropType; });
var EpropType;
(function (EpropType) {
    EpropType[EpropType["black"] = 1] = "black";
    EpropType[EpropType["box"] = 2] = "box";
    EpropType[EpropType["huadao"] = 3] = "huadao";
    EpropType[EpropType["jianci"] = 4] = "jianci";
    EpropType[EpropType["bigJianci"] = 5] = "bigJianci";
    EpropType[EpropType["sphuadao"] = 6] = "sphuadao";
})(EpropType || (EpropType = {}));


/***/ }),

/***/ "./Number-Jump/scripts/script/enum/ESound.ts":
/*!***************************************************!*\
  !*** ./Number-Jump/scripts/script/enum/ESound.ts ***!
  \***************************************************/
/*! exports provided: ESoundName */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ESoundName", function() { return ESoundName; });
var ESoundName;
(function (ESoundName) {
    ESoundName["bgm"] = "bgm";
    ESoundName["wrong"] = "wrong";
    ESoundName["fall"] = "fall";
    ESoundName["jump"] = "jump";
    ESoundName["compress"] = "compress";
    ESoundName["break"] = "break";
    ESoundName["levelup"] = "levelup";
})(ESoundName || (ESoundName = {}));


/***/ }),

/***/ "./Number-Jump/scripts/script/scene/Main3d/NumberJump.ts":
/*!***************************************************************!*\
  !*** ./Number-Jump/scripts/script/scene/Main3d/NumberJump.ts ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return NumberJump; });
/* harmony import */ var _EventEmitter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../EventEmitter */ "./Number-Jump/scripts/EventEmitter.ts");
/* harmony import */ var _LTGame_Manager_BoneNewMgr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../LTGame/Manager/BoneNewMgr */ "./Number-Jump/scripts/LTGame/Manager/BoneNewMgr.ts");
/* harmony import */ var _common_Const__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../common/Const */ "./Number-Jump/scripts/script/common/Const.ts");
/* harmony import */ var _common_GameData__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../common/GameData */ "./Number-Jump/scripts/script/common/GameData.ts");
/* harmony import */ var _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");
/* harmony import */ var _MainStart__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../MainStart */ "./Number-Jump/scripts/script/MainStart.ts");
/* harmony import */ var _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../ui/UI_NumberJumpMainMediator */ "./Number-Jump/scripts/script/ui/UI_NumberJumpMainMediator.ts");







class NumberJump {
    static get instance() {
        if (this._instance == null) {
            this._instance = new NumberJump();
            this._instance.event = new _EventEmitter__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        }
        return this._instance;
    }
    // 升级
    upLv() {
    }
    // 完成关卡
    finishLevel(level) {
    }
    currencyCost(type, value) {
        return true;
    }
    /**
     * @param url 路径
     * @param name 名字
     * @param pos 位置
     * @param skinName 皮肤  可能为空
     */
    playEffect(url, name, pos, skinName, loader, scale = 1) {
        if (!NumberJump.release) {
            let bone = _LTGame_Manager_BoneNewMgr__WEBPACK_IMPORTED_MODULE_1__["NewBoneManager"].createBone(url, this.view.displayObject);
            bone.setXY(pos.x, pos.y);
            // bone.setScale(10, 10);
            bone.playByState(name, skinName, false);
            bone.setScale(scale, scale);
        }
    }
    costCoin(value) {
        // 监听
        if (this.currencyCost("coin", value)) {
            // 成功消耗
            return true;
        }
        return false;
    }
    playAudio(sound, volume, loop) {
        return null;
    }
    /**
     * @param area 省份
     * @param playNum 今日玩家数量
     * @param finishNum 完成玩家数量
     */
    updatePeopelData(area, playNum, finishNum, com) {
        area.text = "";
        playNum.text = "";
        finishNum.text = "";
        com.visible = true;
    }
    /**
     * 加载游戏，返回的 Promise 反应加载结果
     * @param progress 加载进度回调
     */
    loadGame(progress) {
        return new Promise(function (resolve) {
            _MainStart__WEBPACK_IMPORTED_MODULE_5__["default"].instance.loadSplashScene();
            _MainStart__WEBPACK_IMPORTED_MODULE_5__["default"].instance.splashScene.setProgressFunc(progress);
            _MainStart__WEBPACK_IMPORTED_MODULE_5__["default"].instance.splashScene.setLoadOverCall(Laya.Handler.create(this, () => {
                resolve();
            }));
        });
    }
    /** 卸载游戏资源 */
    unloadGame() {
        this.exitGame();
    }
    /** 准备游戏（显示小游戏主页） */
    onReady() { }
    /** 开始游戏 */
    startGame() {
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.startGame();
    }
    // 暂停游戏
    pauseGame() {
        var _a;
        (_a = _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript) === null || _a === void 0 ? void 0 : _a.pause();
    }
    // 重新开始游戏
    resumeGame() {
        var _a;
        (_a = _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript) === null || _a === void 0 ? void 0 : _a.start();
    }
    /** 尝试结束游戏（游戏结算） */
    tryFinish() {
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.gameOver();
    }
    /** 重置游戏（结束当局游戏回到小游戏主页） */
    endGame() {
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_6__["UI_NumberJumpMainMediator"].instance.backWaitGame();
    }
    /** 复活 */
    continueGame() {
        // 暂时没有复活
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.continueGame();
    }
    /** 退出小游戏 */
    exitGame() {
        var _a;
        (_a = _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript) === null || _a === void 0 ? void 0 : _a.exit();
    }
    /** 返回存档数据 */
    save() {
        _common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].SaveToDisk();
        let json = JSON.stringify(_common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].instance);
        let temp = {};
        temp[_common_Const__WEBPACK_IMPORTED_MODULE_2__["default"].gameNameJian] = json;
        return temp;
    }
    /** 读取存档数据，参数是执行 save 时的返回值 */
    load(data) {
        let json = data[_common_Const__WEBPACK_IMPORTED_MODULE_2__["default"].gameNameJian];
        _common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].instance;
        _common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"]._instance.readFromFile(json);
    }
    /** 物理帧更新 */
    fixedUpdate(delta) { }
    /** 恒更新 - 无视暂停、时钟缩放、框架状态等，每帧都执行更新 */
    constantUpdate(delta) { }
    /** 逻辑帧更新 */
    update(delta) { }
    /** 表现层帧更新 */
    lateUpdate(delta) { }
}
// 打开就是线上版本  本地不做数据保存
NumberJump.release = false;
window["NumberJump"] = NumberJump;


/***/ }),

/***/ "./Number-Jump/scripts/script/scene/MainScene.ts":
/*!*******************************************************!*\
  !*** ./Number-Jump/scripts/script/scene/MainScene.ts ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MainScene; });
/* harmony import */ var _LTGame_Fsm_BaseState__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../LTGame/Fsm/BaseState */ "./Number-Jump/scripts/LTGame/Fsm/BaseState.ts");
/* harmony import */ var _LTGame_Start_ESceneType__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../LTGame/Start/ESceneType */ "./Number-Jump/scripts/LTGame/Start/ESceneType.ts");
/* harmony import */ var _data_LevelProxy__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../data/LevelProxy */ "./Number-Jump/scripts/script/data/LevelProxy.ts");
/* harmony import */ var _LTGame_Manager_ShopManager__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../LTGame/Manager/ShopManager */ "./Number-Jump/scripts/LTGame/Manager/ShopManager.ts");
/* harmony import */ var _MainStart__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../MainStart */ "./Number-Jump/scripts/script/MainStart.ts");
/* harmony import */ var _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../ui/UI_NumberJumpMainMediator */ "./Number-Jump/scripts/script/ui/UI_NumberJumpMainMediator.ts");
/* harmony import */ var _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");







class MainScene extends _LTGame_Fsm_BaseState__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor() {
        super(_LTGame_Start_ESceneType__WEBPACK_IMPORTED_MODULE_1__["ESceneType"].Main);
    }
    _DoEnter() {
        return __awaiter(this, void 0, void 0, function* () {
            _MainStart__WEBPACK_IMPORTED_MODULE_4__["default"].instance.splashScene.setPackageItemExtension();
            _data_LevelProxy__WEBPACK_IMPORTED_MODULE_2__["default"].instance;
            _LTGame_Manager_ShopManager__WEBPACK_IMPORTED_MODULE_3__["default"].instance;
            _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_6__["default"].InitAll();
            _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_5__["UI_NumberJumpMainMediator"].instance.Show();
        });
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/scene/SplashScene.ts":
/*!*********************************************************!*\
  !*** ./Number-Jump/scripts/script/scene/SplashScene.ts ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SplashScene; });
/* harmony import */ var _gen_ui_NumberJump_NumberJumpBinder__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../gen/ui/NumberJump/NumberJumpBinder */ "./Number-Jump/gen/ui/NumberJump/NumberJumpBinder.ts");
/* harmony import */ var _LTGame_Config_ConfigManager__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../LTGame/Config/ConfigManager */ "./Number-Jump/scripts/LTGame/Config/ConfigManager.ts");
/* harmony import */ var _LTGame_Start_LTSplashScene__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../LTGame/Start/LTSplashScene */ "./Number-Jump/scripts/LTGame/Start/LTSplashScene.ts");
/* harmony import */ var _MainStart__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../MainStart */ "./Number-Jump/scripts/script/MainStart.ts");
/* harmony import */ var _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../config/NumberJumpGameConst */ "./Number-Jump/scripts/script/config/NumberJumpGameConst.ts");
/* harmony import */ var _config_NumberJumpSkinConfig__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../config/NumberJumpSkinConfig */ "./Number-Jump/scripts/script/config/NumberJumpSkinConfig.ts");
/* harmony import */ var _config_NumberJumpGameName__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../config/NumberJumpGameName */ "./Number-Jump/scripts/script/config/NumberJumpGameName.ts");







class SplashScene extends _LTGame_Start_LTSplashScene__WEBPACK_IMPORTED_MODULE_2__["default"] {
    constructor() {
        super();
    }
    _OnBindUI() {
        _gen_ui_NumberJump_NumberJumpBinder__WEBPACK_IMPORTED_MODULE_0__["default"].bindAll();
    }
    // 注册表格
    _OnSetLoadConfig() {
        _LTGame_Config_ConfigManager__WEBPACK_IMPORTED_MODULE_1__["ConfigManager"].AddConfig(_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_4__["NumberJumpGameConst"]);
        _LTGame_Config_ConfigManager__WEBPACK_IMPORTED_MODULE_1__["ConfigManager"].AddConfig(_config_NumberJumpGameName__WEBPACK_IMPORTED_MODULE_6__["NumberJumpGameName"]);
        _LTGame_Config_ConfigManager__WEBPACK_IMPORTED_MODULE_1__["ConfigManager"].AddConfig(_config_NumberJumpSkinConfig__WEBPACK_IMPORTED_MODULE_5__["NumberJumpSkinConfig"]);
    }
    _OnGameResPrepared(urls) { }
    _OnGameResLoaded() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isFinished = true;
            _MainStart__WEBPACK_IMPORTED_MODULE_3__["default"].instance.enterMainScene();
        });
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/scene/script/GameScript.ts":
/*!***************************************************************!*\
  !*** ./Number-Jump/scripts/script/scene/script/GameScript.ts ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return GameScript; });
/* harmony import */ var _common_GameData__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../common/GameData */ "./Number-Jump/scripts/script/common/GameData.ts");
/* harmony import */ var _data_LevelProxy__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../data/LevelProxy */ "./Number-Jump/scripts/script/data/LevelProxy.ts");
/* harmony import */ var _enum_ESound__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../enum/ESound */ "./Number-Jump/scripts/script/enum/ESound.ts");
/* harmony import */ var _ui_UI_RewardMediator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../ui/UI_RewardMediator */ "./Number-Jump/scripts/script/ui/UI_RewardMediator.ts");
/* harmony import */ var _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../ui/UI_NumberJumpMainMediator */ "./Number-Jump/scripts/script/ui/UI_NumberJumpMainMediator.ts");
/* harmony import */ var _Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Main3d/NumberJump */ "./Number-Jump/scripts/script/scene/Main3d/NumberJump.ts");
/* harmony import */ var _sprite_Sound__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../sprite/Sound */ "./Number-Jump/scripts/script/scene/sprite/Sound.ts");
/* harmony import */ var _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../enum/EPropType */ "./Number-Jump/scripts/script/enum/EPropType.ts");
/* harmony import */ var _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../config/NumberJumpGameConst */ "./Number-Jump/scripts/script/config/NumberJumpGameConst.ts");
/* harmony import */ var _ui_prop_blackItem__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../ui/prop/blackItem */ "./Number-Jump/scripts/script/ui/prop/blackItem.ts");
/* harmony import */ var _ui_prop_jianciItem__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../ui/prop/jianciItem */ "./Number-Jump/scripts/script/ui/prop/jianciItem.ts");
/* harmony import */ var _ui_prop_bigJianciItem__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../ui/prop/bigJianciItem */ "./Number-Jump/scripts/script/ui/prop/bigJianciItem.ts");
/* harmony import */ var _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../LTGame/LTUtils/MathEx */ "./Number-Jump/scripts/LTGame/LTUtils/MathEx.ts");
/* harmony import */ var _LTGame_LTUtils_LTUtils__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../LTGame/LTUtils/LTUtils */ "./Number-Jump/scripts/LTGame/LTUtils/LTUtils.ts");
/* harmony import */ var _gen_ui_NumberJump_UI_lv1__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_lv1 */ "./Number-Jump/gen/ui/NumberJump/UI_lv1.ts");
/* harmony import */ var _ui_prop_boxItem__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../ui/prop/boxItem */ "./Number-Jump/scripts/script/ui/prop/boxItem.ts");
/* harmony import */ var _ui_prop_huadaoItem__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../ui/prop/huadaoItem */ "./Number-Jump/scripts/script/ui/prop/huadaoItem.ts");
/* harmony import */ var _gen_ui_NumberJump_UI_heroIcon__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_heroIcon */ "./Number-Jump/gen/ui/NumberJump/UI_heroIcon.ts");
/* harmony import */ var _config_NumberJumpGameName__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../config/NumberJumpGameName */ "./Number-Jump/scripts/script/config/NumberJumpGameName.ts");
/* harmony import */ var _LTGame_Async_Awaiters__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../../LTGame/Async/Awaiters */ "./Number-Jump/scripts/LTGame/Async/Awaiters.ts");




















class GameScript {
    constructor() {
        // 累计惩罚次数
        this.chengfaNum = 0;
        // 回到家的次数
        this.backHomeNum = 0;
        // 累计弹飞次数
        this.bacKNum = 0;
        this.chengfaCountDown = 0;
        // 到达最大位置
        this.maxIndex = 0;
        // 获得的最大分数
        this.nowMaxScore = 0;
        this.quickMoving = false;
        this.score = 0;
        this._xuliTime = 0;
        this._openXuli = false;
        // private _boxImg: fgui.GImage;
        this._heroIcons = [];
        this._props = [];
        this._nowZawNum = 0;
        this._timer = new Laya.Timer();
        this._timer.frameLoop(1, this, this.update);
    }
    initLv(restart = false) {
        return __awaiter(this, void 0, void 0, function* () {
            this.score = 0;
            this.gaming = false;
            _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.mapLayer.clear();
            _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.heroNum.init();
            this.maxIndex = 0;
            this._xuliTime = 0;
            this._heroIcons = [];
            this._openXuli = false;
            this.quickMoving = false;
            this.chengfaCountDown = 0;
            this._props = [];
            // this._boxImg = null;
            if (_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.needLoadData && _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelNowScore) {
                this.chengfaNum = _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.chengfaNum || 0;
                this.backHomeNum = _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.backHomeNum || 0;
                this.bacKNum = _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.bacKNum || 0;
                this.nowMaxScore = _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelNowMaxScore || 0;
                this.updateScore(_common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelNowScore || 0, false);
                if (_common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelNowScore != 0) {
                    _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.heroNum.noInitAni();
                }
            }
            else {
                this.chengfaNum = 0;
                this.backHomeNum = 0;
                this.bacKNum = 0;
                this.nowMaxScore = 0;
                this.updateScore(0);
            }
            this._initGame(restart);
            this.huifuIndex();
            _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.setCanClick(true);
            _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.updateChengfaTxt();
        });
    }
    startNewLv() {
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelNowScore = 0;
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelNowMaxScore = 0;
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.usedheroicon = null;
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.chengfaNum = 0;
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.backHomeNum = 0;
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.bacKNum = 0;
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelId = 1;
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].SaveToDisk();
        this.initLv(true);
        this.startGame();
    }
    enterNextLv() {
        // this._boxImg && MTween.to(this._boxImg, { alpha: 0 }, 300);
        _Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_5__["default"].instance.finishLevel(_data_LevelProxy__WEBPACK_IMPORTED_MODULE_1__["default"].instance.nowLevelId());
        let bool = _data_LevelProxy__WEBPACK_IMPORTED_MODULE_1__["default"].instance.enterNextLv();
        if (!bool) {
            _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelNowScore = 0;
            _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelNowMaxScore = 0;
            _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.usedheroicon = null;
            _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.chengfaNum = 0;
            _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.backHomeNum = 0;
            _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.bacKNum = 0;
            _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelId = 1;
            _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].SaveToDisk();
        }
        _ui_UI_RewardMediator__WEBPACK_IMPORTED_MODULE_3__["UI_RewardMediator"].instance.Show({
            closeCall: () => {
                // NumberJump.instance.endGame();
                // UI_NumberJumpMainMediator.instance.backWaitGame();
                if (!bool) {
                    this.initLv();
                    this.startGame();
                }
                this.updatelv();
            },
        });
    }
    // 初始化游戏
    _initGame(restart = false) {
        return __awaiter(this, void 0, void 0, function* () {
            // for (let i = 0; i < 1000; i++) {
            //     UI_NumberJumpMainMediator.instance.mapLayer.addChild(jianciItem.createInstance())
            // }
            // return;
            this.updatelv();
            this._spLevel1();
            for (let i = 1; i < _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.lvNums.length; i++) {
                yield this._produceRandomNumProp(i + 1, restart);
            }
            this._produceHeadicon();
            // this._props.forEach((prop: any) => {
            //     prop.visible = false;
            // })
        });
    }
    updatelv() {
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.ui.m_targetCom.m_targetNum.text = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.lvNums[_data_LevelProxy__WEBPACK_IMPORTED_MODULE_1__["default"].instance.nowLevelId() - 1] + _data_LevelProxy__WEBPACK_IMPORTED_MODULE_1__["default"].instance.viewNumOffset() + "";
    }
    // 生成头像，  跟随玩家一起保存， 刷新后清除
    _produceHeadicon() {
        this._heroIcons = [];
        let heroicons = _config_NumberJumpGameName__WEBPACK_IMPORTED_MODULE_18__["NumberJumpGameName"].data;
        if (_common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.usedheroicon && _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.usedheroicon.length > 0) {
            let data = _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.usedheroicon;
            data.forEach((temp, index) => {
                let heroicon = _gen_ui_NumberJump_UI_heroIcon__WEBPACK_IMPORTED_MODULE_17__["default"].createInstance();
                _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.mapLayer.m_propLayer.addChild(heroicon);
                heroicon.setXY(temp[1], 1300);
                // 获得英雄名字
                let config = heroicons[temp[0]];
                heroicon.m_name.text = config.name;
                heroicon.m_icon.m_icon.url = config.url;
                this._heroIcons.push(heroicon);
            });
            return;
        }
        let hasuesdConfig = [];
        let baocundata = [];
        this._props.forEach((prop, index) => {
            if (index >= _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.heroiconGuize) {
                if (_LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_12__["default"].RandomRatio(_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.heroicongailv)) {
                    let heroicon = _gen_ui_NumberJump_UI_heroIcon__WEBPACK_IMPORTED_MODULE_17__["default"].createInstance();
                    _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.mapLayer.m_propLayer.addChild(heroicon);
                    heroicon.setXY(_LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_12__["default"].Random(prop.x, prop.x + prop.width), 1300);
                    // 获得英雄名字
                    let config = _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_12__["default"].RandomFromArrayExcepts(_config_NumberJumpGameName__WEBPACK_IMPORTED_MODULE_18__["NumberJumpGameName"].dataList, hasuesdConfig);
                    heroicon.m_name.text = config.name;
                    heroicon.m_icon.m_icon.url = config.url;
                    baocundata.push([config.id, heroicon.x]);
                    this._heroIcons.push(heroicon);
                }
            }
        });
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.usedheroicon = baocundata;
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].SaveToDisk();
    }
    _spLevel1() {
        let lv1 = _gen_ui_NumberJump_UI_lv1__WEBPACK_IMPORTED_MODULE_14__["default"].createInstance();
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.mapLayer.m_propLayer.addChild(lv1);
        lv1.setXY(0, 0);
        this._nowZawNum = 0;
        let lastitem;
        lv1._children.forEach((child, index) => {
            let len = child.width / _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.oneUnitLen;
            if (child instanceof _ui_prop_boxItem__WEBPACK_IMPORTED_MODULE_15__["default"]) {
                this.produceProp(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].box, 0, 0, child);
            }
            else if (child instanceof _ui_prop_blackItem__WEBPACK_IMPORTED_MODULE_9__["default"]) {
                this.produceProp(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].black, len, this._nowZawNum, child);
                lastitem = child;
            }
            else if (child instanceof _ui_prop_jianciItem__WEBPACK_IMPORTED_MODULE_10__["default"]) {
                this._nowZawNum++;
                this.produceProp(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].jianci, len, this._nowZawNum, child);
            }
            else if (child instanceof _ui_prop_bigJianciItem__WEBPACK_IMPORTED_MODULE_11__["default"]) {
                this._nowZawNum += 2;
                this.produceProp(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].bigJianci, len, this._nowZawNum, child);
            }
            else if (child instanceof _ui_prop_huadaoItem__WEBPACK_IMPORTED_MODULE_16__["default"]) {
                this.produceProp(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].huadao, len, this._nowZawNum, child);
            }
        });
        if (this.nowMaxScore < _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.lvNums[0]) {
            let boxitem = this.produceProp(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].box, 0);
            boxitem.m_type.selectedIndex = 1;
            _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.mapLayer.m_propLayer.addChild(boxitem);
            boxitem.setXY(lastitem.x + 300, 889);
        }
    }
    // 生成规则
    _produceRandomNumProp(levelid = 1, restart = false) {
        return __awaiter(this, void 0, void 0, function* () {
            // let nowLevelId = 1; LevelProxy.instance.nowLevelId();
            // if (nowLevelId == 1) {
            //     this._spLevel1();
            //     // return;
            // }
            let needzawNum = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.lvNums[levelid - 1] + _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.viewLvOffset[levelid - 1];
            let lastType = _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].black;
            // let black = this.produceProp(EpropType.black, NumberJumpGameConst.data.startBlackLen);
            let produceBoxjiange = _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_12__["default"].RandomInt(_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.pdBoxJiange[0], _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.pdBoxJiange[1] + 1);
            for (let i = 0; i >= 0; i++) {
                if (restart && i % 100 == 0) {
                    yield _LTGame_Async_Awaiters__WEBPACK_IMPORTED_MODULE_19__["default"].Seconds(0.1);
                }
                let type = this._produceRandomProp(this._nowZawNum, lastType);
                lastType = type;
                if (type == _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].jianci || type == _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].bigJianci) {
                    this._nowZawNum++;
                }
                else if (type == _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].sphuadao) {
                    this._nowZawNum += 2;
                }
                if (this._nowZawNum >= needzawNum) {
                    // 生成结束地图
                    break;
                }
                if (this._nowZawNum < needzawNum) {
                    let blackLen = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.blackLen;
                    this.produceProp(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].black, _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_12__["default"].RandomInt(blackLen[0] * 2, blackLen[1] * 2 + 1) / 2, this._nowZawNum, null, false);
                }
                if (this._nowZawNum >= this.nowMaxScore) {
                    produceBoxjiange--;
                    if (produceBoxjiange <= 0) {
                        this.produceProp(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].box, 0, 0, null, false);
                        produceBoxjiange = _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_12__["default"].RandomInt(_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.pdBoxJiange[0], _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.pdBoxJiange[1] + 1);
                    }
                }
            }
            let endblack = this.produceProp(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].black, _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.endBlackLen);
            if (this.nowMaxScore < _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.lvNums[levelid - 1]) {
                let boxitem = this.produceProp(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].box, 0);
                boxitem.m_type.selectedIndex = 1;
                _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.mapLayer.m_propLayer.addChild(boxitem);
                boxitem.setXY(endblack.x + 300, 889);
            }
        });
    }
    _produceRandomProp(nowZawNum, excpt) {
        let types = [];
        let gailv = [];
        if (excpt != _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].huadao && excpt != _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].sphuadao) {
            excpt = null;
        }
        if (excpt != _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].jianci && excpt != _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].bigJianci) {
            types.push(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].jianci);
            gailv.push(_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.produceNrJcProbability);
        }
        if (nowZawNum > _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.produceNormalCsTimes &&
            excpt != _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].huadao &&
            excpt != _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].sphuadao) {
            types.push(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].huadao);
            gailv.push(_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.produceNrScProbability);
        }
        if (nowZawNum > _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.produceSpJianciTimes &&
            excpt != _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].jianci &&
            excpt != _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].bigJianci) {
            types.push(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].bigJianci);
            gailv.push(_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.produceSpJcProbability);
        }
        if (nowZawNum > _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.produceSpCsTimes &&
            excpt != _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].huadao &&
            excpt != _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].sphuadao) {
            types.push(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].sphuadao);
            gailv.push(_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.produceSpScProbability);
        }
        // LIUSHAN
        // types.push(EpropType.huadao);
        // gailv.push(NumberJumpGameConst.data.produceNrScProbability);
        let index = nowZawNum + 1;
        let prop;
        let type = _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_12__["default"].RandomFromWithWeight(types, gailv);
        // type = EpropType.sphuadao;
        if (type == _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].sphuadao) {
            // delete this._props[this._props.length - 1];
            let len = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.normalXjLen;
            this.produceProp(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].jianci, _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_12__["default"].RandomInt(len[0] * 2, len[1] * 2 + 1) / 2, index, null, false);
            let huadaolen = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.csLen;
            prop = this.produceProp(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].huadao, _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_12__["default"].RandomInt(huadaolen[0] * 2, huadaolen[1] * 2 + 1) / 2, index, null, false);
            this.produceProp(_enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].jianci, _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_12__["default"].RandomInt(len[0] * 2, len[1] * 2 + 1) / 2, index + 1, null, false);
            index += 1;
        }
        else {
            let len;
            // type = EpropType.bigJianci;
            switch (type) {
                case _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].huadao:
                    len = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.csLen;
                    break;
                case _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].jianci:
                    len = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.normalXjLen;
                    break;
                case _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].bigJianci:
                    len = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.spXjLen;
                    break;
            }
            let templen = _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_12__["default"].RandomInt(len[0] * 2, len[1] * 2 + 1) / 2;
            prop = this.produceProp(type, templen, index, null, false);
            // if (type == EpropType.jianci) {
            //     console.log("尖刺生成长度---", templen);
            // }
        }
        return type;
    }
    startBack(nextScore) {
        this.chengfaNum++;
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.chengfaNum = this.chengfaNum;
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].SaveToDisk();
        this.chengfaCountDown = this.chengfaNum;
        // let heroNum = UI_NumberJumpMainMediator.instance.heroNum;
        if (nextScore < this.chengfaCountDown) {
            this.chengfaCountDown = nextScore;
        }
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.updateDamageNum();
        this._chengfa();
    }
    chengfaoncejiesu() {
        if (this.chengfaCountDown < 0) {
            this.moveEnd();
        }
        else {
            this._chengfa();
        }
    }
    getMvProp() {
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.dffNum += _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.mvDffNum;
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.hundunNum += _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.mvHundunNum;
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].SaveToDisk();
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance._updateHundunIcon();
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance._updateDffIcon();
    }
    _chengfa() {
        let heroNum = _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.heroNum;
        if (this.chengfaCountDown > 0) {
            this.chengfaCountDown--;
            if (heroNum.usedHundun) {
                heroNum.hundunBreak();
                this.chengfaCountDown = 0;
            }
            let index = heroNum.index;
            let targetX;
            if (this.chengfaCountDown <= 0) {
                for (let i = index; i >= 0; i--) {
                    if (this._props[i] instanceof _ui_prop_blackItem__WEBPACK_IMPORTED_MODULE_9__["default"]) {
                        heroNum.index = i;
                        targetX = this._props[i].x + this._props[i].width / 2;
                        if (i == 0) {
                            targetX = 0;
                        }
                        break;
                    }
                }
            }
            else {
                for (let j = index - 1; j >= 0; j--) {
                    if (this._props[j] instanceof _ui_prop_jianciItem__WEBPACK_IMPORTED_MODULE_10__["default"] ||
                        this._props[j] instanceof _ui_prop_bigJianciItem__WEBPACK_IMPORTED_MODULE_11__["default"]) {
                        heroNum.index = j;
                        targetX = this._props[j].x + this._props[j].width / 2;
                        break;
                    }
                }
            }
            if (!targetX) {
                // 起点
                targetX = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.startAnimoveDis;
                this.backHomeNum++;
                _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.backHomeNum = this.backHomeNum;
                // 不再惩罚
                this.chengfaCountDown = 0;
                heroNum.index = 0;
            }
            this.bacKNum++;
            _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.bacKNum = this.bacKNum;
            let len = targetX - heroNum.localX;
            heroNum.chengfaJump(len);
            this.mapBackLen(len);
            return len;
        }
        else {
            this.moveEnd();
            if (heroNum.index <= 1) {
                // 更新描述语言
                _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.updateChengfaTxt(true);
            }
            return 0;
        }
    }
    produceProp(propid, len, score, item, visible = true) {
        len *= _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.oneUnitLen;
        let propItem = _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.mapLayer.addPropItem(propid, len, item);
        propItem["index"] = this._props.length;
        if (score && (propid == _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].jianci || propid == _enum_EPropType__WEBPACK_IMPORTED_MODULE_7__["EpropType"].bigJianci)) {
            propItem.score = score;
        }
        this._props.push(propItem);
        propItem.visible = visible;
        return propItem;
    }
    // 加速移动
    quickMove() {
        let score = this.nowMaxScore;
        let black;
        for (let i = 0; i >= 0; i++) {
            let prop = this._props[i];
            if (prop instanceof _ui_prop_bigJianciItem__WEBPACK_IMPORTED_MODULE_11__["default"] || prop instanceof _ui_prop_jianciItem__WEBPACK_IMPORTED_MODULE_10__["default"]) {
                score--;
            }
            if (score <= 0) {
                if (prop instanceof _ui_prop_blackItem__WEBPACK_IMPORTED_MODULE_9__["default"]) {
                    black = prop;
                    break;
                }
            }
        }
        let targetX = -(black.x + black.width / 2) + _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.startAnimoveDis;
        this.quickMoving = true;
        // targetX = -50000;
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.setCanCaozuo(false);
        let index = black.index;
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.mapLayer.quickMove(targetX, Laya.Handler.create(this, () => {
            this.quickMoving = false;
            // 可以操作
            _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.heroNum.index = index;
            _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.setCanCaozuo(true);
        }));
    }
    mapMoveRight() {
        this.mapMove(1);
    }
    mapMoveLeft() {
        this.mapMove(-1);
    }
    moveEnd(mapStopMove = true) {
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.setCanCaozuo(true);
        mapStopMove && _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.mapLayer.stopMove();
    }
    mapMove(dire) {
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.setCanCaozuo(false);
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.mapLayer.startMove(dire);
    }
    mapBackLen(len) {
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.setCanCaozuo(false);
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.mapLayer.backLen(len);
    }
    // 开始蓄力
    startXuli() {
        this._xuliTime = 0;
        this._openXuli = true;
        this._xulisound = _sprite_Sound__WEBPACK_IMPORTED_MODULE_6__["default"].instance.playSoundSp(_enum_ESound__WEBPACK_IMPORTED_MODULE_2__["ESoundName"].compress);
    }
    touchEnd() {
        this._openXuli = false;
        this._xulisound && this._xulisound.stop();
        // Sound.instance.stopSound(ESoundName.compress);
        let heroNum = _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.heroNum;
        heroNum.startJump();
        this.mapMoveLeft();
    }
    // 更新
    update() {
        let dt = _LTGame_LTUtils_LTUtils__WEBPACK_IMPORTED_MODULE_13__["LTUtils"].deltaTimeBytimer(this._timer);
        let heroNum = _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.heroNum;
        if (this._openXuli) {
            this._xuliTime += dt;
            if (this._xuliTime >= _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.xuliTime) {
                this._xuliTime = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.xuliTime;
            }
            heroNum.updateXuli(this._xuliTime);
        }
        let m_yinying = _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.ui.m_yinying;
        m_yinying.x = heroNum.x;
        let scale = 1 - (_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.numY - heroNum.y) / 500;
        m_yinying.setScale(scale, scale);
        if (this.quickMoving) {
            let localX = heroNum.localX;
            let mapX = localX - 1000;
            let mapX2 = localX + 3000;
            for (let k = 0; k < this._props.length; k++) {
                let prop = this._props[k];
                if (prop && !prop.isDisposed) {
                    if (prop.x > mapX && prop.x < mapX2) {
                        prop.visible = true;
                    }
                    else {
                        prop.visible = false;
                    }
                }
            }
        }
        else {
            let index = heroNum.index;
            let range = 10;
            let range2 = 10;
            for (let i = index - range; i < index + range; i++) {
                if (this._props[i] && !this._props[i].isDisposed) {
                    this._props[i].visible = true;
                }
            }
            for (let j = index - range; j > index - range - range2; j--) {
                if (this._props[j] && !this._props[j].isDisposed) {
                    this._props[j].visible = false;
                }
            }
            for (let z = index + range; z < index + range + range2; z++) {
                if (this._props[z] && !this._props[z].isDisposed) {
                    this._props[z].visible = false;
                }
            }
        }
        let localX = heroNum.localX;
        let mapX = localX - 1000;
        let mapX2 = localX + 3000;
        for (let m = 0; m < this._heroIcons.length; m++) {
            let icon = this._heroIcons[m];
            if (icon && !icon.isDisposed && icon.x > mapX && icon.x < mapX2) {
                icon.visible = true;
            }
            else {
                icon.visible = false;
            }
        }
    }
    huifuIndex() {
        if (this.score == 0)
            return;
        for (let i = 0; i < this._props.length; i++) {
            if (this._props[i] && this._props[i].score == this.score) {
                for (let j = this._props[i].index; j >= 0; j++) {
                    if (this._props[j] instanceof _ui_prop_blackItem__WEBPACK_IMPORTED_MODULE_9__["default"]) {
                        let heroNum = _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.heroNum;
                        heroNum.index = this._props[j].index;
                        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.mapLayer.x =
                            -(this._props[j].x + this._props[j].width / 2) +
                                _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_8__["NumberJumpGameConst"].data.startAnimoveDis;
                        break;
                    }
                }
                break;
            }
        }
    }
    updateScore(score, needAni = true) {
        if (score > this.score && needAni) {
            _sprite_Sound__WEBPACK_IMPORTED_MODULE_6__["default"].instance.playSound(_enum_ESound__WEBPACK_IMPORTED_MODULE_2__["ESoundName"].levelup);
            _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.heroNum.uplv();
            _Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_5__["default"].instance.upLv();
        }
        this.score = score;
        this.nowMaxScore = Math.max(this.score, this.nowMaxScore);
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.heroNum.updateScore();
        if (_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_5__["default"].instance.score == null) {
            _Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_5__["default"].instance.score = 0;
        }
        _Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_5__["default"].instance.score = this.score;
        if (this.score > _Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_5__["default"].instance.bestScore) {
            _Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_5__["default"].instance.bestScore = this.score;
        }
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.updatePro();
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelNowScore = this.score;
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levelNowMaxScore = this.nowMaxScore;
        _common_GameData__WEBPACK_IMPORTED_MODULE_0__["default"].SaveToDisk();
        // let nowLevelId = LevelProxy.instance.nowLevelId();
        // let needzawNum = NumberJumpGameConst.data.lvNums[nowLevelId - 1];
        // if (this.score >= needzawNum) {
        //     // 游戏胜利
        //     // UI_NumberJumpMainMediator.instance.setCanClick(false);
        //     this.enterNextLv();
        // }
    }
    updateIndex(index) {
        let score = 0;
        this.maxIndex = Math.max(this.maxIndex, index);
        for (let i = index; i >= 0; i--) {
            if (i == 0) {
                score = 0;
                break;
            }
            else {
                if (this._props[i] instanceof _ui_prop_jianciItem__WEBPACK_IMPORTED_MODULE_10__["default"] ||
                    this._props[i] instanceof _ui_prop_bigJianciItem__WEBPACK_IMPORTED_MODULE_11__["default"]) {
                    score = this._props[i].score;
                    break;
                }
            }
        }
        this.updateScore(score);
    }
    dispose() {
        this.score = 0;
        this.gaming = false;
        this.maxIndex = 0;
        this._xuliTime = 0;
        this._openXuli = false;
        this.nowMaxScore = 0;
        this.quickMoving = false;
        this.chengfaNum = 0;
        this.backHomeNum = 0;
        this.bacKNum = 0;
        this.chengfaCountDown = 0;
        this._props &&
            this._props.forEach((prop) => {
                prop && prop.dispose();
            });
        this._props = [];
        this._timer && this._timer.clearAll(this);
    }
    startGame() {
        this.gaming = true;
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.startGame();
    }
    // 复活
    continueGame() {
        this.gaming = true;
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.fuhuo();
    }
    gameOver() {
        if (!this.gaming)
            return;
        this.gaming = false;
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.gameOver();
    }
    pause() {
        this._timer.scale = 0;
    }
    start() {
        this._timer.scale = 1;
    }
    jiansu() { }
    exit() {
        this.gaming = false;
        // this._timer && this._timer.clearAll(this);
        this._timer && this._timer.pause();
        this.dispose();
        _ui_UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_4__["UI_NumberJumpMainMediator"].instance.exit();
    }
    get timer() {
        return this._timer;
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/scene/script/MTween.ts":
/*!***********************************************************!*\
  !*** ./Number-Jump/scripts/script/scene/script/MTween.ts ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MTween; });
/* harmony import */ var _common_Const__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../common/Const */ "./Number-Jump/scripts/script/common/Const.ts");
/* harmony import */ var _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");


class MTween {
    constructor() {
        /**@private 唯一标识，TimeLintLite用到*/
        this.gid = 0;
        /**重播次数，如果repeat=0，则表示无限循环播放*/
        this.repeat = 1;
        /**当前播放次数*/
        this._count = 0;
        this._timer = Laya.timer;
    }
    /**
     * 缓动对象的props属性到目标值。
     * @param	target 目标对象(即将更改属性值的对象)。
     * @param	props 变化的属性列表，比如{x:100,y:20,ease:Ease.backOut,complete:Handler.create(this,onComplete),update:new Handler(this,onComplete)}。
     * @param	duration 花费的时间，单位毫秒。
     * @param	ease 缓动类型，默认为匀速运动。
     * @param	complete 结束回调函数。
     * @param	delay 延迟执行时间。
     * @param	coverBefore 是否覆盖之前的缓动。
     * @param	autoRecover 是否自动回收，默认为true，缓动结束之后自动回收到对象池。
     * @return	返回Tween对象。
     */
    static to(target, props, duration, ease = null, complete = null, delay = 0, coverBefore = false, autoRecover = true, timer = _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_1__["default"].gameScript.timer) {
        let tween = Laya.Pool.getItemByClass(_common_Const__WEBPACK_IMPORTED_MODULE_0__["default"].gameNameJian + "_mtween", MTween);
        tween.setTimer(timer);
        tween._create(target, props, duration, ease, complete, delay, coverBefore, true, autoRecover, true);
        return tween;
    }
    /**
     * 从props属性，缓动到当前状态。
     * @param	target 目标对象(即将更改属性值的对象)。
     * @param	props 变化的属性列表，比如{x:100,y:20,ease:Ease.backOut,complete:Handler.create(this,onComplete),update:new Handler(this,onComplete)}。
     * @param	duration 花费的时间，单位毫秒。
     * @param	ease 缓动类型，默认为匀速运动。
     * @param	complete 结束回调函数。
     * @param	delay 延迟执行时间。
     * @param	coverBefore 是否覆盖之前的缓动。
     * @param	autoRecover 是否自动回收，默认为true，缓动结束之后自动回收到对象池。
     * @return	返回Tween对象。
     */
    static from(target, props, duration, ease = null, complete = null, delay = 0, coverBefore = false, autoRecover = true) {
        return Laya.Pool.getItemByClass(_common_Const__WEBPACK_IMPORTED_MODULE_0__["default"].gameNameJian + "_mtween", MTween)._create(target, props, duration, ease, complete, delay, coverBefore, false, autoRecover, true);
    }
    /**
     * 缓动对象的props属性到目标值。
     * @param	target 目标对象(即将更改属性值的对象)。
     * @param	props 变化的属性列表，比如{x:100,y:20,ease:Ease.backOut,complete:Handler.create(this,onComplete),update:new Handler(this,onComplete)}。
     * @param	duration 花费的时间，单位毫秒。
     * @param	ease 缓动类型，默认为匀速运动。
     * @param	complete 结束回调函数。
     * @param	delay 延迟执行时间。
     * @param	coverBefore 是否覆盖之前的缓动。
     * @return	返回Tween对象。
     */
    to(target, props, duration, ease = null, complete = null, delay = 0, coverBefore = false) {
        return this._create(target, props, duration, ease, complete, delay, coverBefore, true, false, true);
    }
    setTimer(timer) {
        this._timer = timer;
    }
    /**
     * 从props属性，缓动到当前状态。
     * @param	target 目标对象(即将更改属性值的对象)。
     * @param	props 变化的属性列表，比如{x:100,y:20,ease:Ease.backOut,complete:Handler.create(this,onComplete),update:new Handler(this,onComplete)}。
     * @param	duration 花费的时间，单位毫秒。
     * @param	ease 缓动类型，默认为匀速运动。
     * @param	complete 结束回调函数。
     * @param	delay 延迟执行时间。
     * @param	coverBefore 是否覆盖之前的缓动。
     * @return	返回Tween对象。
     */
    from(target, props, duration, ease = null, complete = null, delay = 0, coverBefore = false) {
        return this._create(target, props, duration, ease, complete, delay, coverBefore, false, false, true);
    }
    /** @internal */
    _create(target, props, duration, ease, complete, delay, coverBefore, isTo, usePool, runNow) {
        if (!target)
            throw new Error("MTween:target is null");
        this._target = target;
        this._duration = duration;
        this._ease = ease || props.ease || MTween.easeNone;
        this._complete = complete || props.complete;
        this._delay = delay;
        this._props = [];
        this._usedTimer = 0;
        this._startTimer = Laya.Browser.now();
        this._usedPool = usePool;
        this._delayParam = null;
        this.update = props.update;
        //判断是否覆盖
        var gid = target.$_GID || (target.$_GID = Laya.Utils.getGID());
        if (!MTween.tweenMap[gid]) {
            MTween.tweenMap[gid] = [this];
        }
        else {
            if (coverBefore)
                MTween.clearTween(target);
            MTween.tweenMap[gid].push(this);
        }
        if (runNow) {
            if (delay <= 0)
                this.firstStart(target, props, isTo);
            else {
                this._delayParam = [target, props, isTo];
                this._timer.once(delay, this, this.firstStart, this._delayParam);
            }
        }
        else {
            this._initProps(target, props, isTo);
        }
        return this;
    }
    firstStart(target, props, isTo) {
        this._delayParam = null;
        if (target.destroyed) {
            this.clear();
            return;
        }
        this._initProps(target, props, isTo);
        this._beginLoop();
    }
    _initProps(target, props, isTo) {
        //初始化属性
        for (var p in props) {
            if (typeof target[p] == "number") {
                var start = isTo ? target[p] : props[p];
                var end = isTo ? props[p] : target[p];
                this._props.push([p, start, end - start]);
                if (!isTo)
                    target[p] = start;
            }
        }
    }
    _beginLoop() {
        this._timer.frameLoop(1, this, this._doEase);
    }
    /**执行缓动**/
    _doEase() {
        this._updateEase(Laya.Browser.now());
    }
    /**@internal */
    _updateEase(time) {
        var target = this._target;
        if (!target)
            return;
        //如果对象被销毁，则立即停止缓动
        if (target.isDisposed)
            return MTween.clearAll(target);
        if (target.destroyed)
            return MTween.clearAll(target);
        var usedTimer = (this._usedTimer = time - this._startTimer - this._delay);
        if (usedTimer < 0)
            return;
        if (usedTimer >= this._duration)
            return this.complete();
        var ratio = usedTimer > 0 ? this._ease(usedTimer, 0, 1, this._duration) : 0;
        var props = this._props;
        for (var i = 0, n = props.length; i < n; i++) {
            var prop = props[i];
            target[prop[0]] = prop[1] + ratio * prop[2];
        }
        if (this.update)
            this.update.run();
    }
    /**设置当前执行比例**/
    set progress(v) {
        var uTime = v * this._duration;
        this._startTimer = Laya.Browser.now() - this._delay - uTime;
    }
    /**
     * 立即结束缓动并到终点。
     */
    complete() {
        if (!this._target)
            return;
        //立即执行初始化
        this._timer.runTimer(this, this.firstStart);
        //缓存当前属性
        var target = this._target;
        var props = this._props;
        var handler = this._complete;
        //设置终点属性
        for (var i = 0, n = props.length; i < n; i++) {
            var prop = props[i];
            target[prop[0]] = prop[1] + prop[2];
        }
        if (this.update)
            this.update.run();
        this._count++;
        if (this.repeat != 0 && this._count >= this.repeat) {
            //清理
            this.clear();
            //回调
            handler && handler.run();
        }
        else {
            this.restart();
        }
    }
    /**
     * 暂停缓动，可以通过resume或restart重新开始。
     */
    pause() {
        this._timer.clear(this, this._beginLoop);
        this._timer.clear(this, this._doEase);
        this._timer.clear(this, this.firstStart);
        var time = Laya.Browser.now();
        var dTime;
        dTime = time - this._startTimer - this._delay;
        if (dTime < 0) {
            this._usedTimer = dTime;
        }
    }
    /**
     * 设置开始时间。
     * @param	startTime 开始时间。
     */
    setStartTime(startTime) {
        this._startTimer = startTime;
    }
    /**
     * 清理指定目标对象上的所有缓动。
     * @param	target 目标对象。
     */
    static clearAll(target) {
        if (!target || !target.$_GID)
            return;
        var tweens = MTween.tweenMap[target.$_GID];
        if (tweens) {
            for (var i = 0, n = tweens.length; i < n; i++) {
                tweens[i]._clear();
            }
            tweens.length = 0;
        }
    }
    /**
     * 清理指定目标对象上的所有缓动。
     * @param	target 目标对象。
     */
    static clearAllTween() {
        for (let j = 0; j < MTween.tweenMap.length; j++) {
            var tweens = MTween.tweenMap[j];
            if (tweens) {
                for (var i = 0, n = tweens.length; i < n; i++) {
                    tweens[i]._clear();
                }
                tweens.length = 0;
            }
        }
        MTween.tweenMap = [];
    }
    /**
     * 清理某个缓动。
     * @param	tween 缓动对象。
     */
    static clear(tween) {
        tween.clear();
    }
    /**@private 同clearAll，废弃掉，尽量别用。*/
    static clearTween(target) {
        MTween.clearAll(target);
    }
    /**
     * 停止并清理当前缓动。
     */
    clear() {
        if (this._target) {
            this._remove();
            this._clear();
        }
    }
    /**
     * @internal
     */
    _clear() {
        this.pause();
        this._timer.clear(this, this.firstStart);
        this._complete = null;
        this._target = null;
        this._ease = null;
        this._props = null;
        this._delayParam = null;
        this.repeat = 1;
        if (this._usedPool) {
            this.update = null;
            Laya.Pool.recover(_common_Const__WEBPACK_IMPORTED_MODULE_0__["default"].gameNameJian + "_mtween", this);
        }
    }
    /** 回收到对象池。*/
    recover() {
        this._usedPool = true;
        this._clear();
    }
    _remove() {
        var tweens = MTween.tweenMap[this._target.$_GID];
        if (tweens) {
            for (var i = 0, n = tweens.length; i < n; i++) {
                if (tweens[i] === this) {
                    tweens.splice(i, 1);
                    break;
                }
            }
        }
    }
    /**
     * 重新开始暂停的缓动。
     */
    restart() {
        this.pause();
        this._usedTimer = 0;
        this._startTimer = Laya.Browser.now();
        if (this._delayParam) {
            this._timer.once(this._delay, this, this.firstStart, this._delayParam);
            return;
        }
        var props = this._props;
        for (var i = 0, n = props.length; i < n; i++) {
            var prop = props[i];
            this._target[prop[0]] = prop[1];
        }
        this._timer.once(this._delay, this, this._beginLoop);
    }
    /**
     * 恢复暂停的缓动。
     */
    resume() {
        if (this._usedTimer >= this._duration)
            return;
        this._startTimer = Laya.Browser.now() - this._usedTimer - this._delay;
        if (this._delayParam) {
            if (this._usedTimer < 0) {
                this._timer.once(-this._usedTimer, this, this.firstStart, this._delayParam);
            }
            else {
                this.firstStart.apply(this, this._delayParam);
            }
        }
        else {
            this._beginLoop();
        }
    }
    static easeNone(t, b, c, d) {
        return (c * t) / d + b;
    }
}
/**@private */
MTween.tweenMap = [];


/***/ }),

/***/ "./Number-Jump/scripts/script/scene/sprite/Sound.ts":
/*!**********************************************************!*\
  !*** ./Number-Jump/scripts/script/scene/sprite/Sound.ts ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Sound; });
/* harmony import */ var _gen_ui_NumberJump_UI_bgm__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_bgm */ "./Number-Jump/gen/ui/NumberJump/UI_bgm.ts");
/* harmony import */ var _gen_ui_NumberJump_UI_break__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_break */ "./Number-Jump/gen/ui/NumberJump/UI_break.ts");
/* harmony import */ var _gen_ui_NumberJump_UI_fall__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_fall */ "./Number-Jump/gen/ui/NumberJump/UI_fall.ts");
/* harmony import */ var _gen_ui_NumberJump_UI_jump__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_jump */ "./Number-Jump/gen/ui/NumberJump/UI_jump.ts");
/* harmony import */ var _gen_ui_NumberJump_UI_levelup__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_levelup */ "./Number-Jump/gen/ui/NumberJump/UI_levelup.ts");
/* harmony import */ var _gen_ui_NumberJump_UI_wrong__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_wrong */ "./Number-Jump/gen/ui/NumberJump/UI_wrong.ts");
/* harmony import */ var _enum_ESound__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../enum/ESound */ "./Number-Jump/scripts/script/enum/ESound.ts");
/* harmony import */ var _Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Main3d/NumberJump */ "./Number-Jump/scripts/script/scene/Main3d/NumberJump.ts");








class Sound {
    constructor() {
        this._soundType = {};
    }
    static get instance() {
        if (this._instance == null) {
            this._instance = new Sound();
        }
        return this._instance;
    }
    stopSound(soundType) {
        if (this._soundType[soundType]) {
            this._soundType[soundType]["m_" + soundType].stop();
        }
    }
    playSoundSp(soundType) {
        if (!_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_7__["default"].release) {
            return Laya.SoundManager.playSound("Assets/Number-Jump/ui/" + soundType + ".mp3", 1);
        }
        else {
            return _Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_7__["default"].instance.playAudio("Assets/Number-Jump/ui/" + soundType + ".mp3", 1, true);
        }
    }
    playSound(soundType, time = 1) {
        if (this._soundType[soundType]) {
            this._soundType[soundType]["m_" + soundType].play();
            return;
        }
        let sound;
        switch (soundType) {
            case _enum_ESound__WEBPACK_IMPORTED_MODULE_6__["ESoundName"].bgm:
                sound = _gen_ui_NumberJump_UI_bgm__WEBPACK_IMPORTED_MODULE_0__["default"].createInstance();
                break;
            case _enum_ESound__WEBPACK_IMPORTED_MODULE_6__["ESoundName"].wrong:
                sound = _gen_ui_NumberJump_UI_wrong__WEBPACK_IMPORTED_MODULE_5__["default"].createInstance();
                break;
            case _enum_ESound__WEBPACK_IMPORTED_MODULE_6__["ESoundName"].fall:
                sound = _gen_ui_NumberJump_UI_fall__WEBPACK_IMPORTED_MODULE_2__["default"].createInstance();
                break;
            case _enum_ESound__WEBPACK_IMPORTED_MODULE_6__["ESoundName"].jump:
                sound = _gen_ui_NumberJump_UI_jump__WEBPACK_IMPORTED_MODULE_3__["default"].createInstance();
                break;
            // case ESoundName.compress:
            //     sound = UI_compress.createInstance();
            //     break;
            case _enum_ESound__WEBPACK_IMPORTED_MODULE_6__["ESoundName"].break:
                sound = _gen_ui_NumberJump_UI_break__WEBPACK_IMPORTED_MODULE_1__["default"].createInstance();
                break;
            case _enum_ESound__WEBPACK_IMPORTED_MODULE_6__["ESoundName"].levelup:
                sound = _gen_ui_NumberJump_UI_levelup__WEBPACK_IMPORTED_MODULE_4__["default"].createInstance();
                break;
        }
        this._soundType[soundType] = sound;
        if (this._soundType[soundType]) {
            this._soundType[soundType]["m_" + soundType].play();
        }
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/ui/UI_BoxMediator.ts":
/*!*********************************************************!*\
  !*** ./Number-Jump/scripts/script/ui/UI_BoxMediator.ts ***!
  \*********************************************************/
/*! exports provided: UI_BoxMediator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UI_BoxMediator", function() { return UI_BoxMediator; });
/* harmony import */ var _gen_ui_NumberJump_UI_box__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../gen/ui/NumberJump/UI_box */ "./Number-Jump/gen/ui/NumberJump/UI_box.ts");
/* harmony import */ var _UIExt_FGui_BaseUIMediator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../UIExt/FGui/BaseUIMediator */ "./Number-Jump/scripts/UIExt/FGui/BaseUIMediator.ts");
/* harmony import */ var _UIExt_FGui_FGuiEx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../UIExt/FGui/FGuiEx */ "./Number-Jump/scripts/UIExt/FGui/FGuiEx.ts");
/* harmony import */ var _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");
/* harmony import */ var _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../config/NumberJumpGameConst */ "./Number-Jump/scripts/script/config/NumberJumpGameConst.ts");
/* harmony import */ var _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../scene/Main3d/NumberJump */ "./Number-Jump/scripts/script/scene/Main3d/NumberJump.ts");






class UI_BoxMediator extends _UIExt_FGui_BaseUIMediator__WEBPACK_IMPORTED_MODULE_1__["default"] {
    constructor() {
        super(...arguments);
        this._layer = _UIExt_FGui_FGuiEx__WEBPACK_IMPORTED_MODULE_2__["EUILayer"].MainPanel;
    }
    static get instance() {
        if (this._instance == null) {
            this._instance = new UI_BoxMediator();
            this._instance._classDefine = _gen_ui_NumberJump_UI_box__WEBPACK_IMPORTED_MODULE_0__["default"];
        }
        return this._instance;
    }
    _OnShow() {
        const _super = Object.create(null, {
            _OnShow: { get: () => super._OnShow }
        });
        return __awaiter(this, void 0, void 0, function* () {
            _super._OnShow.call(this);
            this.onClick(this.ui.m_noBtn, this._onNoBtn);
            this.onClick(this.ui.m_mvBtn, this._onMvBtn);
            // 暂停游戏
            _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_3__["default"].gameScript.pause();
            this.ui.m_propItem1.m_numItem.text = "x" + _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_4__["NumberJumpGameConst"].data.mvDffNum;
            this.ui.m_propItem2.m_numItem.text = "x" + _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_4__["NumberJumpGameConst"].data.mvHundunNum;
        });
    }
    _onNoBtn() {
        this.Hide();
    }
    _onMvBtn() {
        if (!_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_5__["default"].release) {
            // 获得奖励
            _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_3__["default"].gameScript.getMvProp();
            this.Hide();
            return;
        }
        _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_5__["default"].instance.event.emit("videoPay", "numberjump-chest", () => {
            // 获得奖励
            _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_3__["default"].gameScript.getMvProp();
            this.Hide();
        }, () => { });
    }
    // private _onEnterBtn() {
    //     NumberJump.instance.event.emit("currencyCost", "coin", 10);
    //     this._openParam.closeCall();
    //     NumberJumpGlobalUnit.gameScript.timer.clearAll(this);
    //     this.Hide();
    // }
    _OnHide() {
        super._OnHide();
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_3__["default"].gameScript.start();
    }
    static clear() {
        UI_BoxMediator.instance.Hide(true);
        this._instance = null;
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/ui/UI_NumberJumpMainMediator.ts":
/*!********************************************************************!*\
  !*** ./Number-Jump/scripts/script/ui/UI_NumberJumpMainMediator.ts ***!
  \********************************************************************/
/*! exports provided: UI_NumberJumpMainMediator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UI_NumberJumpMainMediator", function() { return UI_NumberJumpMainMediator; });
/* harmony import */ var _gen_ui_NumberJump_UI_Main__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../gen/ui/NumberJump/UI_Main */ "./Number-Jump/gen/ui/NumberJump/UI_Main.ts");
/* harmony import */ var _UIExt_FGui_BaseUIMediator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../UIExt/FGui/BaseUIMediator */ "./Number-Jump/scripts/UIExt/FGui/BaseUIMediator.ts");
/* harmony import */ var _UIExt_FGui_FGuiEx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../UIExt/FGui/FGuiEx */ "./Number-Jump/scripts/UIExt/FGui/FGuiEx.ts");
/* harmony import */ var _common_GameData__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common/GameData */ "./Number-Jump/scripts/script/common/GameData.ts");
/* harmony import */ var _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");
/* harmony import */ var _MainStart__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../MainStart */ "./Number-Jump/scripts/script/MainStart.ts");
/* harmony import */ var _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../scene/Main3d/NumberJump */ "./Number-Jump/scripts/script/scene/Main3d/NumberJump.ts");
/* harmony import */ var _scene_script_MTween__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../scene/script/MTween */ "./Number-Jump/scripts/script/scene/script/MTween.ts");
/* harmony import */ var _UI_RestartMediator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./UI_RestartMediator */ "./Number-Jump/scripts/script/ui/UI_RestartMediator.ts");
/* harmony import */ var _UI_RewardMediator__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./UI_RewardMediator */ "./Number-Jump/scripts/script/ui/UI_RewardMediator.ts");
/* harmony import */ var _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../LTGame/LTUtils/MathEx */ "./Number-Jump/scripts/LTGame/LTUtils/MathEx.ts");
/* harmony import */ var _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../config/NumberJumpGameConst */ "./Number-Jump/scripts/script/config/NumberJumpGameConst.ts");
/* harmony import */ var _data_LevelProxy__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../data/LevelProxy */ "./Number-Jump/scripts/script/data/LevelProxy.ts");
/* harmony import */ var _UI_BoxMediator__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./UI_BoxMediator */ "./Number-Jump/scripts/script/ui/UI_BoxMediator.ts");














// ui
class UI_NumberJumpMainMediator extends _UIExt_FGui_BaseUIMediator__WEBPACK_IMPORTED_MODULE_1__["default"] {
    constructor() {
        super(...arguments);
        this._layer = _UIExt_FGui_FGuiEx__WEBPACK_IMPORTED_MODULE_2__["EUILayer"].Main;
        this.fuhuoTimes = 0;
    }
    static get instance() {
        if (this._instance == null) {
            this._instance = new UI_NumberJumpMainMediator();
            this._instance._classDefine = _gen_ui_NumberJump_UI_Main__WEBPACK_IMPORTED_MODULE_0__["default"];
        }
        return this._instance;
    }
    _OnShow() {
        const _super = Object.create(null, {
            _OnShow: { get: () => super._OnShow }
        });
        return __awaiter(this, void 0, void 0, function* () {
            _super._OnShow.call(this);
            this.mapLayer.init();
            this.heroNum.init();
            this.init();
            _MainStart__WEBPACK_IMPORTED_MODULE_5__["default"].instance.splashScene.loadOverCall();
            // 判断 是不是刘海屏
            if (Laya.Browser.clientWidth / Laya.Browser.clientHeight > 0.5) {
                // 宽屏
                this.ui.x = 828 * (Laya.Browser.clientWidth / Laya.Browser.clientHeight - 0.5);
                this.ui.m_liuhai.selectedIndex = 0;
            }
            else {
                this.ui.m_liuhai.selectedIndex = 1;
            }
            this.updateChengfaTxt();
            this.updatePro();
            this._updateHundunIcon();
            this._updateDffIcon();
            // this.ui.m_mapLayer.displayObject.cacheAs = "bitmap";
        });
    }
    get mapLayer() {
        return this.ui.m_mapLayer;
    }
    get heroNum() {
        return this.ui.m_heroNum;
    }
    setCanCaozuo(bool) {
        this.ui.m_touch.touchable = bool;
    }
    setCanClick(bool) {
        this.ui.touchable = bool;
    }
    getGuideBtn(data) {
        if (data.btnName) {
            return this.ui["m_" + data.btnName];
        }
    }
    updatePro() {
        let nowLevelId = _data_LevelProxy__WEBPACK_IMPORTED_MODULE_12__["default"].instance.nowLevelId();
        let needzawNum = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_11__["NumberJumpGameConst"].data.lvNums[nowLevelId - 1];
        let score = _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.score;
        this.ui.m_gamePro.max = needzawNum;
        this.ui.m_gamePro.value = score;
        this.ui.m_barNum.m_text.text = score + "";
        this.ui.m_barNum.x = this.ui.m_gamePro.x + (this.ui.m_gamePro.width * score) / needzawNum;
        this.ui.m_gamePro.m_maxbar.x =
            (this.ui.m_gamePro.width * _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.nowMaxScore) / needzawNum;
        this.ui.m_maxScore.text = (_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_6__["default"].instance.bestScore || 0) + "";
    }
    init() {
        this._onEvent();
        _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_6__["default"].instance.view = this.ui;
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.initLv();
    }
    addChild(ui) {
        this.ui.addChild(ui);
    }
    // 添加关卡
    addLv(ui) {
        this.ui.m_mapLayer.addChild(ui);
    }
    _onEvent() {
        this.onClick(this.ui.m_moveMvBtn, this._onMoveMBtn);
        this.onClick(this.ui.m_dffDamageBtn, this._onDffDamageBtn);
        this.onClick(this.ui.m_hudunBtn, this._onHudunBtn);
        this.onClick(this.ui.m_restartBtn, this._onRestartBtn);
        this.onClick(this.ui.m_mapLayer, this._onMapLayer);
        this.ui.m_touch.on(Laya.Event.MOUSE_DOWN, this, this._onTouchMouseDown);
        this.ui.m_touch.on(Laya.Event.MOUSE_UP, this, this._onTouchMouseUp);
    }
    _onDffDamageBtn() {
        if (_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.chengfaNum <= 0) {
            return;
        }
        if (_common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].instance.dffNum > 0) {
            _common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].instance.dffNum--;
            _common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].SaveToDisk();
            _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.chengfaNum = Math.floor((_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.chengfaNum /= 2));
            _common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].instance.chengfaNum = _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.chengfaNum;
            _common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].SaveToDisk();
            this.updateDamageNum();
            this._updateDffIcon();
        }
    }
    _onHudunBtn() {
        if (this.heroNum.usedHundun) {
            return;
        }
        if (_common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].instance.hundunNum > 0) {
            _common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].instance.hundunNum--;
            this.heroNum.usehundun();
            _common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].SaveToDisk();
            this._updateHundunIcon();
        }
    }
    _updateHundunIcon() {
        this.ui.m_hudunBtn.visible = !!_common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].instance.hundunNum;
        this.ui.m_hudunBtn.m_num.text = _common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].instance.hundunNum + "";
    }
    _updateDffIcon() {
        this.ui.m_dffDamageBtn.visible = !!_common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].instance.dffNum;
        this.ui.m_dffDamageBtn.m_num.text = _common_GameData__WEBPACK_IMPORTED_MODULE_3__["default"].instance.dffNum + "";
    }
    _onTouchMouseDown() {
        if (_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.gaming) {
            // 蓄力
            _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.startXuli();
        }
    }
    _onTouchMouseUp() {
        if (_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.gaming) {
            _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.touchEnd();
        }
    }
    _onMapLayer() { }
    _onMoveMBtn() {
        if (this.heroNum.jumping || this.mapLayer.mapMoving) {
            // 跳跃中 不能前进
            return;
        }
        let maxScore = _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.nowMaxScore;
        if (maxScore > 0 && _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.score < maxScore) {
            if (!_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_6__["default"].release) {
                _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.quickMove();
                return;
            }
            _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_6__["default"].instance.event.emit("videoPay", "numberjump-fly", () => {
                // 向前跑到最新的 地方
                _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.quickMove();
            }, () => { });
        }
        else {
            // 暂时没有分数
            // NumberJump.instance.event;
        }
    }
    _onRestartBtn() {
        let bool = _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_6__["default"].instance.costCoin(2);
        // 获得奖励
        bool && _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.startNewLv();
        // UI_RestartMediator.instance.Show();
    }
    updateChengfaTxt(backhome = false) {
        let txt = _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_10__["default"].RandomFromArray(backhome ? _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_11__["NumberJumpGameConst"].data.laterTalkTxt : _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_11__["NumberJumpGameConst"].data.startTalkTxt);
        txt = txt.replace("{0}", _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.bacKNum + "");
        txt = txt.replace("{1}", _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.backHomeNum + "");
        txt = txt.replace("{2}", _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.chengfaNum + "");
        // switch (index) {
        //     case 0:
        //         let bacKNum = ;
        //         this.mapLayer.m_gameDesc.text = "你已经被弹飞" + bacKNum + "次拉～～";
        //         break;
        //     case 1:
        //         let backHomeNum = NumberJumpGlobalUnit.gameScript.backHomeNum;
        //         this.mapLayer.m_gameDesc.text = "你已经回家" + backHomeNum + "次拉～～";
        //         break;
        //     case 2:
        //         let chengfaNum = NumberJumpGlobalUnit.gameScript.chengfaNum;
        //         this.mapLayer.m_gameDesc.text = "你已经被惩罚" + chengfaNum + "次拉～～";
        //         break;
        //     case 3:
        //         this.mapLayer.m_gameDesc.text = "每次弹飞的次数会累计起来作为惩罚";
        //         break;
        // }
        this.mapLayer.m_gameDesc.text = txt;
        this.updateDamageNum();
    }
    updateDamageNum() {
        if (this.ui.m_damageNum.text == "") {
            this.ui.m_damageNum.text = _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.chengfaNum + "";
        }
        else {
            let nowNum = Math.floor(parseInt(this.ui.m_damageNum.text));
            if (Math.abs(nowNum - _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.chengfaNum) <= 1) {
                this.ui.m_damageNum.text = _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.chengfaNum + "";
            }
            else {
                // 滚动
                this._startGundong();
            }
        }
    }
    _startGundong() {
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.timer.clear(this, this._updatedamagenumoffset);
        let offset = _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.chengfaNum - parseInt(this.ui.m_damageNum.text);
        let timer = Math.abs(800 / offset);
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.timer.loop(timer, this, this._updatedamagenumoffset);
    }
    _updatedamagenumoffset() {
        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_7__["default"].to(this.ui.m_damageNum, { scaleX: 1.2, scaleY: 1.2 }, 100, null, Laya.Handler.create(this, () => {
            if (!this.ui.isDisposed) {
                _scene_script_MTween__WEBPACK_IMPORTED_MODULE_7__["default"].to(this.ui.m_damageNum, { scaleX: 1, scaleY: 1 }, 100);
            }
        }));
        let nowNum = Math.floor(parseInt(this.ui.m_damageNum.text));
        let dire = _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.chengfaNum > nowNum ? 1 : -1;
        let next = nowNum + dire;
        this.ui.m_damageNum.text = next + "";
        if (next == _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.chengfaNum) {
            _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.timer.clear(this, this._updatedamagenumoffset);
        }
    }
    _offEvent() { }
    _onSend() { }
    onTouchCom() { }
    startGame() {
        this.ui.m_gameState.selectedIndex = 1;
        this.setCanCaozuo(true);
        this.setCanClick(true);
    }
    // 回到等待游戏界面
    backWaitGame() {
        this.ui.m_gameState.selectedIndex = 0;
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.timer.clear(this, this._jiesuan);
        // NumberJumpGlobalUnit.gameScript.dispose();
        // GameData.SaveToDisk();
    }
    viewBackCall(panel) { }
    // 游戏结束
    gameOver() {
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.timer.once(1500, this, this._jiesuan);
    }
    fuhuo() {
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.timer.clear(this, this._jiesuan);
        this.ui.m_gameState.selectedIndex = 1;
    }
    _jiesuan() {
        if (this.ui.m_gameState.selectedIndex == 0)
            return;
        this.ui.m_gameState.selectedIndex = 2;
        if (this.fuhuoTimes > 0) {
            // UI_EndMvMediator.instance.Show(1);
            this.fuhuoTimes--;
        }
        else {
            _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_6__["default"].instance.event.emit("over");
        }
    }
    _OnHide() {
        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_7__["default"].clearAll(this);
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.timer.clearAll(this);
        super._OnHide();
    }
    exit() {
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.timer.clearAll(this);
        UI_NumberJumpMainMediator._instance.Hide(true);
        UI_NumberJumpMainMediator._instance = null;
        _UI_RestartMediator__WEBPACK_IMPORTED_MODULE_8__["UI_RestartMediator"].clear();
        _UI_RewardMediator__WEBPACK_IMPORTED_MODULE_9__["UI_RewardMediator"].clear();
        _UI_BoxMediator__WEBPACK_IMPORTED_MODULE_13__["UI_BoxMediator"].clear();
        // 移除所有定时器  和 循环
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].dispose();
        _UIExt_FGui_FGuiEx__WEBPACK_IMPORTED_MODULE_2__["default"].removeAllLayer();
        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_7__["default"].clearAllTween();
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/ui/UI_RestartMediator.ts":
/*!*************************************************************!*\
  !*** ./Number-Jump/scripts/script/ui/UI_RestartMediator.ts ***!
  \*************************************************************/
/*! exports provided: UI_RestartMediator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UI_RestartMediator", function() { return UI_RestartMediator; });
/* harmony import */ var _gen_ui_NumberJump_UI_restart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../gen/ui/NumberJump/UI_restart */ "./Number-Jump/gen/ui/NumberJump/UI_restart.ts");
/* harmony import */ var _UIExt_FGui_BaseUIMediator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../UIExt/FGui/BaseUIMediator */ "./Number-Jump/scripts/UIExt/FGui/BaseUIMediator.ts");
/* harmony import */ var _UIExt_FGui_FGuiEx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../UIExt/FGui/FGuiEx */ "./Number-Jump/scripts/UIExt/FGui/FGuiEx.ts");
/* harmony import */ var _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");
/* harmony import */ var _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../scene/Main3d/NumberJump */ "./Number-Jump/scripts/script/scene/Main3d/NumberJump.ts");





// 重新开始
class UI_RestartMediator extends _UIExt_FGui_BaseUIMediator__WEBPACK_IMPORTED_MODULE_1__["default"] {
    constructor() {
        super(...arguments);
        this._layer = _UIExt_FGui_FGuiEx__WEBPACK_IMPORTED_MODULE_2__["EUILayer"].MainPanel;
    }
    static get instance() {
        if (this._instance == null) {
            this._instance = new UI_RestartMediator();
            this._instance._classDefine = _gen_ui_NumberJump_UI_restart__WEBPACK_IMPORTED_MODULE_0__["default"];
        }
        return this._instance;
    }
    _OnShow() {
        const _super = Object.create(null, {
            _OnShow: { get: () => super._OnShow }
        });
        return __awaiter(this, void 0, void 0, function* () {
            _super._OnShow.call(this);
            this.onClick(this.ui.m_enterBtn, this._onMvBtn);
            this.onClick(this.ui.m_noBtn, this._onNoBtn);
        });
    }
    _onMvBtn() {
        if (!_scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_4__["default"].release) {
            // 获得奖励
            _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_3__["default"].gameScript.startNewLv();
            this.Hide();
            return;
        }
        _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_4__["default"].instance.event.emit("videoPay", "numberjump-chest", () => {
            // 获得奖励
            _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_3__["default"].gameScript.startNewLv();
            this.Hide();
        }, () => { });
    }
    _onNoBtn() {
        this.Hide();
    }
    _OnHide() {
        super._OnHide();
    }
    static clear() {
        UI_RestartMediator.instance.Hide(true);
        this._instance = null;
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/ui/UI_RewardMediator.ts":
/*!************************************************************!*\
  !*** ./Number-Jump/scripts/script/ui/UI_RewardMediator.ts ***!
  \************************************************************/
/*! exports provided: UI_RewardMediator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UI_RewardMediator", function() { return UI_RewardMediator; });
/* harmony import */ var _gen_ui_NumberJump_UI_reward__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../gen/ui/NumberJump/UI_reward */ "./Number-Jump/gen/ui/NumberJump/UI_reward.ts");
/* harmony import */ var _UIExt_FGui_BaseUIMediator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../UIExt/FGui/BaseUIMediator */ "./Number-Jump/scripts/UIExt/FGui/BaseUIMediator.ts");
/* harmony import */ var _UIExt_FGui_FGuiEx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../UIExt/FGui/FGuiEx */ "./Number-Jump/scripts/UIExt/FGui/FGuiEx.ts");
/* harmony import */ var _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");
/* harmony import */ var _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../config/NumberJumpGameConst */ "./Number-Jump/scripts/script/config/NumberJumpGameConst.ts");
/* harmony import */ var _data_LevelProxy__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../data/LevelProxy */ "./Number-Jump/scripts/script/data/LevelProxy.ts");
/* harmony import */ var _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../scene/Main3d/NumberJump */ "./Number-Jump/scripts/script/scene/Main3d/NumberJump.ts");







class UI_RewardMediator extends _UIExt_FGui_BaseUIMediator__WEBPACK_IMPORTED_MODULE_1__["default"] {
    constructor() {
        super(...arguments);
        this._layer = _UIExt_FGui_FGuiEx__WEBPACK_IMPORTED_MODULE_2__["EUILayer"].MainPanel;
    }
    static get instance() {
        if (this._instance == null) {
            this._instance = new UI_RewardMediator();
            this._instance._classDefine = _gen_ui_NumberJump_UI_reward__WEBPACK_IMPORTED_MODULE_0__["default"];
        }
        return this._instance;
    }
    _OnShow() {
        const _super = Object.create(null, {
            _OnShow: { get: () => super._OnShow }
        });
        return __awaiter(this, void 0, void 0, function* () {
            _super._OnShow.call(this);
            // UI_NumberJumpMainMediator.instance.setCanClick(true);
            this.onClick(this.ui.m_getBtn, this._onEnterBtn);
            let index = Math.max(_data_LevelProxy__WEBPACK_IMPORTED_MODULE_5__["default"].instance.nowLevelId() - 2, 0);
            this.ui.m_jiangjuanNum.text =
                _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_4__["NumberJumpGameConst"].data.lvGetRewardNum[index] + "";
        });
    }
    _onEnterBtn() {
        let index = Math.max(_data_LevelProxy__WEBPACK_IMPORTED_MODULE_5__["default"].instance.nowLevelId() - 2, 0);
        _scene_Main3d_NumberJump__WEBPACK_IMPORTED_MODULE_6__["default"].instance.event.emit("currencyCost", "ticket", _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_4__["NumberJumpGameConst"].data.lvGetRewardNum[index] || 0);
        this._openParam && this._openParam.closeCall();
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_3__["default"].gameScript.timer.clearAll(this);
        this.Hide();
    }
    _OnHide() {
        super._OnHide();
    }
    static clear() {
        UI_RewardMediator.instance.Hide(true);
        this._instance = null;
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/ui/com/heroItem.ts":
/*!*******************************************************!*\
  !*** ./Number-Jump/scripts/script/ui/com/heroItem.ts ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return heroItem; });
/* harmony import */ var _gen_ui_NumberJump_UI_heroItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_heroItem */ "./Number-Jump/gen/ui/NumberJump/UI_heroItem.ts");
/* harmony import */ var _gen_ui_NumberJump_UI_jianciani__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_jianciani */ "./Number-Jump/gen/ui/NumberJump/UI_jianciani.ts");
/* harmony import */ var _LTGame_Async_Awaiters__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../LTGame/Async/Awaiters */ "./Number-Jump/scripts/LTGame/Async/Awaiters.ts");
/* harmony import */ var _LTGame_LTUtils_LTUtils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../LTGame/LTUtils/LTUtils */ "./Number-Jump/scripts/LTGame/LTUtils/LTUtils.ts");
/* harmony import */ var _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");
/* harmony import */ var _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../config/NumberJumpGameConst */ "./Number-Jump/scripts/script/config/NumberJumpGameConst.ts");
/* harmony import */ var _enum_ESound__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../enum/ESound */ "./Number-Jump/scripts/script/enum/ESound.ts");
/* harmony import */ var _scene_script_MTween__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../scene/script/MTween */ "./Number-Jump/scripts/script/scene/script/MTween.ts");
/* harmony import */ var _scene_sprite_Sound__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../scene/sprite/Sound */ "./Number-Jump/scripts/script/scene/sprite/Sound.ts");
/* harmony import */ var _prop_bigJianciItem__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../prop/bigJianciItem */ "./Number-Jump/scripts/script/ui/prop/bigJianciItem.ts");
/* harmony import */ var _prop_blackItem__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../prop/blackItem */ "./Number-Jump/scripts/script/ui/prop/blackItem.ts");
/* harmony import */ var _prop_boxItem__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../prop/boxItem */ "./Number-Jump/scripts/script/ui/prop/boxItem.ts");
/* harmony import */ var _prop_huadaoItem__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../prop/huadaoItem */ "./Number-Jump/scripts/script/ui/prop/huadaoItem.ts");
/* harmony import */ var _prop_jianciItem__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../prop/jianciItem */ "./Number-Jump/scripts/script/ui/prop/jianciItem.ts");
/* harmony import */ var _UI_BoxMediator__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../UI_BoxMediator */ "./Number-Jump/scripts/script/ui/UI_BoxMediator.ts");
/* harmony import */ var _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../UI_NumberJumpMainMediator */ "./Number-Jump/scripts/script/ui/UI_NumberJumpMainMediator.ts");
















class heroItem extends _gen_ui_NumberJump_UI_heroItem__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor() {
        super(...arguments);
        this.jumping = false;
        this._initAni = false;
        this._stop = false;
        this._ySpeed = 0;
        this.initY = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.numY;
        this.backing = false;
        this._jumpOnceDis = 250;
        this._jumpTwoDis = 150;
        this.usedHundun = false;
        this._index = 0;
        this._jumpTimes = 2;
    }
    set index(num) {
        if (num == undefined)
            return;
        this._index = num;
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.updateIndex(this._index);
    }
    get index() {
        return this._index;
    }
    static createInstance() {
        return fgui.UIPackage.createObject("NumberJump", "heroItem");
    }
    uplv() {
        this.m_upani.visible = true;
        this.m_upani.setPlaySettings(0, -1, 1, -1, () => {
            this.m_upani.visible = false;
        });
    }
    updateScore() {
        let score = _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.score;
        // score = score + LevelProxy.instance.viewNumOffset();
        let heroNum = this.m_heroNum;
        if (score < 10) {
            heroNum.m_type.selectedIndex = 0;
            heroNum.m_num.text = score + "";
        }
        else if (score < 100) {
            heroNum.m_type.selectedIndex = 1;
            heroNum.m_num11.text = Math.floor(score / 10) + "";
            heroNum.m_num12.text = (score % 10) + "";
        }
        else {
            heroNum.m_type.selectedIndex = 2;
            heroNum.m_num21.text = Math.floor(score / 100) + "";
            heroNum.m_num22.text = Math.floor((score % 100) / 10) + "";
            heroNum.m_num23.text = Math.floor(score % 10) + "";
        }
    }
    luodiAni() {
        this.m_luodiAni.visible = true;
        this.m_luodiAni.setPlaySettings(0, -1, 1, -1, () => {
            this.m_luodiAni.visible = false;
        });
    }
    onConstruct() {
        super.onConstruct();
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.timer.frameLoop(1, this, this.update);
    }
    noInitAni() {
        return __awaiter(this, void 0, void 0, function* () {
            this._initAni = false;
            yield _LTGame_Async_Awaiters__WEBPACK_IMPORTED_MODULE_2__["default"].NextFrame();
            this.y = this.initY;
            this.x = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.startAnimoveDis;
        });
    }
    _playStartAni() {
        return __awaiter(this, void 0, void 0, function* () {
            this._initAni = true;
            this._jumpTimes = 0;
            yield _LTGame_Async_Awaiters__WEBPACK_IMPORTED_MODULE_2__["default"].NextFrame();
            if (!this._initAni)
                return;
            this._jumpTimes = 2;
            this.y = this.initY;
            this.x = -150;
            let time = this._jumpOnceDis / _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.xSpeed / 2;
            this._ySpeed = -_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.gravity * time;
            _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_15__["UI_NumberJumpMainMediator"].instance.setCanCaozuo(false);
        });
    }
    collisionBox() {
        let box = {
            x: this.x - this.width / 4,
            y: this.y - this.height,
            width: (this.width / 4) * 2,
            height: this.height,
        };
        let globalPos = this.parent.localToGlobal(box.x, box.y);
        let pos = _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_15__["UI_NumberJumpMainMediator"].instance.mapLayer.globalToLocal(globalPos.x, globalPos.y);
        box.x = pos.x;
        box.y = pos.y;
        return box;
    }
    get localX() {
        let globalPos = this.parent.localToGlobal(this.x, this.y);
        let pos = _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_15__["UI_NumberJumpMainMediator"].instance.mapLayer.globalToLocal(globalPos.x, globalPos.y);
        return pos.x;
    }
    get localY() {
        let globalPos = this.parent.localToGlobal(this.x, this.y);
        let pos = _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_15__["UI_NumberJumpMainMediator"].instance.mapLayer.globalToLocal(globalPos.x, globalPos.y);
        return pos.y;
    }
    _clear() {
        this._index = 0;
        this.jumping = false;
        this._stop = false;
        this._ySpeed = 0;
        this.backing = false;
        this.nowHuadaoitem = null;
    }
    init() {
        this._clear();
        // 播放动画
        this._playStartAni();
    }
    // 使用护盾
    usehundun() {
        this.usedHundun = true;
        this.m_hundun.visible = true;
    }
    // 护盾破碎
    hundunBreak() {
        this.usedHundun = false;
        this.m_hundun.visible = false;
        this.m_hundunBreak.visible = true;
        this.m_hundunBreak.setPlaySettings(0, -1, 1, -1, () => {
            this.m_hundunBreak.visible = false;
        });
        _scene_sprite_Sound__WEBPACK_IMPORTED_MODULE_8__["default"].instance.playSound(_enum_ESound__WEBPACK_IMPORTED_MODULE_6__["ESoundName"].break);
    }
    chengfaJump(backDis) {
        let time = (Math.abs(backDis) / _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.backXSpeed) * 16.7;
        time = Math.min(_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.backXSpeedMaxTime, time);
        this._ySpeed =
            (((_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.gravity * time) / 2) * (backDis > 0 ? 1 : -1)) / 16.7;
        // console.log("ySpeed-------------", this._ySpeed);
        this.backing = true;
        this.startJump();
    }
    startJump() {
        if (this.nowHuadaoitem) {
            this.nowHuadaoitem.endHuadao();
            this.nowHuadaoitem = null;
        }
        this.jumping = true;
        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_7__["default"].to(this, { scaleY: 1 }, 100);
        _scene_sprite_Sound__WEBPACK_IMPORTED_MODULE_8__["default"].instance.playSound(_enum_ESound__WEBPACK_IMPORTED_MODULE_6__["ESoundName"].jump);
    }
    updateXuli(xuliTime) {
        let scale = xuliTime / _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.xuliTime;
        let scaleY = 1 - (1 - _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.minNumScale) * scale;
        this.scaleY = scaleY;
        this._ySpeed = -_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.maxUpSpeed * scale;
        // console.log(
        //     "更新跳跃y的速度--",
        //     this._ySpeed,
        //     "    跳跃距离-----",
        //     (NumberJumpGameConst.data.xSpeed * 2 * this._ySpeed) / NumberJumpGameConst.data.gravity
        // );
    }
    update() {
        let timer = _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.timer;
        let dt = _LTGame_LTUtils_LTUtils__WEBPACK_IMPORTED_MODULE_3__["LTUtils"].deltaTimeBytimer(timer) / 16.7;
        if (dt > 10)
            return;
        if (_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.quickMoving)
            return;
        if (this._initAni && this._jumpTimes) {
            if (this._stop)
                return;
            this._ySpeed += _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.gravity * dt;
            this.y += this._ySpeed;
            this.x += _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.xSpeed;
            if (this.x > _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.startAnimoveDis) {
                this.x = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.startAnimoveDis;
            }
            if (this.y >= this.initY) {
                this.y = this.initY;
                this._jumpTimes -= 1;
                if (this._jumpTimes >= 1) {
                    this._stop = true;
                    _scene_script_MTween__WEBPACK_IMPORTED_MODULE_7__["default"].to(this, { scaleY: 0.8 }, 100);
                    _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.timer.once(100, this, () => {
                        let dis = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.startAnimoveDis - this.x;
                        let time = dis / _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.xSpeed / 2;
                        this._ySpeed = -_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.gravity * time;
                        this._stop = false;
                        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_7__["default"].to(this, { scaleY: 1 }, 100);
                    });
                }
                else {
                    this._jumpTimes = 0;
                    this._initAni = false;
                    _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_15__["UI_NumberJumpMainMediator"].instance.setCanCaozuo(true);
                }
            }
            return;
        }
        if (this.jumping) {
            this._ySpeed += _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_5__["NumberJumpGameConst"].data.gravity * dt;
            this.y += this._ySpeed;
            if (this.backing) {
                if (this.y >= this.initY) {
                    this.y = this.initY;
                    // 本次倒退结束
                    this.jumping = false;
                    this.backing = false;
                    _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.chengfaoncejiesu();
                }
                return;
            }
        }
        // if (!this.jumping) return;
        if (this.jumping || this.nowHuadaoitem) {
            // 判断碰撞
            let items = _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript._props;
            let start = this.index - 5;
            let end = this.index + 5;
            for (let i = start; i < end; i++) {
                // 起跳阶段不能碰到非宝箱东西
                let item = items[i];
                if (!item)
                    continue;
                if (!(item instanceof _prop_boxItem__WEBPACK_IMPORTED_MODULE_11__["default"] || item instanceof _prop_bigJianciItem__WEBPACK_IMPORTED_MODULE_9__["default"]) && this._ySpeed <= 0) {
                    continue;
                }
                if (item && !item.isDisposed) {
                    let myBox = this.collisionBox();
                    let box = item.collisionBox();
                    let bool = this._checkBoxCollision(myBox, box);
                    if (bool) {
                        this._collisionItem(item);
                        if (item instanceof _prop_bigJianciItem__WEBPACK_IMPORTED_MODULE_9__["default"] || item instanceof _prop_jianciItem__WEBPACK_IMPORTED_MODULE_13__["default"]) {
                            return;
                        }
                    }
                    else {
                        let jianciCollisionBox = item["jianciCollisionBox"];
                        if (jianciCollisionBox) {
                            let bool = this._checkBoxCollision(myBox, jianciCollisionBox);
                            if (bool) {
                                this._collisionItem(item);
                                if (item instanceof _prop_bigJianciItem__WEBPACK_IMPORTED_MODULE_9__["default"] || item instanceof _prop_jianciItem__WEBPACK_IMPORTED_MODULE_13__["default"]) {
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (this.jumping) {
            if (this.y >= this.initY) {
                this.y = this.initY;
                this.jumping = false;
                _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.moveEnd(!this.nowHuadaoitem);
                _scene_sprite_Sound__WEBPACK_IMPORTED_MODULE_8__["default"].instance.playSound(_enum_ESound__WEBPACK_IMPORTED_MODULE_6__["ESoundName"].fall);
                this.luodiAni();
                // // 继续后退
                // this._ySpeed = NumberJumpGameConst.data.backYSpeed;
            }
        }
    }
    _collisionItem(item) {
        if (item instanceof _prop_bigJianciItem__WEBPACK_IMPORTED_MODULE_9__["default"] || item instanceof _prop_jianciItem__WEBPACK_IMPORTED_MODULE_13__["default"]) {
            this.jumping = false;
            this.nowHuadaoitem && this.nowHuadaoitem.endHuadao();
            this.nowHuadaoitem = null;
            // 开始倒退
            _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.startBack(item.score);
            _scene_sprite_Sound__WEBPACK_IMPORTED_MODULE_8__["default"].instance.playSound(_enum_ESound__WEBPACK_IMPORTED_MODULE_6__["ESoundName"].wrong);
            // 播放尖刺动画   问下是跟在自身还是原地碰撞点
            let jianciani = _gen_ui_NumberJump_UI_jianciani__WEBPACK_IMPORTED_MODULE_1__["default"].createInstance();
            _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_15__["UI_NumberJumpMainMediator"].instance.mapLayer.m_propLayer.addChild(jianciani);
            jianciani.m_jianciani.setPlaySettings(0, -1, 1, -1, () => {
                jianciani && jianciani.dispose();
            });
            jianciani.setXY(this.localX, this.localY);
        }
        else {
            this.index = item.index;
            if (item instanceof _prop_boxItem__WEBPACK_IMPORTED_MODULE_11__["default"]) {
                if (item.m_type.selectedIndex == 1) {
                    _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.enterNextLv();
                    item.remove();
                }
                else {
                    // 弹出弹窗
                    _UI_BoxMediator__WEBPACK_IMPORTED_MODULE_14__["UI_BoxMediator"].instance.Show();
                    item.remove();
                }
            }
            else if (item instanceof _prop_huadaoItem__WEBPACK_IMPORTED_MODULE_12__["default"]) {
                if (item == this.nowHuadaoitem)
                    return;
                // 滑道开始作用
                this.nowHuadaoitem = item;
                item.startHuadaoItem();
            }
            else if (item instanceof _prop_blackItem__WEBPACK_IMPORTED_MODULE_10__["default"]) {
                // console.log("碰撞地板---");
            }
        }
    }
    // 判读碰撞盒
    _checkBoxCollision(collisionBox1, collisionBox) {
        if (collisionBox.x > collisionBox1.x + collisionBox1.width ||
            collisionBox.x + collisionBox.width < collisionBox1.x) {
            return false;
        }
        else if (collisionBox.y > collisionBox1.y + collisionBox1.height ||
            collisionBox.y + collisionBox.height < collisionBox1.y) {
            return false;
        }
        return true;
    }
    dispose() {
        if (this.isDisposed)
            return;
        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_7__["default"].clearAll(this);
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_4__["default"].gameScript.timer.clearAll(this);
        super.dispose();
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/ui/com/mapLayer.ts":
/*!*******************************************************!*\
  !*** ./Number-Jump/scripts/script/ui/com/mapLayer.ts ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return mapLayer; });
/* harmony import */ var _gen_ui_NumberJump_UI_bigTree__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_bigTree */ "./Number-Jump/gen/ui/NumberJump/UI_bigTree.ts");
/* harmony import */ var _gen_ui_NumberJump_UI_dimian__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_dimian */ "./Number-Jump/gen/ui/NumberJump/UI_dimian.ts");
/* harmony import */ var _gen_ui_NumberJump_UI_dimianjin__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_dimianjin */ "./Number-Jump/gen/ui/NumberJump/UI_dimianjin.ts");
/* harmony import */ var _gen_ui_NumberJump_UI_farMap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_farMap */ "./Number-Jump/gen/ui/NumberJump/UI_farMap.ts");
/* harmony import */ var _gen_ui_NumberJump_UI_mapLayer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_mapLayer */ "./Number-Jump/gen/ui/NumberJump/UI_mapLayer.ts");
/* harmony import */ var _gen_ui_NumberJump_UI_nearMap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_nearMap */ "./Number-Jump/gen/ui/NumberJump/UI_nearMap.ts");
/* harmony import */ var _gen_ui_NumberJump_UI_yun__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_yun */ "./Number-Jump/gen/ui/NumberJump/UI_yun.ts");
/* harmony import */ var _LTGame_LTUtils_LTUtils__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../LTGame/LTUtils/LTUtils */ "./Number-Jump/scripts/LTGame/LTUtils/LTUtils.ts");
/* harmony import */ var _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../LTGame/LTUtils/MathEx */ "./Number-Jump/scripts/LTGame/LTUtils/MathEx.ts");
/* harmony import */ var _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");
/* harmony import */ var _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../config/NumberJumpGameConst */ "./Number-Jump/scripts/script/config/NumberJumpGameConst.ts");
/* harmony import */ var _enum_EPropType__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../enum/EPropType */ "./Number-Jump/scripts/script/enum/EPropType.ts");
/* harmony import */ var _scene_script_MTween__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../scene/script/MTween */ "./Number-Jump/scripts/script/scene/script/MTween.ts");
/* harmony import */ var _prop_bigJianciItem__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../prop/bigJianciItem */ "./Number-Jump/scripts/script/ui/prop/bigJianciItem.ts");
/* harmony import */ var _prop_blackItem__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../prop/blackItem */ "./Number-Jump/scripts/script/ui/prop/blackItem.ts");
/* harmony import */ var _prop_boxItem__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../prop/boxItem */ "./Number-Jump/scripts/script/ui/prop/boxItem.ts");
/* harmony import */ var _prop_huadaoItem__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../prop/huadaoItem */ "./Number-Jump/scripts/script/ui/prop/huadaoItem.ts");
/* harmony import */ var _prop_jianciItem__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../prop/jianciItem */ "./Number-Jump/scripts/script/ui/prop/jianciItem.ts");
/* harmony import */ var _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../UI_NumberJumpMainMediator */ "./Number-Jump/scripts/script/ui/UI_NumberJumpMainMediator.ts");



















class mapLayer extends _gen_ui_NumberJump_UI_mapLayer__WEBPACK_IMPORTED_MODULE_4__["default"] {
    constructor() {
        super(...arguments);
        this._nowX = 0;
        this._initY = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_10__["NumberJumpGameConst"].data.blackY;
        this._xSpeed = 0;
        this._maxWidth = 0;
        this._trees = [];
        this._nears = [];
        this._dimians = [];
        this._fars = [];
        this._yuns = [];
        this._dimianners = [];
        this._treeMoveSpeed = 0;
        this._nearMoveSpeed = 0;
        this._targetX = 0;
    }
    static createInstance() {
        return fgui.UIPackage.createObject("NumberJump", "mapLayer");
    }
    onConstruct() {
        super.onConstruct();
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_9__["default"].gameScript.timer.frameLoop(1, this, this.update);
        this._yuns = [
            _gen_ui_NumberJump_UI_yun__WEBPACK_IMPORTED_MODULE_6__["default"].createInstance(),
            _gen_ui_NumberJump_UI_yun__WEBPACK_IMPORTED_MODULE_6__["default"].createInstance(),
            _gen_ui_NumberJump_UI_yun__WEBPACK_IMPORTED_MODULE_6__["default"].createInstance(),
        ];
        this._trees = [
            _gen_ui_NumberJump_UI_bigTree__WEBPACK_IMPORTED_MODULE_0__["default"].createInstance(),
            _gen_ui_NumberJump_UI_bigTree__WEBPACK_IMPORTED_MODULE_0__["default"].createInstance(),
            _gen_ui_NumberJump_UI_bigTree__WEBPACK_IMPORTED_MODULE_0__["default"].createInstance(),
        ];
        this._nears = [
            _gen_ui_NumberJump_UI_nearMap__WEBPACK_IMPORTED_MODULE_5__["default"].createInstance(),
            _gen_ui_NumberJump_UI_nearMap__WEBPACK_IMPORTED_MODULE_5__["default"].createInstance(),
            _gen_ui_NumberJump_UI_nearMap__WEBPACK_IMPORTED_MODULE_5__["default"].createInstance(),
        ];
        this._fars = [
            _gen_ui_NumberJump_UI_farMap__WEBPACK_IMPORTED_MODULE_3__["default"].createInstance(),
            _gen_ui_NumberJump_UI_farMap__WEBPACK_IMPORTED_MODULE_3__["default"].createInstance(),
            _gen_ui_NumberJump_UI_farMap__WEBPACK_IMPORTED_MODULE_3__["default"].createInstance(),
        ];
        this._dimians = [
            _gen_ui_NumberJump_UI_dimian__WEBPACK_IMPORTED_MODULE_1__["default"].createInstance(),
            _gen_ui_NumberJump_UI_dimian__WEBPACK_IMPORTED_MODULE_1__["default"].createInstance(),
            _gen_ui_NumberJump_UI_dimian__WEBPACK_IMPORTED_MODULE_1__["default"].createInstance(),
        ];
        this._dimianners = [
            _gen_ui_NumberJump_UI_dimianjin__WEBPACK_IMPORTED_MODULE_2__["default"].createInstance(),
            _gen_ui_NumberJump_UI_dimianjin__WEBPACK_IMPORTED_MODULE_2__["default"].createInstance(),
            _gen_ui_NumberJump_UI_dimianjin__WEBPACK_IMPORTED_MODULE_2__["default"].createInstance(),
        ];
        this._treeMoveSpeed = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_10__["NumberJumpGameConst"].data.treeMoveSpeed;
        this._nearMoveSpeed = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_10__["NumberJumpGameConst"].data.nearMoveSpeed;
    }
    clear() {
        this.m_propLayer.removeChildren();
        this._xSpeed = 0;
        this._maxWidth = 0;
        this._nowX = 0;
        this.x = 0;
        this._treeMoveSpeed = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_10__["NumberJumpGameConst"].data.treeMoveSpeed;
        this._nearMoveSpeed = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_10__["NumberJumpGameConst"].data.nearMoveSpeed;
        this._targetX = 0;
        this._overBack = null;
    }
    init() {
        this._yuns.forEach((near, index) => {
            _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_18__["UI_NumberJumpMainMediator"].instance.ui.m_treeLayer.addChild(near);
            near.x = (index - 1) * near.width;
            near.y = 300;
        });
        this._fars.forEach((near, index) => {
            _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_18__["UI_NumberJumpMainMediator"].instance.ui.m_treeLayer.addChild(near);
            near.x = (index - 1) * this.width;
            near.y = 700;
        });
        this._trees.forEach((tree, index) => {
            _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_18__["UI_NumberJumpMainMediator"].instance.ui.m_treeLayer.addChild(tree);
            tree.x = (index - 1) * this.width;
            tree.y = 460;
        });
        this._nears.forEach((near, index) => {
            _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_18__["UI_NumberJumpMainMediator"].instance.ui.m_treeLayer.addChild(near);
            near.x = (index - 1) * this.width;
            near.y = 920;
        });
        this._dimians.forEach((dimian, index) => {
            _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_18__["UI_NumberJumpMainMediator"].instance.ui.m_treeLayer.addChild(dimian);
            dimian.x = (index - 1) * this.width;
            dimian.y = 1060;
        });
        this._dimianners.forEach((dimian, index) => {
            _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_18__["UI_NumberJumpMainMediator"].instance.ui.m_treeLayer.addChild(dimian);
            dimian.x = (index - 1) * dimian.width;
            dimian.y = 1300;
        });
    }
    _updateItemsMove(items, speed) {
        if (!speed)
            return;
        items.forEach((item, index) => {
            item.x += speed;
        });
        for (let i = 0; i < items.length; i++) {
            let item = items[i];
            let pos = item.localToGlobal();
            if (pos.x > item.width * 1.5) {
                items[i].x -= item.width * 3;
            }
            else if (pos.x < -item.width * 1.5) {
                items[i].x += item.width * 3;
            }
        }
    }
    startMove(dire) {
        let xSpeed = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_10__["NumberJumpGameConst"].data.xSpeed;
        xSpeed *= dire;
        this._xSpeed = xSpeed;
    }
    backLen(len) {
        let time = (Math.abs(len) / _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_10__["NumberJumpGameConst"].data.backXSpeed) * 16.7;
        time = Math.min(time, _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_10__["NumberJumpGameConst"].data.backXSpeedMaxTime);
        this._xSpeed = (len / time) * 16.7 * (len > 0 ? 1 : -1);
        this._targetX = this.x - len;
    }
    startMoveDis(dis) {
        this._xSpeed = dis;
    }
    get mapMoving() {
        return !!this._xSpeed;
    }
    // 快速移动
    quickMove(targetX, overBack) {
        this._overBack = overBack;
        this._targetX = targetX;
        let xSpeed = -_config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_10__["NumberJumpGameConst"].data.quickMoveSpeed;
        this._xSpeed = xSpeed;
    }
    canCaozuo(bool) { }
    stopMove() {
        this._xSpeed = 0;
        this._targetX = 0;
    }
    addPropItem(propid, len, item) {
        let nowX;
        if (item) {
            this._nowX = item.x + item.width;
            this._maxWidth = this._nowX;
        }
        else {
            this._maxWidth += len;
            nowX = this._nowX;
            this._nowX += len;
        }
        return item || this._addPropItem(propid, len, nowX);
    }
    update() {
        let dt = _LTGame_LTUtils_LTUtils__WEBPACK_IMPORTED_MODULE_7__["LTUtils"].deltaTimeBytimer(_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_9__["default"].gameScript.timer);
        if (this._xSpeed) {
            let movespeed = (this._xSpeed * dt) / 16.7;
            this.x += movespeed;
            if (this.x >= 0) {
                this.x = 0;
            }
            else if (this.x <= -this._maxWidth) {
                this.x = -this._maxWidth;
            }
            this._updateItemsMove(this._yuns, (this._xSpeed > 0 ? 1 : -1) * this._treeMoveSpeed / 3);
            this._updateItemsMove(this._fars, (this._xSpeed > 0 ? 1 : -1) * this._treeMoveSpeed / 2);
            this._updateItemsMove(this._trees, (this._xSpeed > 0 ? 1 : -1) * this._treeMoveSpeed);
            this._updateItemsMove(this._nears, (this._xSpeed > 0 ? 1 : -1) * this._nearMoveSpeed);
            this._updateItemsMove(this._dimians, movespeed);
            this._updateItemsMove(this._dimianners, movespeed * 1.5);
            if ((this._targetX && this._xSpeed < 0 && this.x <= this._targetX) ||
                (this._xSpeed > 0 && this.x >= this._targetX)) {
                this.x = this._targetX;
                this._targetX = 0;
                this._xSpeed = 0;
                this._overBack && this._overBack.run();
            }
        }
    }
    _addPropItem(propid, len, x) {
        let item;
        switch (propid) {
            case _enum_EPropType__WEBPACK_IMPORTED_MODULE_11__["EpropType"].bigJianci:
                item = _prop_bigJianciItem__WEBPACK_IMPORTED_MODULE_13__["default"].createInstance();
                break;
            case _enum_EPropType__WEBPACK_IMPORTED_MODULE_11__["EpropType"].black:
                item = _prop_blackItem__WEBPACK_IMPORTED_MODULE_14__["default"].createInstance();
                break;
            case _enum_EPropType__WEBPACK_IMPORTED_MODULE_11__["EpropType"].box:
                item = _prop_boxItem__WEBPACK_IMPORTED_MODULE_15__["default"].createInstance();
                break;
            case _enum_EPropType__WEBPACK_IMPORTED_MODULE_11__["EpropType"].huadao:
                item = _prop_huadaoItem__WEBPACK_IMPORTED_MODULE_16__["default"].createInstance();
                break;
            case _enum_EPropType__WEBPACK_IMPORTED_MODULE_11__["EpropType"].jianci:
                item = _prop_jianciItem__WEBPACK_IMPORTED_MODULE_17__["default"].createInstance();
                break;
        }
        item.init(x, len);
        this.m_propLayer.addChild(item);
        if (item instanceof _prop_boxItem__WEBPACK_IMPORTED_MODULE_15__["default"]) {
            item.y = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_10__["NumberJumpGameConst"].data.boxY;
            item.initY = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_10__["NumberJumpGameConst"].data.boxY;
            x += _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_8__["default"].Random(200, 300);
        }
        else {
            item.y = this._initY;
            item.initY = this._initY;
            item.width = len;
        }
        if (x) {
            item.x = x;
        }
        return item;
    }
    dispose() {
        if (this.isDisposed)
            return;
        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_12__["default"].clearAll(this);
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_9__["default"].gameScript.timer.clearAll(this);
        this._maxWidth = 0;
        this._nowX = 0;
        super.dispose();
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/ui/prop/bigJianciItem.ts":
/*!*************************************************************!*\
  !*** ./Number-Jump/scripts/script/ui/prop/bigJianciItem.ts ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return bigJianciItem; });
/* harmony import */ var _gen_ui_NumberJump_UI_bigJianciItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_bigJianciItem */ "./Number-Jump/gen/ui/NumberJump/UI_bigJianciItem.ts");
/* harmony import */ var _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../LTGame/LTUtils/MathEx */ "./Number-Jump/scripts/LTGame/LTUtils/MathEx.ts");
/* harmony import */ var _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");
/* harmony import */ var _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../config/NumberJumpGameConst */ "./Number-Jump/scripts/script/config/NumberJumpGameConst.ts");
/* harmony import */ var _scene_script_MTween__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../scene/script/MTween */ "./Number-Jump/scripts/script/scene/script/MTween.ts");





class bigJianciItem extends _gen_ui_NumberJump_UI_bigJianciItem__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor() {
        super(...arguments);
        this._index = 0;
        this._chosed = false;
        this._jianciing = false;
    }
    static createInstance() {
        return fgui.UIPackage.createObject("NumberJump", "bigJianciItem");
    }
    onConstruct() {
        super.onConstruct();
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_2__["default"].gameScript.timer.frameLoop(1, this, this._onUpdate);
        let arr = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_3__["NumberJumpGameConst"].data.spJianciXhjiange;
        this.interval = _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_1__["default"].Random(arr[0], arr[1]);
        let sarr = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_3__["NumberJumpGameConst"].data.spJianciXhSpeed;
        this.speed = _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_1__["default"].Random(sarr[0], sarr[1]);
    }
    _onUpdate() {
        if (this.visible) {
            this._startBigAni();
        }
        else {
            this._stopBigAni();
        }
    }
    _startBigAni() {
        if (this._jianciing)
            return;
        this.m_dici.height = 46;
        this.setIntervalTime(this.interval);
        this._jianciing = true;
    }
    _stopBigAni() {
        this._jianciing = false;
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_2__["default"].gameScript.timer.clear(this, this._tweenJianci);
        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_4__["default"].clearAll(this.m_dici);
        this.m_dici.height = 46;
    }
    setIntervalTime(interval) {
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_2__["default"].gameScript.timer.loop(interval, this, this._tweenJianci, null, true);
    }
    _tweenJianci() {
        this.m_dici.height = 46;
        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_4__["default"].to(this.m_dici, { height: _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_3__["NumberJumpGameConst"].data.spJiancih }, 1000 / this.speed, null, Laya.Handler.create(this, () => {
            _scene_script_MTween__WEBPACK_IMPORTED_MODULE_4__["default"].to(this.m_dici, { height: 46 }, 1000 / this.speed, null, Laya.Handler.create(this, () => {
                // this._jianciing = false;
            }));
        }));
    }
    init(x, len) { }
    collisionBox() {
        let box = {
            x: this.x,
            y: this.y + _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_3__["NumberJumpGameConst"].data.pengzhuangYoffset,
            width: this.width,
            height: this.height,
        };
        return box;
    }
    get jianciCollisionBox() {
        if (!this._jianciing) {
            return null;
        }
        let box = {
            x: this.x + this.m_dici.x,
            y: this.y + this.m_dici.y - this.m_dici.height * this.m_dici.scaleY,
            width: this.m_dici.width,
            height: this.m_dici.height,
        };
        return box;
    }
    _clear() {
        this._config = null;
    }
    dispose() {
        if (this.isDisposed)
            return;
        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_4__["default"].clearAll(this);
        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_4__["default"].clearAll(this.m_dici);
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_2__["default"].gameScript.timer.clearAll(this);
        this._config = null;
        this._index = null;
        super.dispose();
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/ui/prop/blackItem.ts":
/*!*********************************************************!*\
  !*** ./Number-Jump/scripts/script/ui/prop/blackItem.ts ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return blackItem; });
/* harmony import */ var _gen_ui_NumberJump_UI_blackItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_blackItem */ "./Number-Jump/gen/ui/NumberJump/UI_blackItem.ts");
/* harmony import */ var _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");
/* harmony import */ var _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../config/NumberJumpGameConst */ "./Number-Jump/scripts/script/config/NumberJumpGameConst.ts");
/* harmony import */ var _scene_script_MTween__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../scene/script/MTween */ "./Number-Jump/scripts/script/scene/script/MTween.ts");




class blackItem extends _gen_ui_NumberJump_UI_blackItem__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor() {
        super(...arguments);
        this._index = 0;
        this._blackYOffset = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_2__["NumberJumpGameConst"].data.pengzhuangYoffset;
    }
    static createInstance() {
        return fgui.UIPackage.createObject("NumberJump", "blackItem");
    }
    init(x, len) {
        // 在一定的范围内是  分数奖励地板
    }
    collisionBox() {
        let box = {
            x: this.x,
            y: this.y + this._blackYOffset,
            width: this.width,
            height: this.height,
        };
        return box;
    }
    dispose() {
        if (this.isDisposed)
            return;
        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_3__["default"].clearAll(this);
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_1__["default"].gameScript.timer.clearAll(this);
        this._config = null;
        this._index = null;
        super.dispose();
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/ui/prop/boxItem.ts":
/*!*******************************************************!*\
  !*** ./Number-Jump/scripts/script/ui/prop/boxItem.ts ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return boxItem; });
/* harmony import */ var _gen_ui_NumberJump_UI_boxItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_boxItem */ "./Number-Jump/gen/ui/NumberJump/UI_boxItem.ts");
/* harmony import */ var _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");
/* harmony import */ var _scene_script_MTween__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../scene/script/MTween */ "./Number-Jump/scripts/script/scene/script/MTween.ts");



class boxItem extends _gen_ui_NumberJump_UI_boxItem__WEBPACK_IMPORTED_MODULE_0__["default"] {
    static createInstance() {
        return fgui.UIPackage.createObject("NumberJump", "boxItem");
    }
    init(x, len) { }
    collisionBox() {
        let box = {
            x: this.x,
            y: this.y,
            width: this.width,
            height: this.height,
        };
        return box;
    }
    remove() {
        // LTUtils.arrRemove(NumberJumpGlobalUnit.gameScript._props, this)
        this.dispose();
    }
    dispose() {
        if (this.isDisposed)
            return;
        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_2__["default"].clearAll(this);
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_1__["default"].gameScript.timer.clearAll(this);
        this._config = null;
        super.dispose();
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/ui/prop/huadaoItem.ts":
/*!**********************************************************!*\
  !*** ./Number-Jump/scripts/script/ui/prop/huadaoItem.ts ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return huadaoItem; });
/* harmony import */ var _gen_ui_NumberJump_UI_huadaoItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_huadaoItem */ "./Number-Jump/gen/ui/NumberJump/UI_huadaoItem.ts");
/* harmony import */ var _LTGame_LTUtils_LTUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../LTGame/LTUtils/LTUtils */ "./Number-Jump/scripts/LTGame/LTUtils/LTUtils.ts");
/* harmony import */ var _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../LTGame/LTUtils/MathEx */ "./Number-Jump/scripts/LTGame/LTUtils/MathEx.ts");
/* harmony import */ var _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");
/* harmony import */ var _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../config/NumberJumpGameConst */ "./Number-Jump/scripts/script/config/NumberJumpGameConst.ts");
/* harmony import */ var _scene_script_MTween__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../scene/script/MTween */ "./Number-Jump/scripts/script/scene/script/MTween.ts");
/* harmony import */ var _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../UI_NumberJumpMainMediator */ "./Number-Jump/scripts/script/ui/UI_NumberJumpMainMediator.ts");







class huadaoItem extends _gen_ui_NumberJump_UI_huadaoItem__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor() {
        super(...arguments);
        this.moveSpeed = 1;
        this.moveDire = 1;
        this._collisionYOffset = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_4__["NumberJumpGameConst"].data.pengzhuangYoffset;
    }
    static createInstance() {
        return fgui.UIPackage.createObject("NumberJump", "huadaoItem");
    }
    init(x, len) {
        let arr = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_4__["NumberJumpGameConst"].data.chuansongSpeed;
        this.moveSpeed = _LTGame_LTUtils_MathEx__WEBPACK_IMPORTED_MODULE_2__["default"].Random(arr[0], arr[1]);
    }
    onConstruct() {
        super.onConstruct();
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_3__["default"].gameScript.timer.frameLoop(1, this, this._onUpdate);
    }
    _onUpdate() {
        if (this.huadaoing) {
            let dt = _LTGame_LTUtils_LTUtils__WEBPACK_IMPORTED_MODULE_1__["LTUtils"].deltaTimeBytimer(_common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_3__["default"].gameScript.timer);
            let heroNum = _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_6__["UI_NumberJumpMainMediator"].instance.heroNum;
            if (heroNum) {
                if (heroNum.localX < this.x || heroNum.localX > this.x + this.width) {
                    this.endHuadao();
                }
                else {
                    if (this.moveDire == 1) {
                        // heroNum.x -= this.moveSpeed * dt;
                    }
                }
            }
        }
    }
    collisionBox() {
        let box = {
            x: this.x,
            y: this.y + this._collisionYOffset,
            width: this.width,
            height: this.height,
        };
        return box;
    }
    endHuadao() {
        _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_6__["UI_NumberJumpMainMediator"].instance.mapLayer.stopMove();
        this.huadaoing = false;
    }
    // 开始滑道
    startHuadaoItem() {
        this.huadaoing = true;
        _UI_NumberJumpMainMediator__WEBPACK_IMPORTED_MODULE_6__["UI_NumberJumpMainMediator"].instance.mapLayer.startMoveDis(this.moveSpeed);
    }
    dispose() {
        if (this.isDisposed)
            return;
        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_5__["default"].clearAll(this);
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_3__["default"].gameScript.timer.clearAll(this);
        super.dispose();
    }
}


/***/ }),

/***/ "./Number-Jump/scripts/script/ui/prop/jianciItem.ts":
/*!**********************************************************!*\
  !*** ./Number-Jump/scripts/script/ui/prop/jianciItem.ts ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return jianciItem; });
/* harmony import */ var _gen_ui_NumberJump_UI_jianciItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../gen/ui/NumberJump/UI_jianciItem */ "./Number-Jump/gen/ui/NumberJump/UI_jianciItem.ts");
/* harmony import */ var _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../common/NumberJumpGlobalUnit */ "./Number-Jump/scripts/script/common/NumberJumpGlobalUnit.ts");
/* harmony import */ var _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../config/NumberJumpGameConst */ "./Number-Jump/scripts/script/config/NumberJumpGameConst.ts");
/* harmony import */ var _scene_script_MTween__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../scene/script/MTween */ "./Number-Jump/scripts/script/scene/script/MTween.ts");




class jianciItem extends _gen_ui_NumberJump_UI_jianciItem__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor() {
        super(...arguments);
        this._index = 0;
        this._colliderYOffset = _config_NumberJumpGameConst__WEBPACK_IMPORTED_MODULE_2__["NumberJumpGameConst"].data.pengzhuangYoffset;
    }
    static createInstance() {
        return fgui.UIPackage.createObject("NumberJump", "jianciItem");
    }
    init(x, len) { }
    collisionBox() {
        let box = {
            x: this.x,
            y: this.y + this._colliderYOffset,
            width: this.width,
            height: this.height,
        };
        return box;
    }
    dispose() {
        if (this.isDisposed)
            return;
        _scene_script_MTween__WEBPACK_IMPORTED_MODULE_3__["default"].clearAll(this);
        _common_NumberJumpGlobalUnit__WEBPACK_IMPORTED_MODULE_1__["default"].gameScript.timer.clearAll(this);
        this._config = null;
        this._index = null;
        super.dispose();
    }
}


/***/ }),

/***/ "./demo/Demo.ts":
/*!**********************!*\
  !*** ./demo/Demo.ts ***!
  \**********************/
/*! exports provided: Demo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Demo", function() { return Demo; });
class Demo {
    constructor(view, miniGame) {
        this.view = view;
        this.miniGame = miniGame;
        view.m_state.selectedPage = "大厅";
        view.m_loading.m_progress.max = 1;
        view.m_loading.m_progress.value = 0;
        Laya.timer.frameLoop(1, this, () => {
            const dt = Laya.timer.delta / 1000;
            this.update(dt);
            this.fixedUpdate(dt);
            this.constantUpdate(dt);
        });
        view.m_btn_enter.onClick(this, this.onEnterGame);
        view.m_btn_start.onClick(this, this.onStartGame);
        view.m_btn_exit.onClick(this, this.onExitGame);
        view.m_btn_finish.onClick(this, this.onFinishGame);
        view.m_btn_revive.onClick(this, this.onRevive);
        view.m_btn_home.onClick(this, this.onReturnHome);
    }
    update(dt) {
        this.miniGame.update(dt);
    }
    fixedUpdate(dt) {
        this.miniGame.fixedUpdate(dt);
    }
    constantUpdate(dt) {
        this.miniGame.constantUpdate(dt);
    }
    onEnterGame() {
        return __awaiter(this, void 0, void 0, function* () {
            this.view.m_state.selectedPage = "加载中";
            yield this.miniGame.loadGame((progress) => {
                console.log(progress);
                this.view.m_loading.m_progress.value = progress;
            });
            this.view.m_state.selectedPage = "小游戏主页";
            this.miniGame.view.m_state.selectedIndex = 0;
            this.view.m_layer.displayObject.addChild(this.miniGame.view.displayObject);
            // this.miniGame.load 是否需要在之前 加载存储数据？？？
            // onReady之前需给miniGame.stage赋值，非关卡制的赋值1
            this.miniGame.onReady();
        });
    }
    onExitGame() {
        // this.miniGame.pauseGame();
        this.miniGame.view.removeFromParent();
        // this.miniGame.view.dispose();
        this.miniGame.unloadGame();
        this.view.m_state.selectedPage = "大厅";
    }
    onStartGame() {
        this.miniGame.startGame();
        this.miniGame.view.m_state.selectedIndex = 1;
        this.view.m_state.selectedPage = "游戏中";
    }
    onFinishGame() {
        // 结算之前需给miniGame.score赋值
        this.miniGame.tryFinish();
        this.miniGame.view.m_state.selectedIndex = 2;
        this.view.m_state.selectedPage = "游戏结算";
    }
    onRevive() {
        this.miniGame.continueGame();
        this.miniGame.view.m_state.selectedIndex = 1;
        this.view.m_state.selectedPage = "游戏中";
    }
    onReturnHome() {
        this.miniGame.endGame();
        this.view.m_state.selectedPage = "小游戏主页";
        this.miniGame.view.m_state.selectedIndex = 0;
        // onReady之前需给miniGame.stage赋值，非关卡制的赋值1
        this.miniGame.onReady();
    }
}


/***/ }),

/***/ "./demo/gen/ui/Demo/DemoBinder.ts":
/*!****************************************!*\
  !*** ./demo/gen/ui/Demo/DemoBinder.ts ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DemoBinder; });
/* harmony import */ var _UI_Demo__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UI_Demo */ "./demo/gen/ui/Demo/UI_Demo.ts");
/* harmony import */ var _UI_Loading__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UI_Loading */ "./demo/gen/ui/Demo/UI_Loading.ts");
/* harmony import */ var _UI_IconButton__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./UI_IconButton */ "./demo/gen/ui/Demo/UI_IconButton.ts");
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/



class DemoBinder {
    static bindAll() {
        fgui.UIObjectFactory.setExtension(_UI_Demo__WEBPACK_IMPORTED_MODULE_0__["default"].URL, _UI_Demo__WEBPACK_IMPORTED_MODULE_0__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_Loading__WEBPACK_IMPORTED_MODULE_1__["default"].URL, _UI_Loading__WEBPACK_IMPORTED_MODULE_1__["default"]);
        fgui.UIObjectFactory.setExtension(_UI_IconButton__WEBPACK_IMPORTED_MODULE_2__["default"].URL, _UI_IconButton__WEBPACK_IMPORTED_MODULE_2__["default"]);
    }
}


/***/ }),

/***/ "./demo/gen/ui/Demo/UI_Demo.ts":
/*!*************************************!*\
  !*** ./demo/gen/ui/Demo/UI_Demo.ts ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_Demo; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_Demo extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("Demo", "Demo"));
    }
    onConstruct() {
        this.m_state = this.getControllerAt(0);
        this.m_layer = (this.getChildAt(0));
        this.m_loading = (this.getChildAt(1));
        this.m_btn_enter = (this.getChildAt(2));
        this.m_btn_start = (this.getChildAt(3));
        this.m_btn_exit = (this.getChildAt(4));
        this.m_btn_finish = (this.getChildAt(5));
        this.m_btn_revive = (this.getChildAt(6));
        this.m_btn_home = (this.getChildAt(7));
    }
}
UI_Demo.URL = "ui://8ya44m0bmgk7ml";


/***/ }),

/***/ "./demo/gen/ui/Demo/UI_IconButton.ts":
/*!*******************************************!*\
  !*** ./demo/gen/ui/Demo/UI_IconButton.ts ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_IconButton; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_IconButton extends fgui.GButton {
    static createInstance() {
        return (fgui.UIPackage.createObject("Demo", "IconButton"));
    }
    onConstruct() {
        this.m_bg = this.getControllerAt(0);
    }
}
UI_IconButton.URL = "ui://8ya44m0bmgk7ms";


/***/ }),

/***/ "./demo/gen/ui/Demo/UI_Loading.ts":
/*!****************************************!*\
  !*** ./demo/gen/ui/Demo/UI_Loading.ts ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UI_Loading; });
/** This is an automatically generated class by FairyGUI. Please do not modify it. **/
class UI_Loading extends fgui.GComponent {
    static createInstance() {
        return (fgui.UIPackage.createObject("Demo", "Loading"));
    }
    onConstruct() {
        this.m_progress = (this.getChildAt(2));
    }
}
UI_Loading.URL = "ui://8ya44m0bmgk7mm";


/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map