/**
 * 设置LayaNative屏幕方向，可设置以下值
 * landscape           横屏
 * portrait            竖屏
 * sensor_landscape    横屏(双方向)
 * sensor_portrait     竖屏(双方向)
 */
window.screenOrientation = "portrait";
//-----libs-begin-----

loadLib("bin/libs/laya.core.js");
loadLib("bin/libs/laya.html.js");
loadLib("bin/libs/ltgame/generator.js");
loadLib("bin/libs/ltgame/promise.js");
// loadLib("bin/libs/spine-core.js");
loadLib("bin/libs/spine-core-3.8.js");
loadLib("bin/libs/laya.spine.js");

loadLib("bin/libs/fairygui/fairygui.js");
loadLib("bin/js/bundle.js?" + Math.random());
