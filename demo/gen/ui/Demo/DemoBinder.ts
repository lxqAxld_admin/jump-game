/** This is an automatically generated class by FairyGUI. Please do not modify it. **/

import UI_Demo from "./UI_Demo";
import UI_Loading from "./UI_Loading";
import UI_IconButton from "./UI_IconButton";

export default class DemoBinder {
	public static bindAll():void {
		fgui.UIObjectFactory.setExtension(UI_Demo.URL, UI_Demo);
		fgui.UIObjectFactory.setExtension(UI_Loading.URL, UI_Loading);
		fgui.UIObjectFactory.setExtension(UI_IconButton.URL, UI_IconButton);
	}
}