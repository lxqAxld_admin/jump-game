/** This is an automatically generated class by FairyGUI. Please do not modify it. **/

import UI_Loading from "./UI_Loading";
import UI_IconButton from "./UI_IconButton";

export default class UI_Demo extends fgui.GComponent {

	public m_state:fgui.Controller;
	public m_layer:fgui.GGraph;
	public m_loading:UI_Loading;
	public m_btn_enter:UI_IconButton;
	public m_btn_start:UI_IconButton;
	public m_btn_exit:UI_IconButton;
	public m_btn_finish:UI_IconButton;
	public m_btn_revive:UI_IconButton;
	public m_btn_home:UI_IconButton;
	public static URL:string = "ui://8ya44m0bmgk7ml";

	public static createInstance():UI_Demo {
		return <UI_Demo>(fgui.UIPackage.createObject("Demo", "Demo"));
	}

	protected onConstruct():void {
		this.m_state = this.getControllerAt(0);
		this.m_layer = <fgui.GGraph>(this.getChildAt(0));
		this.m_loading = <UI_Loading>(this.getChildAt(1));
		this.m_btn_enter = <UI_IconButton>(this.getChildAt(2));
		this.m_btn_start = <UI_IconButton>(this.getChildAt(3));
		this.m_btn_exit = <UI_IconButton>(this.getChildAt(4));
		this.m_btn_finish = <UI_IconButton>(this.getChildAt(5));
		this.m_btn_revive = <UI_IconButton>(this.getChildAt(6));
		this.m_btn_home = <UI_IconButton>(this.getChildAt(7));
	}
}