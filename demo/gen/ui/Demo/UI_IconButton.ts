/** This is an automatically generated class by FairyGUI. Please do not modify it. **/

export default class UI_IconButton extends fgui.GButton {

	public m_bg:fgui.Controller;
	public static URL:string = "ui://8ya44m0bmgk7ms";

	public static createInstance():UI_IconButton {
		return <UI_IconButton>(fgui.UIPackage.createObject("Demo", "IconButton"));
	}

	protected onConstruct():void {
		this.m_bg = this.getControllerAt(0);
	}
}