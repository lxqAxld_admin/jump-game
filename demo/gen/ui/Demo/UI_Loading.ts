/** This is an automatically generated class by FairyGUI. Please do not modify it. **/

export default class UI_Loading extends fgui.GComponent {

	public m_progress:fgui.GProgressBar;
	public static URL:string = "ui://8ya44m0bmgk7mm";

	public static createInstance():UI_Loading {
		return <UI_Loading>(fgui.UIPackage.createObject("Demo", "Loading"));
	}

	protected onConstruct():void {
		this.m_progress = <fgui.GProgressBar>(this.getChildAt(2));
	}
}